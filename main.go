package main

import (
	"context"
	"net/http"
	"net/http/pprof"
	"os"
	"os/signal"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"h12.me/config"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
	"vlion/yxw/routers"
	"vlion/yxw/routers/middleware"
)

func init() {
	var c common.Config
	if err := config.Parse(&c); err != nil {
		panic(err)
	}
	routers.Init(c.Apps)

	prometheus.MustRegister(yxwLog.HttpRequestCount)
	prometheus.MustRegister(yxwLog.HttpErrorCount)
	prometheus.MustRegister(yxwLog.HttpRequestDuration)
}

func main() {
	server()
}

func server() {
	mux := http.NewServeMux()

	// 第三方相关API
	mux.Handle("/third_part/wechat/login", middleware.SignCheckAndUserFilter(new(routers.WechatLoginStruct)))
	mux.Handle("/third_part/wechat/bind", middleware.SignCheckFilter(new(routers.WechatBindStruct)))
	mux.Handle("/third_part/alipay/login", middleware.SignCheckAndUserFilter(new(routers.AlipayLoginStruct)))
	mux.Handle("/third_part/alipay/bind", middleware.SignCheckFilter(new(routers.AlipayBindStruct)))
	//	mux.Handle("/third_part/qq/login", middleware.CheckSign(routers.QQLogin))
	//	mux.Handle("/third_part/qq/bind", middleware.CheckSign(routers.QQBind))

	// app初始化接口
	mux.Handle("/appInit", middleware.SignCheckWithoutVersionFiltFilter(new(routers.AppInitStruct)))

	// 获取新闻页面 - json类型
	mux.Handle("/getNewsCatList", middleware.SignCheckAndUserNoForceFilter(new(routers.GetNewsCatListStruct)))
	mux.Handle("/getNewsJson", middleware.SignCheckAndUserNoForceFilter(new(routers.GetNewsJsonStruct)))

	// 阅读红包新闻
	mux.Handle("/getRewardNews", middleware.UserFilterCheckNotForceFilter(new(routers.GetRewardNewsStruct)))

	// 获取剩余红包新闻数接口
	mux.Handle("/getNewsRewardTimes", middleware.SignCheckAndUserFilter(new(routers.GetNewsRewardTimesStruct)))

	// 用户提现
	mux.Handle("/alipayWithdraw", middleware.SignCheckFilter(new(routers.AlipayWithdrawStruct)))
	mux.Handle("/wechatWithdraw", middleware.SignCheckFilter(new(routers.WechatWithdrawStruct)))

	// 获取用户数据个性化信息
	mux.Handle("/getProfile", middleware.SignCheckAndUserFilter(new(routers.GetProfileStruct)))

	// 更新用户信息
	mux.Handle("/setUserInfo", middleware.SignCheckAndUserFilter(new(routers.SetUserInfoStruct)))

	// 获取"我的数据"
	mux.Handle("/getMyData", middleware.SignCheckAndUserFilter(new(routers.GetMyDataStruct)))

	// 获取支付宝登录参数
	mux.Handle("/third_part/alipay/getLoginParams", middleware.SignCheckFilter(new(routers.GetAlipayLoginParamsStruct)))

	// 获取"我的徒弟"列表
	mux.Handle("/getMyApprentice", middleware.SignCheckAndUserFilter(new(routers.GetMyApprenticeLeaderboardsStruct)))

	// 获取徒弟大体情况
	mux.Handle("/apprenticeBasic", middleware.SignCheckAndUserFilter(new(routers.ApprenticeBasic)))

	// 获取徒弟详情
	mux.Handle("/apprenticeDetail", middleware.SignCheckAndUserFilter(new(routers.ApprenticeDetail)))

	// 获取师徒奖励(开师徒宝箱)
	mux.Handle("/getApprenticeReward", middleware.SignCheckAndUserFilter(new(routers.GetApprenticeReward)))

	// 师徒宝箱排行榜及广播
	mux.Handle("/apprenticeRewardLeaderboard", middleware.SignCheckAndUserNoForceFilter(new(routers.ApprenticeRewardLeaderboard)))

	// 获取收益排行
	mux.Handle("/getBenifitLeaderboards", middleware.SignCheckFilter(new(routers.GetBenifitLeaderboardsStruct)))

	// 获取最近提现成功列表
	mux.Handle("/getRecentWithdrawList", middleware.SignCheckFilter(new(routers.GetRecentWithdrawListStruct)))

	// 获取提现汇率及账户, 待废弃
	mux.Handle("/getExchangeRate", middleware.SignCheckFilter(new(routers.GetExchangeRateStruct)))

	// 获取广告列表
	mux.Handle("/getAds", middleware.SignCheckFilter(new(routers.GetAdsStruct)))

	// 获取转盘参数
	mux.Handle("/getFixedBoundConf", middleware.SignCheckFilter(new(routers.GetFixedBoundConfStruct)))

	// 获取阅读新闻奖励(转盘奖励)
	mux.Handle("/getFixedBound", middleware.SignCheckAndUserFilter(new(routers.GetFixedBoundStruct)))

	// 转盘提示语
	mux.Handle("/getFixedBoundTip", middleware.SignCheckAndUserFilter(new(routers.GetFixedBoundTipStruct)))

	// 获取随机两条新闻
	mux.Handle("/getRadomNews", middleware.SignCheckFilter(new(routers.GetRadomNewsStruct)))

	// 获取分享内容
	mux.Handle("/getShareContent", middleware.SignCheckFilter(new(routers.GetShareContentStruct)))

	// 时段奖励相关
	mux.Handle("/getPeriodDuration", middleware.SignCheckAndUserFilter(new(routers.GetPriodDurationStruct)))
	mux.Handle("/getPeriodReward", middleware.SignCheckAndUserFilter(new(routers.GetPriodRewardStruct)))

	// 获取文章分享内容
	mux.Handle("/getArticleShareContent", middleware.SignCheckFilter(new(routers.GetArticleShareContentStruct)))

	// 分享奖励
	mux.Handle("/articleShare", http.HandlerFunc(routers.ArticleShare))

	// 我的订单
	mux.Handle("/myOrder", middleware.SignCheckAndUserFilter(new(routers.MyOrderStruct)))

	// 获取用户收益明细
	mux.Handle("/getProfitsDetails", middleware.SignCheckFilter(new(routers.GetProfitsDetailsStruct)))

	// 设置师徒验证码
	mux.Handle("/setRecommendCode", middleware.SignCheckAndUserFilter(new(routers.SetRecommendCodeStruct)))

	// 注册&登录
	mux.Handle("/registerLogin", middleware.SignCheckNoForceWithoutVersionFiltAndUserNoForceFilter(new(routers.RegisterLoginStruct)))

	// 获取某些链接url
	mux.Handle("/getLink", middleware.SignCheckFilter(new(routers.GetLinkStruct)))

	// 获取任务列表
	mux.Handle("/getTaskList", middleware.SignCheckFilter(new(routers.GetTaskListStruct)))

	// 对接闲玩
	mux.Handle("/xianwan", http.HandlerFunc(routers.XianwanAPI))

	// 看看赚 - 停留时长任务
	mux.Handle("/stayPageTask", middleware.SignCheckAndUserFilter(new(routers.StayPageTask)))

	// 看看赚 - 每日一次性任务
	mux.Handle("/dailyOnceTask", middleware.SignCheckAndUserFilter(new(routers.DailyOnceTask)))

	// 看看赚 - 每日多次任务
	mux.Handle("/dailyManyTimesTask", middleware.SignCheckAndUserFilter(new(routers.DailyManyTimesTask)))

	// 看看赚 - 根据任务id查询每日多次任务时间间隔
	mux.Handle("/dailyManyTimesTaskByID", middleware.SignCheckAndUserFilter(new(routers.DailyManyTimesTaskByID)))

	// 看看赚 - 完成任务
	mux.Handle("/completeSpecialTask", middleware.SignCheckAndUserFilter(new(routers.CompleteSpecialTaskStruct)))

	// 查询获取到的用户ip
	mux.Handle("/getMyIP", middleware.SignCheckFilter(new(routers.GetMyIPStruct)))

	// 查询版本
	mux.Handle("/getAppVersion", middleware.SignCheckNoForceWithoutVersionFiltAndUserNoForceFilter(new(routers.GetAppVersionStruct)))

	// 查询签到
	mux.Handle("/getSignInData", middleware.SignCheckAndUserFilter(new(routers.GetSignInDataStruct)))

	// 签到
	mux.Handle("/signIn", middleware.SignCheckAndUserFilter(new(routers.SignInStruct)))

	// 获取红包雨间隔
	mux.Handle("/getTimingDuration", middleware.SignCheckAndUserFilter(new(routers.GetTimingDurationStruct)))

	// 领取红包雨奖励
	mux.Handle("/getTimingReward", middleware.SignCheckAndUserFilter(new(routers.GetTimingRewardStruct)))

	// 实现临时功能的接口
	mux.Handle("/tmp", middleware.SignCheckAndUserNoForceFilter(new(routers.TempStruct)))

	// 广告点击
	mux.Handle("/adClick", middleware.SignCheckAndUserNoForceFilter(new(routers.ADClickStruct)))

	// 广告请求
	mux.Handle("/adReq", middleware.SignCheckAndUserNoForceFilter(new(routers.ADReqStruct)))

	// 广告返回
	mux.Handle("/adRet", middleware.SignCheckAndUserNoForceFilter(new(routers.ADRetStruct)))

	// 广告展示
	mux.Handle("/adShow", middleware.SignCheckAndUserNoForceFilter(new(routers.ADShowStruct)))

	// 视频金币
	mux.Handle("/videoWatch", middleware.SignCheckAndUserFilter(new(routers.VideoWatchStruct)))

	// 推送给我的消息
	mux.Handle("/myPushMessage", middleware.SignCheckAndUserFilter(new(routers.MyPushMessageStruct)))

	// 获取推送新闻详情
	mux.Handle("/getPushNewsDetails", middleware.SignCheckFilter(new(routers.GetPushNewsDetailsStruct)))

	// 获取短信验证码
	mux.Handle("/getMsgValidateCode", middleware.SignCheckNoForceFilter(new(routers.GetMsgVlidateCodeStruct)))

	// 获取提现等级评分
	mux.Handle("/getWithdrawLevel", middleware.SignCheckAndUserFilter(new(routers.MyWithdrawLevel)))

	// 我的可提现列表
	mux.Handle("/myWithdrawList", middleware.SignCheckAndUserFilter(new(routers.MyWithdrawList)))

	// 最近未完成的订单
	mux.Handle("/getRecentOrder", middleware.SignCheckAndUserFilter(new(routers.GetRecentOrderStruct)))

	// 日志上传
	mux.Handle("/uploadLog", middleware.SignCheckFilter(new(routers.UploadLogStruct)))

	// 测试页, 用来测试用户dns链路是否畅通
	mux.Handle("/testPage", http.HandlerFunc(routers.TestPage))

	// 内部接口, 不对外
	// 版本发布
	mux.Handle("/released", http.HandlerFunc(routers.UploadReleasedFile))
	// 上传头像
	mux.Handle("/setRadomImage", http.HandlerFunc(routers.UploadRadomImage))
	// 上传广告图片
	mux.Handle("/uploadAdsImage", http.HandlerFunc(routers.UploadAdsImage))
	// 提现审核
	mux.Handle("/withdrawCheck", middleware.SignCheckNoForceFilter(new(routers.WithdrawCheckStruct)))
	// 上传广告图片
	mux.Handle("/setAdsImage", http.HandlerFunc(routers.UploadAdsImage))
	// 信息发送
	mux.Handle("/youmengPush", http.HandlerFunc(routers.YoumengPush))
	// 查询用户提现等级分数
	mux.Handle("/getUserWithdrawLevel", http.HandlerFunc(routers.GetUserWithdrawLevel))
	// 查询最近提现
	mux.Handle("/getRecentUserWithdraw", http.HandlerFunc(routers.GetRecentUserWithdraw))
	// 查询当天金币结构
	mux.Handle("/getDailyCoinStruct", http.HandlerFunc(routers.GetDailyCoinStruct))

	// 添加pprof, 用以进行在线性能分析
	mux.Handle("/debug/pprof/", http.HandlerFunc(pprof.Index))
	mux.Handle("/debug/pprof/cmdline", http.HandlerFunc(pprof.Cmdline))
	mux.Handle("/debug/pprof/profile", http.HandlerFunc(pprof.Profile))
	mux.Handle("/debug/pprof/symbol", http.HandlerFunc(pprof.Symbol))
	mux.Handle("/debug/pprof/trace", http.HandlerFunc(pprof.Trace))

	// prometheus监控
	mux.Handle("/metrics", promhttp.Handler())

	srv := &http.Server{
		Addr:    ":8848",
		Handler: mux,
	}

	// 如果接收到关闭信号(如 kill -2)则进行graceful shutdown
	go gracefulShutdown(srv)

	// service connections
	if err := srv.ListenAndServe(); err != nil {
		log.Fields{
			"port": "8848",
			"err":  err.Error(),
		}.Error("unexpected service stop")
		return
	}
}

func gracefulShutdown(srv *http.Server) {
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt, os.Kill)
	<-stopChan
	log.Info("receive shutdown signal. Ready to exit")
	defer log.Info("program exists")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	srv.Shutdown(ctx)
	routers.FinishSignalChan <- struct{}{}
}
