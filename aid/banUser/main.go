package main

import (
	"errors"
	"strconv"
	"strings"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"
)

const (
	mongoAddr = "172.16.189.222?maxPoolSize=3000"
)

func main() {
	xf, err := xlsx.OpenFile("t.xlsx")
	if err != nil {
		panic(err)
	}
	ms, err := mgo.Dial(mongoAddr)
	if err != nil {
		panic(err)
	}
	defer ms.Close()
	ms.SetSocketTimeout(time.Hour * 10)
	ms.SetSyncTimeout(time.Hour * 10)
	ms.SetCursorTimeout(0)
	if err := ms.Ping(); err != nil {
		panic(err)
	}

	ms.SetMode(mgo.Monotonic, true)
	userBasicC := ms.DB("cr_app_101").C("user_basic")
	sheets := xf.Sheets
	for _, sheet := range sheets {
		totalStatus, totalStatusErr := strconv.Atoi(sheet.Name)
		if totalStatus > 0 || totalStatus < -4 {
			totalStatusErr = errors.New("invalid")
		}
		var userCode string
		var userStatus int
		for i := 0; i < sheet.MaxRow; i++ {
			if sheet.MaxCol >= 2 {
				userCode = strings.TrimSpace(sheet.Cell(i, 0).Value)
				userStatusString := strings.TrimSpace(sheet.Cell(i, 1).Value)
				if userCode == "" && userStatusString == "" {
					continue
				}
				var err error
				userStatus, err = strconv.Atoi(userStatusString)
				if err != nil {
					if totalStatusErr != nil {
						log.Fields{
							"sheet":              sheet.Name,
							"user code":          userCode,
							"user status string": userStatusString,
						}.Error("invalid user status string")
						continue
					}
					userStatus = totalStatus
				}
			} else if sheet.MaxCol == 1 {
				if totalStatusErr != nil {
					log.Fields{
						"sheet":              sheet.Name,
						"user code":          userCode,
						"user status string": sheet.Name,
						"err":                totalStatusErr.Error(),
					}.Error("invalid user status string")
					continue
				}
				userCode = strings.TrimSpace(sheet.Cell(i, 0).Value)
				userStatus = totalStatus
			}

			if err := userBasicC.Update(bson.M{"code": userCode}, bson.M{"$set": bson.M{"status": userStatus}}); err != nil {
				log.Fields{
					"err":  err.Error(),
					"code": userCode,
				}.Error("fail to update")
			}
		}
	}
}
