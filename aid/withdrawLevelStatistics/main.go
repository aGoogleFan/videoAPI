package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/globalsign/mgo/bson"
	"gopkg.in/mgo.v2"

	"vlion/yxw/common"
)

const mongoAddr = "172.16.189.222?maxPoolSize=3000"

func main() {
	ms, err := mgo.Dial(mongoAddr)
	if err != nil {
		panic(err)
	}
	ms.SetSocketTimeout(time.Hour * 5)
	ms.SetSyncTimeout(time.Second * 10)
	ms.SetCursorTimeout(0)
	ms.SetMode(mgo.Monotonic, true)

	var ui common.UserIncomeHistory
	now := time.Now()
	startTimestamp := common.GetDateStartUnix(now.Add(time.Hour * -240).Unix())
	endTimestamp := common.GetDateStartUnix(now.Unix())
	v := make(map[string]userLevel)
	iter := ms.DB("cr_app_101").C("user_income").Find(bson.M{"timestamp": bson.M{"$gte": startTimestamp, "$lt": endTimestamp}}).Iter()
	for iter.Next(&ui) {
		var ul userLevel
		if _, ok := v[ui.ID]; !ok {
			ul.m = make(map[string]struct{})
		} else {
			ul = v[ui.ID]
		}
		v[ui.ID] = getUserLevelDetail(ul, ui)
	}

	var t common.UserWithdrawLevelTotal
	userBasicC := ms.DB("cr_app_101").C("user_basic")
	for userID, ul := range v {
		var ub common.UserBasic
		if err := userBasicC.Find(bson.M{"id": userID}).One(&ub); err != nil {
			panic(err)
		}

		var userDays int
		if ub.Create <= startTimestamp {
			userDays = 10
		} else {
			userDays = getDays(ub.Create, endTimestamp)
		}
		if userDays > 10 {
			userDays = 10
		}
		if ul.incomeTotal != 0 {
			ud := common.UserWithdrawLevelData{
				UserID: userID,
				Num:    ul.incomeTotal,
				Days:   userDays,
			}
			t.Read = append(t.Read, ud)
		}
		if ul.apprenticeTotal != 0 {
			ud := common.UserWithdrawLevelData{
				UserID: userID,
				Num:    ul.apprenticeTotal,
				Days:   userDays,
			}
			t.Apprentice = append(t.Apprentice, ud)
		}
		if ul.taskNum != 0 {
			ud := common.UserWithdrawLevelData{
				UserID: userID,
				Num:    ul.taskNum,
				Tasks:  ul.tasks,
				Days:   userDays,
			}
			t.TasksComplete = append(t.TasksComplete, ud)
		}
		if ul.validApprenticeNum != 0 {
			ud := common.UserWithdrawLevelData{
				UserID: userID,
				Num:    ul.validApprenticeNum,
				Days:   userDays,
			}
			t.ValidApprenticeNum = append(t.ValidApprenticeNum, ud)
		}
		if ul.adClick != 0 {
			ud := common.UserWithdrawLevelData{
				UserID: userID,
				Num:    ul.adClick,
				Days:   userDays,
			}
			t.AdClick = append(t.AdClick, ud)
		}
	}

	if err := iter.Err(); err != nil {
		fmt.Println(err.Error())
	}
	fileName := fmt.Sprintf("t%s_101", now.Local().Format("2006-01-02"))
	f, _ := os.OpenFile(fileName, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	c, _ := json.MarshalIndent(t, "", "\t")
	f.WriteString(string(c))
}

// return userLevel after h
func getUserLevelDetail(ul userLevel, h common.UserIncomeHistory) userLevel {
	if h.Type != common.INCOME_TYPE_INCOME {
		return ul
	}
	ul.incomeTotal += h.Count
	switch h.TaskType {
	case common.TASK_TYPE_APPRENTICE:
		ul.apprenticeTotal += h.Count
		if _, ok := ul.m[h.From]; ok {
			return ul
		}
		ul.m[h.From] = struct{}{}
		ul.validApprenticeNum++
	case common.TASK_TYPE_DAILY:
		ul.incomeTotal += h.Count
		ul.tasks = append(ul.tasks, h.TaskID)
		ul.taskNum += 1
	case common.TASK_TYPE_AD_CLICK:
		ul.adClick += h.Count
	}
	return ul
}

type userLevel struct {
	incomeTotal        int // 统计所有收入
	apprenticeTotal    int // 徒弟上供统计
	taskNum            int // 日常任务完成数统计
	validApprenticeNum int // 徒弟数统计
	adClick            int // 广告收入统计
	tasks              []string
	userStartTimestamp int64
	m                  map[string]struct{}
}

func getDays(userStartTimestamp int64, endTimestamp int64) int {
	return int(time.Unix(endTimestamp, 0).Sub(time.Unix(userStartTimestamp, 0))/(time.Hour*24)) + 1
}
