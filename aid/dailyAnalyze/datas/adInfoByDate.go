package datas

import (
	"runtime"
	"runtime/debug"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/aid/dailyAnalyze/dbCommon"
	"vlion/yxw/common"
)

const mongoAddr = "172.16.189.222?maxPoolSize=3000"

var adInfoByDate map[int64][]common.ADInfo

func InitAdInfo(t time.Time) {
	if adInfoByDate == nil {
		adInfoByDate = make(map[int64][]common.ADInfo)
	}
	ms := dbCommon.MongoSessionPool.Clone()
	dateString := t.Format("2006-01-02")
	adInfoC := ms.DB("cr_app_101_ad").C("ad_info_" + dateString)
	pips := []bson.M{}
	var adShowAndClickTotal []common.ADInfo
	err := adInfoC.Pipe(pips).AllowDiskUse().All(&adShowAndClickTotal)
	ms.Close()
	if err != nil {
		panic(err)
	}
	adInfoByDate[common.GetDateStartUnix(t.Unix())] = adShowAndClickTotal
}

func GetAdInfoByDate(t int64) []common.ADInfo {
	return adInfoByDate[common.GetDateStartUnix(t)]
}

func ClearAdInfo(t time.Time) {
	defer func() {
		runtime.GC()
		debug.FreeOSMemory()
	}()
	if adInfoByDate == nil {
		return
	}
	timeKey := common.GetDateStartUnix(t.Unix())
	if _, ok := adInfoByDate[timeKey]; !ok {
		return
	}
	delete(adInfoByDate, timeKey)
}
