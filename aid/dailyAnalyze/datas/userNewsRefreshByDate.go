package datas

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"vlion/yxw/common"
)

var userDateNewsReadMap map[int64]map[string]int   // map[dateStartUinx]map[userID]refreshTime
var userDateLastDevice map[int64]map[string]string // map[dateStartUinx]map[userID]lastDevice

func GetUserDateLastDevice(timestamp int64, userID string) string {
	userLastDevice, ok := userDateLastDevice[common.GetDateStartUnix(timestamp)]
	if !ok {
		return ""
	}
	return userLastDevice[userID]
}

func GetDateNewsRefreshTime(timestamp int64, userID string) int {
	userNewsReadMap, ok := userDateNewsReadMap[common.GetDateStartUnix(timestamp)]
	if !ok {
		return 0
	}
	return userNewsReadMap[userID]
}

func InitUserNewsDateRead(t time.Time) {
	userDateNewsReadMap = make(map[int64]map[string]int)
	userDateLastDevice = make(map[int64]map[string]string)
	startUnix := common.GetDateStartUnix(t.Unix())
	dateString := t.Format("2006-01-02")
	for i := 23; i >= 0; i-- {
		fileName := fmt.Sprintf("/data/log/yxw/monitorLog_vlionserver-217_%s_%02d.log", dateString, i)
		tmpMap := analyzeUserDateInfo(fileName)
		for userID, tud := range tmpMap {
			if userDeviceMap, ok := userDateLastDevice[startUnix]; !ok {
				userDateLastDevice[startUnix] = map[string]string{userID: tud.device}
			} else {
				if _, ok := userDeviceMap[userID]; !ok {
					userDeviceMap[userID] = tud.device
					userDateLastDevice[startUnix] = userDeviceMap
				}
			}

			if tud.readTime == 0 {
				continue
			}
			if userNewsReadMap, ok := userDateNewsReadMap[startUnix]; !ok {
				userDateNewsReadMap[startUnix] = map[string]int{userID: tud.readTime}
			} else {
				if userReadTime, ok := userNewsReadMap[userID]; !ok {
					userNewsReadMap[userID] = tud.readTime
					userDateNewsReadMap[startUnix] = userNewsReadMap
				} else {
					userNewsReadMap[userID] = userReadTime + tud.readTime
					userDateNewsReadMap[startUnix] = userNewsReadMap
				}
			}
		}
	}
	fmt.Println(userDateNewsReadMap[startUnix])
}

type tmpUserDaily struct {
	device   string
	readTime int
}

func analyzeUserDateInfo(fileName string) map[string]tmpUserDaily {
	f, err := os.Open(fileName)
	if err != nil {
		return nil
	}
	defer f.Close()

	tempUserDateMap := make(map[string]tmpUserDaily)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		ar := ResolveFileLine(scanner.Text())
		if ar == nil {
			continue
		}
		if ar.UserID == "" {
			continue
		}
		tud, ok := tempUserDateMap[ar.UserID]
		if !ok {
			tempUserDateMap[ar.UserID] = tmpUserDaily{
				device: ar.DeviceModel,
			}
		} else {
			tud.device = ar.DeviceModel
			tempUserDateMap[ar.UserID] = tud
		}
		if ar.APICode != common.API_LOG_CODE_NEWS_JSON {
			continue
		}
		tud, _ = tempUserDateMap[ar.UserID]
		tud.readTime++
		tempUserDateMap[ar.UserID] = tud
		fmt.Println(ar.UserID, ":", tud)
	}

	return tempUserDateMap
}

func ResolveFileLine(s string) *common.APIRecord {
	var ar common.APIRecord
	ts := strings.Split(s, "\t")
	tsLen := len(ts)
	if tsLen != 32 {
		return nil
	}
	var err error
	ar.APICode, err = strconv.Atoi(ts[0])
	if err != nil {
		ar.APICode = -1
	}
	ar.SignOnce = ts[1]
	ar.SignVersion = ts[2]
	ar.IsSafe = ts[3]
	ar.AppChannel = ts[4]
	ar.UserIP = ts[5]
	ar.UserID = ts[11]
	ar.UserCode = ts[12]
	ar.UserMobile = ts[13]
	ar.DeviceModel = ts[15]
	ar.System = ts[16]
	ar.SystemVersion = ts[17]
	ar.AndroidID = ts[18]
	ar.APPVersion = ts[21]
	ar.RetCode, err = strconv.Atoi(ts[30])
	if err != nil {
		ar.RetCode = -1
	}
	ar.Reward, err = strconv.Atoi(ts[26])
	if err != nil {
		ar.Reward = -1
	}

	return &ar
}
