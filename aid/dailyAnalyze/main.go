package main

import (
	"time"

	"vlion/yxw/aid/dailyAnalyze/datas"
	"vlion/yxw/aid/dailyAnalyze/project/adAnalyze"
	"vlion/yxw/aid/dailyAnalyze/project/antiCheat"
	"vlion/yxw/aid/dailyAnalyze/project/apiCodeStatistics"
	"vlion/yxw/aid/dailyAnalyze/project/sendMail"
	"vlion/yxw/aid/dailyAnalyze/project/userAdClick"
	"vlion/yxw/aid/dailyAnalyze/project/userApprenticeReward"
	"vlion/yxw/aid/dailyAnalyze/project/withdrawList"
)

func main() {
	t := time.Now().Add(time.Hour * -24)
	apiCodeStatistics.Main(t)
	withdrawList.Main(t)
	datas.InitUserNewsDateRead(t)
	datas.InitAdInfo(t)
	adAnalyze.Main(t)
	antiCheat.Main(t)
	userAdClick.Main(t)
	userApprenticeReward.Main(t)
	sendMail.Main(t)
}
