package dbCommon

import (
	"sync"
	"time"

	"github.com/globalsign/mgo"
)

const mongoAddr = "172.16.189.222?maxPoolSize=30000"

var MongoSessionPool *mspool

type mspool struct {
	dbSession *mgo.Session
	wg        *sync.WaitGroup
}

func (m *mspool) Clone() *mspool {
	m.wg.Add(1)
	return &mspool{
		dbSession: m.dbSession.Clone(),
		wg:        m.wg,
	}
}

func (m *mspool) DB(name string) *mgo.Database {
	return m.dbSession.DB(name)
}

func (m *mspool) Close() {
	m.dbSession.Close()
	m.wg.Done()
}

func init() {
	ms, err := mgo.Dial(mongoAddr)
	if err != nil {
		panic(err)
	}
	ms.SetSocketTimeout(time.Hour * 5)
	ms.SetSyncTimeout(time.Hour)
	ms.SetCursorTimeout(0)
	ms.SetMode(mgo.Monotonic, true)

	if err := ms.Ping(); err != nil {
		ms.Close()
		panic(err)
	}

	oldMongoSessionPool := MongoSessionPool
	MongoSessionPool = &mspool{
		dbSession: ms,
		wg:        new(sync.WaitGroup),
	}

	// 关闭老链接
	go func() {
		if oldMongoSessionPool == nil {
			return
		}
		oldMongoSessionPool.wg.Wait()
		oldMongoSessionPool.dbSession.Close()
	}()
}
