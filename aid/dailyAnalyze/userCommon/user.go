package userCommon

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/aid/dailyAnalyze/datas"
	"vlion/yxw/common"
)

type User struct {
	UserID              string
	Code                string
	Mobile              string
	MasterCode          string
	Status              int
	IncomeTotal         int
	IncomeRemaining     int
	Create              int64
	DateReadCount       int
	DateReadTotal       int
	DateAdClickCount    int
	DateAdClickTotal    int
	DateIncomeTotal     int
	ReadTotal           int
	AdClickTotal        int
	LastActive          int64
	LogAttr             common.APIRecord
	IncomeStruct        map[string]int
	EightClick          int
	DateLastDevice      string
	DateNewsRefreshTime int
}

type UserIncomeType struct {
	UserIncomeTaskType []Detail `bson:"user_income_task_type"`
}

type Detail struct {
	Name  string `bson:"name"`
	Value int    `bson:"value"`
}

func (u *User) InitBasic(userID string, userBasicC *mgo.Collection) error {
	var ub common.UserBasic
	if err := userBasicC.Find(bson.M{"id": userID}).One(&ub); err != nil {
		return err
	}
	u.InitUserByBasic(ub, userBasicC)
	return nil
}

func (u *User) InitUserByBasic(ub common.UserBasic, userBasicC *mgo.Collection) {
	u.UserID = ub.ID
	u.Code = ub.Code
	u.Mobile = ub.Info.Mobile
	u.Create = ub.Create
	u.IncomeTotal = ub.IncomeTotal
	u.IncomeRemaining = ub.IncomeRemaining
	u.Status = ub.Status
	if ub.From != "" {
		var masterUb common.UserBasic
		if err := userBasicC.Find(bson.M{"id": ub.From}).One(&masterUb); err != nil {
			u.MasterCode = "获取失败"
		} else {
			u.MasterCode = masterUb.Code
		}
	}
}

func (u *User) InitAdvance(userIncomeByDateC, userIncomeTotalC *mgo.Collection, t time.Time) {
	dateCount, err := GetDateCount(u.UserID, t.Unix(), common.TASK_TYPE_FIXED_BOUNDS, userIncomeByDateC)
	if err != nil {
		u.DateReadCount = -1
	} else {
		u.DateReadCount = dateCount
	}

	dateCount, err = GetDateCount(u.UserID, t.Unix(), common.TASK_TYPE_AD_CLICK, userIncomeByDateC)
	if err != nil {
		u.DateAdClickCount = -1
	} else {
		u.DateAdClickCount = dateCount
	}

	dateTotal, err := GetDateIncomeTotal(u.UserID, t.Unix(), userIncomeByDateC)
	if err != nil {
		u.DateIncomeTotal = -1
	} else {
		u.DateIncomeTotal = dateTotal
	}

	dateTotal, err = GetDateTotal(u.UserID, t.Unix(), common.TASK_TYPE_FIXED_BOUNDS, userIncomeByDateC)
	if err != nil {
		u.DateReadTotal = -1
	} else {
		u.DateReadTotal = dateTotal
	}

	dateTotal, err = GetDateTotal(u.UserID, t.Unix(), common.TASK_TYPE_AD_CLICK, userIncomeByDateC)
	if err != nil {
		u.DateAdClickTotal = -1
	} else {
		u.DateAdClickTotal = dateTotal
	}

	readTotal, err := GetTotalByType(u.UserID, common.TASK_TYPE_FIXED_BOUNDS, userIncomeTotalC)
	if err != nil {
		u.ReadTotal = -1
	} else {
		u.ReadTotal = readTotal
	}
	adClickTotal, err := GetTotalByType(u.UserID, common.TASK_TYPE_AD_CLICK, userIncomeTotalC)
	if err != nil {
		u.AdClickTotal = -1
	} else {
		u.AdClickTotal = adClickTotal
	}
}

func GetDateCount(userID string, dateUnix int64, taskType int, userIncomeByDateC *mgo.Collection) (int, error) {
	startUnix := common.GetDateStartUnix(dateUnix)
	var uibd common.UserIncomeByDate
	if err := userIncomeByDateC.Find(bson.M{"id": userID, "timestamp": startUnix, "task_type": taskType}).One(&uibd); err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return uibd.Count, nil
}

func GetDateTotal(userID string, dateUnix int64, taskType int, userIncomeByDateC *mgo.Collection) (int, error) {
	startUnix := common.GetDateStartUnix(dateUnix)
	var uibd common.UserIncomeByDate
	if err := userIncomeByDateC.Find(bson.M{"id": userID, "timestamp": startUnix, "task_type": taskType}).One(&uibd); err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return uibd.Total, nil
}

func GetDateIncomeTotal(userID string, dateUnix int64, userIncomeByDateC *mgo.Collection) (int, error) {
	startUnix := common.GetDateStartUnix(dateUnix)
	var uibd common.UserIncomeByDate

	if err := userIncomeByDateC.Find(bson.M{"id": userID, "timestamp": startUnix, "task_type": common.TOTAL_CONF_USER_INCOME}).One(&uibd); err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return uibd.Total, nil
}

func GetTotalByType(userID string, taskType int, userIncomeTotalC *mgo.Collection) (int, error) {
	var uibt common.UserIncomeByType
	if err := userIncomeTotalC.Find(bson.M{"id": userID, "task_type": taskType}).One(&uibt); err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return uibt.Total, nil
}

func (u *User) GetIncomeStruct(ds []Detail, userIncomeTotalC *mgo.Collection) {
	u.IncomeStruct = make(map[string]int)
	for _, d := range ds {
		value, err := GetTotalByType(u.UserID, d.Value, userIncomeTotalC)
		if err != nil {
			u.IncomeStruct[d.Name] = -1
		} else {
			u.IncomeStruct[d.Name] = value
		}
	}
}

func (u *User) GetLastActive(userChannelC *mgo.Collection) {
	var ac common.AppChannel
	userChannelC.Find(bson.M{"id": u.UserID}).Sort("-timestamp").One(&ac)
	u.LastActive = ac.Timestamp
}

func (u *User) GetEightClicks(ais []common.ADInfo) {
	var total int
	for _, ai := range ais {
		if ai.ADReqType != common.AD_REQ_TYPE_CLICK {
			continue
		}
		if ai.UserID != u.UserID {
			continue
		}
		total++
	}
	u.EightClick = total
}

func (u *User) GetDateLastDeviceInfo(t int64) {
	u.DateLastDevice = datas.GetUserDateLastDevice(t, u.UserID)
}

func (u *User) GetDateNewsRefreshTime(t int64) {
	u.DateNewsRefreshTime = datas.GetDateNewsRefreshTime(t, u.UserID)
}
