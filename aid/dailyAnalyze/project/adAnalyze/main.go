package adAnalyze

import (
	"fmt"
	"time"

	"github.com/tealeg/xlsx"
)

func Main(t time.Time) {
	xlsxFile := xlsx.NewFile()
	dateString := t.Format("2006-01-02")
	sheet, _ := xlsxFile.AddSheet(dateString)
	genSheetByDate(sheet, t)
	xlsxFile.Save(fmt.Sprintf("ad_analyze_%s.xlsx", dateString))
}
