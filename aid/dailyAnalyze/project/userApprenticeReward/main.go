package userApprenticeReward

import (
	"fmt"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/dbCommon"
	"vlion/yxw/aid/dailyAnalyze/userCommon"
	"vlion/yxw/common"
)

const mongoAddr = "172.16.189.222?maxPoolSize=3000"

func Main(t time.Time) {
	ms := dbCommon.MongoSessionPool.Clone()
	defer ms.Close()
	userBasicC := ms.DB("cr_app_101").C("user_basic")
	userIncomeStructC := ms.DB("cr_app_all").C("cr_app_back_conf")
	userChannelC := ms.DB("cr_app_101").C("user_channel")
	userIncomeTotalC := ms.DB("cr_app_101").C("user_income_total")
	apprenticeReawrdC := ms.DB("cr_app_101").C("user_apprentice_reward")
	xlsxFile := xlsx.NewFile()
	dateString := t.Format("2006-01-02")
	log.Info(dateString)
	startTimestamp := common.GetDateStartUnix(t.Unix())
	endTimestamp := common.GetDateEndUnix(t.Unix())
	sheet, _ := xlsxFile.AddSheet(dateString)
	var uars []common.UserApprenticeReward
	if err := apprenticeReawrdC.Find(bson.M{"open_timestamp": bson.M{"$gte": startTimestamp, "$lt": endTimestamp}}).All(&uars); err != nil {
		log.Fields{
			"err":  err.Error(),
			"date": dateString,
		}.Error("e")
		return
	}
	countMap, totalMap := make(map[string]int), make(map[string]int)
	for _, uar := range uars {
		countMap[uar.ID]++
		totalMap[uar.ID] += uar.Reward
	}
	columns := []string{
		"code",
		"用户状态",
		"注册时间",
		"开宝箱数量",
		"开宝箱金额",
		"最后活跃时间",
	}
	row := sheet.AddRow()
	for _, column := range columns {
		cell := row.AddCell()
		cell.SetString(column)
	}
	var uit userCommon.UserIncomeType
	uitErr := userIncomeStructC.Find(bson.M{"id": "101"}).One(&uit)
	if uitErr != nil {
		log.Fields{
			"err": uitErr.Error(),
		}.Error("fail to get user income struct c")
	}
	if uitErr == nil {
		for _, dt := range uit.UserIncomeTaskType {
			cell := row.AddCell()
			cell.Value = dt.Name
		}
	}
	for k := range countMap {
		u := new(userCommon.User)
		if err := u.InitBasic(k, userBasicC); err != nil {
			log.Fields{
				"err": err.Error(),
			}.Error("f")
			continue
		}
		row := sheet.AddRow()
		cell := row.AddCell()
		cell.SetString(u.Code)
		cell = row.AddCell()
		cell.SetInt(u.Status)
		cell = row.AddCell()
		cell.SetString(time.Unix(u.Create, 0).Format("2006-01-02 15:04:05"))
		cell = row.AddCell()
		cell.SetInt(countMap[k])
		cell = row.AddCell()
		cell.SetInt(totalMap[k])
		u.GetLastActive(userChannelC)
		cell = row.AddCell()
		cell.SetString(time.Unix(u.LastActive, 0).Format("2006-01-02"))
		if uitErr == nil {
			u.GetIncomeStruct(uit.UserIncomeTaskType, userIncomeTotalC)
			for _, dt := range uit.UserIncomeTaskType {
				cell := row.AddCell()
				cell.SetInt(u.IncomeStruct[dt.Name])
			}
		}
	}

	xlsxFile.Save(fmt.Sprintf("apprentice_reward_%s.xlsx", dateString))
}
