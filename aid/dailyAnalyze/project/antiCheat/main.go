package antiCheat

import (
	"fmt"
	"sync"
	"time"

	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/dbCommon"
)

const mongoAddr = "172.16.189.222?maxPoolSize=3000"

func Main(t time.Time) {
	xlsxFile := xlsx.NewFile()
	ms := dbCommon.MongoSessionPool.Clone()
	defer ms.Close()
	userBasicC := ms.DB("cr_app_101").C("user_basic")
	userIncomeStructC := ms.DB("cr_app_all").C("cr_app_back_conf")
	userChannelC := ms.DB("cr_app_101").C("user_channel")
	userIncomeDailyC := ms.DB("cr_app_101").C("user_income_by_date")
	userIncomeTotalC := ms.DB("cr_app_101").C("user_income_total")
	wg := new(sync.WaitGroup)
	hourlyReadSheet, err := xlsxFile.AddSheet("阅读每小时到达或超过60次且状态正常")
	if err != nil {
		panic(err)
	}
	wg.Add(1)
	go genHourlyReadSheet(hourlyReadSheet, userBasicC, userIncomeDailyC, userIncomeTotalC, userIncomeStructC, userChannelC, t, wg)

	//	dailyReadSheet, err := xlsxFile.AddSheet("单日阅读超2400名单")
	//	if err != nil {
	//		panic(err)
	//	}
	//	wg.Add(1)
	//	go genDailyReadSheet(dailyReadSheet, userBasicC, userIncomeDailyC, userIncomeTotalC, userIncomeStructC, userChannelC, adInfoC, now, wg)
	//	totalReadSheet, err := xlsxFile.AddSheet("阅读>5W广告<500,状态正常,红闻少于200,一月内活跃")
	//	if err != nil {
	//		panic(err)
	//	}
	//	wg.Add(1)
	//	go genTotalReadSheet(totalReadSheet, userBasicC, userIncomeDailyC, userIncomeTotalC, userIncomeStructC, userChannelC, now, wg)

	wg.Wait()
	xlsxFile.Save(fmt.Sprintf("antiCheat_%s.xlsx", t.Local().Format("2006-01-02")))
}
