package antiCheat

import (
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/userCommon"
	"vlion/yxw/common"
)

func genTotalReadSheet(sheet *xlsx.Sheet, userBasicC, userIncomeDailyC, userIncomeTotalC, userIncomeStructC, userChannelC *mgo.Collection, now time.Time, wg *sync.WaitGroup) {
	defer wg.Done()

	columns := []string{
		"收徒码",
		"手机号",
		"用户状态",
		"注册时间",
		"总金币",
		"总阅读金币",
		"总广告金币",
		"广告金币/阅读金币",
		"最后活跃时间",
	}
	SheetRow := sheet.AddRow()
	for _, column := range columns {
		cell := SheetRow.AddCell()
		cell.Value = column
	}
	var uit userCommon.UserIncomeType
	uitErr := userIncomeStructC.Find(bson.M{"id": "101"}).One(&uit)
	if uitErr != nil {
		log.Fields{
			"err": uitErr.Error(),
		}.Error("fail to get user income struct c")
	}
	if uitErr == nil {
		for _, dt := range uit.UserIncomeTaskType {
			cell := SheetRow.AddCell()
			cell.Value = dt.Name
		}
	}
	count, _ := userBasicC.Find(bson.M{"income_total": bson.M{"$gte": 50000}}).Count()
	iter := userBasicC.Find(bson.M{"income_total": bson.M{"$gte": 50000}}).Iter()
	var ub common.UserBasic
	i := 1
	for iter.Next(&ub) {
		log.Fields{
			"current": fmt.Sprintf("%d/%d", i, count),
			"proc":    "totalReadSheet",
		}.Info("progressing")
		i++
		u := new(userCommon.User)
		u.InitUserByBasic(ub, userBasicC)
		if u.Status != common.STATUS_USER_VALID {
			continue
		}
		u.GetLastActive(userChannelC)
		if u.LastActive < common.GetDateStartUnix(now.Add(time.Hour*-24*30).Unix()) {
			continue
		}
		u.InitAdvance(userIncomeDailyC, userIncomeTotalC, now)
		if u.ReadTotal < 50000 {
			continue
		}
		if u.AdClickTotal > 500 {
			continue
		}
		if uitErr == nil {
			u.GetIncomeStruct(uit.UserIncomeTaskType, userIncomeTotalC)
			if u.IncomeStruct["阅读红包新闻奖励"] > 200 {
				continue
			}
		}
		cells := []string{
			u.Code,
			u.Mobile,
			strconv.Itoa(u.Status),
			time.Unix(u.Create, 0).Local().Format("2006-01-02 15:04:05"),
			strconv.Itoa(u.IncomeTotal),
			strconv.Itoa(u.ReadTotal),
			strconv.Itoa(u.AdClickTotal),
			strconv.FormatFloat(float64(u.AdClickTotal)/float64(u.ReadTotal), 'f', 3, 64),
			time.Unix(u.LastActive, 0).Local().Format("2006-01-02 15:04:05"),
		}
		sheetDetailRow := sheet.AddRow()
		for _, detail := range cells {
			cell := sheetDetailRow.AddCell()
			cell.Value = detail
		}
		if uitErr == nil {
			for _, dt := range uit.UserIncomeTaskType {
				cell := sheetDetailRow.AddCell()
				cell.Value = strconv.Itoa(u.IncomeStruct[dt.Name])
			}
		}
	}
}
