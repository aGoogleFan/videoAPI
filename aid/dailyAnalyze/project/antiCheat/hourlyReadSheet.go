package antiCheat

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/datas"
	"vlion/yxw/aid/dailyAnalyze/userCommon"
	"vlion/yxw/common"
)

func genHourlyReadSheet(sheet *xlsx.Sheet, userBasicC, userIncomeDailyC, userIncomeTotalC, userIncomeStructC, userChannelC *mgo.Collection, t time.Time, wg *sync.WaitGroup) {
	defer wg.Done()

	lastDateString := t.Local().Format("2006-01-02")
	m := make(map[string]common.APIRecord)
	for i := 0; i <= 23; i++ {
		fileName := fmt.Sprintf("/data/log/yxw/monitorLog_vlionserver-217_%s_%d.log", lastDateString, i)
		mm := analyzeFileReadOK(fileName)
		for k, v := range mm {
			m[k] = v
		}
	}
	columns := []string{
		"收徒码",
		"手机号",
		"用户状态",
		"注册时间",
		"总金币",
		"is_safe",
		"ip",
		"app版本",
		"签名版本",
		"最后活跃时间",
		"当日最后使用设备",
		"当日阅读金币",
		"8号位点击数",
		"阅读列表获取次数",
	}
	SheetRow := sheet.AddRow()
	for _, column := range columns {
		cell := SheetRow.AddCell()
		cell.Value = column
	}
	var uit userCommon.UserIncomeType
	uitErr := userIncomeStructC.Find(bson.M{"id": "101"}).One(&uit)
	if uitErr != nil {
		log.Fields{
			"err": uitErr.Error(),
		}.Error("fail to get user income struct c")
	}
	if uitErr == nil {
		for _, dt := range uit.UserIncomeTaskType {
			cell := SheetRow.AddCell()
			cell.Value = dt.Name
		}
	}
	adInfos := datas.GetAdInfoByDate(t.Unix())
	for _, ar := range m {
		u := new(userCommon.User)
		u.InitBasic(ar.UserID, userBasicC)
		if u.Status != common.STATUS_USER_VALID {
			continue
		}
		u.GetLastActive(userChannelC)
		u.InitAdvance(userIncomeDailyC, userIncomeTotalC, t)
		u.GetEightClicks(adInfos)
		u.GetDateNewsRefreshTime(t.Unix())
		u.GetDateLastDeviceInfo(t.Unix())
		u.LogAttr = ar
		cells := []string{
			u.Code,
			u.Mobile,
			strconv.Itoa(u.Status),
			time.Unix(u.Create, 0).Local().Format("2006-01-02 15:04:05"),
			strconv.Itoa(u.IncomeTotal),
			u.LogAttr.IsSafe,
			u.LogAttr.UserIP,
			u.LogAttr.APPVersion,
			u.LogAttr.SignVersion,
			time.Unix(u.LastActive, 0).Local().Format("2006-01-02"),
			u.DateLastDevice,
		}
		sheetDetailRow := sheet.AddRow()
		for _, detail := range cells {
			cell := sheetDetailRow.AddCell()
			cell.SetString(detail)
		}

		ints := []int{
			u.DateReadTotal,
			u.EightClick,
			u.DateNewsRefreshTime,
		}
		for _, it := range ints {
			cell := sheetDetailRow.AddCell()
			cell.SetInt(it)
		}
		if uitErr == nil {
			u.GetIncomeStruct(uit.UserIncomeTaskType, userIncomeTotalC)
			for _, dt := range uit.UserIncomeTaskType {
				cell := sheetDetailRow.AddCell()
				cell.SetInt(u.IncomeStruct[dt.Name])
			}
		}
	}
}

// map[userID]APIRecord
func analyzeFileReadOK(fileName string) map[string]common.APIRecord {
	f, err := os.Open(fileName)
	if err != nil {
		return nil
	}
	defer f.Close()

	m := make(map[string]int)
	m1 := make(map[string]common.APIRecord)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		ar := datas.ResolveFileLine(scanner.Text())
		if ar == nil {
			continue
		}
		if ar.APICode != common.API_LOG_CODE_GET_READ_REWARD || ar.RetCode != common.STATUS_OK {
			continue
		}
		m1[ar.UserID] = *ar
		m[ar.UserID]++
	}

	if err := scanner.Err(); err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("eeerrr")
		return nil
	}
	for k, v := range m {
		if v < 60 {
			delete(m1, k)
		}
	}

	return m1
}
