package antiCheat

import (
	"strconv"
	"sync"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/datas"
	"vlion/yxw/aid/dailyAnalyze/userCommon"
	"vlion/yxw/common"
)

func genDailyReadSheet(sheet *xlsx.Sheet, userBasicC, userIncomeDailyC, userIncomeTotalC, userIncomeStructC, userChannelC *mgo.Collection, t time.Time, wg *sync.WaitGroup) {
	defer wg.Done()
	columns := []string{
		"收徒码",
		"手机号",
		"用户状态",
		"注册时间",
		"最后活跃时间",
		"当日最后使用设备",
		"当日阅读金币",
		"总金币",
		"总阅读金币",
		"总广告金币",
		"8号位点击数",
		"阅读列表获取次数",
		"广告金币/阅读金币",
	}
	SheetRow := sheet.AddRow()
	for _, column := range columns {
		cell := SheetRow.AddCell()
		cell.Value = column
	}
	var uit userCommon.UserIncomeType
	uitErr := userIncomeStructC.Find(bson.M{"id": "101"}).One(&uit)
	if uitErr != nil {
		log.Fields{
			"err": uitErr.Error(),
		}.Error("fail to get user income struct c")
	}
	if uitErr == nil {
		for _, dt := range uit.UserIncomeTaskType {
			cell := SheetRow.AddCell()
			cell.Value = dt.Name
		}
	}
	adInfos := datas.GetAdInfoByDate(t.Unix())
	dateStartUnix := common.GetDateStartUnix(t.Unix())
	iter := userIncomeDailyC.Find(bson.M{
		"total":     bson.M{"$gte": 2400},
		"task_type": common.TASK_TYPE_FIXED_BOUNDS,
		"timestamp": dateStartUnix,
	}).Iter()
	var uibd common.UserIncomeByDate
	for iter.Next(&uibd) {
		u := new(userCommon.User)
		u.InitBasic(uibd.ID, userBasicC)
		u.GetLastActive(userChannelC)
		u.InitAdvance(userIncomeDailyC, userIncomeTotalC, t)
		u.GetEightClicks(adInfos)
		u.GetDateNewsRefreshTime(t.Unix())
		u.GetDateLastDeviceInfo(t.Unix())
		cells := []string{
			u.Code,
			u.Mobile,
			strconv.Itoa(u.Status),
			time.Unix(u.Create, 0).Local().Format("2006-01-02 15:04:05"),
			time.Unix(u.LastActive, 0).Local().Format("2006-01-02"),
			u.DateLastDevice,
		}
		sheetDetailRow := sheet.AddRow()
		for _, detail := range cells {
			cell := sheetDetailRow.AddCell()
			cell.SetString(detail)
		}

		ints := []int{
			u.DateReadTotal,
			u.IncomeTotal,
			u.ReadTotal,
			u.AdClickTotal,
			u.EightClick,
			u.DateNewsRefreshTime,
		}
		for _, it := range ints {
			cell := sheetDetailRow.AddCell()
			cell.SetInt(it)
		}
		cell := sheetDetailRow.AddCell()
		cell.SetFloatWithFormat(float64(u.AdClickTotal)/float64(u.ReadTotal), "0.00%")
		if uitErr == nil {
			u.GetIncomeStruct(uit.UserIncomeTaskType, userIncomeTotalC)
			for _, dt := range uit.UserIncomeTaskType {
				cell := sheetDetailRow.AddCell()
				cell.SetInt(u.IncomeStruct[dt.Name])
			}
		}
	}
}

type tmpUserID struct {
	UserID string `bson:"_id"`
}
