package withdrawList

import (
	"fmt"
	"strconv"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/dbCommon"
	"vlion/yxw/aid/dailyAnalyze/userCommon"
	"vlion/yxw/common"
)

func Main(t time.Time) {
	ms := dbCommon.MongoSessionPool.Clone()
	defer ms.Close()

	userOrderC := ms.DB("cr_app_101").C("user_order")
	userBasicC := ms.DB("cr_app_101").C("user_basic")
	userIncomeStructC := ms.DB("cr_app_all").C("cr_app_back_conf")
	userChannelC := ms.DB("cr_app_101").C("user_channel")
	// userIncomeDailyC := ms.DB("cr_app_101").C("user_income_by_date")
	userIncomeTotalC := ms.DB("cr_app_101").C("user_income_total")
	dateString := t.Format("2006-01-02")
	xlsxFile := xlsx.NewFile()
	sheet, _ := xlsxFile.AddSheet(dateString)
	GetWithdrawDetail(sheet, userOrderC, userBasicC, userIncomeStructC, userChannelC, userIncomeTotalC, t)
	xlsxFile.Save(fmt.Sprintf("withdraw_%s.xlsx", dateString))
}

func GetWithdrawDetail(sheet *xlsx.Sheet, userOrderC, userBasicC, userIncomeStructC, userChannelC, userIncomeTotalC *mgo.Collection, t time.Time) {
	timeUnix := t.Unix()
	startUnix := common.GetDateStartUnix(timeUnix)
	endUnix := common.GetDateEndUnix(timeUnix)
	pips := []bson.M{
		bson.M{
			"$match": bson.M{
				"history": bson.M{
					"$elemMatch": bson.M{
						"ftimestamp": bson.M{"$gte": startUnix, "$lt": endUnix},
						"status":     common.ORDER_STATUS_PASSED,
					},
				},
			},
		},
	}

	var uos []common.UserOrder
	if err := userOrderC.Pipe(pips).AllowDiskUse().All(&uos); err != nil {
		log.Fields{
			"err":  err.Error(),
			"date": t.Format("2006-01-02"),
		}.Error("date fail")
	}

	columns := []string{
		"收徒码",
		"手机号",
		"用户状态",
		"注册时间",
		"申请提现时间",
		"提现审核通过时间",
		"最后活跃时间",
		"提现金额",
		"花费金币",
		"总金币",
	}
	row := sheet.AddRow()
	for _, column := range columns {
		cell := row.AddCell()
		cell.Value = column
	}
	var uit userCommon.UserIncomeType
	uitErr := userIncomeStructC.Find(bson.M{"id": "101"}).One(&uit)
	if uitErr != nil {
		log.Fields{
			"err": uitErr.Error(),
		}.Error("fail to get user income struct c")
	}
	if uitErr == nil {
		for _, dt := range uit.UserIncomeTaskType {
			cell := row.AddCell()
			cell.Value = dt.Name
		}
	}
	for _, uo := range uos {
		for _, h := range uo.History {
			if h.Status != common.ORDER_STATUS_PASSED {
				continue
			}
			if h.FTimestamp < startUnix {
				continue
			}
			if h.FTimestamp >= endUnix {
				continue
			}

			u := new(userCommon.User)
			u.InitBasic(uo.ID, userBasicC)
			u.GetLastActive(userChannelC)
			cells := []string{
				u.Code,
				u.Mobile,
				strconv.Itoa(u.Status),
				time.Unix(u.Create, 0).Local().Format("2006-01-02 15:04:05"),
				time.Unix(h.Timestamp, 0).Local().Format("2006-01-02 15:04:05"),
				time.Unix(h.FTimestamp, 0).Local().Format("2006-01-02 15:04:05"),
				time.Unix(u.LastActive, 0).Local().Format("2006-01-02"),
			}
			sheetDetailRow := sheet.AddRow()
			for _, detail := range cells {
				cell := sheetDetailRow.AddCell()
				cell.SetString(detail)
			}
			ints := []int{
				h.Amount,
				h.Cost,
				u.IncomeTotal,
			}
			for _, it := range ints {
				cell := sheetDetailRow.AddCell()
				cell.SetInt(it)
			}

			if uitErr == nil {
				u.GetIncomeStruct(uit.UserIncomeTaskType, userIncomeTotalC)
				for _, dt := range uit.UserIncomeTaskType {
					cell := sheetDetailRow.AddCell()
					cell.SetInt(u.IncomeStruct[dt.Name])
				}
			}

		}
	}
}
