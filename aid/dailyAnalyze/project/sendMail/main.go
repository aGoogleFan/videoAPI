package sendMail

import (
	"fmt"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/go-gomail/gomail"
)

func Main(t time.Time) {
	dateString := t.Format("2006-01-02")
	m := gomail.NewMessage()

	m.SetAddressHeader("From", "ht@vlion.cn", "HuangTao") // 发件人

	m.SetHeader("To", m.FormatAddress("info@xlion.cn", "收件人"))  // 收件人
	m.SetHeader("Cc", m.FormatAddress("29677846@qq.com", "抄送")) //抄送
	//	m.SetHeader("Bcc", m.FormatAddress("xxxx@gmail.com", "收件人"))  // 暗送

	m.SetHeader("Subject", "报告_"+dateString) // 主题

	// m.SetBody("text/html",xxxxx ") // 可以放html..还有其他的
	// m.SetBody("我是正文") // 正文

	attachs := []string{
		fmt.Sprintf("ad_analyze_%s.xlsx", dateString),
		fmt.Sprintf("antiCheat_%s.xlsx", dateString),
		fmt.Sprintf("%s_user_analyze_8.xlsx", dateString),
		fmt.Sprintf("apprentice_reward_%s.xlsx", dateString),
		fmt.Sprintf("api_code_%s.xlsx", dateString),
		fmt.Sprintf("withdraw_%s.xlsx", dateString),
	}
	for _, attach := range attachs {
		m.Attach(attach) //添加附件
	}

	d := gomail.NewPlainDialer("smtp.exmail.qq.com", 587, "ht@vlion.cn", "1qaz@WSX") // 发送邮件服务器、端口、发件人账号、发件人密码
	if err := d.DialAndSend(m); err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to send mail")
		return
	}

}
