package apiCodeStatistics

import (
	"bufio"
	"fmt"
	"os"
	"time"

	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/datas"
)

var apiCodeMap = map[int]string{
	0:   "签名验证错误",
	101: "签名验证错误",
	102: "广告点击",
	103: "微信绑定",
	104: "支付宝绑定",
	105: "获取分享文章内容",
	107: "获取短信验证码",
	108: `获取"我的数据"`,
	109: "获取阅读奖励",
	110: "获取时段奖励时间间隔",
	111: "获取时段奖励",
	112: "获取金币明细",
	113: `获取"推送给我的消息"`,
	114: "获取签到信息",
	115: "用户签到",
	116: "获取任务列表",
	117: "用户注册/登录",
	118: "微信登录",
	119: "支付宝登录",
	120: "设置师徒码",
	121: "获取视频金币",
	122: "微信提现",
	123: "支付宝提现",
	124: "闲玩游戏奖励",
	125: "抢宝箱(红包雨)",
	126: "更新用户信息",
	127: "app初始化",
	128: "获取新闻分类",
	129: "获取新闻页面",
	130: "阅读红包新闻",
	131: "获取剩余红包新闻数量",
	132: "获取用户个性化信息",
	133: "获取支付宝登录参数",
	134: "获取师徒列表",
	135: `获取"我的徒弟"列表`,
	136: "获取收益排行",
	137: "获取最近提现列表",
	138: "获取广告列表",
	139: "获取阅读转盘参数",
	140: "获取转盘提示语",
	141: "获取随机新闻",
	142: "获取分享内容",
	143: `获取"我的订单"`,
	144: `获取"我的收益明细"`,
	145: "获取app链接",
	146: "获取app版本",
	147: "获取红包雨时间间隔",
	148: "获取推送新闻详情",
	149: `获取"我的提现活跃度"`,
	150: `获取"我的可提现列表"`,
	151: `获取"我最近未完成的订单"`,
	152: "日志上传",
	160: "广告请求上报",
	161: "广告返回上报",
	162: "广告展示上报",
	163: "广告点击上报",
	200: "提现手动审批",
}

var retCodeMap = map[int]string{
	0:     "正常",
	20009: "其它错误",
	21001: "该手机号已存在",
	21002: "短信验证码错误",
	21003: "短信平台发送验证码出错",
	21005: "邀请码错误",
	21006: "用户已经绑定过邀请码",
	21007: "用户试图绑定自己的邀请码",
	21008: "非法手机号码",
	21009: "用户密码错误",
	21010: "任务类别不存在",
	21011: "任务无效或不存在",
	21012: "设备登录账户数超过限制",
	21013: "ip登录账户数超过限制",
	21014: "循环师徒绑定(徒弟绑定师傅，师傅试图绑定徒弟)",
	22000: "师徒宝箱不存在",
	22001: "师徒宝箱未生效",
	22002: "师徒宝箱已被开启",
	30000: "服务器错误",
	30001: "签名校验失败",
	30002: "应用ID无效",
	30003: "登录超时,请重新登录(校验token的有效时间)",
	30004: "参数缺失或错误",
	30005: "帐号暂不可用",
	30006: "数据统计中",
	30007: "时间校验错误",
	30008: "app版本过老,请升级",
	40001: "金币不足",
	40002: "提现汇率发生变化",
	40003: "获取金币到达/超过上限",
	40004: "每日提现到达/超过上限",
	40005: "每日提交提现申请次数达上限",
	40006: "提现金额与用户等级不符",
	50000: "该账号未绑定支付宝帐号",
	50001: "该账号未绑定微信帐号",
	50002: "该支付宝帐号已被绑定",
	50003: "该微信帐号已被绑定",
	50004: "该支付宝账号尚未绑定用户",
	50005: "该微信账号尚未绑定用户",
	50006: "用户已绑定过支付宝账户",
	50007: "用户已绑定过微信账户",
	60001: "未到可领取奖励时间",
	60002: "领取奖励时间已过",
	60003: "已领取过该奖励",
	60004: "不在时间段内",
}

func Main(t time.Time) {
	apiCodeInfoMap := make(map[int]map[int]int) // map[apiCode]map[returnCode]times
	dateString := t.Local().Format("2006-01-02")
	for i := 0; i <= 23; i++ {
		fileName := fmt.Sprintf("/data/log/yxw/monitorLog_vlionserver-217_%s_%d.log", dateString, i)
		mm := doIt(fileName)
		for apiCode, codeInfo := range mm {
			apiCodeInfo, ok := apiCodeInfoMap[apiCode]
			if !ok {
				apiCodeInfo = make(map[int]int)
			}
			for retCode, times := range codeInfo {
				apiCodeInfo[retCode] += times
			}
			apiCodeInfoMap[apiCode] = apiCodeInfo
		}
	}
	xlsxFile := xlsx.NewFile()
	sheet, _ := xlsxFile.AddSheet("api返回码统计")
	columns := []string{
		"api_code",
		"接口",
		"返回码",
		"返回码含义",
		"次数",
		"百分比",
	}
	row := sheet.AddRow()
	for _, column := range columns {
		cell := row.AddCell()
		cell.SetString(column)
	}
	for apiCode, codeInfo := range apiCodeInfoMap {
		var total int
		for _, times := range codeInfo {
			total += times
		}
		for retCode, times := range codeInfo {
			row := sheet.AddRow()
			cell := row.AddCell()
			cell.SetInt(apiCode)
			cell = row.AddCell()
			cell.SetString(apiCodeMap[apiCode])
			cell = row.AddCell()
			cell.SetInt(retCode)
			cell = row.AddCell()
			cell.SetString(retCodeMap[retCode])
			cell = row.AddCell()
			cell.SetInt(times)
			cell = row.AddCell()
			cell.SetFloatWithFormat(float64(times)/float64(total), "0.00%")
		}
	}
	xlsxFile.Save("api_code_" + dateString + ".xlsx")
}

// map[apiCode]map[returnCode]times
func doIt(fileName string) map[int]map[int]int {
	f, err := os.Open(fileName)
	if err != nil {
		return nil
	}
	defer f.Close()

	m := make(map[int]map[int]int) // map[apiCode]map[returnCode]times
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		ar := datas.ResolveFileLine(scanner.Text())
		if ar == nil {
			continue
		}
		tmpMap, ok := m[ar.APICode]
		if !ok {
			tmpMap = make(map[int]int)
		}
		tmpMap[ar.RetCode]++
		m[ar.APICode] = tmpMap
	}

	return m
}
