package userAdClick

import (
	"sync"
	"time"

	"github.com/globalsign/mgo"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/datas"
	"vlion/yxw/aid/dailyAnalyze/userCommon"
	"vlion/yxw/common"
)

type adShowAndClick struct {
	adClick          int
	adClickAfterTrim int
	clickExists      map[string]struct{} // map[req_id]struct
	adShow           int
	adShowAfterTrim  int
	showExists       map[string]struct{} // map[req_id]struct
}

func genSheetByDate(sheet *xlsx.Sheet, t time.Time, userInfoC, userIncomeDailyC *mgo.Collection, wg *sync.WaitGroup) {
	defer wg.Done()
	m := make(map[string]map[string]adShowAndClick) // map[appVersion]map[userID]adShowAndClick
	adInfos := datas.GetAdInfoByDate(t.Unix())
	for _, d := range adInfos {
		if d.ADID != "8" {
			continue
		}
		switch d.ADReqType {
		case common.AD_REQ_TYPE_CLICK:
			userVersionMap, ok := m[d.AppVersion]
			if !ok {
				m[d.AppVersion] = map[string]adShowAndClick{
					d.UserID: adShowAndClick{
						adClick:          1,
						adClickAfterTrim: 1,
						clickExists:      map[string]struct{}{d.ADReqID: struct{}{}},
						adShow:           0,
						adShowAfterTrim:  0,
					},
				}
				continue
			}
			s, ok := userVersionMap[d.UserID]
			if !ok {
				userVersionMap[d.UserID] = adShowAndClick{
					adClick:          1,
					adClickAfterTrim: 1,
					clickExists:      map[string]struct{}{d.ADReqID: struct{}{}},
					adShow:           0,
					adShowAfterTrim:  0,
				}
				m[d.AppVersion] = userVersionMap
				continue
			}
			if s.clickExists == nil {
				s.clickExists = make(map[string]struct{})
			}
			s.adClick++
			if _, ok := s.clickExists[d.ADReqID]; !ok {
				s.adClickAfterTrim++
				s.clickExists[d.ADReqID] = struct{}{}
			}
			userVersionMap[d.UserID] = s
			m[d.AppVersion] = userVersionMap
		case common.AD_REQ_TYPE_SHOW:
			userVersionMap, ok := m[d.AppVersion]
			if !ok {
				m[d.AppVersion] = map[string]adShowAndClick{
					d.UserID: adShowAndClick{
						adClick:          0,
						adClickAfterTrim: 0,
						adShow:           1,
						adShowAfterTrim:  1,
						showExists:       map[string]struct{}{d.ADReqID: struct{}{}},
					},
				}
				continue
			}
			s, ok := userVersionMap[d.UserID]
			if !ok {
				userVersionMap[d.UserID] = adShowAndClick{
					adClick:          0,
					adClickAfterTrim: 0,
					adShow:           1,
					adShowAfterTrim:  1,
					showExists:       map[string]struct{}{d.ADReqID: struct{}{}},
				}

				m[d.AppVersion] = userVersionMap
				continue
			}
			if s.showExists == nil {
				s.showExists = make(map[string]struct{})
			}
			s.adShow++
			if _, ok := s.showExists[d.ADReqID]; !ok {
				s.adShowAfterTrim++
				s.showExists[d.ADReqID] = struct{}{}
			}
			userVersionMap[d.UserID] = s
			m[d.AppVersion] = userVersionMap
		}
	}

	columns := []string{"版本号", "用户code", "手机号", "注册时间", "用户状态", "点击次数", "去重后点击次数", "展示次数", "去重后展示次数", "点击率", "去重后点击率", "总金币", "剩余金币", "当日阅读金币", "当日红包雨", "当日红包新闻", "当日时段奖励", "当日总金币"}
	row := sheet.AddRow()
	for _, culumn := range columns {
		cell := row.AddCell()
		cell.SetString(culumn)
	}

	types := []int{
		common.TASK_TYPE_FIXED_BOUNDS, common.TASK_TYPE_TIMING_REWARD, common.TASK_TYPE_REWARD_NEWS, common.TASK_TYPE_HOURLY_REWARD,
	}
	timeUnix := t.Unix()
	for appVersion, userMap := range m {
		for userID, userData := range userMap {
			u := new(userCommon.User)
			if err := u.InitBasic(userID, userInfoC); err != nil {
				continue
			}
			row := sheet.AddRow()
			sts := []string{
				appVersion, u.Code, u.Mobile, time.Unix(u.Create, 0).Format("2006-01-02 15:04:05"),
			}
			for _, st := range sts {
				cell := row.AddCell()
				cell.SetString(st)
			}

			its := []int{
				u.Status,
				userData.adClick,
				userData.adClickAfterTrim,
				userData.adShow,
				userData.adShowAfterTrim,
			}

			for _, it := range its {
				cell := row.AddCell()
				cell.SetInt(it)
			}

			fts := []float64{
				float64(userData.adClick) / float64(userData.adShow),
				float64(userData.adClickAfterTrim) / float64(userData.adShowAfterTrim),
			}

			for _, ft := range fts {
				cell := row.AddCell()
				cell.SetFloatWithFormat(ft, "0.00%")
			}

			its = []int{
				u.IncomeTotal,
				u.IncomeRemaining,
			}

			for _, it := range its {
				cell := row.AddCell()
				cell.SetInt(it)
			}

			for _, t := range types {
				total, _ := userCommon.GetDateTotal(u.UserID, timeUnix, t, userIncomeDailyC)
				cell := row.AddCell()
				cell.SetInt(total)
			}

			total, _ := userCommon.GetDateIncomeTotal(u.UserID, timeUnix, userIncomeDailyC)
			cell := row.AddCell()
			cell.SetInt(total)
		}
	}
}
