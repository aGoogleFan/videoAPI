package userAdClick

import (
	"sync"
	"time"

	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/dbCommon"
)

func Main(t time.Time) {
	ms := dbCommon.MongoSessionPool.Clone()
	defer ms.Close()
	dateString := t.Format("2006-01-02")
	userInfoC := ms.DB("cr_app_101").C("user_basic")
	userIncomeDailyC := ms.DB("cr_app_101").C("user_income_by_date")
	xlsxFile := xlsx.NewFile()

	wg := new(sync.WaitGroup)

	userSheet, _ := xlsxFile.AddSheet("用户维度")
	wg.Add(1)
	go genSheetByDate(userSheet, t, userInfoC, userIncomeDailyC, wg)

	ipSheet, _ := xlsxFile.AddSheet("ip维度")
	wg.Add(1)
	go genIPSheetByDate(ipSheet, userInfoC, t, wg)

	deviceSheet, _ := xlsxFile.AddSheet("设备维度")
	wg.Add(1)
	go genDeviceSheetByDate(deviceSheet, userInfoC, t, wg)

	wg.Wait()
	xlsxFile.Save(dateString + "_user_analyze_8.xlsx")
}
