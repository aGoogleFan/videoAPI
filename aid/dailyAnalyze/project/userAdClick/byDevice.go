package userAdClick

import (
	"strings"
	"sync"
	"time"

	"github.com/globalsign/mgo"

	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/datas"
	"vlion/yxw/common"
)

func genDeviceSheetByDate(sheet *xlsx.Sheet, userInfoC *mgo.Collection, t time.Time, wg *sync.WaitGroup) {
	defer wg.Done()
	m := make(map[string]map[string]adShowAndClick) // map[appVersion]map[device]adShowAndClick
	adInfos := datas.GetAdInfoByDate(t.Unix())
	for _, d := range adInfos {
		if d.ADID != "8" {
			continue
		}
		switch d.ADReqType {
		case common.AD_REQ_TYPE_CLICK:
			deviceVersionMap, ok := m[d.AppVersion]
			if !ok {
				m[d.AppVersion] = map[string]adShowAndClick{
					d.AndroidID: adShowAndClick{
						adClick:          1,
						adClickAfterTrim: 1,
						clickExists:      map[string]struct{}{d.ADReqID: struct{}{}},
						adShow:           0,
						adShowAfterTrim:  0,
					},
				}
				continue
			}
			s, ok := deviceVersionMap[d.AndroidID]
			if !ok {
				deviceVersionMap[d.AndroidID] = adShowAndClick{
					adClick:          1,
					adClickAfterTrim: 1,
					clickExists:      map[string]struct{}{d.ADReqID: struct{}{}},
					adShow:           0,
					adShowAfterTrim:  0,
				}
				m[d.AppVersion] = deviceVersionMap
				continue
			}
			if s.clickExists == nil {
				s.clickExists = make(map[string]struct{})
			}
			s.adClick++
			if _, ok := s.clickExists[d.ADReqID]; !ok {
				s.adClickAfterTrim++
				s.clickExists[d.ADReqID] = struct{}{}
			}
			deviceVersionMap[d.AndroidID] = s
			m[d.AppVersion] = deviceVersionMap
		case common.AD_REQ_TYPE_SHOW:
			deviceVersionMap, ok := m[d.AppVersion]
			if !ok {
				m[d.AppVersion] = map[string]adShowAndClick{
					d.AndroidID: adShowAndClick{
						adClick:          0,
						adClickAfterTrim: 0,
						adShow:           1,
						adShowAfterTrim:  1,
						showExists:       map[string]struct{}{d.ADReqID: struct{}{}},
					},
				}
				continue
			}
			s, ok := deviceVersionMap[d.AndroidID]
			if !ok {
				deviceVersionMap[d.AndroidID] = adShowAndClick{
					adClick:          0,
					adClickAfterTrim: 0,
					adShow:           1,
					adShowAfterTrim:  1,
					showExists:       map[string]struct{}{d.ADReqID: struct{}{}},
				}

				m[d.AppVersion] = deviceVersionMap
				continue
			}
			if s.showExists == nil {
				s.showExists = make(map[string]struct{})
			}
			s.adShow++
			if _, ok := s.showExists[d.ADReqID]; !ok {
				s.adShowAfterTrim++
				s.showExists[d.ADReqID] = struct{}{}
			}
			deviceVersionMap[d.AndroidID] = s
			m[d.AppVersion] = deviceVersionMap
		}
	}

	columns := []string{"版本号", "android_id", "点击次数", "去重后点击次数", "展示次数", "去重后展示次数", "点击率", "去重后点击率", "登录过的用户数", "登录过的用户名单"}
	row := sheet.AddRow()
	for _, culumn := range columns {
		cell := row.AddCell()
		cell.SetString(culumn)
	}

	for appVersion, ipMap := range m {
		for androidID, data := range ipMap {
			row := sheet.AddRow()
			sts := []string{
				appVersion,
				androidID,
			}
			for _, st := range sts {
				cell := row.AddCell()
				cell.SetString(st)
			}

			its := []int{
				data.adClick,
				data.adClickAfterTrim,
				data.adShow,
				data.adShowAfterTrim,
			}

			for _, it := range its {
				cell := row.AddCell()
				cell.SetInt(it)
			}

			fts := []float64{
				float64(data.adClick) / float64(data.adShow),
				float64(data.adClickAfterTrim) / float64(data.adShowAfterTrim),
			}

			for _, ft := range fts {
				cell := row.AddCell()
				cell.SetFloatWithFormat(ft, "0.00%")
			}

			userExistsMap := make(map[string]struct{})
			var total int
			var userDatas []string
			for _, d := range adInfos {
				if d.ADID != "8" {
					continue
				}
				if d.AppVersion != appVersion {
					continue
				}
				if d.AndroidID != androidID {
					continue
				}
				if _, ok := userExistsMap[d.UserID]; ok {
					continue
				}
				userExistsMap[d.UserID] = struct{}{}
				total++
				var ub common.UserBasic
				userInfoC.Find(bson.M{"id": d.UserID}).One(&ub)
				userDatas = append(userDatas, ub.Code)
			}
			cell := row.AddCell()
			cell.SetInt(total)
			cell = row.AddCell()
			cell.SetString(strings.Join(userDatas, "|"))

		}
	}
}
