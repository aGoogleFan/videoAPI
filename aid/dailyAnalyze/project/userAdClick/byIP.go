package userAdClick

import (
	"strings"
	"sync"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/datas"
	"vlion/yxw/common"
)

func genIPSheetByDate(sheet *xlsx.Sheet, userInfoC *mgo.Collection, t time.Time, wg *sync.WaitGroup) {
	defer wg.Done()
	m := make(map[string]map[string]adShowAndClick) // map[appVersion]map[ip]adShowAndClick
	adInfos := datas.GetAdInfoByDate(t.Unix())
	for _, d := range adInfos {
		if d.ADID != "8" {
			continue
		}
		switch d.ADReqType {
		case common.AD_REQ_TYPE_CLICK:
			ipVersionMap, ok := m[d.AppVersion]
			if !ok {
				m[d.AppVersion] = map[string]adShowAndClick{
					d.IP: adShowAndClick{
						adClick:          1,
						adClickAfterTrim: 1,
						clickExists:      map[string]struct{}{d.ADReqID: struct{}{}},
						adShow:           0,
						adShowAfterTrim:  0,
					},
				}
				continue
			}
			s, ok := ipVersionMap[d.IP]
			if !ok {
				ipVersionMap[d.IP] = adShowAndClick{
					adClick:          1,
					adClickAfterTrim: 1,
					clickExists:      map[string]struct{}{d.ADReqID: struct{}{}},
					adShow:           0,
					adShowAfterTrim:  0,
				}
				m[d.AppVersion] = ipVersionMap
				continue
			}
			if s.clickExists == nil {
				s.clickExists = make(map[string]struct{})
			}
			s.adClick++
			if _, ok := s.clickExists[d.ADReqID]; !ok {
				s.adClickAfterTrim++
				s.clickExists[d.ADReqID] = struct{}{}
			}
			ipVersionMap[d.IP] = s
			m[d.AppVersion] = ipVersionMap
		case common.AD_REQ_TYPE_SHOW:
			ipVersionMap, ok := m[d.AppVersion]
			if !ok {
				m[d.AppVersion] = map[string]adShowAndClick{
					d.IP: adShowAndClick{
						adClick:          0,
						adClickAfterTrim: 0,
						adShow:           1,
						adShowAfterTrim:  1,
						showExists:       map[string]struct{}{d.ADReqID: struct{}{}},
					},
				}
				continue
			}
			s, ok := ipVersionMap[d.IP]
			if !ok {
				ipVersionMap[d.IP] = adShowAndClick{
					adClick:          0,
					adClickAfterTrim: 0,
					adShow:           1,
					adShowAfterTrim:  1,
					showExists:       map[string]struct{}{d.ADReqID: struct{}{}},
				}

				m[d.AppVersion] = ipVersionMap
				continue
			}
			if s.showExists == nil {
				s.showExists = make(map[string]struct{})
			}
			s.adShow++
			if _, ok := s.showExists[d.ADReqID]; !ok {
				s.adShowAfterTrim++
				s.showExists[d.ADReqID] = struct{}{}
			}
			ipVersionMap[d.IP] = s
			m[d.AppVersion] = ipVersionMap
		}
	}

	columns := []string{"版本号", "ip", "点击次数", "去重后点击次数", "展示次数", "去重后展示次数", "点击率", "去重后点击率", "登录过的用户数", "登录过的用户名单"}
	row := sheet.AddRow()
	for _, culumn := range columns {
		cell := row.AddCell()
		cell.SetString(culumn)
	}

	for appVersion, ipMap := range m {
		for ip, userData := range ipMap {
			row := sheet.AddRow()
			sts := []string{
				appVersion,
				ip,
			}
			for _, st := range sts {
				cell := row.AddCell()
				cell.SetString(st)
			}

			its := []int{
				userData.adClick,
				userData.adClickAfterTrim,
				userData.adShow,
				userData.adShowAfterTrim,
			}

			for _, it := range its {
				cell := row.AddCell()
				cell.SetInt(it)
			}

			fts := []float64{
				float64(userData.adClick) / float64(userData.adShow),
				float64(userData.adClickAfterTrim) / float64(userData.adShowAfterTrim),
			}

			for _, ft := range fts {
				cell := row.AddCell()
				cell.SetFloatWithFormat(ft, "0.00%")
			}

			userExistsMap := make(map[string]struct{})
			var total int
			var userDatas []string
			for _, d := range adInfos {
				if d.ADID != "8" {
					continue
				}
				if d.AppVersion != appVersion {
					continue
				}
				if d.IP != ip {
					continue
				}
				if _, ok := userExistsMap[d.UserID]; ok {
					continue
				}
				userExistsMap[d.UserID] = struct{}{}
				total++
				var ub common.UserBasic
				userInfoC.Find(bson.M{"id": d.UserID}).One(&ub)
				userDatas = append(userDatas, ub.Code)
			}
			cell := row.AddCell()
			cell.SetInt(total)
			cell = row.AddCell()
			cell.SetString(strings.Join(userDatas, "|"))
		}
	}
}
