package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/datas"
	"vlion/yxw/aid/dailyAnalyze/dbCommon"
	"vlion/yxw/aid/dailyAnalyze/project/withdrawList"
	"vlion/yxw/aid/dailyAnalyze/userCommon"
	"vlion/yxw/common"
	"vlion/yxw/db"
)

func main() {
	xlsxFile := xlsx.NewFile()
	for i := 39; i >= 1; i-- {
		t := time.Now().Add(time.Hour * -24 * time.Duration(i))
		ms := dbCommon.MongoSessionPool.Clone()
		defer ms.Close()

		userOrderC := ms.DB("cr_app_101").C("user_order")
		userBasicC := ms.DB("cr_app_101").C("user_basic")
		userIncomeStructC := ms.DB("cr_app_all").C("cr_app_back_conf")
		userChannelC := ms.DB("cr_app_101").C("user_channel")
		// userIncomeDailyC := ms.DB("cr_app_101").C("user_income_by_date")
		userIncomeTotalC := ms.DB("cr_app_101").C("user_income_total")
		dateString := t.Format("2006-01-02")
		fmt.Println(dateString)
		sheet, _ := xlsxFile.AddSheet(dateString)
		withdrawList.GetWithdrawDetail(sheet, userOrderC, userBasicC, userIncomeStructC, userChannelC, userIncomeTotalC, t)
	}
	xlsxFile.Save("withdrawList.xlsx")
}

func w() {
	xlsxFile := xlsx.NewFile()

	t := time.Now().Add(time.Hour * -24)
	datas.InitUserNewsDateRead(t)
	datas.InitAdInfo(t)
	ms := dbCommon.MongoSessionPool.Clone()
	defer ms.Close()
	dateString := t.Format("2006-01-02")
	userInfoC := ms.DB("cr_app_101").C("user_basic")

	wg := new(sync.WaitGroup)
	//		userSheet, _ := xlsxFile.AddSheet("用户维度")
	//		wg.Add(1)
	//		go genSheetByDate(userSheet, t, userInfoC, userIncomeDailyC, wg)

	ipSheet, _ := xlsxFile.AddSheet("ip维度_" + dateString)
	wg.Add(1)
	go getDailyUserIPe(ipSheet, userInfoC, t, wg)

	//		deviceSheet, _ := xlsxFile.AddSheet("设备维度")
	//		wg.Add(1)
	//		go genDeviceSheetByDate(deviceSheet, t, wg)

	wg.Wait()

	xlsxFile.Save("user_analyze_8_weekly.xlsx")
}

type adShowAndClick struct {
	adClick          int
	adClickAfterTrim int
	clickExists      map[string]struct{} // map[req_id]struct
	adShow           int
	adShowAfterTrim  int
	showExists       map[string]struct{} // map[req_id]struct
}

func getDailyUserIPe(sheet *xlsx.Sheet, userInfoC *mgo.Collection, t time.Time, wg *sync.WaitGroup) {
	m := make(map[string]map[string]adShowAndClick) // map[appVersion]map[ip]adShowAndClick
	adInfos := datas.GetAdInfoByDate(t.Unix())
	for _, d := range adInfos {
		if d.ADID != "8" {
			continue
		}
		switch d.ADReqType {
		case common.AD_REQ_TYPE_CLICK:
			ipVersionMap, ok := m[d.AppVersion]
			if !ok {
				m[d.AppVersion] = map[string]adShowAndClick{
					d.IP: adShowAndClick{
						adClick:          1,
						adClickAfterTrim: 1,
						clickExists:      map[string]struct{}{d.ADReqID: struct{}{}},
						adShow:           0,
						adShowAfterTrim:  0,
					},
				}
				continue
			}
			s, ok := ipVersionMap[d.IP]
			if !ok {
				ipVersionMap[d.IP] = adShowAndClick{
					adClick:          1,
					adClickAfterTrim: 1,
					clickExists:      map[string]struct{}{d.ADReqID: struct{}{}},
					adShow:           0,
					adShowAfterTrim:  0,
				}
				m[d.AppVersion] = ipVersionMap
				continue
			}
			if s.clickExists == nil {
				s.clickExists = make(map[string]struct{})
			}
			s.adClick++
			if _, ok := s.clickExists[d.ADReqID]; !ok {
				s.adClickAfterTrim++
				s.clickExists[d.ADReqID] = struct{}{}
			}
			ipVersionMap[d.IP] = s
			m[d.AppVersion] = ipVersionMap
		case common.AD_REQ_TYPE_SHOW:
			ipVersionMap, ok := m[d.AppVersion]
			if !ok {
				m[d.AppVersion] = map[string]adShowAndClick{
					d.IP: adShowAndClick{
						adClick:          0,
						adClickAfterTrim: 0,
						adShow:           1,
						adShowAfterTrim:  1,
						showExists:       map[string]struct{}{d.ADReqID: struct{}{}},
					},
				}
				continue
			}
			s, ok := ipVersionMap[d.IP]
			if !ok {
				ipVersionMap[d.IP] = adShowAndClick{
					adClick:          0,
					adClickAfterTrim: 0,
					adShow:           1,
					adShowAfterTrim:  1,
					showExists:       map[string]struct{}{d.ADReqID: struct{}{}},
				}

				m[d.AppVersion] = ipVersionMap
				continue
			}
			if s.showExists == nil {
				s.showExists = make(map[string]struct{})
			}
			s.adShow++
			if _, ok := s.showExists[d.ADReqID]; !ok {
				s.adShowAfterTrim++
				s.showExists[d.ADReqID] = struct{}{}
			}
			ipVersionMap[d.IP] = s
			m[d.AppVersion] = ipVersionMap
		}
	}

	// columns := []string{"版本号", "ip", "点击次数", "去重后点击次数", "展示次数", "去重后展示次数", "点击率", "去重后点击率", "登录过的用户数", "登录过的用户名单"}
	columns := []string{"用户收徒码", "预计封禁等级", "版本号", "所属ip", "ip点击数", "ip点击次数", "ip去重后点击次数", "ip展示次数", "ip去重后展示次数", "ip点击率", "ip去重后点击率"}

	row := sheet.AddRow()
	for _, culumn := range columns {
		cell := row.AddCell()
		cell.SetString(culumn)
	}

	for appVersion, ipMap := range m {
		for ip, userData := range ipMap {
			userExistsMap := make(map[string]struct{})
			var total int
			var userDatas []string
			for _, d := range adInfos {
				if d.ADID != "8" {
					continue
				}
				if d.AppVersion != appVersion {
					continue
				}
				if d.IP != ip {
					continue
				}
				if _, ok := userExistsMap[d.UserID]; ok {
					continue
				}
				userExistsMap[d.UserID] = struct{}{}
				total++
				var ub common.UserBasic
				userInfoC.Find(bson.M{"id": d.UserID}).One(&ub)
				userDatas = append(userDatas, ub.Code)
			}
			if total <= 10 {
				continue
			}
			for _, user := range userDatas {
				row := sheet.AddRow()
				cell := row.AddCell()
				cell.SetString(user)
				cell = row.AddCell()
				cell.SetInt(-1)

				sts := []string{
					appVersion,
					ip,
				}
				for _, st := range sts {
					cell := row.AddCell()
					cell.SetString(st)
				}

				its := []int{
					userData.adClick,
					userData.adClickAfterTrim,
					userData.adShow,
					userData.adShowAfterTrim,
				}

				for _, it := range its {
					cell := row.AddCell()
					cell.SetInt(it)
				}

				fts := []float64{
					float64(userData.adClick) / float64(userData.adShow),
					float64(userData.adClickAfterTrim) / float64(userData.adShowAfterTrim),
				}

				for _, ft := range fts {
					cell := row.AddCell()
					cell.SetFloatWithFormat(ft, "0.00%")
				}
			}
		}
	}
}

func queryApprentice() {
	appID := "101"
	userID := "24f415c1-b896-11e8-8130-00163e10de7e"
	db.InitMongo()
	uos, err := db.GetUserIncomeHistoryByDetail(appID, userID, common.NO_LIMIT, common.NO_LIMIT, common.INCOME_TYPE_INCOME, common.TASK_TYPE_USER_APPRENTICE_REWARD, "")
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to get user apprentice reward")
		return
	}
	xlsxFile := xlsx.NewFile()
	sheet, _ := xlsxFile.AddSheet("徒弟宝箱列表")
	row := sheet.AddRow()
	columns := []string{
		"徒弟code",
		"注册时间",
		"开宝箱时间",
		"宝箱金额",
		"最后活跃时间",
	}
	for _, column := range columns {
		cell := row.AddCell()
		cell.SetString(column)
	}

	ms := dbCommon.MongoSessionPool.Clone()
	defer ms.Close()

	userBasicC := ms.DB("cr_app_101").C("user_basic")
	userIncomeStructC := ms.DB("cr_app_all").C("cr_app_back_conf")
	userChannelC := ms.DB("cr_app_101").C("user_channel")
	// userIncomeDailyC := ms.DB("cr_app_101").C("user_income_by_date")
	userIncomeTotalC := ms.DB("cr_app_101").C("user_income_total")

	var uit userCommon.UserIncomeType
	uitErr := userIncomeStructC.Find(bson.M{"id": "101"}).One(&uit)
	if uitErr != nil {
		log.Fields{
			"err": uitErr.Error(),
		}.Error("fail to get user income struct c")
	}
	if uitErr == nil {
		for _, dt := range uit.UserIncomeTaskType {
			cell := row.AddCell()
			cell.Value = dt.Name
		}
	}

	for _, uo := range uos {
		u := new(userCommon.User)
		if err := u.InitBasic(uo.From, userBasicC); err != nil {
			log.Fields{
				"err": err.Error(),
			}.Error("f")
			continue
		}
		row := sheet.AddRow()
		cell := row.AddCell()
		cell.SetString(u.Code)
		cell = row.AddCell()
		cell.SetString(time.Unix(u.Create, 0).Format("2006-01-02 15:04:05"))
		cell = row.AddCell()
		cell.SetString(time.Unix(uo.Timestamp, 0).Format("2006-01-02 15:04:05"))
		cell = row.AddCell()
		cell.SetInt(uo.Count)
		u.GetLastActive(userChannelC)
		cell = row.AddCell()
		cell.SetString(time.Unix(u.LastActive, 0).Format("2006-01-02"))
		if uitErr == nil {
			u.GetIncomeStruct(uit.UserIncomeTaskType, userIncomeTotalC)
			for _, dt := range uit.UserIncomeTaskType {
				cell := row.AddCell()
				cell.SetInt(u.IncomeStruct[dt.Name])
			}
		}
	}
	xlsxFile.Save("t.xlsx")
}

func getContinousNumber() {
	xlsxFile := xlsx.NewFile()
	ms := dbCommon.MongoSessionPool.Clone()
	defer ms.Close()

	userBasicC := ms.DB("cr_app_101").C("user_basic")
	userIncomeStructC := ms.DB("cr_app_all").C("cr_app_back_conf")
	userChannelC := ms.DB("cr_app_101").C("user_channel")
	userIncomeDailyC := ms.DB("cr_app_101").C("user_income_by_date")
	userIncomeTotalC := ms.DB("cr_app_101").C("user_income_total")
	now := time.Now()
	wg := new(sync.WaitGroup)

	sheet, err := xlsxFile.AddSheet("..")
	if err != nil {
		panic(err)
	}
	wg.Add(1)
	go genSheet(sheet, userBasicC, userIncomeDailyC, userIncomeTotalC, userIncomeStructC, userChannelC, now, wg)

	wg.Wait()
	xlsxFile.Save(fmt.Sprintf("continous.xlsx"))
}
