package main

import (
	"bufio"
	"fmt"
	"os"
	"time"

	"vlion/yxw/aid/dailyAnalyze/datas"
)

//func t() {
//	xlsxFile := xlsx.NewFile()
//	sheet, _ := xlsxFile.AddSheet("uv")
//	row := sheet.AddRow()
//	columns := []string{"日期", "subchannel", "uv"}
//	for _, column := range columns {
//		cell := row.AddCell()
//		cell.SetString(column)
//	}
//	for i := 1; i <= 7; i++ {
//		t := time.Now().Add(time.Hour * -24 * time.Duration(i))
//		dateString := t.Format("2006-01-02")
//		t1, t2 := uvBySubmedia(t)
//		row := sheet.AddRow()
//		cell := row.AddCell()
//		cell.SetString(dateString)
//		cell = row.AddCell()
//		cell.SetString("46")
//		cell = row.AddCell()
//		cell.SetInt(t1)

//		row = sheet.AddRow()
//		cell = row.AddCell()
//		cell.SetString(dateString)
//		cell = row.AddCell()
//		cell.SetString("155")
//		cell = row.AddCell()
//		cell.SetInt(t2)
//	}

//	xlsxFile.Save("uv.xlsx")
//}

func uvBySubmedia(t time.Time) (int, int) {
	userExistsMap := make(map[string]struct{})
	deviceExistsMap := make(map[string]struct{})
	var subchannel46 int
	var subchannel155 int
	dateString := t.Format("2006-01-02")
	fmt.Println(dateString)
	for i := 23; i >= 0; i-- {
		fileName := fmt.Sprintf("/data/log/yxw/monitorLog_vlionserver-217_%s_%02d.log", dateString, i)
		f, err := os.Open(fileName)
		if err != nil {
			continue
		}
		defer f.Close()
		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			ar := datas.ResolveFileLine(scanner.Text())
			if ar == nil {
				continue
			}
			if _, ok := userExistsMap[ar.UserID]; ok {
				continue
			}
			if _, ok := deviceExistsMap[ar.AndroidID]; ok {
				continue
			}
			userExistsMap[ar.UserID] = struct{}{}
			deviceExistsMap[ar.AndroidID] = struct{}{}
			subMedia := getUserSubmedia(ar.UserID, ar.UserMobile, ar.AndroidID)
			switch subMedia {
			case "46":
				subchannel46++
			case "155":
				subchannel155++
			}
		}
	}
	return subchannel46, subchannel155
}
