package main

import (
	"fmt"
	"sort"
	"strconv"
	"sync"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/userCommon"
	"vlion/yxw/common"
)

func genSheet(sheet *xlsx.Sheet, userBasicC, userIncomeDailyC, userIncomeTotalC, userIncomeStructC, userChannelC *mgo.Collection, now time.Time, wg *sync.WaitGroup) {
	defer wg.Done()
	sheetRow := sheet.AddRow()
	columns := []string{
		"手机号",
		"用户code",
		"用户状态码",
		"注册时间",
		"总金币",
		"总阅读金币",
		"总广告金币",
		"广告金币/阅读金币",
		"最后活跃时间",
	}
	for _, v := range columns {
		cell := sheetRow.AddCell()
		cell.Value = v
	}
	var uit userCommon.UserIncomeType
	uitErr := userIncomeStructC.Find(bson.M{"id": "101"}).One(&uit)
	if uitErr != nil {
		log.Fields{
			"err": uitErr.Error(),
		}.Error("fail to get user income struct c")
	}
	if uitErr == nil {
		for _, dt := range uit.UserIncomeTaskType {
			cell := sheetRow.AddCell()
			cell.Value = dt.Name
		}
	}
	continousNumberMap := make(map[int]ContinousNumber)
	iter := userBasicC.Find(bson.M{"create": bson.M{"$gte": common.GetDateStartUnix(now.Add(time.Hour * 24 * -30).Unix())}}).Iter()
	var ub common.UserBasic
	userMap := make(map[string]common.UserBasic) // map[mobile]userBasic
	for iter.Next(&ub) {
		mobile := ub.Info.Mobile
		if mobile == "" {
			continue
		}
		if len(mobile) != 11 {
			log.Fields{
				"mobile": mobile,
			}.Error("invalid mobile")
			continue
		}
		key, err := strconv.Atoi(mobile[:8])
		if err != nil {
			log.Fields{
				"mobile": mobile,
				"prefix": mobile[:8],
			}.Error("invalid mobile prefix")
			continue
		}
		value, err := strconv.Atoi(mobile[8:])
		if err != nil {
			log.Fields{
				"mobile": mobile,
				"prefix": mobile[8:],
			}.Error("invalid mobile suffix")
			continue
		}
		userBasicCopy := ub
		userMap[mobile] = userBasicCopy
		v, ok := continousNumberMap[key]
		if !ok {
			continousNumberMap[key] = ContinousNumber{
				numbers: []int{value},
				count:   1,
			}
			continue
		}
		v.numbers = append(v.numbers, value)
		v.count += 1
		continousNumberMap[key] = v
	}
	for key, v := range continousNumberMap {
		if v.count == 1 {
			continue
		}
		numbers := v.numbers
		sort.Ints(numbers)
		last := 0
		continous := false
		for k, number := range numbers {
			if k == 0 {
				last = number
				continue
			}
			if number == last+1 {
				if !continous {
					u := new(userCommon.User)
					mobile := fmt.Sprintf("%d%03d", key, last)
					u.InitUserByBasic(userMap[mobile], userBasicC)
					u.InitAdvance(userIncomeDailyC, userIncomeTotalC, now)
					u.GetLastActive(userChannelC)
					cells := []string{
						u.Mobile,
						u.Code,
						strconv.Itoa(u.Status),
						time.Unix(u.Create, 0).Local().Format("2006-01-02 15:04:05"),
						strconv.Itoa(u.IncomeTotal),
						strconv.Itoa(u.ReadTotal),
						strconv.Itoa(u.AdClickTotal),
						strconv.FormatFloat(float64(u.AdClickTotal)/float64(u.ReadTotal), 'f', 3, 64),
						time.Unix(u.LastActive, 0).Local().Format("2006-01-02"),
					}
					sheetDetailRow := sheet.AddRow()
					for _, detail := range cells {
						cell := sheetDetailRow.AddCell()
						cell.Value = detail
					}
					if uitErr == nil {
						u.GetIncomeStruct(uit.UserIncomeTaskType, userIncomeTotalC)
						for _, dt := range uit.UserIncomeTaskType {
							cell := sheetDetailRow.AddCell()
							cell.Value = strconv.Itoa(u.IncomeStruct[dt.Name])
						}
					}
				}
				u := new(userCommon.User)
				mobile := fmt.Sprintf("%d%03d", key, number)
				u.InitUserByBasic(userMap[mobile], userBasicC)
				u.InitAdvance(userIncomeDailyC, userIncomeTotalC, now)
				u.GetLastActive(userChannelC)
				cells := []string{
					u.Mobile,
					u.Code,
					strconv.Itoa(u.Status),
					time.Unix(u.Create, 0).Local().Format("2006-01-02 15:04:05"),
					strconv.Itoa(u.IncomeTotal),
					strconv.Itoa(u.ReadTotal),
					strconv.Itoa(u.AdClickTotal),
					strconv.FormatFloat(float64(u.AdClickTotal)/float64(u.ReadTotal), 'f', 3, 64),
					time.Unix(u.LastActive, 0).Local().Format("2006-01-02"),
				}
				sheetDetailRow := sheet.AddRow()
				for _, detail := range cells {
					cell := sheetDetailRow.AddCell()
					cell.Value = detail
				}
				if uitErr == nil {
					u.GetIncomeStruct(uit.UserIncomeTaskType, userIncomeTotalC)
					for _, dt := range uit.UserIncomeTaskType {
						cell := sheetDetailRow.AddCell()
						cell.Value = strconv.Itoa(u.IncomeStruct[dt.Name])
					}
				}
				continous = true
				last = number
				continue
			}
			continous = false
			last = number
		}
	}
}

type ContinousNumber struct {
	numbers []int
	count   int
}
