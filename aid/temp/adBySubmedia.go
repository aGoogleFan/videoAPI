package main

import (
	"fmt"
	"time"

	"github.com/tealeg/xlsx"

	"vlion/yxw/aid/dailyAnalyze/datas"
	"vlion/yxw/common"
)

func adConf(t time.Time) {
	datas.InitAdInfo(t)
	xlsxFile := xlsx.NewFile()
	dateString := t.Format("2006-01-02")
	sheet, _ := xlsxFile.AddSheet(dateString)
	genSheetByDate(sheet, t)
	xlsxFile.Save(fmt.Sprintf("ad_analyze_%s.xlsx", dateString))
	datas.ClearAdInfo(t)
}

type adData struct {
	req            int
	reqAfterTrim   int
	reqExists      map[string]struct{}
	ret            int
	retAfterTrim   int
	retExists      map[string]struct{}
	show           int
	showAfterTrim  int
	showExists     map[string]struct{}
	click          int
	clickAfterTrim int
	clickExists    map[string]struct{}
}

func genSheetByDate(sheet *xlsx.Sheet, t time.Time) {
	columns := []string{"app版本", "广告位", "广告源", "submedia", "请求数", "去重后请求数", "返回数", "去重后返回数", "展示数", "去重后展示数", "点击数", "去重后点击数", "展示率(展示/返回)", "去重后展示率", "点击率(点击/展示)", "去重后点击率"}
	row := sheet.AddRow()
	for _, column := range columns {
		cell := row.AddCell()
		cell.Value = column
	}
	ds := getDataFromDataSource(t)
	for appVersion, appVersionData := range ds {
		for adID, adIDData := range appVersionData {
			for adRes, adWithSC := range adIDData {
				for subMedia, ad := range adWithSC {
					row := sheet.AddRow()
					values := []string{appVersion, adID, adRes, subMedia}
					for _, v := range values {
						cell := row.AddCell()
						cell.SetString(v)
					}
					cell := row.AddCell()
					cell.SetInt(ad.req)
					cell = row.AddCell()
					cell.SetInt(ad.reqAfterTrim)
					cell = row.AddCell()
					cell.SetInt(ad.ret)
					cell = row.AddCell()
					cell.SetInt(ad.retAfterTrim)
					cell = row.AddCell()
					cell.SetInt(ad.show)
					cell = row.AddCell()
					cell.SetInt(ad.showAfterTrim)
					cell = row.AddCell()
					cell.SetInt(ad.click)
					cell = row.AddCell()
					cell.SetInt(ad.clickAfterTrim)
					cell = row.AddCell()
					cell.SetFloatWithFormat(float64(ad.show)/float64(ad.ret), "0.00%")
					cell = row.AddCell()
					cell.SetFloatWithFormat(float64(ad.showAfterTrim)/float64(ad.retAfterTrim), "0.00%")
					cell = row.AddCell()
					cell.SetFloatWithFormat(float64(ad.click)/float64(ad.show), "0.00%")
					cell = row.AddCell()
					cell.SetFloatWithFormat(float64(ad.clickAfterTrim)/float64(ad.showAfterTrim), "0.00%")
				}
			}
		}
	}

}

func getDataFromDataSource(t time.Time) map[string]map[string]map[string]map[string]adData { // map[appVersion]map[adID]map[source]map[submedia]adData
	sourceData := datas.GetAdInfoByDate(t.Unix())
	m := make(map[string]map[string]map[string]map[string]adData) // map[appVersion]map[adID]adData
	for _, d := range sourceData {
		subMedia := getUserSubmedia(d.UserID, "", d.AndroidID)
		appVersionData, ok := m[d.AppVersion]
		if !ok {
			ad := adData{}
			switch d.ADReqType {
			case common.AD_REQ_TYPE_REQ:
				ad.req = 1
				ad.reqAfterTrim = 1
				ad.reqExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_RET:
				ad.ret = 1
				ad.retAfterTrim = 1
				ad.retExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_SHOW:
				ad.show = 1
				ad.showAfterTrim = 1
				ad.showExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_CLICK:
				ad.click = 1
				ad.clickAfterTrim = 1
				ad.clickExists = map[string]struct{}{d.ADReqID: struct{}{}}
			}
			m[d.AppVersion] = map[string]map[string]map[string]adData{d.ADID: map[string]map[string]adData{d.ADRes: map[string]adData{subMedia: ad}}}
			continue
		}

		adIDData, ok := appVersionData[d.ADID]
		if !ok {
			ad := adData{}
			switch d.ADReqType {
			case common.AD_REQ_TYPE_REQ:
				ad.req = 1
				ad.reqAfterTrim = 1
				ad.reqExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_RET:
				ad.ret = 1
				ad.retAfterTrim = 1
				ad.retExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_SHOW:
				ad.show = 1
				ad.showAfterTrim = 1
				ad.showExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_CLICK:
				ad.click = 1
				ad.clickAfterTrim = 1
				ad.clickExists = map[string]struct{}{d.ADReqID: struct{}{}}
			}
			appVersionData[d.ADID] = map[string]map[string]adData{d.ADRes: map[string]adData{subMedia: ad}}
			m[d.AppVersion] = appVersionData
			continue
		}

		adReqDataWithSubmedia, ok := adIDData[d.ADRes]
		if !ok {
			ad := adData{}
			switch d.ADReqType {
			case common.AD_REQ_TYPE_REQ:
				ad.req = 1
				ad.reqAfterTrim = 1
				ad.reqExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_RET:
				ad.ret = 1
				ad.retAfterTrim = 1
				ad.retExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_SHOW:
				ad.show = 1
				ad.showAfterTrim = 1
				ad.showExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_CLICK:
				ad.click = 1
				ad.clickAfterTrim = 1
				ad.clickExists = map[string]struct{}{d.ADReqID: struct{}{}}
			}
			adIDData[d.ADRes] = map[string]adData{subMedia: ad}
			appVersionData[d.ADID] = adIDData
			m[d.AppVersion] = appVersionData
			continue
		}

		adReqData, ok := adReqDataWithSubmedia[subMedia]
		if !ok {
			ad := adData{}
			switch d.ADReqType {
			case common.AD_REQ_TYPE_REQ:
				ad.req = 1
				ad.reqAfterTrim = 1
				ad.reqExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_RET:
				ad.ret = 1
				ad.retAfterTrim = 1
				ad.retExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_SHOW:
				ad.show = 1
				ad.showAfterTrim = 1
				ad.showExists = map[string]struct{}{d.ADReqID: struct{}{}}
			case common.AD_REQ_TYPE_CLICK:
				ad.click = 1
				ad.clickAfterTrim = 1
				ad.clickExists = map[string]struct{}{d.ADReqID: struct{}{}}
			}
			adReqDataWithSubmedia[subMedia] = ad
			adIDData[d.ADRes] = adReqDataWithSubmedia
			appVersionData[d.ADID] = adIDData
			m[d.AppVersion] = appVersionData
			continue
		}

		switch d.ADReqType {
		case common.AD_REQ_TYPE_REQ:
			adReqData.req++
			if adReqData.reqExists == nil {
				adReqData.reqExists = make(map[string]struct{})
			}
			if _, ok := adReqData.reqExists[d.ADReqID]; !ok {
				adReqData.reqAfterTrim++
				adReqData.reqExists[d.ADReqID] = struct{}{}
			}
		case common.AD_REQ_TYPE_RET:
			adReqData.ret++
			if adReqData.retExists == nil {
				adReqData.retExists = make(map[string]struct{})
			}
			if _, ok := adReqData.retExists[d.ADReqID]; !ok {
				adReqData.retAfterTrim++
				adReqData.retExists[d.ADReqID] = struct{}{}
			}
		case common.AD_REQ_TYPE_SHOW:
			adReqData.show++
			if adReqData.showExists == nil {
				adReqData.showExists = make(map[string]struct{})
			}
			if _, ok := adReqData.showExists[d.ADReqID]; !ok {
				adReqData.showAfterTrim++
				adReqData.showExists[d.ADReqID] = struct{}{}
			}
		case common.AD_REQ_TYPE_CLICK:
			adReqData.click++
			if adReqData.clickExists == nil {
				adReqData.clickExists = make(map[string]struct{})
			}
			if _, ok := adReqData.clickExists[d.ADReqID]; !ok {
				adReqData.clickAfterTrim++
				adReqData.clickExists[d.ADReqID] = struct{}{}
			}
		}
		adReqDataWithSubmedia[subMedia] = adReqData
		adIDData[d.ADRes] = adReqDataWithSubmedia
		appVersionData[d.ADID] = adIDData
		m[d.AppVersion] = appVersionData
	}
	return m
}

func getUserSubmedia(userID, mobile, androidID string) string {

	if mobile == "18721903920" || mobile == "15901829157" || mobile == "18701476121" || mobile == "13163291237" || mobile == "18601740110" {
		return "155"
	}

	var judge string
	if userID != "" {
		judge = userID
	} else {
		judge = androidID
	}
	if judge == "" {
		return "46"
	}
	temp := common.StringToMD5(judge)
	tempLen := len(temp)
	if tempLen == 0 {
		return "46"
	}
	switch temp[tempLen-1] % 2 {
	case 0:
		return "46"
	case 1:
		return "155"
	default:
		return "46"
	}
}
