package main

import (
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
)

const mongoAddr = "172.16.189.222?maxPoolSize=3000"

func main() {
	ms, err := mgo.Dial(mongoAddr)
	if err != nil {
		panic(err)
	}
	ms.SetSocketTimeout(time.Hour * 5)
	ms.SetSyncTimeout(time.Minute * 10)
	ms.SetCursorTimeout(0)
	ms.SetMode(mgo.Monotonic, true)
	// userBasicC := ms.DB("cr_app_101").C("user_basic")
	userIncomeC := ms.DB("cr_app_101").C("user_income")
	// userApprenticeC := ms.DB("cr_app_101").C("user_apprentice")
	// userApprenticesC := ms.DB("cr_app_101").C("user_apprentices")
	// userOrderC := ms.DB("cr_app_101").C("user_order")
	// userApprenticeRewardC := ms.DB("cr_app_101").C("user_apprentice_reward")
	// checkUserNewbie(userBasicC, userIncomeC, userApprenticesC, userOrderC)
	userIncomeByDateCol := ms.DB("cr_app_101").C("user_income_by_date")
	userIncomeTotalCol := ms.DB("cr_app_101").C("user_income_total")
	adIncomeByDateCol := ms.DB("cr_app_101").C("user_ad_income_by_date")
	articleShareByDateCol := ms.DB("cr_app_101").C("user_article_share_by_date")
	userApprenticeByDateCol := ms.DB("cr_app_101").C("user_apprentice_by_date")
	userApprenticeTotalCol := ms.DB("cr_app_101").C("user_apprentice_total")
	DoUserIncomeHistoryTrans(userIncomeC, userIncomeByDateCol, userIncomeTotalCol, adIncomeByDateCol, articleShareByDateCol, userApprenticeByDateCol, userApprenticeTotalCol)
}

//func transApprenticeReward(userApprenticeC, userApprenticeRewardC *mgo.Collection) {
//	iter := userApprenticeC.Find(nil).Iter()
//	var ap common.UserApprenticeHistory
//	for iter.Next(&ap) {
//		for _, d := range ap.List {
//			uar := common.UserApprenticeReward{
//				ID:             ap.ID,
//				ApprenticeID:   d.ApprenticeID,
//				Timestamp:      d.Timestamp,
//				Status:         common.STATUS_USER_APPRENTICE_REWARD_NOT_EXIST,
//				ValidTimestamp: 0,
//				OpenTimestamp:  0,
//				Reward:         0,
//			}
//			if err := userApprenticeRewardC.Insert(uar); err != nil {
//				log.Fields{
//					"err":           err.Error(),
//					"id":            ap.ID,
//					"apprentice_id": d.ApprenticeID,
//				}.Error("fail to get user apprentice")
//				continue
//			}
//		}
//	}
//}

//func transApprentices(userApprenticeC, userApprenticesC *mgo.Collection) {
//	iter := userApprenticeC.Find(nil).Iter()
//	var ap common.UserApprenticeHistory
//	for iter.Next(&ap) {
//		for _, d := range ap.List {
//			ua := common.UserApprentice{
//				ID:           ap.ID,
//				Timestamp:    d.Timestamp,
//				ApprenticeID: d.ApprenticeID,
//				Source:       d.Source,
//				Amount:       d.Amount,
//			}
//			if err := userApprenticesC.Insert(ua); err != nil {
//				log.Fields{
//					"err":           err.Error(),
//					"id":            ap.ID,
//					"apprentice_id": d.ApprenticeID,
//				}.Error("fail to get user apprentice")
//				continue
//			}
//		}
//	}
//}

func checkUserNewbie(userBasicC, userIncomeC, userApprenticesC, userOrderC *mgo.Collection) {
	iter := userBasicC.Find(nil).Iter()
	var ub common.UserBasic
	for iter.Next(&ub) {
		log.Fields{
			"id":   ub.ID,
			"code": ub.Code,
		}.Info("doing")
		// check wechat
		if ub.ThirdPart.Wechat.OpenID != "" {
			if err := userBasicC.Update(bson.M{
				"id": ub.ID,
			}, bson.M{
				"$addToSet": bson.M{
					"newbie_task": common.UserBasicNewbieTask{
						ID:        common.TASK_NEWBIE_ID_WECHAT_BIND,
						Timestamp: ub.ThirdPart.Wechat.BindTime,
					},
				},
			}); err != nil {
				log.Fields{
					"id":  ub.ID,
					"err": err.Error(),
				}.Error("fail to complete bind wechat")
			}
		}
		// check recommend code
		if ub.From != "" {
			if err := userBasicC.Update(bson.M{
				"id": ub.ID,
			}, bson.M{
				"$addToSet": bson.M{
					"newbie_task": common.UserBasicNewbieTask{
						ID:        common.TASK_NEWBIE_ID_SET_RECOMMEND_CODE,
						Timestamp: ub.FromTimestamp,
					},
				},
			}); err != nil {
				log.Fields{
					"id":  ub.ID,
					"err": err.Error(),
				}.Error("fail to complete bind wechat")
			}
		}
		// check apprentice
		var ap common.UserApprentice
		if err := userApprenticesC.Find(bson.M{"id": ub.ID}).One(&ap); err != nil {
			if err != mgo.ErrNotFound {
				log.Fields{
					"id":  ub.ID,
					"err": err.Error(),
				}.Error("fail to query apprentice")
			}
		} else {
			if err := userBasicC.Update(bson.M{
				"id": ub.ID,
			}, bson.M{
				"$addToSet": bson.M{
					"newbie_task": common.UserBasicNewbieTask{
						ID:        common.TASK_NEWBIE_ID_FIRST_APPRENTICE,
						Timestamp: ap.Timestamp,
					},
				},
			}); err != nil {
				log.Fields{
					"id":  ub.ID,
					"err": err.Error(),
				}.Error("fail to complete first apprentice")
			}
		}
		// check apprentice withdraw
		iter := userApprenticesC.Find(bson.M{"id": ub.ID}).Iter()
		for iter.Next(&ap) {
			var uo common.UserOrder
			if err := userOrderC.Find(bson.M{"id": ub.ID}); err != nil {
				continue
			}
			for _, h := range uo.History {
				if h.Success {
					if err := userBasicC.Update(bson.M{
						"id": ub.ID,
					}, bson.M{
						"$addToSet": bson.M{
							"newbie_task": common.UserBasicNewbieTask{
								ID:        common.TASK_NEWBIE_ID_FIRST_APPRENTICE_WITHDRAW,
								Timestamp: h.FTimestamp,
							},
						},
					}); err != nil {
						log.Fields{
							"id":  ub.ID,
							"err": err.Error(),
						}.Error("fail to complete first apprentice")
					}
					break
				}
			}
		}

		// do first check
		taskType := []int{
			common.TASK_TYPE_FIXED_BOUNDS,
			common.TASK_TYPE_VIDEO_WATCH,
			common.TASK_TYPE_TIMING_REWARD,
			common.TASK_TYPE_SIGN_IN,
			common.TASK_TYPE_HOURLY_REWARD,
			common.TASK_TYPE_SHARE_ARTICLE_READ,
		}
		completeID := []string{
			common.TASK_NEWBIE_ID_FIRST_READ,
			common.TASK_NEWBIE_ID_FIRST_WATCH_VIDEO,
			common.TASK_NEWBIE_ID_FIRST_TIMING_REWARD,
			common.TASK_NEWBIE_ID_FIRST_SIGN_IN,
			common.TASK_NEWBIE_ID_FIRST_PERIOD_REWARD,
			common.TASK_NEWBIE_ID_FIRST_SHARE_ARTICLE,
		}
		for i := range taskType {
			complete, timestamp, err := checkFirstComplete(userIncomeC, ub.ID, taskType[i])
			if err != nil {
				log.Fields{
					"id":      ub.ID,
					"task_id": completeID[i],
					"err":     err.Error(),
				}.Error("fail to complete first apprentice")
				continue
			}
			if complete {
				if err := userBasicC.Update(bson.M{
					"id": ub.ID,
				}, bson.M{
					"$addToSet": bson.M{
						"newbie_task": common.UserBasicNewbieTask{
							ID:        completeID[i],
							Timestamp: timestamp,
						},
					},
				}); err != nil {
					log.Fields{
						"id":  ub.ID,
						"err": err.Error(),
					}.Error("fail to complete first apprentice")
				}
			}
		}
	}
}

func checkFirstComplete(userIncomeC *mgo.Collection, userID string, taskType int) (bool, int64, error) {
	var uih common.UserIncomeHistory
	if err := userIncomeC.Find(bson.M{"id": userID, "type": common.INCOME_TYPE_INCOME, "task_type": taskType}).One(&uih); err != nil {
		if err != mgo.ErrNotFound {
			return false, 0, err
		}
		return false, 0, nil
	}
	return true, uih.Timestamp, nil
}

func DoUserIncomeHistoryTrans(userIncomeC, userIncomeByDateCol, userIncomeTotalCol, adIncomeByDateCol, articleShareByDateCol, userApprenticeByDateCol, userApprenticeTotalCol *mgo.Collection) {
	iter := userIncomeC.Find(nil).Iter()
	var uih common.UserIncomeHistory
	var i int
	for iter.Next(&uih) {
		log.Fields{"current": i}.Info("start")
		i++
		if uih.Timestamp > 1535787040 {
			continue
		}
		doSaveUserIncomeByDateRedudance(userIncomeByDateCol, uih.ID, uih.Type, uih.TaskType, uih.Count, uih.Timestamp)
		doSaveUserIncomeByDateRedudance(userIncomeByDateCol, uih.ID, uih.Type, common.TOTAL_CONF_USER_INCOME, uih.Count, uih.Timestamp)
		doSaveUserIncomeTotalRedudance(userIncomeTotalCol, uih.ID, uih.Type, uih.TaskType, uih.Count)
		doSaveUserIncomeTotalRedudance(userIncomeTotalCol, uih.ID, uih.Type, common.TOTAL_CONF_USER_INCOME, uih.Count)
		switch uih.TaskType {
		case common.TASK_TYPE_AD_CLICK:
			doSaveUserADIncomeByDate(adIncomeByDateCol, uih.ID, uih.From, uih.Count, uih.Timestamp)
			doSaveUserADIncomeByDate(adIncomeByDateCol, uih.ID, common.TOTAL_CONF_USER_AD, uih.Count, uih.Timestamp)
		case common.TASK_TYPE_SHARE_ARTICLE_READ:
			doSaveUserArticleShareByDate(articleShareByDateCol, uih.ID, uih.From, uih.Count, uih.Timestamp)
			doSaveUserArticleShareByDate(articleShareByDateCol, uih.ID, common.TOTAL_CONF_USER_ARTICLE, uih.Count, uih.Timestamp)
		case common.TASK_TYPE_APPRENTICE:
			doSaveUserApprenticeByDate(userApprenticeByDateCol, uih.ID, uih.From, uih.Count, uih.Timestamp)
			doSaveUserApprenticeByDate(userApprenticeByDateCol, uih.ID, common.TOTAL_CONF_USER_APPRENTICE, uih.Count, uih.Timestamp)
			doSaveUserApprenticeTotalRedudance(userApprenticeTotalCol, uih.ID, uih.From, uih.Count)
			doSaveUserApprenticeTotalRedudance(userApprenticeTotalCol, uih.ID, common.TOTAL_CONF_USER_APPRENTICE, uih.Count)
		}
	}
}

func doSaveUserIncomeByDateRedudance(col *mgo.Collection, userID string, mtype, taskType, amount int, timestamp int64) error {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	err := col.Update(bson.M{"id": userID, "type": mtype, "task_type": taskType, "timestamp": dateTimestamp}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserIncomeByDate{
			ID:        userID,
			Type:      mtype,
			TaskType:  taskType,
			Timestamp: dateTimestamp,
			Total:     amount,
			Count:     1,
		})
	}
	return nil
}

func doSaveUserIncomeTotalRedudance(col *mgo.Collection, userID string, mtype, taskType, amount int) error {
	err := col.Update(bson.M{"id": userID, "type": mtype, "task_type": taskType}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserIncomeByType{
			ID:       userID,
			Type:     mtype,
			TaskType: taskType,
			Total:    amount,
			Count:    1,
		})
	}
	return nil
}

func doSaveUserADIncomeByDate(col *mgo.Collection, userID, adID string, amount int, timestamp int64) error {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	err := col.Update(bson.M{"id": userID, "ad_id": adID, "timestamp": dateTimestamp}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserAdDetailByDate{
			ID:        userID,
			ADID:      adID,
			Timestamp: dateTimestamp,
			Total:     amount,
			Count:     1,
		})
	}
	return nil
}

func doSaveUserArticleShareByDate(col *mgo.Collection, userID, articleID string, amount int, timestamp int64) error {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	err := col.Update(bson.M{"id": userID, "article_id": articleID, "timestamp": dateTimestamp}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserArticleDetailByDate{
			ID:        userID,
			ArticleID: articleID,
			Timestamp: dateTimestamp,
			Total:     amount,
			Count:     1,
		})
	}
	return nil
}

func doSaveUserApprenticeByDate(col *mgo.Collection, userID, apprenticeID string, amount int, timestamp int64) error {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	err := col.Update(bson.M{"id": userID, "apprentice_id": apprenticeID, "timestamp": dateTimestamp}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserApprenticeDetailByDate{
			ID:           userID,
			ApprenticeID: apprenticeID,
			Timestamp:    dateTimestamp,
			Total:        amount,
			Count:        1,
		})
	}
	return nil
}

func doSaveUserApprenticeTotalRedudance(col *mgo.Collection, userID, apprenticeID string, amount int) error {
	err := col.Update(bson.M{"id": userID, "apprentice_id": apprenticeID}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserApprenticeDetailTotal{
			ID:           userID,
			ApprenticeID: apprenticeID,
			Total:        amount,
			Count:        1,
		})
	}
	return nil
}
