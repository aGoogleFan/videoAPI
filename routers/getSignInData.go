package routers

import (
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetSignInDataStruct struct {
	usr common.UserBasic
	yl  common.APIRecord
}

func (g *GetSignInDataStruct) New() common.LogAndUserSetHandler {
	return new(GetSignInDataStruct)
}

func (g *GetSignInDataStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_SIGN_IN_INFO
}

func (g *GetSignInDataStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetSignInDataStruct) SetUser(u common.UserBasic) {
	g.usr = u
}

func (g *GetSignInDataStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"token", "app_id"}
	return getUserByToken(rq, mustParams)
}

func (g *GetSignInDataStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")

	recentSignIn, err := db.GetUserSignIn(appID, g.usr.ID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	confMux.RLock()
	asich := conf[appID].SignInConf
	confMux.RUnlock()
	if asich == nil {
		errResp := common.ServerErrorResp("")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	asichds := asich.Details
	if asichds == nil {
		errResp := common.ServerErrorResp("")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	now := time.Now()
	continuous := getContinuous(now, recentSignIn)
	var usitr userSignInDataToReturn
	for i := time.Duration(-7); i < 8; i++ {
		if i < 0 {
			var usid userSignInDataDetail
			usid.Date = now.Add(time.Hour * 24 * i).Local().Format("2006-01-02")
			for _, h := range recentSignIn {
				if h.Date != usid.Date {
					continue
				}
				usid.Reward = h.Reward
				usid.IsB = h.IsB
				usid.Signed = true
				break
			}
			usitr.List = append(usitr.List, usid)
			continue
		}
		if i == 0 {
			var usid userSignInDataDetail
			usid.Date = now.Local().Format("2006-01-02")
			find := false
			for _, h := range recentSignIn {
				if h.Date != usid.Date {
					continue
				}
				usid.Reward = h.Reward
				usid.IsB = h.IsB
				usid.Signed = true
				find = true
				break
			}
			if !find {
				usid.Reward = getRewardByContinuous(continuous+1, asichds)
			}
			usitr.List = append(usitr.List, usid)
			continue
		}
		var usid userSignInDataDetail
		usid.Date = now.Add(time.Hour * 24 * i).Local().Format("2006-01-02")
		usid.Reward = getRewardByContinuous(continuous+int(i)+1, asichds)
		usitr.List = append(usitr.List, usid)
	}

	resp := common.SuccessResp(usitr)
	g.yl.Ret = resp.String()
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

func getContinuous(t time.Time, usihs []common.UserSignInHistory) int {
	if len(usihs) == 0 {
		return 0
	}
	last := 0
	for i := time.Duration(1); ; i++ {
		k := t.Add(time.Hour * 24 * i * (-1)).Local().Format("2006-01-02")
		find := false
		for _, usih := range usihs {
			if usih.Date != k {
				continue
			}
			find = true
			break
		}
		if !find {
			break
		}
		last++
	}
	return last
}

func getRewardByContinuous(continous int, asichds []common.AppSignInConfHistoryDetail) int {
	for _, asichd := range asichds {
		if asichd.Max == 0 {
			if continous >= asichd.Min {
				return asichd.Coin
			}
		} else {
			if continous >= asichd.Min && continous <= asichd.Max {
				return asichd.Coin
			}
		}
	}
	return 0
}

type userSignInDataToReturn struct {
	List []userSignInDataDetail `json:"list"`
}

type userSignInDataDetail struct {
	Date   string `json:"date"`
	Signed bool   `json:"signed"`
	IsB    bool   `json:"is_b"`
	Reward int    `json:"reward"`
}
