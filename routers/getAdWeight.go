package routers

import (
	"net/http"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
)

func GetAdWeight(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	mustParams := []string{"app_id", "ad_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		log.Fields{
			"err": errResp.ErrMsg,
		}.Error("params check fail")
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	confMux.RLock()
	adcf := conf[appID].AdConf
	ads := conf[appID].AdPositionConf
	confMux.RUnlock()
	if adcf == nil || ads == nil {
		http.Error(w, common.OtherErrorResp("not found").String(), http.StatusOK)
		return
	}

	find := false
	adID := rq.Get("ad_id")
	actr := adWeightToReturn{
		ADID: adID,
	}
	for _, d := range adcf.Details {
		if d.ADID != adID {
			continue
		}
		find = true
		actr.ADWeight = d.ADWeight
		actr.ThirdPartADID = d.ThirdPartADID
		actr.ThirdPartADWeight = d.ThirdPartADWeight
		for _, d := range ads.Details {
			if d.Status == common.ADS_STATUS_INVALID {
				continue
			}
			if adID != "" {
				if d.ADID != adID {
					continue
				}
				actr.ADData = append(actr.ADData, d)
				continue
			}

			actr.ADData = append(actr.ADData, d)
		}
		break
	}

	if !find {
		http.Error(w, common.OtherErrorResp("not found").String(), http.StatusOK)
		return
	}

	http.Error(w, common.SuccessResp(actr).String(), http.StatusOK)
}

type adWeightToReturn struct {
	ADID              string                       `json:"ad_id"`
	ADWeight          int                          `json:"ad_weight"`
	ADData            []common.AppAdsHistoryDetail `json:"ad_data"`
	ThirdPartADID     string                       `json:"third_part_ad_id"`
	ThirdPartADWeight int                          `json:"third_part_ad_weight"`
}
