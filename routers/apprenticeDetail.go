package routers

import (
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type ApprenticeDetail struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (a *ApprenticeDetail) New() common.LogAndUserSetHandler {
	return new(ApprenticeDetail)
}

func (a *ApprenticeDetail) GetAPICode() int {
	return common.API_LOG_CODE_APPRENTICE_DETAIL_LEADERBOARDS
}

func (a *ApprenticeDetail) SetLog(yl common.APIRecord) {
	a.yl = yl
}

func (a *ApprenticeDetail) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token", "timestamp"}
	return getUserByToken(rq, mustParams)
}

func (a *ApprenticeDetail) SetUser(u common.UserBasic) {
	a.usr = u
}

func (a *ApprenticeDetail) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(a.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	page := 1
	if opage := rq.Get("page"); opage != "" {
		fpage, err := strconv.Atoi(opage)
		if err == nil {
			page = fpage
		}
	}

	pageStart := (page - 1) * 10
	// pageEnd := page * 10
	total, err := db.GetUserApprenticeRewardCount(appID, a.usr.ID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		a.yl.Ret = errResp.String()
		a.yl.RetCode = errResp.Status
		a.yl.ErrorMsg = err.Error()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	pageContiner := common.PageContiner{
		TotalCount: total,
		Page:       page,
		MaxPerPage: 10,
	}
	if pageStart >= total {
		resp := common.SuccessResp(pageContiner)
		a.yl.Ret = resp.String()
		a.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	sortBy := []string{
		"status",
		"valid_timestamp",
		"open_timestamp",
		"timestamp",
	}
	apprenticeRewards, err := db.GetUserApprenticeRewardDetail(appID, a.usr.ID, nil, sortBy, pageStart, 10)
	if err != nil {
		resp := common.SuccessResp(pageContiner)
		a.yl.Ret = resp.String()
		a.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	apprentices := make([]apprentice, len(apprenticeRewards))
	nowUnix := time.Now().Unix()
	// dateStartUnix, dateEndUnix := common.GetDateStartUnix(nowUnix), common.GetDateEndUnix(nowUnix)
	for i, apprenticeReward := range apprenticeRewards {
		appr := apprentice{
			ID:        apprenticeReward.ApprenticeID,
			Reward:    apprenticeReward.Reward,
			Timestamp: apprenticeReward.OpenTimestamp,
		}
		switch apprenticeReward.Status {
		case common.STATUS_USER_APPRENTICE_REWARD_INVALID:
			appr.HasReward = true
			appr.IsValid = false
			appr.IsOpened = false
		case common.STATUS_USER_APPRENTICE_REWARD_NOT_EXIST:
			appr.HasReward = false
			appr.IsValid = false
			appr.IsOpened = false
		case common.STATUS_USER_APPRENTICE_REWARD_OPENED:
			appr.HasReward = true
			appr.IsValid = true
			appr.IsOpened = true
		case common.STATUS_USER_APPRENTICE_REWARD_VALID:
			appr.HasReward = true
			appr.IsValid = true
			appr.IsOpened = false
		}
		user, err := db.GetUserInfo(appID, bson.M{"id": appr.ID})
		if err == nil && user != nil {
			appr.Code = user.Code
			appr.Name = user.Info.Name
		}
		appr.Count, _, _ = db.GetUserApprenticeTotal(appID, a.usr.ID, appr.ID)
		appr.CountToday, _, _ = db.GetUserApprenticeByDate(appID, a.usr.ID, appr.ID, nowUnix)
		appr.TotalWithdraw, _ = db.GetUserSuccessWithdrawTotal(appID, appr.ID, common.NO_LIMIT, common.NO_LIMIT, true)
		apprentices[i] = appr
	}

	pageContiner.List = apprentices
	resp := common.SuccessResp(pageContiner)
	a.yl.Ret = resp.String()
	a.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

type apprentice struct {
	ID            string `json:"-"`
	Code          string `json:"code"`
	Name          string `json:"name"`
	Count         int    `json:"count"`          // 徒弟上供
	CountToday    int    `json:"count_today"`    // 徒弟今日上供
	TotalWithdraw int    `json:"total_withdraw"` // 徒弟总提现
	HasReward     bool   `json:"has_reward"`     // 是否有宝箱
	IsValid       bool   `json:"is_valid"`       // 宝箱是否有效
	IsOpened      bool   `json:"is_opened"`      // 宝箱是否已被开启
	Reward        int    `json:"reward"`         // 宝箱奖励
	Timestamp     int64  `json:"timestamp"`      // 宝箱开启时间
}

func apprenticeExists(apprenticeID string, as []apprentice) bool {
	for _, a := range as {
		if apprenticeID == a.ID {
			return true
		}
	}
	return false
}
