package routers

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
)

func sha2Sign(content, privateKey string) (sign string, err error) {
	der, err := base64.StdEncoding.DecodeString(privateKey)
	if err != nil {
		return
	}

	privatekey, err := x509.ParsePKCS1PrivateKey(der)
	if err != nil {
		return
	}

	hashType := crypto.SHA256
	if !hashType.Available() {
		err = errors.New("unsupport sha1 hash type")
		return
	}

	h := hashType.New()
	h.Write([]byte(content))
	digest := h.Sum(nil)

	signature, err := rsa.SignPKCS1v15(rand.Reader, privatekey, crypto.SHA256, digest)
	if err != nil {
		return
	}

	sign = base64.StdEncoding.EncodeToString(signature)
	return
}

func sha2CheckSign(data, sign, privateKey string) error {
	signByte, err := base64.StdEncoding.DecodeString(sign)
	if err != nil {
		return err
	}
	s := sha256.New()
	_, err = s.Write([]byte(data))
	if err != nil {
		return err
	}
	hash := s.Sum(nil)
	block, _ := pem.Decode([]byte(privateKey))
	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return err
	}

	publicKey, ok := pub.(*rsa.PublicKey)
	if !ok {
		return errors.New("unknown error")
	}
	return rsa.VerifyPKCS1v15(publicKey, crypto.SHA256, hash[:], signByte)
}
