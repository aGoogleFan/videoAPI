package routers

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
)

func uploadToKS3(url, fileName string, f multipart.File, headerMap, formMap map[string]string) (*http.Response, error) {
	// Prepare a form that you will submit to that URL.
	var b bytes.Buffer
	w := multipart.NewWriter(&b)

	for k, v := range formMap {
		fw, err := w.CreateFormField(k)
		if err != nil {
			return nil, err
		}
		if _, err = fw.Write([]byte(v)); err != nil {
			return nil, err
		}
	}

	fw, err := w.CreateFormFile("file", fileName)
	if err != nil {
		return nil, err
	}

	if _, err = io.Copy(fw, f); err != nil {
		return nil, err
	}

	// Don't forget to close the multipart writer.
	// If you don't close it, your request will be missing the terminating boundary.
	w.Close()

	// Now that you have a form, you can submit it to your handler.
	req, err := http.NewRequest("POST", url, &b)
	if err != nil {
		return nil, err
	}

	// Don't forget to set the content type, this will contain the boundary.
	req.Header.Set("Content-Type", w.FormDataContentType())
	for k, v := range headerMap {
		req.Header.Set(k, v)
	}

	return http.DefaultClient.Do(req)
}
