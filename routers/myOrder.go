package routers

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type MyOrderStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (m *MyOrderStruct) New() common.LogAndUserSetHandler {
	return new(MyOrderStruct)
}

func (m *MyOrderStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_MY_ORDER
}

func (m *MyOrderStruct) SetLog(yl common.APIRecord) {
	m.yl = yl
}

func (m *MyOrderStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	return getUserByToken(rq, []string{"app_id", "token"})
}

func (m *MyOrderStruct) SetUser(usr common.UserBasic) {
	m.usr = usr
}

func (m *MyOrderStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(m.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	page := 1
	if opage := rq.Get("page"); opage != "" {
		fpage, err := strconv.Atoi(opage)
		if err == nil {
			page = fpage
		}
	}
	uo, err := db.GetUserOrder(appID, m.usr.ID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		m.yl.ErrorMsg = err.Error()
		m.yl.RetCode = errResp.Status
		m.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	pageContiner := common.PageContiner{
		MaxPerPage: 10,
		Page:       page,
	}
	if uo == nil {
		resp := common.SuccessResp(pageContiner)
		m.yl.RetCode = resp.Status
		m.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	uoLen := len(uo.History)
	if uoLen == 0 {
		resp := common.SuccessResp(pageContiner)
		m.yl.RetCode = resp.Status
		m.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	pageContiner.TotalCount = uoLen
	if page <= 0 || page > (uoLen/10)+1 {
		resp := common.SuccessResp(pageContiner)
		m.yl.RetCode = resp.Status
		m.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageStart := (page - 1) * 10
	if (page-1)*10 > uoLen {
		resp := common.SuccessResp(pageContiner)
		m.yl.RetCode = resp.Status
		m.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	newUO := make([]common.OrderDetail, uoLen)
	ud := getUserLevelData(appID, m.usr.ID)
	confMux.RLock()
	wsc := conf[appID].WithdrawStandConf
	confMux.RUnlock()
	uwsl := getUserStand(m.usr.Status, ud.Total, *wsc)
	for i, h := range uo.History {
		if h.Status != common.ORDER_STATUS_TO_BE_CHECKED {
			newUO[uoLen-(i+1)] = h
			continue
		}

		if ud == nil {
			newUO[uoLen-(i+1)] = h
			continue
		}

		if wsc == nil {
			newUO[uoLen-(i+1)] = h
			continue
		}
		if uwsl == nil {
			newUO[uoLen-(i+1)] = h
			continue
		}
		if !uwsl.IsAutoWithdraw {
			h.Desc = "人工审核中.."
		} else {
			extra := getWithdrawExtra(m.usr.IncomeRemaining, h.Cost, uwsl.CoinKeepRate, h.PreDeducate)
			if extra > 0 {
				h.Desc = fmt.Sprintf("您最近的提现还差%d金币就可以自动到账", extra)
			} else {
				h.Desc = "您的提现请求已通过, 提现即将到账"
			}
		}

		newUO[uoLen-(i+1)] = h
	}

	pageEnd := page * 10
	if pageEnd > uoLen {
		pageContiner.List = newUO[pageStart:uoLen]
		resp := common.SuccessResp(pageContiner)
		m.yl.RetCode = resp.Status
		m.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageContiner.List = newUO[pageStart:pageEnd]
	resp := common.SuccessResp(pageContiner)
	m.yl.RetCode = resp.Status
	m.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}
