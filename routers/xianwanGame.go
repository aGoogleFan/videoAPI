package routers

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	// "github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

const (
	WhiteList1 = "139.224.165.158"
	WhiteList2 = "122.224.200.114"

	XianwanSuccessCode = 1
	XianwanFailCode    = 0

	XianwanRepeatRequestMessage = "订单已接收"
	XianwanSuccessMessage       = "接收成功"
)

// map[{{.闲玩APPID}}]{{.我们的APPID}}
var appsMap = map[string]struct {
	appID            string
	xianwanAppSecret string
}{
	"1400": {
		appID:            "101",
		xianwanAppSecret: "rsl8nfsz85mrq8u2",
	},
}

func XianwanAPI(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	now := time.Now()
	nowTimestamp := now.Unix()
	yl := common.APIRecord{
		APICode:   common.API_LOG_CODE_XIANWAN_GAME,
		StartTime: now,
		Timestamp: nowTimestamp,
	}
	defer func() {
		yxwLog.RecordLog(yl)
	}()
	var uxwrh common.UserXianwanRewardHistory
	uxwrh.Access = r.URL.String()
	uxwrh.Timestamp = nowTimestamp
	ip := common.GetIPFromRequest(r)
	uxwrh.FromIP = ip
	yl.UserIP = ip
	if ip != WhiteList1 && ip != WhiteList2 {
		uxwrh.Desc = "invalid ip"
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "invalid ip source",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	rq := r.URL.Query()
	xianwanOrderNum := rq.Get("ordernum")
	uxwrh.OrderNumber = xianwanOrderNum
	xianwanAppID := rq.Get("appid")
	xianwanInfo, ok := appsMap[xianwanAppID]
	if !ok {
		uxwrh.Desc = "invalid appid"
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "invalid appid",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := xianwanInfo.appID
	yl.APPID = appID
	var userID, code string
	defer func() { db.SaveXianwanOrder(appID, userID, code, uxwrh) }()
	appSecret := xianwanInfo.xianwanAppSecret
	oadID := rq.Get("adid")
	adID, err := strconv.Atoi(oadID)
	if err != nil {
		uxwrh.Desc = "invalid ad id"
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "invalid adid",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	odLevel := rq.Get("dlevel")
	dLevel, err := strconv.Atoi(odLevel)
	if err != nil {
		uxwrh.Desc = "invalid ad dlevel"
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "invalid dlevel",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	oprice := rq.Get("price")
	price, err := strconv.ParseFloat(oprice, 64)
	if err != nil {
		uxwrh.Desc = "invalid price"
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "invalid price",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	uxwrh.Price = price
	omoney := rq.Get("money")
	money, err := strconv.ParseFloat(omoney, 64)
	if err != nil {
		uxwrh.Desc = "invalid money"
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "invalid money",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	uxwrh.Money = money
	iTime, err := time.ParseInLocation("2006/1/2 15:4:5", rq.Get("itime"), time.Local)
	if err != nil {
		uxwrh.Desc = "invalid itime"
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "invalid date",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	xw := common.Xianwan{
		ADID:   adID,
		AppID:  xianwanAppID,
		DLevel: dLevel,
		Price:  price,
		Money:  money,
		ITime:  iTime,
	}
	xw.ADName = rq.Get("adname")
	xw.OrderNumber = xianwanOrderNum
	xw.PageName = rq.Get("pagename")
	xw.DeviceID = rq.Get("deviceid")
	xw.SimID = rq.Get("simid")
	xw.AppSign = rq.Get("appsign")
	xw.MerID = rq.Get("merid")
	xw.Event = rq.Get("event")
	xw.KeyCode = rq.Get("keycode")

	stringToSign := strconv.Itoa(xw.ADID) + xw.AppID + xw.OrderNumber + xw.DeviceID + appSecret
	validKey := common.StringToMD5(stringToSign)
	if xw.KeyCode != strings.ToUpper(validKey) {
		uxwrh.Desc = "invalid sign"
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "invalid key code",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	ub, err := getUserByAppsign(appID, xw.AppSign)
	if err != nil {
		uxwrh.Desc = err.Error()
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "server error",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if ub == nil {
		uxwrh.Desc = "user not found"
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "user not found",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	userID = ub.ID
	code = ub.Code
	yl.UserID = ub.ID
	yl.UserCode = ub.Code
	yl.UserMobile = ub.Info.Mobile
	yl.WechatOpenID = ub.ThirdPart.Wechat.OpenID
	yl.AlipayOpenID = ub.ThirdPart.Alipay.ID

	key := fmt.Sprintf("xianwanordernum:%s:%s:%s", appID, userID, xw.OrderNumber)
	d, err := db.CheckIfExpired(key)
	if err != nil {
		uxwrh.Desc = err.Error()
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "server error",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if !d {
		uxwrh.Desc = "duplicate order id"
		errResp := common.XianwanToReturn{
			Success: XianwanSuccessCode,
			Message: XianwanRepeatRequestMessage,
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	value := strconv.FormatInt(nowTimestamp, 10)
	timeOut := time.Hour * 24
	if err := db.SaveToRedis(key, value, timeOut); err != nil {
		uxwrh.Desc = err.Error()
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "server error",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	confMux.RLock()
	arc := conf[appID].AppRewardConf
	confMux.RUnlock()
	if arc == nil {
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "server error",
		}
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	xianwanGameDailyLimit := arc.XianwanGameReward
	if xianwanGameDailyLimit == 0 {
		resp := common.XianwanToReturn{
			Success: XianwanSuccessCode,
			Message: XianwanSuccessMessage,
		}
		yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	if xianwanGameDailyLimit > 0 {
		userDailyXianwanReward, _, err := db.GetUserDailyIncomeByType(appID, userID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_XIANWAN_REWARD, nowTimestamp)
		if err != nil {
			uxwrh.Desc = err.Error()
			errResp := common.XianwanToReturn{
				Success: XianwanFailCode,
				Message: "server error",
			}
			yl.ErrorMsg = uxwrh.Desc
			yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if userDailyXianwanReward >= xianwanGameDailyLimit {
			errResp := common.XianwanToReturn{
				Success: XianwanSuccessCode,
				Message: XianwanSuccessMessage,
			}
			yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
	}

	reward := int(xw.Money * 10000)
	uits := db.UserIncomeToSave{
		AppID:     appID,
		Usr:       *ub,
		OpType:    common.INCOME_TYPE_INCOME,
		TaskID:    "",
		TaskType:  common.TASK_TYPE_XIANWAN_REWARD,
		Count:     reward,
		Timestamp: nowTimestamp,
		Desc:      "闲玩游戏奖励",
		SetMaster: false, // 是否给师傅一定比例的奖励
		From:      xianwanOrderNum,
	}
	if err := uits.Save(); err != nil {
		uxwrh.Desc = err.Error()
		errResp := common.XianwanToReturn{
			Success: XianwanFailCode,
			Message: "server error",
		}
		yl.ErrorMsg = uxwrh.Desc
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	uxwrh.Success = true
	resp := common.XianwanToReturn{
		Success: XianwanSuccessCode,
		Message: XianwanSuccessMessage,
	}
	yl.Ret = resp.String()
	yl.Reward = reward
	http.Error(w, resp.String(), http.StatusOK)
}

func getUserByAppsign(appID, appSign string) (*common.UserBasic, error) {
	userID, err := db.CheckToken(appSign, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		return nil, err
	}
	if userID == "" {
		return db.GetUserInfo(appID, bson.M{"code": appSign})
	}
	return db.GetUserInfo(appID, bson.M{"id": userID})
}
