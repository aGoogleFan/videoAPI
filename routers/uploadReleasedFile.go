package routers

import (
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

const (
	maxSize           = 1024 << 20
	adminUserName     = "admin"
	adminUserPassword = "useradmin"
)

func UploadReleasedFile(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		io.Copy(ioutil.Discard, r.Body)
		http.Error(w, "only post allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	mustParams := []string{"app_id", "app_version", "user", "password"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		io.Copy(ioutil.Discard, r.Body)
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if rq.Get("user") != adminUserName || rq.Get("password") != adminUserPassword {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}

	r.ParseMultipartForm(maxSize)
	file, handler, err := r.FormFile("uploadfile")
	if err != nil {
		errResp := common.ServerErrorResp("fail to find file: " + err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	defer file.Close()

	appID := rq.Get("app_id")
	appVersion := rq.Get("app_version")

	if handler.Size > maxSize {
		errResp := common.OtherErrorResp("file too big")
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	f, err := os.OpenFile("/vlion/cr_lua/views/yuexinwen/cn.viaweb.toutiao.apk", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to create file")
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	io.Copy(f, file)
	f.Close()

	downloadURL := "http://yuexinwen.cn/views/cn.viaweb.toutiao.apk"
	db.SaveReleasedFile(appID, appVersion, "", downloadURL)
	http.Error(w, common.SuccessResp(downloadURL).String(), http.StatusOK)
}
