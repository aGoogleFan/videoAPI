package routers

import (
	"fmt"
	"net/http"

	"vlion/yxw/common"
)

type GetMyIPStruct struct{}

func (g *GetMyIPStruct) New() common.LogSetHandler {
	return new(GetMyIPStruct)
}

func (g *GetMyIPStruct) GetAPICode() int {
	return 0
}

func (g *GetMyIPStruct) SetLog(yl common.APIRecord) {}

func (g *GetMyIPStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	GetMyIP(w, r)
}

func GetMyIP(w http.ResponseWriter, r *http.Request) {
	ip := common.GetIPFromRequest(r)
	http.Error(w, fmt.Sprintf("your ip is : %s", ip), http.StatusOK)
}
