package routers

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

var wechatTlsConf *tls.Config

func init() {
	cert, err := tls.LoadX509KeyPair("apiclient_cert.pem", "apiclient_key.pem")
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Panic("fail to load key pair")
		return
	}
	certBytes, err := ioutil.ReadFile("apiclient_cert.pem")
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Panic("fail to load cert bytes")
		return
	}
	clientCertPool := x509.NewCertPool()
	if ok := clientCertPool.AppendCertsFromPEM(certBytes); !ok {
		log.Fields{
			"err": err.Error(),
		}.Panic("failed to parse root certificate")
		return
	}
	wechatTlsConf = &tls.Config{
		RootCAs:            clientCertPool,
		Certificates:       []tls.Certificate{cert},
		InsecureSkipVerify: true,
	}
}

type WechatWithdrawStruct struct {
	yl common.APIRecord
}

func (wws *WechatWithdrawStruct) New() common.LogSetHandler {
	return new(WechatWithdrawStruct)
}

func (wws *WechatWithdrawStruct) GetAPICode() int {
	return common.API_LOG_CODE_WECHAT_WITHDRAW
}

func (wws *WechatWithdrawStruct) SetLog(yl common.APIRecord) {
	wws.yl = yl
}

func (wws *WechatWithdrawStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		yxwLog.RecordLog(wws.yl)
	}()
	wi, yl, needContinue := WithdrawCommon(w, r, common.ORDER_TYPE_WECHAT_WITHDRAWA, wws.yl)
	wws.yl = yl
	if !needContinue {
		return
	}

	rq := r.URL.Query()
	mustParams := []string{"wechat_name"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		wws.yl.ErrorMsg = errResp.ErrMsg
		wws.yl.Ret = errResp.String()
		wws.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if wi.WechatOpenID == "" {
		errResp := common.Resp{
			Timestamp: time.Now().Unix(),
			Status:    common.STATUS_WECHAT_NOT_BIND,
		}
		wws.yl.ErrorMsg = errResp.ErrMsg
		wws.yl.Ret = errResp.String()
		wws.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	next, err := autoWithdrawCheck(wi.AppID, wi.user.ID, wi.user.Create, wi.user.Status, wi.Amount)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		wws.yl.ErrorMsg = err.Error()
		wws.yl.Ret = errResp.String()
		wws.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	wechatName := rq.Get("wechat_name")
	orderID := strings.Replace(common.NewUUID(), "-", "", -1)
	od := common.OrderDetail{
		Timestamp:   wi.Timestamp,
		OrderID:     orderID,
		OrderType:   common.ORDER_TYPE_WECHAT_WITHDRAWA,
		Account:     wi.WechatOpenID,
		AccountName: wechatName,
		Content:     "",
		Cost:        wi.RealCost,
		Amount:      wi.Amount * (-1),
		IP:          wi.IP,
		AppVersion:  wi.AppVersion,
		PreDeducate: true,
	}
	wws.yl.WithdrawAccount = wi.WechatOpenID
	wws.yl.OrderID = od.OrderID

	passed := false
	switch next {
	case common.WITHDRAW_AUTO_DO_REFUSE:
		od.Desc = "审核不通过."
		od.Status = common.ORDER_STATUS_REFUSED
	case common.WITHDRAW_AUTO_DO_PERSON:
		if wi.CoinKeepRate == -1 {
			od.Desc = "订单已提交, 待审批"
			od.Status = common.ORDER_STATUS_TO_BE_CHECKED
			break
		}
		if !wi.LevelAuto {
			od.Desc = "订单已提交, 待审批"
			od.Status = common.ORDER_STATUS_TO_BE_CHECKED
			break
		}
		extra := getWithdrawExtra(wi.user.IncomeRemaining, wi.RealCost, wi.CoinKeepRate, false)
		if extra > 0 {
			od.Desc = "订单已提交, 待审批"
			od.Status = common.ORDER_STATUS_TO_BE_CHECKED
			break
		}
		fallthrough
	case common.WITHDRAW_AUTO_DO_PASS:
		od.Desc = "订单已提交, 待审批"
		passed = true
		od.Status = common.ORDER_STATUS_TO_BE_CHECKED
	}
	wws.yl.AutoResult = od.Desc
	if err := db.SaveUserOrderHistory(wi.AppID, wi.user, od, wi.AppVersion); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		wws.yl.ErrorMsg = err.Error()
		wws.yl.Ret = errResp.String()
		wws.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if passed {
		go withdrawPass(wi.AppID, rq.Get("app_version"), wi.user, od)
	}

	resp := common.SuccessResp(nil)
	wws.yl.Ret = resp.String()
	wws.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

func wechatWithdraw(appID string, usr common.UserBasic, od common.OrderDetail) (bool, error) {
	success, desc, err := withdrawByWechat(appID, od.OrderID, od.Account, od.AccountName, od.Amount)
	if err != nil {
		return false, err
	}
	if desc == "余额不足" {
		go sendNotEnoughAlarm()
		return false, nil
	}

	if err := db.UpdateUserOrderResult(appID, usr.ID, od, success, desc); err != nil {
		log.Fields{
			"orderid": od.OrderID,
			"appid":   appID,
			"userid":  usr.ID,
			"err":     err.Error(),
		}.Error("fail to save user order history")
		return false, err
	}
	if !od.PreDeducate {
		if !success {
			return false, errors.New(desc)
		}
		uits := db.UserIncomeToSave{
			AppID:     appID,
			Usr:       usr,
			OpType:    common.INCOME_TYPE_WITHDRAW,
			TaskID:    od.OrderID,
			TaskType:  common.TASK_TYPE_WECHAT_WITHDRAW,
			Count:     od.Cost,
			Timestamp: time.Now().Unix(),
			Desc:      "提现成功, 扣除提现金币",
			From:      od.OrderID,
			SetMaster: false,
		}
		if err := uits.Save(); err != nil {
			log.Fields{
				"appID":    appID,
				"userID":   usr.ID,
				"orderID":  od.OrderID,
				"cost":     od.Cost,
				"withdraw": common.WITHDRAW_TYPE_ALIPAY,
				"err":      err.Error(),
			}.Error("fail to deal withdraw")
			return true, err
		}
		return true, nil
	}
	if success {
		return true, nil
	}
	uits := db.UserIncomeToSave{
		AppID:     appID,
		Usr:       usr,
		OpType:    common.INCOME_TYPE_INCOME_NO_TOTAL,
		TaskID:    od.OrderID,
		TaskType:  common.TASK_TYPE_PRE_DEDUCATE_RET,
		Count:     od.Cost,
		Timestamp: time.Now().Unix(),
		Desc:      common.USER_ORDER_RET_DESC,
		From:      od.OrderID,
		SetMaster: false,
	}
	uits.Save()
	return false, errors.New(desc)
}

// 返回值: 提现是否成功, 描述, 错误信息
func withdrawByWechat(appID string, orderID, account, accountName string, amount int) (bool, string, error) {
	confMux.RLock()
	wechatAppID := conf[appID].ThirdpartConf.Wechat.AppID
	wechatMuchID := conf[appID].ThirdpartConf.Wechat.MuchID
	confMux.RUnlock()
	wt := WechatTransfer{
		MchAPPID:       wechatAppID,
		MchID:          wechatMuchID,
		NonceStr:       common.NewRandomString(32),
		PartnerTradeNo: orderID,
		OpenID:         account,
		// CheckName:"NO_CHECK",
		CheckName:      "FORCE_CHECK",
		ReUserName:     accountName,
		Amount:         amount * -100,
		Desc:           "阅新闻微信支付兑换成功，继续加油",
		SpbillCreateIP: "127.0.0.1",
	}

	wt.Sign = getWechatSign(appID, url.Values{
		"mch_appid":        []string{wt.MchAPPID},
		"mchid":            []string{wt.MchID},
		"nonce_str":        []string{wt.NonceStr},
		"partner_trade_no": []string{wt.PartnerTradeNo},
		"openid":           []string{wt.OpenID},
		"check_name":       []string{wt.CheckName},
		"re_user_name":     []string{wt.ReUserName},
		"amount":           []string{strconv.Itoa(wt.Amount)},
		"desc":             []string{wt.Desc},
		"spbill_create_ip": []string{wt.SpbillCreateIP},
	})

	ct, _ := xml.Marshal(wt)
	tr := &http.Transport{
		TLSClientConfig: wechatTlsConf,
	}
	client := http.Client{
		Timeout:   time.Second * 3,
		Transport: tr,
	}
	resp, err := client.Post(WECHAT_TRANSFER_URL, "application/xml", bytes.NewBuffer(ct))
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to send wechat trans post")
		return false, "", err
	}

	if resp.StatusCode != http.StatusOK {
		resp.Body.Close()
		log.Fields{
			"code": resp.StatusCode,
		}.Error("unexpected status code of wechat trans")
		return false, "", fmt.Errorf("unexpected code: %d", resp.StatusCode)
	}

	respCt, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to read wechat trans response")
		return false, "", err
	}

	var wtr common.WechatTransResp
	if err := xml.Unmarshal(respCt, &wtr); err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to unmarshal response of wechat trans")
		return false, "", err
	}

	var success bool
	var desc string
	if wtr.ReturnCode != "SUCCESS" {
		success = false
		desc = wtr.ReturnMsg
	} else {
		if wtr.ResultCode == "SUCCESS" {
			success = true
			desc = "提现成功"
		} else {
			success = false
			desc = wtr.ErrCodeDes
			if desc == "" {
				desc = wtr.ErrCode
			}
		}
	}
	return success, desc, nil
}
