package routers

import (
	"net/http"
	"net/url"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type SetRecommendCodeStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (s *SetRecommendCodeStruct) New() common.LogAndUserSetHandler {
	return new(SetRecommendCodeStruct)
}

func (s *SetRecommendCodeStruct) GetAPICode() int {
	return common.API_LOG_CODE_SET_RECOMMAND_CODE
}

func (s *SetRecommendCodeStruct) SetLog(yl common.APIRecord) {
	s.yl = yl
}

func (s *SetRecommendCodeStruct) SetUser(ub common.UserBasic) {
	s.usr = ub
}

func (s *SetRecommendCodeStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token", "recommend_code"}
	return getUserByToken(rq, mustParams)
}

func (s *SetRecommendCodeStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(s.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	if s.usr.From != "" {
		errResp := common.Resp{
			Status:    common.STATUS_ALREADY_BIND_RECOMMEND_CODE,
			Timestamp: time.Now().Unix(),
		}
		s.yl.ErrorMsg = errResp.ErrMsg
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	recommendCode := rq.Get("recommend_code")
	if recommendCode == s.usr.Code {
		errResp := common.Resp{
			Status:    common.STATUS_BIND_SELF,
			Timestamp: time.Now().Unix(),
		}
		s.yl.ErrorMsg = errResp.ErrMsg
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	master, err := db.GetUserInfo(appID, bson.M{"code": recommendCode})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		s.yl.ErrorMsg = err.Error()
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if master == nil {
		errResp := common.Resp{
			Status:    common.STATUS_RECOMMEND_CODE_INVALID,
			Timestamp: time.Now().Unix(),
		}
		s.yl.ErrorMsg = errResp.ErrMsg
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if master.From == s.usr.ID {
		errResp := common.Resp{
			Status:    common.STATUS_APPRENTICE_CIRCLED,
			Timestamp: time.Now().Unix(),
		}
		s.yl.ErrorMsg = errResp.ErrMsg
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	nowTimestamp := time.Now().Unix()
	if err := db.UpdateUser(appID, bson.M{"id": s.usr.ID}, bson.M{"$set": bson.M{"from": master.ID, "from_timestamp": nowTimestamp}}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		s.yl.ErrorMsg = err.Error()
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	go db.SetUserApprenticeReward(appID, master.ID, s.usr.ID, nowTimestamp)

	if err := db.SetUserApprentice(appID, master.ID, s.usr.ID, 3, nowTimestamp); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		s.yl.ErrorMsg = err.Error()
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if err := db.InitApprenticeMiddle(appID, s.usr.ID, master.ID, false, nowTimestamp); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		s.yl.ErrorMsg = err.Error()
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	go func() {
		t := task{
			appID:      appID,
			usr:        *master,
			taskType:   common.TASK_TYPE_NEWBIE,
			taskID:     "3",
			timestamp:  nowTimestamp,
			appVersion: rq.Get("app_version"),
		}
		t.complete()
	}()
	go func() {
		t := task{
			appID:      appID,
			usr:        s.usr,
			taskType:   common.TASK_TYPE_NEWBIE,
			taskID:     "4",
			timestamp:  nowTimestamp,
			appVersion: rq.Get("app_version"),
			ip:         common.GetIPFromRequest(r),
		}
		t.complete()
	}()

	resp := common.SuccessResp(nil)
	s.yl.Ret = resp.String()
	s.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}
