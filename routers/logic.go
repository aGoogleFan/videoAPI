package routers

import (
	"errors"
	"net/url"
	"time"

	// "github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

func getUserByToken(rq url.Values, mustParams []string) (*common.UserBasic, *common.Resp, error) {
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		return nil, errResp, errors.New("params lack")
	}
	appID := rq.Get("app_id")
	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		return nil, &errResp, err
	}
	if userID == "" {
		errResp := common.TokenInvalidResp()
		return nil, &errResp, nil
	}

	usr, err := db.GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		return nil, &errResp, err
	}
	if usr == nil {
		errResp := common.TokenInvalidResp()
		return nil, &errResp, nil
	}

	return usr, nil, nil
}

func getUserByUserID(rq url.Values, mustParams []string) (*common.UserBasic, *common.Resp, error) {
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		return nil, errResp, errors.New("params lack")
	}

	appID := rq.Get("app_id")
	userID := rq.Get("user_id")
	usr, err := db.GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		return nil, &errResp, err
	}

	if usr == nil {
		errResp := common.TokenInvalidResp()
		return nil, &errResp, nil
	}
	return usr, nil, nil
}

// 返回值:
// true/false, 通过/不通过设备限制验证
func CheckDeviceLimit(appID, userID, androidID string) (bool, error) {
	ud, err := db.GetUserDevice(appID, androidID, true, userID)
	if err != nil {
		return false, err
	}

	if ud == nil {
		return true, nil
	}
	confMux.RLock()
	al := conf[appID].AppDeviceAndIPLimitConf
	confMux.RUnlock()
	if al == nil {
		return false, errors.New("device and ip config not found")
	}

	if al.DeviceLimit.AccountLimit == 0 {
		return true, nil
	}

	startTimestamp := int64(0)
	if al.DeviceLimit.Duration > 0 {
		startTimestamp = time.Now().Add(time.Hour * 24 * time.Duration(al.DeviceLimit.Duration) * (-1)).Unix()
	}
	mp := make(map[string]int64) // map[userID]lastTimestamp
	for _, adh := range ud.DeviceHistory {
		if adh.Timestamp <= startTimestamp {
			continue
		}
		mp[adh.UserID] = adh.Timestamp
	}

	userTimestamp, ok := mp[userID]
	if !ok {
		if len(mp) >= al.DeviceLimit.AccountLimit {
			return false, nil
		}
		return true, nil
	}
	var timestamps []int
	for _, v := range mp {
		timestamps = append(timestamps, int(v))
	}
	od := common.OrderInNumber(int(userTimestamp), timestamps)
	if od > al.DeviceLimit.AccountLimit {
		return false, nil
	}

	return true, nil
}

// 返回值:
// true/false, 通过/不通过ip限制验证
func CheckIPLimit(appID, userID, ip string) (bool, error) {
	ui, err := db.GetUserIP(appID, ip, true, userID)
	if err != nil {
		return false, err
	}

	if ui == nil {
		return true, nil
	}
	confMux.RLock()
	al := conf[appID].AppDeviceAndIPLimitConf
	confMux.RUnlock()
	if al == nil {
		return false, errors.New("device and ip config not found")
	}
	if al.IPLimit.AccountLimit == 0 {
		return true, nil
	}

	startTimestamp := int64(0)
	if al.IPLimit.Duration > 0 {
		startTimestamp = time.Now().Add(time.Hour * 24 * time.Duration(al.DeviceLimit.Duration) * (-1)).Unix()
	}
	mp := make(map[string]int64) // map[ip]lastTimestamp
	for _, uih := range ui.DeviceHistory {
		if uih.Timestamp <= startTimestamp {
			continue
		}
		mp[uih.UserID] = uih.Timestamp
	}

	userTimestamp, ok := mp[userID]
	if !ok {
		if len(mp) >= al.IPLimit.AccountLimit {
			return false, nil
		}
		return true, nil
	}
	var timestamps []int
	for _, v := range mp {
		timestamps = append(timestamps, int(v))
	}
	od := common.OrderInNumber(int(userTimestamp), timestamps)
	if od > al.IPLimit.AccountLimit {
		return false, nil
	}

	return true, nil
}

// 微信余额不足时报警
func sendNotEnoughAlarm() {
	alarmed, err := db.IsAlarmSet()
	if err != nil {
		return
	}
	if alarmed {
		return
	}
	sendMessage("101", "13816995089", 0)
}

// 记录子渠
type appChannelRecorder struct {
	appID       string
	userID      string
	channelName string
	recordType  string // user or device

}

func getUserSubmedia(usr common.UserBasic, rq url.Values) string {
	var judge string
	if mobile := usr.Info.Mobile; mobile != "" {
		if mobile == "18721903920" || mobile == "15901829157" || mobile == "18701476121" || mobile == "13163291237" || mobile == "18601740110" {
			return "155"
		}
	}
	if usr.ID != "" {
		judge = usr.ID
	} else {
		judge = rq.Get("android_id")
	}
	if judge == "" {
		return "46"
	}
	temp := common.StringToMD5(judge)
	tempLen := len(temp)
	if tempLen == 0 {
		return "46"
	}
	switch temp[tempLen-1] % 2 {
	case 0:
		return "46"
	case 1:
		return "155"
	default:
		return "46"
	}
}

/*
18721903920
15901829157
18701476121
13163291237

*/
