package routers

import (
	"net/http"
	"net/url"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type ApprenticeBasic struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (a *ApprenticeBasic) New() common.LogAndUserSetHandler {
	return new(ApprenticeBasic)
}

func (a *ApprenticeBasic) GetAPICode() int {
	return common.API_LOG_CODE_APPRENTICE_BASIC
}

func (a *ApprenticeBasic) SetLog(yl common.APIRecord) {
	a.yl = yl
}

func (a *ApprenticeBasic) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (a *ApprenticeBasic) SetUser(u common.UserBasic) {
	a.usr = u
}

func (a *ApprenticeBasic) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(a.yl)
	}()

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	apprenticeNum, err := db.GetMyApprenticeLeaderboardsCount(appID, a.usr.ID, common.NO_LIMIT, common.NO_LIMIT)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		a.yl.ErrorMsg = err.Error()
		a.yl.RetCode = errResp.Status
		a.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	apprenticeTotal, _, err := db.GetUserIncomeTotalByDetail(appID, a.usr.ID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_APPRENTICE)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		a.yl.ErrorMsg = err.Error()
		a.yl.RetCode = errResp.Status
		a.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	validReward, err := db.GetUserApprenticeRewardCountByStatus(appID, a.usr.ID, common.STATUS_USER_APPRENTICE_REWARD_VALID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		a.yl.ErrorMsg = err.Error()
		a.yl.RetCode = errResp.Status
		a.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(ApprenticeBasicToRet{
		Total:            apprenticeTotal,
		ApprenticeNumber: apprenticeNum,
		ValidReward:      validReward,
	})
	a.yl.RetCode = resp.Status
	a.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

type ApprenticeBasicToRet struct {
	Total            int `json:"income_total"`      // 徒弟上供总额
	ApprenticeNumber int `json:"apprentice_number"` // 徒弟数
	ValidReward      int `json:"valid_reward"`      // 可开宝箱数量
}
