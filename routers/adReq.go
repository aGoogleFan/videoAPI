package routers

import (
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type ADReqStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (a *ADReqStruct) New() common.LogAndUserSetHandler {
	return new(ADReqStruct)
}

func (a *ADReqStruct) GetAPICode() int {
	return common.API_LOG_AD_REQ
}

func (a *ADReqStruct) SetLog(yl common.APIRecord) {
	a.yl = yl
}

func (a *ADReqStruct) SetUser(ub common.UserBasic) {
	a.usr = ub
}

func (a *ADReqStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	return getUserByToken(rq, nil)
}

func (a *ADReqStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(a.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	mustParams := []string{"ad_id", "ad_req_id", "ad_res", "ad_res_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		a.yl.RetCode = errResp.Status
		a.yl.ErrorMsg = errResp.ErrMsg
		a.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	ip := common.GetIPFromRequest(r)
	cai := common.ADInfo{
		ADID:       rq.Get("ad_id"),
		ADReqID:    rq.Get("ad_req_id"),
		ADReqType:  common.AD_REQ_TYPE_REQ,
		ADRes:      rq.Get("ad_res"),
		ADResID:    rq.Get("ad_res_id"),
		IP:         ip,
		Timestamp:  time.Now().Unix(),
		AndroidID:  rq.Get("android_id"),
		UserID:     a.usr.ID,
		AppVersion: rq.Get("app_version"),
	}
	go sendADInfo(appID, cai)
	resp := common.SuccessResp(nil)
	a.yl.RetCode = resp.Status
	a.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}
