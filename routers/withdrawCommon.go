package routers

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type withdrawInfo struct {
	AppID        string
	user         common.UserBasic
	Cost         int
	CoinKeepRate float64
	WithdrawRate float64
	RealCost     int
	Amount       int
	Timestamp    int64
	WechatOpenID string
	IP           string
	LevelAuto    bool
	AppVersion   string
}

func WithdrawCommon(w http.ResponseWriter, r *http.Request, withdrawOrderType int, yl common.APIRecord) (*withdrawInfo, common.APIRecord, bool) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return nil, yl, false
	}
	rq := r.URL.Query()
	mustParams := []string{"app_id", "token", "cost", "amount"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	// check cost and amount
	cost, err := strconv.Atoi(rq.Get("cost"))
	if err != nil || cost < 0 {
		errResp := common.OtherErrorResp("invalid cost")
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}
	amount, err := strconv.Atoi(rq.Get("amount"))
	if err != nil || amount < 0 {
		errResp := common.OtherErrorResp("invalid amount")
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	// check rate
	appID := rq.Get("app_id")
	confMux.RLock()
	reh := conf[appID].WithdrawRateConf
	confMux.RUnlock()
	if reh == nil {
		errResp := common.OtherErrorResp("rate not found")
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}
	rateValid, err := checkRate(appID, amount, cost, withdrawOrderType, *reh)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		yl.ErrorMsg = err.Error()
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	if !rateValid {
		errResp := common.RateErrorResp("")
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	// check token
	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		yl.ErrorMsg = err.Error()
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}
	if userID == "" {
		errResp := common.TokenInvalidResp()
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	ui, err := db.GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	yl.UserID = ui.ID
	yl.UserCode = ui.Code
	yl.UserMobile = ui.Info.Mobile
	yl.WechatOpenID = ui.ThirdPart.Wechat.OpenID
	yl.AlipayOpenID = ui.ThirdPart.Alipay.ID

	// check remaining
	var realCost int
	var withdrawRate float64
	var coinKeepRate float64
	var setAuto bool
	mwltr := getUserLevelData(appID, userID)
	if mwltr == nil {
		realCost = cost
		withdrawRate = 1
		coinKeepRate = -1
	} else {
		confMux.RLock()
		wsc := conf[appID].WithdrawStandConf
		confMux.RUnlock()
		uwsl := getUserStand(ui.Status, mwltr.Total, *wsc)
		if uwsl == nil {
			realCost = cost
			withdrawRate = 1
			coinKeepRate = -1
		} else {
			setAuto = uwsl.IsAutoWithdraw
			tmpCost := float64(cost) * uwsl.WithdrawRate
			if tmpCost == float64(int(tmpCost)) {
				realCost = int(tmpCost)
				withdrawRate = uwsl.WithdrawRate
				coinKeepRate = uwsl.CoinKeepRate
			} else {
				realCost = int(tmpCost) + 1
				withdrawRate = uwsl.WithdrawRate
				coinKeepRate = uwsl.CoinKeepRate
			}
		}
	}
	if ui.IncomeRemaining < realCost {
		errResp := common.Resp{
			Timestamp: time.Now().Unix(),
			Status:    common.STATUS_COIN_NOT_ENOUGH,
		}
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	if ui == nil {
		errResp := common.ServerErrorResp("")
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	//  用户信息确认: 账户必须绑定手机号
	if ui.Info.Mobile == "" {
		errResp := common.Resp{
			Timestamp: time.Now().Unix(),
			Status:    common.STATUS_MOBILE_INVALID,
		}
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	// 用户每日提交申请上限确认
	count, err := db.GetWithdrawDailyTimes(appID, userID, time.Now().Unix())
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}
	if count >= common.WITHDRAW_REQUEST_DAILY_LIMIT {
		errResp := common.Resp{
			Timestamp: time.Now().Unix(),
			Status:    common.STATUS_WITHDRAW_REQ_OUT_OF_LIMIT,
		}
		yl.ErrorMsg = errResp.ErrMsg
		yl.Ret = errResp.String()
		yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return nil, yl, false
	}

	timestamp, err := strconv.ParseInt(rq.Get("timestamp"), 10, 0)
	if err != nil {
		timestamp = time.Now().Unix()
	}

	return &withdrawInfo{
		AppID:        appID,
		user:         *ui,
		Cost:         cost,
		CoinKeepRate: coinKeepRate,
		WithdrawRate: withdrawRate,
		RealCost:     realCost,
		Amount:       amount,
		Timestamp:    timestamp,
		WechatOpenID: ui.ThirdPart.Wechat.OpenID,
		IP:           common.GetIPFromRequest(r),
		LevelAuto:    setAuto,
		AppVersion:   rq.Get("app_version"),
	}, yl, true
}

func checkRate(appID string, amount, cost int, withdrawType int, h common.RateExchangeHistory) (bool, error) {
	for _, r := range h.Rates {
		if !((r.Type == "alipay" && withdrawType == common.ORDER_TYPE_ALIPAY_WITHDRAWA) || (r.Type == "wechat" && withdrawType == common.ORDER_TYPE_WECHAT_WITHDRAWA)) {
			continue
		}
		for _, d := range r.Details {
			if d.Coin == cost && d.Amount == amount {
				return true, nil
			}
		}
	}
	return false, nil
}

// 自动匹配规则
// 返回值: 自动执行规则(自动通过/自动拒绝/转手动), 错误
func autoWithdrawCheck(appID, userID string, registerTimestamp int64, userStatus, withdrawAmount int) (int, error) {
	switch userStatus {
	case common.STATUS_USER_DISABLED, common.STATUS_USER_NO_WITHDRAW, common.STATUS_USER_NO_AUTO_WITHDRAW:
		return common.WITHDRAW_AUTO_DO_PERSON, nil
	default:
		confMux.RLock()
		wac := conf[appID].WithdrawAutoCheck
		confMux.RUnlock()
		if wac == nil {
			return common.WITHDRAW_AUTO_DO_PERSON, nil
		}
		for _, role := range wac.Roles {
			invalid := false
			for _, d := range role.Details {
				flag := false // flag used to judge if the condition detail is suit
				var stand float64
				var err error
				switch d.By {
				case common.WITHDRAW_AUTO_BY_REGISTER_DATE:
					stand = float64(time.Now().Sub(time.Unix(registerTimestamp, 0)).Truncate(time.Hour * 24))
				case common.WITHDRAW_AUTO_BY_READ_PERCENT:
					stand, err = getRateByUser(appID, userID, common.TASK_TYPE_FIXED_BOUNDS)
					if err != nil {
						return -1, err
					}
				case common.WITHDRAW_AUTO_BY_SHARE_PERCENT:
					stand, err = getRateByUser(appID, userID, common.TASK_TYPE_SHARE_ARTICLE_READ)
					if err != nil {
						return -1, err
					}
				case common.WITHDRAW_AUTO_BY_APPRENTICE_PERCENT:
					stand, err = getRateByUser(appID, userID, common.TASK_TYPE_APPRENTICE)
					if err != nil {
						return -1, err
					}
				case common.WITHDRAW_AUTO_BY_APPRENTICE_ONCE_PERCENT:
					stand, err = getRateByUser(appID, userID, common.TASK_TYPE_APPRENTICE_ONCE)
					if err != nil {
						return -1, err
					}
				case common.WITHDRAW_AUTO_BY_APPRENTICE_NUMBER:
					stand, err = getUserApprenticeNumber(appID, userID)
					if err != nil {
						return -1, err
					}
				case common.WITHDRAW_AUTO_BY_WITHDRAW_TIMES:
					stand, err = getUserWithdrawTimes(appID, userID)
					if err != nil {
						return -1, err
					}
				case common.WITHDRAW_AUTO_BY_AMOUNT_OF_WITHDRAW:
					stand = float64(withdrawAmount)
				default:
					return -1, fmt.Errorf("unexpected withdraw role by: %d", d.By)
				}
				switch d.JudgedBy {
				case common.WITHDRAW_AUTO_JUDGED_BY_GRATER_THAN:
					flag = (stand > d.Amount)
				case common.WITHDRAW_AUTO_JUDGED_BY_GRATER_THAN_AND_EQUAL:
					flag = (stand >= d.Amount)
				case common.WITHDRAW_AUTO_JUDGED_BY_EQUAL:
					flag = (stand == d.Amount)
				case common.WITHDRAW_AUTO_JUDGED_BY_LESS_THAN_AND_EQUAL:
					flag = (stand <= d.Amount)
				case common.WITHDRAW_AUTO_JUDGED_BY_LESS_THAN:
					flag = (stand < d.Amount)
				case common.WITHDRAW_AUTO_JUDGED_BY_NOT_EQUAL:
					flag = (stand != d.Amount)
				default:
					return -1, fmt.Errorf("unexpected withdraw role judge by: %d", d.JudgedBy)
				}
				if flag {
					continue
				}
				invalid = true
				break
			}
			if invalid {
				continue
			}
			return role.Done, nil
		}
		return common.WITHDRAW_AUTO_DO_PERSON, nil
	}
}

func getRateByUser(appID, userID string, rateBy int) (float64, error) {
	uih, err := db.GetUserIncomeHistory(appID, userID)
	if err != nil {
		return -1, err
	}
	if uih == nil {
		return 0, nil
	}
	if len(uih) == 0 {
		return 0, nil
	}
	var total int
	var stand int
	for _, v := range uih {
		if v.Type != common.INCOME_TYPE_INCOME {
			continue
		}
		total += v.Count
		if v.TaskType != rateBy {
			continue
		}
		stand += v.Count
	}
	if total == 0 {
		return 0, nil
	}
	return float64(stand) / float64(total), nil
}

func getUserApprenticeNumber(appID, userID string) (float64, error) {
	apprentices, err := db.GetMyApprenticeLeaderboards(appID, userID)
	if err != nil {
		return -1, err
	}
	return float64(len(apprentices)), nil
}

func getUserWithdrawTimes(appID, userID string) (float64, error) {
	uo, err := db.GetUserOrder(appID, userID)
	if err != nil {
		return -1, err
	}
	if uo == nil {
		return 0, nil
	}
	return float64(len(uo.History)), nil
}
