package routers

import (
	"encoding/json"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

const (
	MOBILE_CLIENT_REGEXP1 = `(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i`
	MOBILE_CLIENT_REGEXP2 = `1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i`
)

func ArticleShare(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	id := r.URL.Query().Get("id")
	if id == "" {
		return
	}

	ct, err := db.GetFromRedis(id)
	if err != nil {
		return
	}

	var scts shareContentSaveToRedis
	if err := json.Unmarshal([]byte(ct), &scts); err != nil {
		return
	}

	if scts.ID != "" {
		ip := common.GetIPFromRequest(r)
		ua := r.UserAgent()
		referrer := r.Referer()
		if referrer == "" {
			referrer = r.Header.Get("Referrer")
		}
		go func() {
			user, err := db.GetUserInfo(scts.AppID, bson.M{"id": scts.ID})
			if err != nil {
				return
			}
			if user == nil {
				return
			}
			f, err := url.Parse(scts.URL)
			if err != nil {
				return
			}
			oshareTimestamp := f.Query().Get("share_timestamp")
			if oshareTimestamp == "" {
				return
			}
			shareTimestamp, err := strconv.ParseInt(oshareTimestamp, 10, 0)
			if err != nil {
				return
			}
			nowTimestamp := time.Now().Unix()
			if common.GetDateStartUnix(shareTimestamp) != common.GetDateStartUnix(nowTimestamp) {
				return
			}
			setUserArticleShareClicks(scts.AppID, id, *user, scts.URL, ip, ua, referrer, nowTimestamp)
			t := task{
				appID:     scts.AppID,
				usr:       *user,
				taskType:  common.TASK_TYPE_DAILY,
				taskID:    common.TASK_DAILY_ID_ARTICLE_SHARE,
				timestamp: nowTimestamp,
				ip:        common.GetIPFromRequest(r),
			}
			t.complete()
		}()
	}

	http.Redirect(w, r, scts.URL, http.StatusFound)
}

func setUserArticleShareClicks(appID, key string, user common.UserBasic, url, ip, ua, referrer string, timestamp int64) {
	arh, err := db.GetCurrentAppUserReward(appID)
	if err != nil {
		return
	}
	if arh == nil {
		return
	}
	reward := arh.ShareRead
	uas, err := db.GetUserArticleShareClicks(appID, key)
	if err != nil {
		return
	}
	isMobile := checkMobile(ua)
	if !isMobile {
		return
	}

	confMux.RLock()
	arc := conf[appID].AppRewardConf
	confMux.RUnlock()
	if uas == nil {
		if err := db.SaveArticleShareClick(appID, user.ID, key, url, ip, ua, referrer, timestamp, true); err != nil {
			return
		}
		if arc == nil {
			return
		}
		if arc.ReadSharePerArticle == 0 || arc.ReadSharePerUser == 0 {
			return
		}
		if arc.ReadSharePerArticle > 0 || arc.ReadSharePerUser > 0 {
			articleDailyReward, userDailyReward, err := getArticleClickDailyReward(appID, user.ID, key, timestamp)
			if err != nil {
				// TODO:
				return
			}
			if arc.ReadSharePerArticle > 0 && articleDailyReward >= arc.ReadSharePerArticle {
				return
			}
			if arc.ReadSharePerUser > 0 && userDailyReward >= arc.ReadSharePerUser {
				return
			}
		}
		uits := db.UserIncomeToSave{
			AppID:     appID,
			Usr:       user,
			OpType:    common.INCOME_TYPE_INCOME,
			TaskID:    "",
			TaskType:  common.TASK_TYPE_SHARE_ARTICLE_READ,
			Count:     reward,
			Timestamp: timestamp,
			Desc:      "分享文章被阅读奖励",
			SetMaster: false,
			From:      key,
		}
		uits.Save()
		return
	}

	for _, v := range uas.Clicks {
		if v.IP == ip && v.UA == ua {
			return
		}
	}

	if err := db.SaveArticleShareClick(appID, user.ID, key, url, ip, ua, referrer, timestamp, false); err != nil {
		return
	}

	if arc == nil {
		return
	}
	if arc.ReadSharePerArticle == 0 || arc.ReadSharePerUser == 0 {
		return
	}

	if arc.ReadSharePerArticle > 0 || arc.ReadSharePerUser > 0 {
		articleDailyReward, userDailyReward, err := getArticleClickDailyReward(appID, user.ID, key, timestamp)
		if err != nil {
			// TODO:
			return
		}
		if arc.ReadSharePerArticle > 0 && articleDailyReward >= arc.ReadSharePerArticle {
			return
		}
		if arc.ReadSharePerUser > 0 && userDailyReward >= arc.ReadSharePerUser {
			return
		}
	}

	uits := db.UserIncomeToSave{
		AppID:     appID,
		Usr:       user,
		OpType:    common.INCOME_TYPE_INCOME,
		TaskID:    "",
		TaskType:  common.TASK_TYPE_SHARE_ARTICLE_READ,
		Count:     reward,
		Timestamp: timestamp,
		Desc:      "分享文章被阅读奖励",
		SetMaster: false,
		From:      key,
	}
	uits.Save()
}

func checkMobile(ua string) bool {
	ua = strings.ToLower(ua)
	match, err := regexp.MatchString(MOBILE_CLIENT_REGEXP1, ua)
	if err == nil && match {
		return true
	}
	if len(ua) >= 4 {
		ua = ua[0:4]
	}
	match, err = regexp.MatchString(MOBILE_CLIENT_REGEXP2, ua)
	if err != nil {
		return false
	}
	return match
}

func getArticleClickDailyReward(appID, userID, articleID string, timestamp int64) (int, int, error) {
	userDailyTotal, _, err := db.GetUserDailyAdShare(appID, userID, common.TOTAL_CONF_USER_ARTICLE, timestamp)
	if err != nil {
		return 0, 0, err
	}
	articleDailytotal, _, err := db.GetUserDailyAdShare(appID, userID, articleID, timestamp)
	if err != nil {
		return 0, 0, err
	}
	return articleDailytotal, userDailyTotal, nil
}
