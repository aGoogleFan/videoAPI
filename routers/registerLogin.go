package routers

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type RegisterLoginStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (rls *RegisterLoginStruct) New() common.LogAndUserSetHandler {
	return new(RegisterLoginStruct)
}

func (rls *RegisterLoginStruct) GetAPICode() int {
	return common.API_LOG_CODE_USER_REGISTER_LOGIN
}

func (rls *RegisterLoginStruct) SetLog(yl common.APIRecord) {
	rls.yl = yl
}

func (rls *RegisterLoginStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"phone_number", "m_v_code"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		return nil, errResp, errors.New("params lack")
	}

	appID := rq.Get("app_id")
	if appID == "" {
		appID = "101"
	}

	mobile := rq.Get("phone_number")
	usr, err := db.GetUserInfo(appID, bson.M{"info.mobile": mobile})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		return nil, &errResp, err
	}

	return usr, nil, nil
}

func (rls *RegisterLoginStruct) SetUser(u common.UserBasic) {
	rls.usr = u
}

func (rls *RegisterLoginStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	defer func() {
		yxwLog.RecordLog(rls.yl)
	}()

	rq := r.URL.Query()
	mustParams := []string{"phone_number", "m_v_code"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	if appID == "" {
		appID = "101"
	}

	mobile := rq.Get("phone_number")
	mvCode := rq.Get("m_v_code")
	// 后门 用于测试
	// 开启后门后, 会自动进入账户, 且不会触发ip限定和设备限定
	// 测试完记得及时关闭!
	var backDoorValid bool
	if mobile == "18601763847" || mobile == "18521708501" || mobile == "18601740110" {
		backDoorValid = true
	}
	if mobile == "13816995089" && mvCode == "890603" {
		backDoorValid = true
	}

	// check m_v_code
	if !backDoorValid {
		key := "msg_" + mobile
		code, err := db.GetFromRedis(key)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			rls.yl.ErrorMsg = err.Error()
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		if mvCode != code {
			errResp := common.Resp{
				Status:    common.STATUS_MSM_INVALID,
				Timestamp: time.Now().Unix(),
			}
			rls.yl.ErrorMsg = errResp.ErrMsg
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
	}
	token := common.NewUUID()
	ip := common.GetIPFromRequest(r)
	nowUnix := time.Now().Unix()

	// 如果是新用户
	if rls.usr.ID == "" {
		var image string
		radomImages, err := db.GetRadomImages(appID)
		if err == nil {
			image = radomImages.Images[nowUnix%int64(len(radomImages.Images))].URL
		}
		gName, err := db.GenRegisterUserName(appID)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			rls.yl.ErrorMsg = err.Error()
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		ub := common.UserBasic{
			ID:   common.NewUUID(),
			Code: gName,
			Info: common.UserInfo{
				Name:   gName,
				Icon:   image,
				Mobile: mobile,
			},
			Create:    nowUnix,
			LastLogin: nowUnix,
		}

		if err := db.SaveNewUser(appID, ub); err != nil {
			log.Fields{
				"appID":     appID,
				"userBasic": ub,
				"err":       err.Error(),
			}.Error("mongo error")
			errResp := common.ServerErrorResp(err.Error())
			rls.yl.ErrorMsg = err.Error()
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		rls.yl.UserID = ub.ID
		rls.yl.UserMobile = ub.Info.Mobile
		rls.yl.UserCode = ub.Code

		if err := db.SaveToken(token, ub.ID, common.REDIS_KEY_VALID_DURATION_TOKEN); err != nil {
			log.Fields{
				"token":  token,
				"userid": ub.ID,
				"err":    err.Error(),
			}.Error("redis error")
			errResp := common.ServerErrorResp(err.Error())
			rls.yl.ErrorMsg = err.Error()
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		if recommendCode := rq.Get("recommend_code"); recommendCode != "" {
			if recommendCode == ub.Code {
				errResp := common.OtherErrorResp("cannot set yourself")
				rls.yl.ErrorMsg = errResp.ErrMsg
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}
			master, err := db.GetUserInfo(appID, bson.M{"code": recommendCode})
			if err != nil {
				errResp := common.ServerErrorResp(err.Error())
				rls.yl.ErrorMsg = err.Error()
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}
			if master == nil {
				errResp := common.Resp{
					Status:    common.STATUS_RECOMMEND_CODE_INVALID,
					Timestamp: time.Now().Unix(),
				}
				rls.yl.ErrorMsg = errResp.ErrMsg
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}

			if err := db.UpdateUser(appID, bson.M{"id": ub.ID}, bson.M{"$set": bson.M{"from": master.ID}}); err != nil {
				errResp := common.ServerErrorResp(err.Error())
				rls.yl.ErrorMsg = err.Error()
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}

			go db.SetUserApprenticeReward(appID, master.ID, ub.ID, nowUnix)

			nowTimestamp := time.Now().Unix()
			if err := db.SetUserApprentice(appID, master.ID, ub.ID, 3, nowTimestamp); err != nil {
				errResp := common.ServerErrorResp(err.Error())
				rls.yl.ErrorMsg = err.Error()
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}

			if err := db.InitApprenticeMiddle(appID, ub.ID, master.ID, false, nowTimestamp); err != nil {
				errResp := common.ServerErrorResp(err.Error())
				rls.yl.ErrorMsg = err.Error()
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}
			go func() {
				t := task{
					appID:      appID,
					usr:        *master,
					taskType:   common.TASK_TYPE_NEWBIE,
					taskID:     common.TASK_NEWBIE_ID_FIRST_APPRENTICE,
					timestamp:  nowTimestamp,
					appVersion: rq.Get("app_version"),
				}
				t.complete()
			}()
			go func() {
				t := task{
					appID:      appID,
					usr:        ub,
					taskType:   common.TASK_TYPE_NEWBIE,
					taskID:     common.TASK_NEWBIE_ID_SET_RECOMMEND_CODE,
					timestamp:  nowTimestamp,
					appVersion: rq.Get("app_version"),
					ip:         common.GetIPFromRequest(r),
				}
				t.complete()
			}()
			go func() {
				key := fmt.Sprintf("task_daily_apprentice_%s", master.ID)
				value, err := db.GetFromRedis(key)
				if err != nil {
					return
				}
				ts, err := strconv.ParseInt(value, 10, 0)
				if err != nil {
					return
				}
				if common.GetDateStartUnix(ts) != common.GetDateStartUnix(nowTimestamp) {
					return
				}
				t := task{
					appID:      appID,
					usr:        *master,
					taskType:   common.TASK_TYPE_DAILY,
					taskID:     common.TASK_DAILY_ID_APPRENTICE,
					timestamp:  nowTimestamp,
					appVersion: rq.Get("app_version"),
				}
				t.complete()
			}()
		}

		if androidID := rq.Get("android_id"); androidID != "" && !backDoorValid {
			devicePassed, err := CheckDeviceLimit(appID, ub.ID, androidID)
			if err != nil {
				errResp := common.ServerErrorResp(err.Error())
				rls.yl.ErrorMsg = err.Error()
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}
			if !devicePassed {
				errResp := common.Resp{
					Status:    common.STATUS_DEVICE_LIMIT,
					Timestamp: nowUnix,
				}
				rls.yl.ErrorMsg = errResp.ErrMsg
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}

			ipPassed, err := CheckIPLimit(appID, ub.ID, ip)
			if err != nil {
				errResp := common.ServerErrorResp(err.Error())
				rls.yl.ErrorMsg = err.Error()
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}
			if !ipPassed {
				errResp := common.Resp{
					Status:    common.STATUS_IP_LIMIT,
					Timestamp: nowUnix,
				}
				rls.yl.ErrorMsg = errResp.ErrMsg
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}

			if err := db.SaveUserIP(appID, ip, ub.ID, time.Now().Unix()); err != nil {
				errResp := common.ServerErrorResp(err.Error())
				rls.yl.ErrorMsg = err.Error()
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}

			if err := db.SaveUserDevice(appID, androidID, ub.ID, time.Now().Unix()); err != nil {
				errResp := common.ServerErrorResp(err.Error())
				rls.yl.ErrorMsg = err.Error()
				rls.yl.Ret = errResp.String()
				rls.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}
		}

		rsp := common.SuccessResp(struct {
			Token string `json:"token"`
			Code  string `json:"code"`
			IsNew bool   `json:"is_new"`
		}{
			Token: token,
			Code:  ub.Code,
			IsNew: true,
		})
		rls.yl.Ret = rsp.String()
		rls.yl.RetCode = rsp.Status
		http.Error(w, rsp.String(), http.StatusOK)
		return
	}

	// 如果是老用户
	if err := db.UpdateUser(appID, bson.M{"id": rls.usr.ID}, bson.M{"$set": bson.M{"last_login": time.Now().Unix()}}); err != nil {
		log.Fields{
			"token":  token,
			"userid": rls.usr.ID,
			"err":    err.Error(),
		}.Error("mongo error")
		errResp := common.ServerErrorResp(err.Error())
		rls.yl.ErrorMsg = err.Error()
		rls.yl.Ret = errResp.String()
		rls.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if err := db.SaveToken(token, rls.usr.ID, common.REDIS_KEY_VALID_DURATION_TOKEN); err != nil {
		log.Fields{
			"token":  token,
			"userid": rls.usr.ID,
			"err":    err.Error(),
		}.Error("redis error")
		errResp := common.ServerErrorResp(err.Error())
		rls.yl.ErrorMsg = err.Error()
		rls.yl.Ret = errResp.String()
		rls.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if androidID := rq.Get("android_id"); androidID != "" && !backDoorValid {
		devicePassed, err := CheckDeviceLimit(appID, rls.usr.ID, androidID)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			rls.yl.ErrorMsg = err.Error()
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if !devicePassed {
			errResp := common.Resp{
				Status:    common.STATUS_DEVICE_LIMIT,
				Timestamp: nowUnix,
			}
			rls.yl.ErrorMsg = errResp.ErrMsg
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		ipPassed, err := CheckIPLimit(appID, rls.usr.ID, ip)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			rls.yl.ErrorMsg = err.Error()
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if !ipPassed {
			errResp := common.Resp{
				Status:    common.STATUS_IP_LIMIT,
				Timestamp: nowUnix,
			}
			rls.yl.ErrorMsg = errResp.ErrMsg
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		if err := db.SaveUserIP(appID, ip, rls.usr.ID, time.Now().Unix()); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			rls.yl.ErrorMsg = err.Error()
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		if err := db.SaveUserDevice(appID, androidID, rls.usr.ID, time.Now().Unix()); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			rls.yl.ErrorMsg = err.Error()
			rls.yl.Ret = errResp.String()
			rls.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
	}

	rsp := common.SuccessResp(struct {
		Token string `json:"token"`
		Code  string `json:"code"`
		IsNew bool   `json:"is_new"`
	}{
		Token: token,
		Code:  rls.usr.Code,
		IsNew: false,
	})

	rls.yl.Ret = rsp.String()
	rls.yl.RetCode = rsp.Status
	http.Error(w, rsp.String(), http.StatusOK)
}
