package routers

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetPriodRewardStruct struct {
	usr common.UserBasic
	yl  common.APIRecord
}

func (g *GetPriodRewardStruct) New() common.LogAndUserSetHandler {
	return new(GetPriodRewardStruct)
}

func (g *GetPriodRewardStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_PERIOD_REWARD
}

func (g *GetPriodRewardStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetPriodRewardStruct) SetUser(usr common.UserBasic) {
	g.usr = usr
}

func (g *GetPriodRewardStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"token", "app_id"}
	return getUserByToken(rq, mustParams)
}

func (g *GetPriodRewardStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	appVersion := strings.ToLower(rq.Get("app_version"))
	appVersion = strings.TrimPrefix(appVersion, "v")
	appVersion = strings.TrimPrefix(appVersion, "b")
	arh, err := db.GetCurrentAppUserReward(appID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if arh == nil {
		resp := getPeriodRewardValueByVersion(appID, appVersion, 0)
		g.yl.Ret = resp.String()
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	reward := arh.TimeDuration

	confMux.RLock()
	arc := conf[appID].AppRewardConf
	confMux.RUnlock()
	if arc == nil {
		errResp := common.OtherErrorResp("reward daily limit not found")
		g.yl.ErrorMsg = errResp.String()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	periodRewardDailyLimit := arc.PeriodReward
	if periodRewardDailyLimit == 0 {
		errResp := common.RewardLimitResp()
		g.yl.ErrorMsg = errResp.String()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	key := fmt.Sprintf("period:%s:%s", appID, g.usr.ID)
	d, err := db.CheckIfExpired(key)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	nowTimestamp := time.Now().Unix()
	if !d {
		resp := common.Resp{
			Status:    common.STATUS_NOT_IN_ACTIVE_TIME,
			Timestamp: nowTimestamp,
		}
		g.yl.ErrorMsg = resp.ErrMsg
		g.yl.Ret = resp.String()
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	value := strconv.FormatInt(nowTimestamp, 10)
	timeOut := time.Hour
	if err := db.SaveToRedis(key, value, timeOut); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if periodRewardDailyLimit > 0 {
		userDailyPeriodReward, _, err := db.GetUserDailyIncomeByType(appID, g.usr.ID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_HOURLY_REWARD, nowTimestamp)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if userDailyPeriodReward >= periodRewardDailyLimit {
			errResp := common.RewardLimitResp()
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
	}

	uits := db.UserIncomeToSave{
		AppID:      appID,
		Usr:        g.usr,
		OpType:     common.INCOME_TYPE_INCOME,
		TaskID:     "",
		TaskType:   common.TASK_TYPE_HOURLY_REWARD,
		Count:      reward,
		Timestamp:  nowTimestamp,
		Desc:       "时段奖励",
		SetMaster:  false,
		AppVersion: rq.Get("app_version"),
		IP:         common.GetIPFromRequest(r),
	}
	if err := uits.Save(); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	g.yl.Reward = reward
	resp := getPeriodRewardValueByVersion(appID, appVersion, reward)
	g.yl.Ret = resp.String()
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)

	go func() {
		t := task{
			appID:      appID,
			usr:        g.usr,
			taskType:   common.TASK_TYPE_NEWBIE,
			taskID:     common.TASK_NEWBIE_ID_FIRST_PERIOD_REWARD,
			timestamp:  nowTimestamp,
			appVersion: rq.Get("app_version"),
			ip:         common.GetIPFromRequest(r),
		}
		t.complete()
	}()
	go func() {
		_, count, err := db.GetUserDailyIncomeByType(appID, g.usr.ID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_HOURLY_REWARD, nowTimestamp)
		if err != nil {
			return
		}
		if count >= 5 {
			t := task{
				appID:      appID,
				usr:        g.usr,
				taskType:   common.TASK_TYPE_DAILY,
				taskID:     common.TASK_DAILY_ID_PERIOD_GTE_5,
				timestamp:  nowTimestamp,
				appVersion: rq.Get("app_version"),
				ip:         common.GetIPFromRequest(r),
			}
			t.complete()
		}
	}()
}

func getPeriodRewardValueByVersion(appID, appVersion string, reward int) common.Resp {

	confMux.RLock()
	rd := conf[appID].RadomNews
	confMux.RUnlock()
	return common.SuccessResp(
		struct {
			Reward int           `json:"reward"`
			News   []interface{} `json:"news"`
		}{
			Reward: reward,
			News:   rd,
		})

}
