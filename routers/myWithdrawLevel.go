package routers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"sort"
	"time"
	"vlion/yxw/db"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type MyWithdrawLevel struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (m *MyWithdrawLevel) New() common.LogAndUserSetHandler {
	return new(MyWithdrawLevel)
}

func (m *MyWithdrawLevel) GetAPICode() int {
	return common.API_LOG_CODE_GET_MY_WITHDRAW_LEVEL
}

func (m *MyWithdrawLevel) SetLog(yl common.APIRecord) {
	m.yl = yl
}

func (m *MyWithdrawLevel) SetUser(usr common.UserBasic) {
	m.usr = usr
}

func (m *MyWithdrawLevel) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (m *MyWithdrawLevel) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(m.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	ud := getUserLevelData(appID, m.usr.ID)
	if ud == nil {
		errResp := common.Resp{
			Status:    common.STATUS_DATA_TO_BE_STATISTICAL,
			Timestamp: time.Now().Unix(),
		}
		m.yl.ErrorMsg = errResp.ErrMsg
		m.yl.RetCode = errResp.Status
		m.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	resp := common.SuccessResp(ud)
	m.yl.RetCode = resp.Status
	m.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

type myWithdrawLevelToReturn struct {
	Total   float64                `json:"total"`
	Details []common.UserLevelData `json:"details"`
}

func getUserLevelData(appID, userID string) *myWithdrawLevelToReturn {
	nowUnix := time.Now().Unix()
	t := common.GetDateStartUnix(nowUnix)
	confMux.RLock()
	uld := conf[appID].WithdrawLevelData
	confMux.RUnlock()
	if uld == nil {
		go refreshUserLevelData(appID)
		return nil
	}
	if uld.UL == nil {
		go refreshUserLevelData(appID)
		return nil
	}
	switch uld.UL.Timestamp {
	case common.GetDateStartUnix(time.Unix(nowUnix, 0).Add(time.Hour * (-24)).Unix()):
		go refreshUserLevelData(appID)
		fallthrough
	case t:
		uwrwp := getUserWithdrawRole(appID)
		if uwrwp == nil {
			uwrwp = &userWithdrawRoleWeightProbability{}
		}
		var mwltr myWithdrawLevelToReturn
		var totalScore float64
		var ul common.UserLevelData
		if apprentice, ok := uld.UL.Apprentices[userID]; ok {
			ul = apprentice
		} else {
			ul = common.UserLevelData{
				Name:   common.USER_WITHDRAW_LEVEL_STAND_APPRENTICE,
				Weight: uwrwp.ApprenticeWeight,
			}
		}
		mwltr.Details = append(mwltr.Details, ul)
		totalScore += ul.Score

		if mission, ok := uld.UL.MissionScores[userID]; ok {
			ul = mission
		} else {
			ul = common.UserLevelData{
				Name:   common.USER_WITHDRAW_LEVEL_STAND_TASK,
				Weight: uwrwp.TaskCompleteWeight,
			}
		}
		mwltr.Details = append(mwltr.Details, ul)
		totalScore += ul.Score

		if readScore, ok := uld.UL.ReadScores[userID]; ok {
			ul = readScore
		} else {
			ul = common.UserLevelData{
				Name:   common.USER_WITHDRAW_LEVEL_STAND_READ,
				Weight: uwrwp.ReadWeight,
			}
		}
		mwltr.Details = append(mwltr.Details, ul)
		totalScore += ul.Score

		if validApprentice, ok := uld.UL.ValidApprentice[userID]; ok {
			ul = validApprentice
		} else {
			ul = common.UserLevelData{
				Name:   common.USER_WITHDRAW_LEVEL_STAND_VALID_APPRENTICE_NUM,
				Weight: uwrwp.ValidApprenticeNumWeight,
			}
		}
		mwltr.Details = append(mwltr.Details, ul)
		totalScore += ul.Score

		mwltr.Total = totalScore
		return &mwltr
	default:
		go refreshUserLevelData(appID)
		return nil
	}
}

func refreshUserLevelData(appID string) {
	ul, err := getNewUL(appID, common.GetDateStartUnix(time.Now().Unix()))
	if err != nil {
		return
	}
	if ul == nil {
		return
	}
	confMux.Lock()
	ca := conf[appID]
	if ca.WithdrawLevelData == nil {
		ca.WithdrawLevelData = new(withdrawLevelData)
	}
	ca.WithdrawLevelData.UL = ul
	conf[appID] = ca
	confMux.Unlock()
}

func getNewUL(appID string, timestamp int64) (*common.UserLevel, error) {
	uwrwp := getUserWithdrawRole(appID)
	if uwrwp == nil {
		return nil, nil
	}
	fileName := fmt.Sprintf("./aid/withdrawLevelStatistics/t%s_%s", time.Unix(timestamp, 0).Local().Format("2006-01-02"), appID)
	f, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	ct, err := ioutil.ReadAll(f)
	f.Close()
	if err != nil {
		return nil, err
	}
	var uwt common.UserWithdrawLevelTotal
	if err := json.Unmarshal(ct, &uwt); err != nil {
		return nil, err
	}
	tasks, err := db.GetTaskList(appID, common.TASK_TYPE_DAILY)
	if err != nil {
		return nil, err
	}
	for i := range uwt.Apprentice {
		uwt.Apprentice[i].Avg = float64(uwt.Apprentice[i].Num) / float64(uwt.Apprentice[i].Days)
	}
	for i := range uwt.Read {
		uwt.Read[i].Avg = float64(uwt.Read[i].Num) / float64(uwt.Read[i].Days)
	}
	for i := range uwt.ValidApprenticeNum {
		uwt.ValidApprenticeNum[i].Avg = float64(uwt.ValidApprenticeNum[i].Num) / float64(uwt.ValidApprenticeNum[i].Days)
	}
	for i := range uwt.TasksComplete {
		var taskTotal int
		for _, taskID := range uwt.TasksComplete[i].Tasks {
			for _, task := range tasks {
				if task.ID != taskID {
					continue
				}
				taskTotal += task.Weight
			}
		}
		uwt.TasksComplete[i].Avg = float64(taskTotal) / float64(uwt.TasksComplete[i].Days)
	}
	sort.Sort(userLevelDatas(uwt.Apprentice))
	sort.Sort(userLevelDatas(uwt.Read))
	sort.Sort(userLevelDatas(uwt.TasksComplete))
	sort.Sort(userLevelDatas(uwt.ValidApprenticeNum))
	var ul common.UserLevel
	ul.Timestamp = timestamp
	ul.MissionScores = make(map[string]common.UserLevelData)
	taskCompleteLen := len(uwt.TasksComplete)
	for i, a := range uwt.TasksComplete {
		uld := common.UserLevelData{
			Name:      common.USER_WITHDRAW_LEVEL_STAND_TASK,
			Value:     int(float64(i) / float64(taskCompleteLen) * 10),
			Rank:      taskCompleteLen - i,
			RankTotal: taskCompleteLen,
			Weight:    uwrwp.TaskCompleteWeight,
		}
		uld.Score = float64(uld.Value) * uwrwp.TaskCompleteProbability
		ul.MissionScores[a.UserID] = uld
	}
	ul.Apprentices = make(map[string]common.UserLevelData)
	apprenticesLen := len(uwt.Apprentice)
	for i, a := range uwt.Apprentice {
		uld := common.UserLevelData{
			Name:      common.USER_WITHDRAW_LEVEL_STAND_APPRENTICE,
			Value:     int(float64(i) / float64(apprenticesLen) * 10),
			Rank:      apprenticesLen - i,
			RankTotal: apprenticesLen,
			Weight:    uwrwp.ApprenticeWeight,
		}
		uld.Score = float64(uld.Value) * uwrwp.ApprenticeProbability
		ul.Apprentices[a.UserID] = uld
	}
	ul.ReadScores = make(map[string]common.UserLevelData)
	readLen := len(uwt.Read)
	for i, a := range uwt.Read {
		uld := common.UserLevelData{
			Name:      common.USER_WITHDRAW_LEVEL_STAND_READ,
			Value:     int(float64(i) / float64(readLen) * 10),
			Rank:      readLen - i,
			RankTotal: readLen,
			Weight:    uwrwp.ReadWeight,
		}
		uld.Score = float64(uld.Value) * uwrwp.ReadProbability
		ul.ReadScores[a.UserID] = uld
	}
	ul.ValidApprentice = make(map[string]common.UserLevelData)
	validApprenticeLen := len(uwt.ValidApprenticeNum)
	for i, a := range uwt.ValidApprenticeNum {
		uld := common.UserLevelData{
			Name:      common.USER_WITHDRAW_LEVEL_STAND_VALID_APPRENTICE_NUM,
			Value:     int(float64(i) / float64(validApprenticeLen) * 10),
			Rank:      validApprenticeLen - i,
			RankTotal: validApprenticeLen,
			Weight:    uwrwp.ValidApprenticeNumWeight,
		}
		uld.Score = float64(uld.Value) * uwrwp.ValidApprenticeNumProbability
		ul.ValidApprentice[a.UserID] = uld
	}
	ul.Timestamp = timestamp
	return &ul, nil
}

type userWithdrawRoleWeightProbability struct {
	ReadWeight                    int
	ReadProbability               float64
	ApprenticeWeight              int
	ApprenticeProbability         float64
	TaskCompleteWeight            int
	TaskCompleteProbability       float64
	ValidApprenticeNumWeight      int
	ValidApprenticeNumProbability float64

	AdClickWeight      int
	AdClickProbability float64
}

func getUserWithdrawRole(appID string) *userWithdrawRoleWeightProbability {
	confMux.RLock()
	ulf := conf[appID].WithdrawLevelConf
	confMux.RUnlock()
	if ulf == nil {
		return nil
	}
	var totalWeight int
	for _, uwlw := range ulf.Weights {
		if uwlw.Weight >= 0 {
			totalWeight += uwlw.Weight
		}
	}
	if totalWeight == 0 {
		return nil
	}
	var uwrwp userWithdrawRoleWeightProbability
	for _, uwlw := range ulf.Weights {
		switch uwlw.ID {
		case 1:
			if uwlw.Weight <= 0 {
				uwrwp.TaskCompleteWeight = 0
				uwrwp.TaskCompleteProbability = 0
			} else {
				uwrwp.TaskCompleteWeight = uwlw.Weight
				uwrwp.TaskCompleteProbability = float64(uwlw.Weight) / float64(totalWeight)
			}
		case 2:
			if uwlw.Weight <= 0 {
				uwrwp.ReadWeight = 0
				uwrwp.ReadProbability = 0
			} else {
				uwrwp.ReadWeight = uwlw.Weight
				uwrwp.ReadProbability = float64(uwlw.Weight) / float64(totalWeight)
			}
		case 3:
			if uwlw.Weight <= 0 {
				uwrwp.ApprenticeWeight = 0
				uwrwp.ApprenticeProbability = 0
			} else {
				uwrwp.ApprenticeWeight = uwlw.Weight
				uwrwp.ApprenticeProbability = float64(uwlw.Weight) / float64(totalWeight)
			}
		case 4:
			if uwlw.Weight <= 0 {
				uwrwp.ValidApprenticeNumWeight = 0
				uwrwp.ValidApprenticeNumProbability = 0
			} else {
				uwrwp.ValidApprenticeNumWeight = uwlw.Weight
				uwrwp.ValidApprenticeNumProbability = float64(uwlw.Weight) / float64(totalWeight)
			}
		}
	}
	return &uwrwp
}

type userLevelDatas []common.UserWithdrawLevelData

func (u userLevelDatas) Len() int           { return len(u) }
func (u userLevelDatas) Swap(i, j int)      { u[i], u[j] = u[j], u[i] }
func (u userLevelDatas) Less(i, j int) bool { return u[i].Avg < u[j].Avg }
