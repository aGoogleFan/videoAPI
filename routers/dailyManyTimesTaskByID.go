package routers

import (
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type DailyManyTimesTaskByID struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (d *DailyManyTimesTaskByID) New() common.LogAndUserSetHandler {
	return new(DailyManyTimesTaskByID)
}

func (d *DailyManyTimesTaskByID) GetAPICode() int {
	return 0
}

func (d *DailyManyTimesTaskByID) SetLog(yl common.APIRecord) {
	d.yl = yl
}

func (d *DailyManyTimesTaskByID) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token", "task_id"}
	return getUserByToken(rq, mustParams)
}

func (d *DailyManyTimesTaskByID) SetUser(u common.UserBasic) {
	d.usr = u
}

func (d *DailyManyTimesTaskByID) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	confMux.Lock()
	dailyManyTimesTasks := conf[appID].DailyManyTimesTasks
	confMux.Unlock()
	taskID := rq.Get("task_id")
	t := time.Now().Unix()
	var interval int
	var taskFound bool
	for _, task := range dailyManyTimesTasks {
		if task.TaskID == taskID {
			interval = task.Interval
			taskFound = true
			break
		}
	}
	if !taskFound {
		resp := common.Resp{
			Status:    common.STATUS_TASK_INVALID,
			Timestamp: t,
		}
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	spcs, err := db.GetUserSpecialTaskComplete(appID, d.usr.ID, taskID, common.SPECIAL_TASK_TYPE_MULTY, t)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	var lastComplete int64
	for _, spc := range spcs {
		if spc.FTimestamp > lastComplete {
			lastComplete = spc.FTimestamp
		}
	}
	passSeconds := int(t - lastComplete)
	var timeInterval int
	if passSeconds >= interval {
		timeInterval = 0
	} else {
		timeInterval = interval - passSeconds
	}

	resp := common.SuccessResp(struct {
		TimeInterval int `json:"time_interval"`
	}{
		TimeInterval: timeInterval,
	})

	http.Error(w, resp.String(), http.StatusOK)
}
