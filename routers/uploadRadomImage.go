package routers

import (
	"net/http"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

func UploadRadomImage(w http.ResponseWriter, r *http.Request) {
	path := "radomimage"

	appID, fileURL, needContinue, _ := UploadInnerImage(w, r, path, false, "", nil)
	if !needContinue {
		return
	}
	http.Error(w, common.SuccessResp(fileURL).String(), http.StatusOK)
	go db.SaveRadomImage(appID, fileURL)
}
