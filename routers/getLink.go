package routers

import (
	"net/http"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type GetLinkStruct struct {
	yl common.APIRecord
}

func (g *GetLinkStruct) New() common.LogSetHandler {
	return new(GetLinkStruct)
}

func (g *GetLinkStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_LINK
}

func (g *GetLinkStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetLinkStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	mustParams := []string{"app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")

	confMux.RLock()
	auh := conf[appID].AppURLConf
	confMux.RUnlock()
	if auh == nil {
		resp := common.SuccessResp(common.AppUrlHistory{})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(auh)
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}
