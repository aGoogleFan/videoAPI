package routers

import (
	"net/http"
	"net/url"

	"vlion/yxw/common"
)

type TempStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (a *TempStruct) New() common.LogAndUserSetHandler {
	return new(TempStruct)
}

func (a *TempStruct) GetAPICode() int {
	return common.API_LOG_CODE_APPRENTICE_BASIC
}

func (a *TempStruct) SetLog(yl common.APIRecord) {
	a.yl = yl
}

func (a *TempStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (a *TempStruct) SetUser(u common.UserBasic) {
	a.usr = u
}

func (a *TempStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {

}
