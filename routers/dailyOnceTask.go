package routers

import (
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type DailyOnceTask struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (d *DailyOnceTask) New() common.LogAndUserSetHandler {
	return new(DailyOnceTask)
}

func (d *DailyOnceTask) GetAPICode() int {
	return 0
}

func (d *DailyOnceTask) SetLog(yl common.APIRecord) {
	d.yl = yl
}

func (d *DailyOnceTask) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (d *DailyOnceTask) SetUser(u common.UserBasic) {
	d.usr = u
}

func (d *DailyOnceTask) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	confMux.Lock()
	dailyOnceTasks := conf[appID].DailyOnceTasks
	confMux.Unlock()
	t := time.Now().Unix()
	for i, dailyOnceTask := range dailyOnceTasks {
		spcs, err := db.GetUserSpecialTaskComplete(appID, d.usr.ID, dailyOnceTask.TaskID, common.SPECIAL_TASK_TYPE_ONCE, t)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if len(spcs) >= 1 {
			dailyOnceTask.Status = 1
		}
		dailyOnceTasks[i] = dailyOnceTask
	}

	spt := dailyOnceTaskToReturn{
		Tasks: dailyOnceTasks,
		Count: 6,
	}
	http.Error(w, common.SuccessResp(spt).String(), http.StatusOK)
}

type dailyOnceTaskToReturn struct {
	Tasks []common.DailyOnceTask `json:"tasks"`
	Count int                    `json:"count"`
}
