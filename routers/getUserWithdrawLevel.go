package routers

import (
	"net/http"
	"strings"

	"vlion/yxw/common"
)

func GetUserWithdrawLevel(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	mustParams := []string{"app_id", "user_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	userIDs := strings.Split(rq.Get("user_id"), "|")
	uds := make(map[string]*myWithdrawLevelToReturn)
	for _, userID := range userIDs {
		uds[userID] = getUserLevelData(appID, userID)
	}
	http.Error(w, common.SuccessResp(uds).String(), http.StatusOK)
}
