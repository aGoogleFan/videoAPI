package routers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type AlipayWithdrawStruct struct {
	yl common.APIRecord
}

func (a *AlipayWithdrawStruct) New() common.LogSetHandler {
	return new(AlipayWithdrawStruct)
}

func (a *AlipayWithdrawStruct) GetAPICode() int {
	return common.API_LOG_CODE_ALIPAY_WITHDRAW
}

func (a *AlipayWithdrawStruct) SetLog(yl common.APIRecord) {
	a.yl = yl
}

func (a *AlipayWithdrawStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		yxwLog.RecordLog(a.yl)
	}()
	wi, yl, needContinue := WithdrawCommon(w, r, common.ORDER_TYPE_ALIPAY_WITHDRAWA, a.yl)
	a.yl = yl
	if !needContinue {
		return
	}

	rq := r.URL.Query()
	mustParams := []string{"alipay_account", "alipay_name"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		a.yl.ErrorMsg = errResp.ErrMsg
		a.yl.Ret = errResp.String()
		a.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	orderID := common.NewUUID()
	payeeAccount := rq.Get("alipay_account")
	payeeRealName := rq.Get("alipay_name")
	if payeeRealName == "" {
		errResp := common.Resp{
			Status:    common.STATUS_PARAMS_LACK,
			ErrMsg:    "param alipay_name not exists",
			Timestamp: time.Now().Unix(),
		}
		a.yl.ErrorMsg = errResp.ErrMsg
		a.yl.Ret = errResp.String()
		a.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	next, err := autoWithdrawCheck(wi.AppID, wi.user.ID, wi.user.Create, wi.user.Status, wi.Amount)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		a.yl.ErrorMsg = err.Error()
		a.yl.Ret = errResp.String()
		a.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	od := common.OrderDetail{
		Timestamp:   wi.Timestamp,
		OrderID:     orderID,
		OrderType:   common.ORDER_TYPE_ALIPAY_WITHDRAWA,
		Account:     payeeAccount,
		AccountName: payeeRealName,
		Content:     "",
		Cost:        wi.RealCost,
		Amount:      wi.Amount * (-1),
		IP:          wi.IP,
		AppVersion:  wi.AppVersion,
		PreDeducate: true,
	}
	a.yl.OrderID = od.OrderID
	a.yl.WithdrawAccount = payeeAccount
	passed := false
	switch next {
	case common.WITHDRAW_AUTO_DO_REFUSE:
		od.Desc = "审核不通过."
		od.Status = common.ORDER_STATUS_REFUSED
	case common.WITHDRAW_AUTO_DO_PERSON:
		if wi.CoinKeepRate == -1 {
			od.Desc = "订单已提交, 待审批"
			od.Status = common.ORDER_STATUS_TO_BE_CHECKED
			break
		}
		if !wi.LevelAuto {
			od.Desc = "订单已提交, 待审批"
			od.Status = common.ORDER_STATUS_TO_BE_CHECKED
			break
		}
		extra := getWithdrawExtra(wi.user.IncomeRemaining, wi.RealCost, wi.CoinKeepRate, false)
		if extra > 0 {
			od.Desc = "订单已提交, 待审批"
			od.Status = common.ORDER_STATUS_TO_BE_CHECKED
			break
		}
		passed = true
	case common.WITHDRAW_AUTO_DO_PASS:
		od.Desc = "订单已提交, 待审批"
		passed = true
		od.Status = common.ORDER_STATUS_TO_BE_CHECKED
	}
	yl.AutoResult = od.Desc
	if err := db.SaveUserOrderHistory(wi.AppID, wi.user, od, wi.AppVersion); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		a.yl.ErrorMsg = err.Error()
		a.yl.Ret = errResp.String()
		a.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if passed {
		go withdrawPass(wi.AppID, rq.Get("app_version"), wi.user, od)
	}

	resp := common.SuccessResp(nil)
	a.yl.Ret = resp.String()
	a.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

func alipayWithdraw(appID string, usr common.UserBasic, od common.OrderDetail) (bool, error) {
	success, desc, err := withdrayByAlipay(appID, od.OrderID, od.Account, od.AccountName, strconv.Itoa(od.Amount*-1))
	if err != nil {
		return false, err
	}

	if err := db.UpdateUserOrderResult(appID, usr.ID, od, success, desc); err != nil {
		log.Fields{
			"orderid": od.OrderID,
			"appid":   appID,
			"userid":  usr.ID,
			"err":     err.Error(),
		}.Error("fail to save user order history")
		return success, err
	}
	if !od.PreDeducate {
		if !success {
			return false, errors.New(desc)
		}
		uits := db.UserIncomeToSave{
			AppID:     appID,
			Usr:       usr,
			OpType:    common.INCOME_TYPE_WITHDRAW,
			TaskID:    od.OrderID,
			TaskType:  common.TASK_TYPE_ALIPAY_WITHDRAW,
			Count:     od.Cost,
			From:      od.OrderID,
			Timestamp: time.Now().Unix(),
			Desc:      "提现成功, 扣除提现金币",
			SetMaster: false,
		}
		if err := uits.Save(); err != nil {
			log.Fields{
				"appID":    appID,
				"userID":   usr.ID,
				"orderID":  od.OrderID,
				"cost":     od.Cost,
				"withdraw": common.WITHDRAW_TYPE_ALIPAY,
				"err":      err.Error(),
			}.Error("fail to deal withdraw")
			return true, err
		}
		return true, nil
	}

	if success {
		return true, nil
	}

	uits := db.UserIncomeToSave{
		AppID:     appID,
		Usr:       usr,
		OpType:    common.INCOME_TYPE_INCOME_NO_TOTAL,
		TaskID:    od.OrderID,
		TaskType:  common.TASK_TYPE_PRE_DEDUCATE_RET,
		Count:     od.Cost,
		Timestamp: time.Now().Unix(),
		Desc:      common.USER_ORDER_RET_DESC,
		From:      od.OrderID,
		SetMaster: false,
	}
	uits.Save()
	return false, errors.New(desc)
}

// return: sucess, fail reason, error
func withdrayByAlipay(appID, orderID, account, accountName string, amount string) (bool, string, error) {
	alipayParams := getAlipayCommonParams(appID, ALI_FUND_TRANS)
	aftbc := alipayFoundTransBitContent{
		OutBizNo:      orderID,
		PayeeType:     "ALIPAY_LOGONID",
		PayeeAccount:  account,
		Amount:        amount,
		PayeeRealName: accountName,
		Remark:        "阅新闻支付宝支付兑换成功，继续加油",
	}
	bzc, _ := json.Marshal(aftbc)
	alipayParams.Set("biz_content", string(bzc))
	u, err := getURLWithSha2Sign(appID, alipayParams)
	if err != nil {
		return false, "", err
	}

	ct, code, err := common.VisitURL(u, time.Second)
	if err != nil {
		log.Fields{
			"url":     u,
			"code":    code,
			"content": string(ct),
			"err":     err.Error(),
		}.Error("visit alipay fail")
		// TODO:
		return false, "", err
	}

	if code != http.StatusOK {
		// TODO:
		return false, "", fmt.Errorf("unexpected code: %d", code)
	}

	var atr common.AlipayTransResponse
	if err := json.Unmarshal(ct, &atr); err != nil {
		// TODO:
		return false, "", err
	}

	var success bool
	var desc string
	if atr.AFTTTR.Code == "10000" {
		success = true
		desc = "提现成功"
	} else {
		success = false
		if atr.AFTTTR.SubMsg != "" {
			desc = atr.AFTTTR.SubMsg
		} else {
			desc = atr.AFTTTR.Msg
		}
	}
	return success, desc, nil
}
