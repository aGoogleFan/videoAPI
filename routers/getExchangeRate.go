package routers

import (
	"net/http"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type GetExchangeRateStruct struct{}

func (g *GetExchangeRateStruct) New() common.LogSetHandler {
	return new(GetExchangeRateStruct)
}

func (g *GetExchangeRateStruct) GetAPICode() int {
	return 0
}

func (g *GetExchangeRateStruct) SetLog(yl common.APIRecord) {}

func (g *GetExchangeRateStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	GetExchangeRate(w, r)
}

func GetExchangeRate(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	mustParams := []string{"app_id", "token", "exchange_type"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	exchangeType := rq.Get("exchange_type")
	var orderType int
	switch exchangeType {
	case "wechat":
		orderType = common.ORDER_TYPE_WECHAT_WITHDRAWA
	case "alipay":
		orderType = common.ORDER_TYPE_ALIPAY_WITHDRAWA
	default:
		http.Error(w, common.OtherErrorResp("invalid exchange type").String(), http.StatusOK)
		return
	}

	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if userID == "" {
		http.Error(w, common.TokenInvalidResp().String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	er, err := db.GetCurrentExchangeRate(appID)
	if err != nil {
		http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
		return
	}
	if er == nil {
		http.Error(w, common.SuccessResp(nil).String(), http.StatusOK)
		return
	}

	var d []common.RateDetail
	for _, v := range er.Rates {
		if v.Type != exchangeType {
			continue
		}
		d = v.Details
	}

	var res struct {
		List        []common.RateDetail `json:"list"`
		Account     string              `json:"account"`
		AccountName string              `json:"account_name"`
	}
	res.List = d

	uo, err := db.GetUserOrder(appID, userID)
	if err != nil {
		http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
		return
	}

	if uo == nil {
		http.Error(w, common.SuccessResp(res).String(), http.StatusOK)
		return
	}

	historyLen := len(uo.History)
	if historyLen == 0 {
		http.Error(w, common.SuccessResp(res).String(), http.StatusOK)
		return
	}

	for i := 0; i < historyLen; i++ {
		v := uo.History[historyLen-(i+1)]
		if !v.Success {
			continue
		}
		if v.OrderType != orderType {
			continue
		}
		res.Account = v.Account
		res.AccountName = v.AccountName
	}

	http.Error(w, common.SuccessResp(res).String(), http.StatusOK)

}
