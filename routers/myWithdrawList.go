package routers

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	// "github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type MyWithdrawList struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (m *MyWithdrawList) New() common.LogAndUserSetHandler {
	return new(MyWithdrawList)
}

func (m *MyWithdrawList) GetAPICode() int {
	return common.API_LOG_CODE_GET_MY_WITHDRAW_LIST
}

func (m *MyWithdrawList) SetLog(yl common.APIRecord) {
	m.yl = yl
}

func (m *MyWithdrawList) SetUser(usr common.UserBasic) {
	m.usr = usr
}

func (m *MyWithdrawList) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (m *MyWithdrawList) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() { yxwLog.RecordLog(m.yl) }()

	rq := r.URL.Query()
	appID := rq.Get("app_id")

	confMux.RLock()
	wsc := conf[appID].WithdrawStandConf
	wrc := conf[appID].WithdrawRateConf
	confMux.RUnlock()
	if wsc == nil || wrc == nil {
		errResp := common.ServerErrorResp("invalid stand or rate")
		m.yl.ErrorMsg = errResp.ErrMsg
		m.yl.RetCode = errResp.Status
		m.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	ud := getUserLevelData(appID, m.usr.ID)
	if ud == nil {
		errResp := common.Resp{
			Status:    common.STATUS_DATA_TO_BE_STATISTICAL,
			Timestamp: time.Now().Unix(),
		}
		m.yl.ErrorMsg = errResp.ErrMsg
		m.yl.RetCode = errResp.Status
		m.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	uwsl := getUserStand(m.usr.Status, ud.Total, *wsc)
	if uwsl == nil {
		errResp := common.ServerErrorResp("no stand matched")
		m.yl.ErrorMsg = errResp.ErrMsg
		m.yl.RetCode = errResp.Status
		m.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	// 新用户  有三次一元提现机会
	// 青铜 每周拥有一次1元提现权利
	// 白银 每周拥有2次1元提现权利
	// 黄金 每周用户4次1元提现权利
	// 钻石 不做限制
	withdrawCountThisWeek, err := db.GetUserWithdrawCountThisWeek(appID, m.usr.ID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		m.yl.ErrorMsg = err.Error()
		m.yl.RetCode = errResp.Status
		m.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	wa := uwsl.WithdrawAmount
	var waAfterTrim []int
	for _, w := range wa {
		if w == 1 {
			continue
		}
		waAfterTrim = append(waAfterTrim, w)
	}

	if time.Now().Sub(time.Unix(m.usr.Create, 0)) >= time.Hour*24*7 || withdrawCountThisWeek >= 3 {
		switch uwsl.LevelName {
		case "青铜级":
			if withdrawCountThisWeek >= 1 {
				uwsl.WithdrawAmount = waAfterTrim
			}
		case "白银级":
			if withdrawCountThisWeek >= 2 {
				uwsl.WithdrawAmount = waAfterTrim
			}
		case "黄金级":
			if withdrawCountThisWeek >= 4 {
				uwsl.WithdrawAmount = waAfterTrim
			}
		case "钻石级":
		default:
			uwsl.WithdrawAmount = waAfterTrim
		}
	}

	var mwltr myWithdrawListToReturn
	var details []myWithdrawListToReturnDetail
	for _, amount := range uwsl.WithdrawAmount {
		var ms []myWithdrawListToReturnDetail
		for _, r := range wrc.Rates {
			for _, d := range r.Details {
				if d.Amount != amount {
					continue
				}
				mwltrd := myWithdrawListToReturnDetail{
					Amount:  amount,
					Coin:    d.Coin,
					Rate:    uwsl.WithdrawRate,
					Method:  r.Type,
					IsValid: true,
				}
				coinAfterRate := float64(mwltrd.Coin) * uwsl.WithdrawRate
				if coinAfterRate == float64(int(coinAfterRate)) {
					mwltrd.CoinAfterRate = int(coinAfterRate)
				} else {
					mwltrd.CoinAfterRate = int(coinAfterRate) + 1
				}
				if uwsl.CoinKeepRate <= 0 || uwsl.CoinKeepRate >= 1 || !uwsl.IsAutoWithdraw {
					mwltrd.Extra = -1
					mwltrd.TipMsg = "需人工审核，预计3个工作日内受理完成"
				} else {
					mwltrd.Extra = getWithdrawExtra(m.usr.IncomeRemaining, mwltrd.CoinAfterRate, uwsl.CoinKeepRate, false)
					if mwltrd.Extra <= 0 {
						mwltrd.Extra = 0
					}
					if mwltrd.Extra == 0 {
						mwltrd.TipMsg = "预计可立即到账"
					} else {
						// mwltrd.TipMsg = fmt.Sprintf("花费%d金币提现%d元，可立即提交。\n再额外获得%d金币后，可立即到账。\n或者等待人工审核，大概需要3个工作日。", mwltrd.CoinAfterRate, amount, mwltrd.Extra)
						mwltrd.TipMsg = fmt.Sprintf("再额外获得%d金币后，可立即到账。\n或者等待人工审核，大概需要3个工作日。", mwltrd.Extra)
					}
				}
				ms = append(ms, mwltrd)
			}
		}
		var newMS []myWithdrawListToReturnDetail
		for i := range ms {
			exists := false
			for j := range newMS {
				if ms[i].Amount != newMS[j].Amount {
					continue
				}
				if ms[i].Extra != newMS[j].Extra {
					continue
				}
				exists = true
				newMS[j].Method = newMS[j].Method + "|" + ms[i].Method
			}
			if !exists {
				newMS = append(newMS, ms[i])
			}
		}
		details = append(details, newMS...)
	}

	for _, r := range wrc.Rates {
		for _, d := range r.Details {
			needAdd := true
			for _, dt := range details {
				if d.Amount != dt.Amount {
					continue
				}
				if d.Coin != dt.Coin {
					continue
				}
				if !strings.Contains(dt.Method, r.Type) {
					continue
				}
				needAdd = false
			}
			if needAdd {
				details = append(details, myWithdrawListToReturnDetail{
					Amount:        d.Amount,
					Method:        r.Type,
					IsValid:       false,
					TipMsg:        "您的用户等级暂不支持该提现金额. 请继续努力, 提升用户等级",
					Coin:          d.Coin,
					CoinAfterRate: d.Coin,
				})
			}
		}
	}

	mwltr.Details = details

	uo, err := db.GetUserOrder(appID, m.usr.ID)
	if err != nil {
		http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
		return
	}

	if uo != nil {
		historyLen := len(uo.History)
		if historyLen != 0 {
			for i := 0; i < historyLen; i++ {
				v := uo.History[historyLen-(i+1)]
				if !v.Success {
					continue
				}
				switch v.OrderType {
				case common.ORDER_TYPE_ALIPAY_WITHDRAWA:
					mwltr.AlipayAccount = v.Account
					mwltr.AlipayRealName = v.AccountName
				case common.ORDER_TYPE_WECHAT_WITHDRAWA:
					mwltr.WechatAccount = v.Account
					mwltr.WechatRealName = v.AccountName
				default:
					continue
				}
			}
		}
	}
	resp := common.SuccessResp(mwltr)
	m.yl.RetCode = resp.Status
	m.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

// 目前计算标准: 提现后剩余金币*(1-留存率)>= 提现金额
func getWithdrawExtra(remain, realWithdrawAmount int, keepRate float64, alreadyReCost bool) int {
	x := float64(realWithdrawAmount)/(1-keepRate) - float64(remain)
	if alreadyReCost {
		x = x - float64(realWithdrawAmount)
	}
	if x == float64(int(x)) {
		return int(x)
	}
	return int(x) + 1
}

type myWithdrawListToReturn struct {
	Details        []myWithdrawListToReturnDetail `json:"details"`
	AlipayAccount  string                         `json:"alipay_account"`
	AlipayRealName string                         `json:"alipay_real_name"`
	WechatAccount  string                         `json:"wechat_account"`
	WechatRealName string                         `json:"wechat_real_name"`
}

type myWithdrawListToReturnDetail struct {
	Amount        int     `json:"amount"`
	Method        string  `json:"method"`
	Extra         int     `json:"extra"`
	IsValid       bool    `json:"is_valid"`
	TipMsg        string  `json:"tip_msg"`
	Rate          float64 `json:"rate"`
	Coin          int     `json:"coin"`
	CoinAfterRate int     `json:"coin_after_rate"`
}

func getUserStand(userStatus int, score float64, uwsh common.UserWithdrawStandHistory) *common.UserWithdrawStandLevel {
	var levelName string
	var auto bool
	var uwsl *common.UserWithdrawStandLevel
	for _, l := range uwsh.Levels {
		if score < float64(l.Score[0]) || score >= float64(l.Score[1]) {
			continue
		}
		uwsl = &l
		break
	}
	if uwsl == nil {
		return nil
	}

	switch userStatus {
	case common.STATUS_USER_VALID:
		return uwsl
	case common.STATUS_USER_DISABLED:
		levelName = "新手3"
		auto = false
	case common.STATUS_USER_NO_WITHDRAW:
		levelName = "新手2"
		auto = false
	case common.STATUS_USER_NO_AUTO_WITHDRAW:
		levelName = "新手3"
		auto = false
	default:
		return nil
	}
	return &common.UserWithdrawStandLevel{
		LevelName:      levelName,
		CoinKeepRate:   uwsl.CoinKeepRate,
		WithdrawRate:   uwsl.WithdrawRate,
		WithdrawAmount: []int{1},
		IsAutoWithdraw: auto,
	}
}
