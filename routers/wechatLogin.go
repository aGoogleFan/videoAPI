package routers

import (
	"net/http"
	"net/url"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type WechatLoginStruct struct {
	yl common.APIRecord
	ub common.UserBasic
}

func (w *WechatLoginStruct) New() common.LogAndUserSetHandler {
	return new(WechatLoginStruct)
}

func (wls *WechatLoginStruct) GetAPICode() int {
	return common.API_LOG_CODE_WECHAT_LOGIN
}

func (wls *WechatLoginStruct) SetLog(yl common.APIRecord) {
	wls.yl = yl
}

func (wls *WechatLoginStruct) SetUser(ub common.UserBasic) {
	wls.ub = ub
}

func (wls *WechatLoginStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"wechat_code", "app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		return nil, errResp, nil
	}
	appID := rq.Get("app_id")
	wechatCode := rq.Get("wechat_code")
	wat, errResp := getWechatAccessTokenByWechatCode(appID, wechatCode)
	if errResp != nil {
		return nil, errResp, nil
	}
	if wat.ErrCode != 0 {
		errResp := common.OtherErrorResp(wat.ErrMsg)
		return nil, &errResp, nil
	}
	usr, err := db.GetUserInfo(appID, bson.M{"third_part.wechat.openid": wat.OpenID})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		return nil, &errResp, err
	}
	if usr == nil {
		errResp := common.Resp{
			Status:    common.STATUS_WECHAT_UNBIND,
			Timestamp: time.Now().Unix(),
		}
		return nil, &errResp, nil
	}

	wui, err := getWechatUserInfoByAccessToken(wat.OpenID, wat.AccessToken)
	if err == nil {
		go db.UpdateUser(appID, bson.M{"id": usr.ID}, bson.M{"$set": bson.M{"$set": bson.M{"info.name": wui.NickName}}})
	}
	return usr, nil, nil
}

func (wls *WechatLoginStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(wls.yl)
	}()
	rq := r.URL.Query()

	appID := rq.Get("app_id")
	updator := bson.M{"last_login": time.Now().Unix()}
	if err := db.UpdateUser(appID, bson.M{"id": wls.ub.ID}, bson.M{"$set": updator}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		wls.yl.Ret = err.Error()
		wls.yl.RetCode = errResp.Status
		wls.yl.ErrorMsg = errResp.ErrMsg
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	token := common.NewUUID()
	if err := db.SaveToken(token, wls.ub.ID, common.REDIS_KEY_VALID_DURATION_TOKEN); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		wls.yl.Ret = err.Error()
		wls.yl.RetCode = errResp.Status
		wls.yl.ErrorMsg = errResp.ErrMsg
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	rsp := common.SuccessResp(struct {
		Token string `json:"token"`
		Code  string `json:"code"`
		IsNew bool   `json:"is_new"`
	}{
		Token: token,
		Code:  wls.ub.Code,
		IsNew: false,
	})
	wls.yl.RetCode = rsp.Status
	wls.yl.Ret = rsp.String()
	http.Error(w, rsp.String(), http.StatusOK)
}

func WechatLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	mustParams := []string{"wechat_code", "app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		log.Fields{
			"err": "no wechat_code",
		}.Error("params check fail")
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	wechatCode := rq.Get("wechat_code")
	wat, errResp := getWechatAccessTokenByWechatCode(appID, wechatCode)
	if errResp != nil {
		log.Fields{
			"err": "no wechat_code",
		}.Error("code check fail")
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if wat.ErrCode != 0 {
		log.Fields{
			"err_msg":  string(wat.ErrMsg),
			"err_code": wat.ErrCode,
			"scope":    wat.Scope,
		}.Error("wechat return unexpected")
		errResp := common.OtherErrorResp(wat.ErrMsg)
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	usr, err := db.GetUserInfo(appID, bson.M{"third_part.wechat.openid": wat.OpenID})
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to get user info by openid")
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if usr == nil {
		errResp := common.Resp{
			Status:    common.STATUS_WECHAT_NOT_BIND,
			Timestamp: time.Now().Unix(),
		}
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	updator := bson.M{"last_login": time.Now().Unix()}
	wui, err := getWechatUserInfoByAccessToken(wat.OpenID, wat.AccessToken)
	if err == nil {
		updator["info.name"] = wui.NickName
	}

	if err := db.UpdateUser(appID, bson.M{"id": usr.ID}, bson.M{"$set": updator}); err != nil {
		log.Fields{
			"userid": usr.ID,
			"err":    err.Error(),
		}.Error("mongo error")
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	token := common.NewUUID()
	if err := db.SaveToken(token, usr.ID, common.REDIS_KEY_VALID_DURATION_TOKEN); err != nil {
		log.Fields{
			"token":  token,
			"userid": usr.ID,
			"err":    err.Error(),
		}.Error("redis error")
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if androidID := rq.Get("android_id"); androidID != "" {
		devicePassed, err := CheckDeviceLimit(appID, usr.ID, androidID)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if !devicePassed {
			http.Error(w, common.Resp{
				Status:    common.STATUS_DEVICE_LIMIT,
				Timestamp: time.Now().Unix(),
			}.String(), http.StatusOK)
			return
		}

		ip := common.GetIPFromRequest(r)
		ipPassed, err := CheckIPLimit(appID, usr.ID, ip)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if !ipPassed {
			http.Error(w, common.Resp{
				Status:    common.STATUS_IP_LIMIT,
				Timestamp: time.Now().Unix(),
			}.String(), http.StatusOK)
			return
		}

		if err := db.SaveUserIP(appID, ip, usr.ID, time.Now().Unix()); err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}

		if err := db.SaveUserDevice(appID, androidID, usr.ID, time.Now().Unix()); err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
	}

	rsp := common.SuccessResp(struct {
		Token string `json:"token"`
		Code  string `json:"code"`
		IsNew bool   `json:"is_new"`
	}{
		Token: token,
		Code:  usr.Code,
		IsNew: false,
	})
	http.Error(w, rsp.String(), http.StatusOK)
	return
}
