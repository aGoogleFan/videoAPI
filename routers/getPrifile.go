package routers

import (
	"net/http"
	"net/url"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type GetProfileStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetProfileStruct) New() common.LogAndUserSetHandler {
	return new(GetProfileStruct)
}

func (g *GetProfileStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_PROFILE
}

func (g *GetProfileStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetProfileStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (g *GetProfileStruct) SetUser(u common.UserBasic) {
	g.usr = u
}

func (g *GetProfileStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	uptr := userProfileToReturn{
		Name:  g.usr.Info.Name,
		Icon:  g.usr.Info.Icon,
		Sex:   g.usr.Info.Sex,
		Birth: g.usr.Info.Birth,
		Cell:  g.usr.Info.Mobile,
		Code:  g.usr.Code,
	}
	if g.usr.From != "" {
		uptr.HasMaster = true
	}
	if g.usr.ThirdPart.Wechat.OpenID != "" {
		uptr.WechatBind = true
	}
	if g.usr.ThirdPart.Alipay.ID != "" {
		uptr.AlipayBind = true
	}
	if g.usr.ThirdPart.QQ.OpenID != "" {
		uptr.QQBind = true
	}
	resp := common.SuccessResp(uptr)
	g.yl.Ret = resp.String()
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

type userProfileToReturn struct {
	Name       string `json:"name"`
	Icon       string `json:"icon"`
	Sex        int    `json:"sex"`
	Birth      string `json:"birth"`
	Cell       string `json:"cell"`
	HasMaster  bool   `json:"has_master"`
	QQBind     bool   `json:"qq_bind"`
	WechatBind bool   `json:"wechat_bind"`
	AlipayBind bool   `json:"alipay_bind"`
	Code       string `json:"code"`
}
