package routers

import (
	"fmt"
	"net/http"
	"net/url"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetRecentOrderStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetRecentOrderStruct) New() common.LogAndUserSetHandler {
	return new(GetRecentOrderStruct)
}

func (g *GetRecentOrderStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_RECENT_UNCOMPLETE_ORDER
}

func (g *GetRecentOrderStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetRecentOrderStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	return getUserByToken(rq, []string{"app_id", "token"})
}

func (g *GetRecentOrderStruct) SetUser(usr common.UserBasic) {
	g.usr = usr
}

func (g *GetRecentOrderStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() { yxwLog.RecordLog(g.yl) }()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	uo, err := db.GetUserOrder(appID, g.usr.ID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if uo == nil {
		resp := common.SuccessResp(getRecentOrderToReturn{
			HasUncomplete: false,
			MsgTip:        "您目前没有未完成的订单",
		})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	iLen := len(uo.History)
	if iLen == 0 {
		resp := common.SuccessResp(getRecentOrderToReturn{
			HasUncomplete: false,
			MsgTip:        "您目前没有未完成的订单",
		})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	for i := range uo.History {
		index := iLen - (i + 1)
		if uo.History[index].Status != common.ORDER_STATUS_TO_BE_CHECKED {
			continue
		}
		ud := getUserLevelData(appID, g.usr.ID)
		if ud == nil {
			continue
		}
		confMux.RLock()
		wsc := conf[appID].WithdrawStandConf
		confMux.RUnlock()
		if wsc == nil {
			continue
		}
		uwsl := getUserStand(g.usr.Status, ud.Total, *wsc)
		if uwsl == nil {
			continue
		}
		extra := getWithdrawExtra(g.usr.IncomeRemaining, uo.History[index].Cost, uwsl.CoinKeepRate, uo.History[index].PreDeducate)
		if extra > 0 {
			resp := common.SuccessResp(getRecentOrderToReturn{
				HasUncomplete: false,
				NeedCoin:      extra,
				MsgTip:        fmt.Sprintf("您最近的提现还差%d金币就可以自动到账", extra),
			})
			g.yl.RetCode = resp.Status
			g.yl.Ret = resp.String()
			http.Error(w, resp.String(), http.StatusOK)
			return
		}
		resp := common.SuccessResp(getRecentOrderToReturn{
			HasUncomplete: false,
			NeedCoin:      extra,
			MsgTip:        "您的提现请求已通过, 提现即将到账",
		})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(getRecentOrderToReturn{
		HasUncomplete: false,
		MsgTip:        "您目前没有未完成的订单",
	})
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
	return
}

type getRecentOrderToReturn struct {
	HasUncomplete bool   `json:"has_uncomplete"`
	NeedCoin      int    `json:"need_coin"`
	MsgTip        string `json:"msg_tip"`
}
