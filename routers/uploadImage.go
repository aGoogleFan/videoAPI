package routers

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
)

const (
	imageMaxSize = 32 << 20
)

func UploadInnerImage(w http.ResponseWriter, r *http.Request, path string, byDate bool, fileName string, oyl *common.APIRecord) (appID string, fileURL string, needContinue bool, yl common.APIRecord) {
	needContinue = false
	if r.Method != http.MethodPost {
		io.Copy(ioutil.Discard, r.Body)
		http.Error(w, "only post allowed", http.StatusMethodNotAllowed)
		return
	}
	if oyl != nil {
		yl = *oyl
	}

	rq := r.URL.Query()
	mustParams := []string{"app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		io.Copy(ioutil.Discard, r.Body)
		yl.ErrorMsg = errResp.ErrMsg
		yl.RetCode = errResp.Status
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	now := time.Now()
	appID = rq.Get("app_id")
	r.ParseMultipartForm(imageMaxSize)
	file, handler, err := r.FormFile("uploadfile")
	if err != nil {
		errResp := common.ServerErrorResp("fail to find file: " + err.Error())
		yl.ErrorMsg = err.Error()
		yl.RetCode = errResp.Status
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	defer file.Close()

	if handler.Size > imageMaxSize {
		errResp := common.OtherErrorResp("file too big")
		yl.ErrorMsg = errResp.ErrMsg
		yl.RetCode = errResp.Status
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	fileDir := fmt.Sprintf("/vlion/cr_lua/views/%s/%s", path, appID)
	if byDate {
		fileDir = fmt.Sprintf("/vlion/cr_lua/views/%s/%s/%s", path, appID, now.Local().Format("2006-01-02"))
	}
	if _, err := os.Stat(fileDir); err != nil {
		if !os.IsNotExist(err) {
			errResp := common.ServerErrorResp(err.Error())
			yl.ErrorMsg = errResp.ErrMsg
			yl.RetCode = errResp.Status
			yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if err := os.Mkdir(fileDir, os.ModeDir); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			yl.ErrorMsg = errResp.ErrMsg
			yl.RetCode = errResp.Status
			yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
	}

	if fileName == "" {
		fileName = handler.Filename
	}
	f, err := os.OpenFile(fmt.Sprintf("%s/%s", fileDir, fileName), os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		log.Fields{
			"file":  handler.Filename,
			"appid": appID,
			"err":   err.Error(),
		}.Error("fail to create file")
		errResp := common.ServerErrorResp(err.Error())
		yl.ErrorMsg = errResp.ErrMsg
		yl.RetCode = errResp.Status
		yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	io.Copy(f, file)
	f.Close()

	fileURL = fmt.Sprintf("http://viaweb.cn/views/%s/%s/%s", path, appID, handler.Filename)
	needContinue = true
	return
}
