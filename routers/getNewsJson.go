package routers

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

// http://wiki.vlion.cn/pages/viewpage.action?pageId=4655609

const (
	// newsJsonURL = "http://www.yuexinwen.cn/bd/news/list"
	// newsJsonURL = "http://www.viaweb.cn/bd/news/list"
	newsJsonURL = "http://api.viaweb.cn/bd/news/list"
)

type GetNewsJsonStruct struct {
	usr common.UserBasic
	yl  common.APIRecord
}

func (g *GetNewsJsonStruct) New() common.LogAndUserSetHandler {
	return new(GetNewsJsonStruct)
}

func (g *GetNewsJsonStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id"}
	return getUserByToken(rq, mustParams)
}

func (g *GetNewsJsonStruct) SetUser(u common.UserBasic) {
	g.usr = u
}

func (g *GetNewsJsonStruct) GetAPICode() int {
	return common.API_LOG_CODE_NEWS_JSON
}

func (g *GetNewsJsonStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetNewsJsonStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	// 对参数进行转化
	rq := r.URL.Query()

	// 对需要强制升级的老版本不返回内容
	appID := rq.Get("app_id")
	appVersion := rq.Get("app_version")
	confMux.RLock()
	appVersionHistory := conf[appID].VersionConf
	confMux.RUnlock()
	if appVersionHistory == nil {
		resp := common.SuccessResp(appVersionToReturn{
			AppVersion: appVersion,
		})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	avtr := getAppVersionReturnValue(appVersion, appVersionHistory)
	if avtr.HasNew && avtr.IsForce {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       nil,
				TotalCount: 9999999,
				Page:       1,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	uv := make(url.Values)
	uv.Set("media", "24")
	uv.Set("submedia", getUserSubmedia(g.usr, rq))
	uv.Set("make", rq.Get("make"))
	uv.Set("model", rq.Get("model"))
	uv.Set("sw", rq.Get("w"))
	uv.Set("sh", rq.Get("h"))
	uv.Set("devicetype", rq.Get("devicetype"))
	uv.Set("conn", rq.Get("conn"))
	uv.Set("carrier", rq.Get("carrier"))

	os := rq.Get("os")
	uv.Set("os", os)
	uv.Set("osv", rq.Get("osv"))
	if os == "1" {
		uv.Set("imei", rq.Get("imei"))
		uv.Set("anid", rq.Get("android_id"))
	}
	ip := common.GetIPFromRequest(r)
	uv.Set("ip", ip)
	ua := r.UserAgent()
	uv.Set("ua", ua)
	category := rq.Get("category")
	if category != "" {
		uv.Set("category", rq.Get("category"))
	}

	opage := rq.Get("page")
	page, err := strconv.Atoi(opage)
	if err != nil {
		page = 1
	}

	uv.Set("page", strconv.Itoa(page))
	t := newsJsonURL + "?" + uv.Encode()
	ct, _, err := common.VisitURL(t, time.Second)
	if err != nil {
		log.Fields{
			"target": t,
			"err":    err.Error(),
		}.Error("fail to get news")
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	var i []map[string]interface{}
	if err := json.Unmarshal(ct, &i); err != nil {
		errResp := common.SuccessResp(
			common.PageContiner{
				List:       nil,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	token := rq.Get("token")
	if token == "" {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	ub, _, _ := getUserByToken(rq, nil)
	if ub == nil {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	iLen := len(i)
	if iLen == 0 {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	confMux.RLock()
	rnc := conf[appID].RewardNewsConf
	confMux.RUnlock()
	if rnc == nil {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	rnhd := getRewardNewsConf(appID, category, *rnc)
	if rnhd == nil {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	if rnhd.RefreshInterval <= 0 {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	nowTimestamp := time.Now().Unix()
	crnd := getCurrentRewardNewsDuration(*rnhd, nowTimestamp)
	// alreadyRead, err := db.GetUserIncomeHistoryCountByDetail(appID, ub.ID, crnd.startTimestamp, crnd.endTimestamp, common.INCOME_TYPE_INCOME, common.TASK_TYPE_REWARD_NEWS, "")
	alreadyRead, err := db.GetUserAlreadyRead(appID, ub.ID, crnd.startTimestamp)
	if err != nil {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	if alreadyRead >= rnhd.Amount {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	readTotal, _, err := db.GetUserDailyIncomeByType(appID, ub.ID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_REWARD_NEWS, nowTimestamp)
	if err != nil {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	if readTotal >= rnc.MaxPerDay {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	hrn := hasRewardNews(appID, category, page)
	if !hrn {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	index := common.GenRadomNumber(0, iLen)
	clkURL, ok := i[index]["clk_url"]
	if !ok {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	clkURLString, ok := clkURL.(string)
	if !ok {
		resp := common.SuccessResp(
			common.PageContiner{
				List:       i,
				TotalCount: 9999999,
				Page:       page,
				MaxPerPage: 7,
			})
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	confMux.RLock()
	ac := conf[appID].AppConf
	confMux.RUnlock()
	i[index]["star"] = true
	urlID := base64.URLEncoding.EncodeToString([]byte(clkURLString))
	i[index]["clk_url"] = fmt.Sprintf("%s/api/getRewardNews?id=%s&category=%s", strings.TrimSuffix(ac.Domain, "/"), urlID, category)
	resp := common.SuccessResp(
		common.PageContiner{
			List:       i,
			TotalCount: 9999999,
			Page:       page,
			MaxPerPage: 7,
		})
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
	return
}

type userNewsJson struct {
	UserID    string `json:"user_id"`
	NewsURL   string `json:"news_url"`
	Timestamp int64  `json:"int64"`
}

func hasRewardNews(appID, category string, page int) bool {
	confMux.RLock()
	rnc := conf[appID].RewardNewsConf
	confMux.RUnlock()
	if rnc == nil {
		return false
	}
	for _, h := range rnc.Details {
		if h.Category != category {
			continue
		}
		for _, d := range h.RewardFrequency {
			if page < d[0] || page >= d[1] || d[0] > d[1] {
				continue
			}
			if d[2] <= 0 {
				continue
			}
			random := common.GenRadomNumber(0, 100)
			return random < d[2]
		}
	}
	return false
}
