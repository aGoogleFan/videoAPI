package routers

import (
	"fmt"
	"html/template"
	"net/http"
	"strings"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/aid/dailyAnalyze/userCommon"
	"vlion/yxw/common"
)

func GetDailyCoinStruct(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	mustParams := []string{"app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	user, password, ok := r.BasicAuth()
	if !ok {
		w.Header().Set("WWW-Authenticate", `Basic realm="THE REALM"`)
		http.Error(w, "need login", http.StatusUnauthorized)
		return
	}
	//	user := rq.Get("user")
	//	password := rq.Get("password")
	dateString := rq.Get("date")
	if user != "admin" || password != "vlion.cn" {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	ms, err := mgo.Dial(mongoAddr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusOK)
		return
	}
	defer ms.Close()

	ms.SetSocketTimeout(time.Hour * 5)
	ms.SetSyncTimeout(time.Minute * 10)
	ms.SetCursorTimeout(0)
	ms.SetMode(mgo.Monotonic, true)
	if err := ms.Ping(); err != nil {
		http.Error(w, err.Error(), http.StatusOK)
		return
	}
	userIncomeStructC := ms.DB("cr_app_all").C("cr_app_back_conf")
	userIncomeDailyC := ms.DB("cr_app_" + appID).C("user_income_by_date")
	var uit userCommon.UserIncomeType
	if err := userIncomeStructC.Find(bson.M{"id": appID}).One(&uit); err != nil {
		http.Error(w, err.Error(), http.StatusOK)
		return
	}

	if dateString == "" {
		dateString = time.Now().Format("2006-01-02")
	}
	d, err := time.ParseInLocation("2006-01-02", dateString, time.Local)
	if err != nil {
		http.Error(w, err.Error(), http.StatusOK)
		return
	}
	dateUnix := common.GetDateStartUnix(d.Unix())
	m := make(map[string]int)
	var total int
	for _, detail := range uit.UserIncomeTaskType {
		var ui common.UserIncomeByType
		if err := userIncomeDailyC.Find(bson.M{"id": common.TOTAL_CONF_VLION_ALL_ID, "task_type": detail.Value, "timestamp": dateUnix}).One(&ui); err != nil {
			if err != mgo.ErrNotFound {
				http.Error(w, err.Error(), http.StatusOK)
				return
			}
			continue
		}
		total += ui.Total
		m[detail.Name] = ui.Total
	}
	var tmp []string
	for k, v := range m {
		tmp = append(tmp, fmt.Sprintf(`['%s(%d)', %.3f]`, k, v, float64(v)/float64(total)*100))
	}
	data := strings.Join(tmp, ",")
	t := template.New("coin struct template")
	t, _ = t.Parse(coinStructTemplate)
	t.Execute(w, dailyCoinStructTemplateParams{
		DateString: dateString,
		Data:       template.JS(data),
	})

}

type dailyCoinStructTemplateParams struct {
	DateString string
	Data       template.JS
}

const coinStructTemplate = `
<!DOCTYPE HTML>
<html>
	<head>
	</head>
	<body>
	    <!-- 图表容器 DOM -->
	    <div id="container" style="width: 600px;height:400px;"></div>
	    <!-- 引入 highcharts.js -->
	    <script src="http://cdn.hcharts.cn/highcharts/highcharts.js"></script>
		<script src="http://cdn.hcharts.cn/highcharts/highcharts-3d.js"></script>
		<script>
			var chart = Highcharts.chart('container', {
				chart: {
					type: 'pie',
					options3d: {
							enabled: true,
							alpha: 45,
							beta: 0
					}
				},
				title: {
						text: '{{.DateString}}金币获取分布'
				},
				tooltip: {
					pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
					pie: {
						allowPointSelect: true,
						cursor: 'pointer',
						depth: 35,
						dataLabels: {
							enabled: true,
							format: '{point.name}'
						}
					}
				},
				series: [{
					type: 'pie',
					name: '金币分布',
					data: [
						{{.Data}}
					]
				}]
			});
		</script>
	</body>
</html>
`

/*
['Firefox',   45.0],
['IE',       26.8],
['chrome',   12.8],
['Safari',    8.5],
['Opera',     6.2],
['Others',   0.7]
*/
