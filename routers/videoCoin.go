package routers

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type VideoWatchStruct struct {
	usr common.UserBasic
	yl  common.APIRecord
}

func (v *VideoWatchStruct) New() common.LogAndUserSetHandler {
	return new(VideoWatchStruct)
}

func (v *VideoWatchStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_VIDEO_REWARD
}

func (v *VideoWatchStruct) SetLog(yl common.APIRecord) {
	v.yl = yl
}

func (v *VideoWatchStruct) SetUser(usr common.UserBasic) {
	v.usr = usr
}

func (v *VideoWatchStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	appVersion := strings.ToLower(rq.Get("app_version"))
	appVersion = strings.TrimPrefix(appVersion, "v")
	appVersion = strings.TrimPrefix(appVersion, "b")

	mustParams := []string{"token", "app_id"}

	return getUserByToken(rq, mustParams)
}

func (v *VideoWatchStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(v.yl)
	}()
	if v.usr.ThirdPart.Wechat.OpenID == "" {
		errResp := common.Resp{
			Status:    common.STATUS_WECHAT_NOT_BIND,
			Timestamp: time.Now().Unix(),
		}
		v.yl.ErrorMsg = errResp.ErrMsg
		v.yl.Ret = errResp.String()
		v.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	rq := r.URL.Query()
	appID := rq.Get("app_id")

	// 获取转盘参数
	confMux.RLock()
	vcc := conf[appID].VideoCoinConf
	confMux.RUnlock()
	if vcc == nil {
		errResp := common.OtherErrorResp("not found")
		v.yl.ErrorMsg = errResp.ErrMsg
		v.yl.RetCode = errResp.Status
		v.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	maxPerDay := vcc.MaxPerDay
	if maxPerDay == 0 {
		errResp := common.RewardLimitResp()
		v.yl.ErrorMsg = errResp.ErrMsg
		v.yl.RetCode = errResp.Status
		v.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	vccWeightsLen := len(vcc.Weights)
	if vccWeightsLen == 0 {
		resp := common.SuccessResp(videoCoinToReturn{Reward: 0})
		v.yl.RetCode = resp.Status
		v.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	weights, values := make([]int, vccWeightsLen), make([]int, vccWeightsLen)
	for i, vccWeight := range vcc.Weights {
		weights[i] = vccWeight.Weight
		values[i] = vccWeight.Coin
	}
	coin := common.GetValueByWeight(weights, values)
	if coin == 0 {
		resp := common.SuccessResp(videoCoinToReturn{Reward: 0})
		v.yl.RetCode = resp.Status
		v.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	successResp := common.SuccessResp(videoCoinToReturn{Reward: coin})
	key := fmt.Sprintf("video:%s:%s", appID, v.usr.ID)
	d, err := db.CheckIfExpired(key)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		v.yl.ErrorMsg = err.Error()
		v.yl.RetCode = errResp.Status
		v.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	nowTimestamp := time.Now().Unix()
	if !d {
		errResp := common.Resp{
			Status:    common.STATUS_NOT_IN_ACTIVE_TIME,
			Timestamp: time.Now().Unix(),
		}
		v.yl.ErrorMsg = errResp.ErrMsg
		v.yl.RetCode = errResp.Status
		v.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	value := strconv.FormatInt(nowTimestamp, 10)

	// 因为客户端提交间隔有可能比服务端短, 故而预留50%余量
	oTimeout := vcc.TimeInterval
	timeOut := time.Second * time.Duration(oTimeout)
	if oTimeout >= 2 {
		timeOut = time.Second * time.Duration(float64(oTimeout)*0.5)
	}
	if timeOut > 0 {
		if err := db.SaveToRedis(key, value, timeOut); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			v.yl.ErrorMsg = err.Error()
			v.yl.RetCode = errResp.Status
			v.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
	}

	if vcc.MaxPerDay >= 0 {
		dateTotal, _, err := db.GetUserDailyIncomeByType(appID, v.usr.ID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_VIDEO_WATCH, nowTimestamp)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			v.yl.ErrorMsg = err.Error()
			v.yl.RetCode = errResp.Status
			v.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if dateTotal >= vcc.MaxPerDay {
			http.Error(w, common.RewardLimitResp().String(), http.StatusOK)
			return
		}
	}

	uits := db.UserIncomeToSave{
		AppID:      appID,
		Usr:        v.usr,
		OpType:     common.INCOME_TYPE_INCOME,
		TaskID:     "",
		TaskType:   common.TASK_TYPE_VIDEO_WATCH,
		Count:      coin,
		Timestamp:  nowTimestamp,
		Desc:       "观看视频奖励",
		SetMaster:  false,
		AppVersion: rq.Get("app_version"),
		IP:         common.GetIPFromRequest(r),
	}

	if err := uits.Save(); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		v.yl.ErrorMsg = err.Error()
		v.yl.RetCode = errResp.Status
		v.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	v.yl.Reward = coin
	v.yl.RetCode = successResp.Status
	v.yl.Ret = successResp.String()
	http.Error(w, successResp.String(), http.StatusOK)

	go func() {
		t := task{
			appID:      appID,
			usr:        v.usr,
			taskType:   common.TASK_TYPE_DAILY,
			taskID:     common.TASK_DAILY_ID_VIDEO_WATCH,
			timestamp:  nowTimestamp,
			appVersion: rq.Get("app_version"),
			ip:         common.GetIPFromRequest(r),
		}
		t.complete()
	}()

	go func() {
		t := task{
			appID:      appID,
			usr:        v.usr,
			taskType:   common.TASK_TYPE_NEWBIE,
			taskID:     common.TASK_NEWBIE_ID_FIRST_WATCH_VIDEO,
			timestamp:  nowTimestamp,
			appVersion: rq.Get("app_version"),
			ip:         common.GetIPFromRequest(r),
		}
		t.complete()
	}()

	go func() {
		seconds, _, _, _, _, err := getDailyRewardDetail(appID, v.usr.ID, "video", nowTimestamp)
		if err != nil {
			return
		}
		if seconds >= 3600 {
			t := task{
				appID:      appID,
				usr:        v.usr,
				taskType:   common.TASK_TYPE_DAILY,
				taskID:     common.TASK_DAILY_ID_WATCH_AN_HOUR,
				timestamp:  nowTimestamp,
				appVersion: rq.Get("app_version"),
				ip:         common.GetIPFromRequest(r),
			}
			t.complete()
		}
	}()
}

type videoCoinToReturn struct {
	Reward int `json:"reward"`
}
