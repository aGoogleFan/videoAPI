package middleware

import (
	"net/http"

	"vlion/yxw/common"
)

func SignCheckWithoutVersionFiltFilter(lsh common.LogSetHandler) http.HandlerFunc {
	return CheckSign(lsh, true, false)
}

func SignCheckFilter(lsh common.LogSetHandler) http.HandlerFunc {
	return CheckSign(lsh, true, true)
}

func SignCheckNoForceFilter(lsh common.LogSetHandler) http.HandlerFunc {
	return CheckSign(lsh, false, true)
}

func SignCheckAndUserFilter(Laush common.LogAndUserSetHandler) http.HandlerFunc {
	return CheckSign(&UserDataFilter{
		Laush: Laush,
		Force: true,
	}, true, true)
}

func SignCheckAndUserNoForceFilter(Laush common.LogAndUserSetHandler) http.HandlerFunc {
	return CheckSign(&UserDataFilter{
		Laush: Laush,
		Force: false,
	}, true, true)
}

func SignCheckNoForceAndUserFilter(Laush common.LogAndUserSetHandler) http.HandlerFunc {
	return CheckSign(&UserDataFilter{
		Laush: Laush,
		Force: true,
	}, false, true)
}

func SignCheckNoForceWithoutVersionFiltAndUserNoForceFilter(Laush common.LogAndUserSetHandler) http.HandlerFunc {
	return CheckSign(&UserDataFilter{
		Laush: Laush,
		Force: false,
	}, false, false)
}

func SignCheckNoForceAndUserNoForceFilter(Laush common.LogAndUserSetHandler) http.HandlerFunc {
	return CheckSign(&UserDataFilter{
		Laush: Laush,
		Force: false,
	}, false, true)
}

func UserFilterCheckFilter(Laush common.LogAndUserSetHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		u := new(UserDataFilter)
		u.Laush = Laush
		u.ServeHTTP(w, r)
	}
}

func UserFilterCheckNotForceFilter(Laush common.LogAndUserSetHandler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		u := new(UserDataFilter)
		u.Laush = Laush
		u.Force = false
		u.ServeHTTP(w, r)
	}
}
