package middleware

import (
	"net/http"
)

func RepeatFileter(next func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		signOnce := r.URL.Query().Get("sign_once")
		_ = signOnce
		next(w, r)
	})
}
