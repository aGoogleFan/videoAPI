package middleware

import (
	"fmt"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

const (
	appKey  = "101"
	sha1Key = "6b56be6d0da386e7ae01b14e381d0324"

	v2Salt = "38cc1a27ad7256f87b30ae6c93ff036d"

	mustVersion = "1.0.7"
)

func CheckSign(lsh common.LogSetHandler, force bool, versionFilt bool) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rq := r.URL.Query()
		if versionFilt {
			appVersion := strings.ToLower(rq.Get("app_version"))
			appVersion = strings.TrimPrefix(appVersion, "v")
			appVersion = strings.TrimPrefix(appVersion, "b")
			lessThanMust := common.IsNewVersion(appVersion, mustVersion, false)
			if lessThanMust {
				http.Error(w, common.Resp{
					Timestamp: time.Now().Unix(),
					Status:    common.STATUS_APP_VERSION_TOO_OLD,
				}.String(), http.StatusOK)
				return
			}
		}

		lsh := lsh.New()
		var yl common.APIRecord
		yl.APICode = common.API_LOG_CODE_SIGN_ERR
		finalPass := false
		t := time.Now()
		yl.StartTime = t
		yl.Timestamp = t.Unix()

		yl.APPID = rq.Get("app_id")
		yl.SignVersion = rq.Get("ver")
		yl.SignOnce = rq.Get("sign_once")
		yl.APPVersion = rq.Get("app_version")
		yl.UserToken = rq.Get("token")
		yl.DeviceModel = rq.Get("make") + " " + rq.Get("model")
		yl.AndroidID = rq.Get("android_id")
		yl.IMEI = rq.Get("imei")
		yl.IsSafe = rq.Get("is_safe") + "|" + rq.Get("is_safe_e") + "|" + rq.Get("is_safe_r")
		yl.AppChannel = "old_version"
		if oAppChannel := rq.Get("app_channel"); oAppChannel != "" {
			yl.AppChannel = oAppChannel
		}
		opSystem := rq.Get("os")
		if opSystem == "1" {
			yl.System = "Android"
		} else if opSystem == "2" {
			yl.System = "IOS"
		}
		yl.SystemVersion = rq.Get("osv")
		ip := common.GetIPFromRequest(r)
		yl.UserIP = ip
		if strings.TrimSpace(ip) == "127.0.0.1" || strings.TrimSpace(ip) == "" {
			log.Fields{
				"X-Real-IP":       r.Header.Get("X-Real-IP"),
				"X-Forwarded-For": r.Header.Get("X-Forwarded-For"),
				"remote-addr":     r.RemoteAddr,
				"access":          r.URL.String(),
				"ip":              ip,
			}.Info("unexpected ip")
		}

		if rq.Get("jump") == "HT" {
			finalPass = true
			yl.APICode = lsh.GetAPICode()
			lsh.SetLog(yl)
			lsh.ServeHTTP(w, r)
			return
		}

		defer func() {
			if !force || (force && finalPass) {
				db.SaveDeviceChannel(yl.APPID, yl.AndroidID, yl.AppChannel, t.Unix())
			}
			if !finalPass && force {
				yl.Access = r.URL.String()
				yl.Duration = time.Now().Sub(time.Unix(yl.Timestamp, 0))
				yxwLog.RecordLog(yl)
			}
		}()

		if force {
			mustParams := []string{"app_version", "os", "osv", "make", "model", "devicetype", "w", "h", "carrier", "conn", "app_id", "ver", "sign_once", "timestamp", "sign"}
			if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
				yl.ErrorMsg = errResp.ErrMsg
				yl.RetCode = errResp.Status
				yl.Ret = errResp.String()
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}
			if rq["device_id"] == nil {
				if errResp := common.ParamsFilter(rq, []string{"device_id"}); errResp != nil {
					yl.ErrorMsg = errResp.ErrMsg
					yl.RetCode = errResp.Status
					yl.Ret = errResp.String()
					http.Error(w, errResp.String(), http.StatusOK)
					return
				}
			}
			otimestamp := rq.Get("timestamp")
			timestamp, err := strconv.ParseInt(otimestamp, 10, 0)
			if err != nil {
				resp := common.Resp{
					Timestamp: time.Now().Unix(),
					Status:    common.STATUS_PARAMS_LACK,
				}
				yl.ErrorMsg = err.Error()
				yl.RetCode = resp.Status
				yl.Ret = resp.String()
				http.Error(w, resp.String(), http.StatusOK)
				return
			}

			tst := time.Unix(timestamp, 0)
			if (t.Sub(tst) > time.Minute*5 || tst.Sub(t) > time.Minute) && force {
				resp := common.Resp{
					Timestamp: time.Now().Unix(),
					Status:    common.STATUS_TIME_ERROR,
					ErrMsg:    "invalid timestamp",
				}
				yl.ErrorMsg = resp.ErrMsg
				yl.RetCode = resp.Status
				yl.Ret = resp.String()
				http.Error(w, resp.String(), http.StatusOK)
				return
			}

			if !checkAppSign(rq, rq.Get("sign")) {
				resp := common.SignInvalidErrorResp()
				yl.ErrorMsg = resp.ErrMsg
				yl.RetCode = resp.Status
				yl.Ret = resp.String()
				http.Error(w, resp.String(), http.StatusOK)
				return
			}
		}

		finalPass = true
		yl.APICode = lsh.GetAPICode()
		lsh.SetLog(yl)
		lsh.ServeHTTP(w, r)
	})
}

// true if pass, else false
func checkAppSign(rq url.Values, signInQuery string) bool {
	var keys []string
	for k, _ := range rq {
		if k == "sign" {
			continue
		}
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var paramsToSignString string
	for k, v := range keys {
		if k == 0 {
			paramsToSignString = fmt.Sprintf("%s=%s", v, url.QueryEscape(rq.Get(v)))
			continue
		}
		paramsToSignString = fmt.Sprintf("%s&%s=%s", paramsToSignString, v, url.QueryEscape(rq.Get(v)))
	}
	switch rq.Get("ver") {
	case "1.0":
		finalString := appKey + "&" + paramsToSignString
		sign := common.HmacSha1AndBase64(sha1Key, finalString)
		signInQuery := rq.Get("sign")
		if sign == signInQuery {
			return true
		}
		return sign == strings.Replace(signInQuery, " ", "+", -1)
	case "2.0":
		finalString := appKey + "&" + paramsToSignString + v2Salt
		sign := common.HmacSha1AndBase64(sha1Key, finalString)
		if sign == signInQuery {
			return true
		}
		if sign == strings.Replace(signInQuery, " ", "+", -1) {
			return true
		}
		if strings.Replace(sign, "=", "", -1) == strings.Replace(signInQuery, "=", "", -1) {
			return true
		}
		return strings.Replace(sign, "=", "", -1) == strings.Replace(strings.Replace(signInQuery, " ", "+", -1), "=", "", -1)
	default:
		return false
	}
}
