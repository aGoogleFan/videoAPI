package middleware

import (
	"net/http"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	"vlion/yxw/log"
	"vlion/yxw/routers"
)

type DeviceAndIPFilterData struct {
	logRecord *common.APIRecord
	Laush     common.LogAndUserSetHandler
}

func (d DeviceAndIPFilterData) GetAPICode() int {
	return d.Laush.GetAPICode()
}

func (d DeviceAndIPFilterData) SetLog(l common.APIRecord) {
	d.logRecord = &l
}

func (d DeviceAndIPFilterData) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var yl common.APIRecord
	if d.logRecord != nil {
		yl = *d.logRecord
	} else {
		yl.Timestamp = time.Now().Unix()
	}
	finalPasse := false
	defer func() {
		if !finalPasse {
			yl.Duration = time.Now().Sub(time.Unix(yl.Timestamp, 0))
			log.RecordLog(yl)
		}
	}()
	yl.APICode = d.Laush.GetAPICode()
	rq := r.URL.Query()
	yl.UserToken = rq.Get("token")
	ub, resp, err := d.Laush.GetUser(rq)
	if err != nil {
		yl.ErrorMsg = err.Error()
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	if resp != nil {
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	yl.UserID = ub.ID
	yl.UserCode = ub.Code
	yl.UserMobile = ub.Info.Mobile
	yl.WechatOpenID = ub.ThirdPart.Wechat.OpenID
	yl.AlipayOpenID = ub.ThirdPart.Alipay.ID

	// check device
	appID := rq.Get("app_id")
	androidID := rq.Get("android_id")
	if androidID == "" {
		resp := common.ServerErrorResp("empty android id")
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	devicePassed, err := routers.CheckDeviceLimit(appID, ub.ID, androidID)
	if err != nil {
		resp := common.ServerErrorResp(err.Error())
		yl.ErrorMsg = err.Error()
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	if !devicePassed {
		resp := common.Resp{
			Timestamp: time.Now().Unix(),
			Status:    common.STATUS_DEVICE_LIMIT,
		}
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	if err := db.SaveUserDevice(appID, androidID, ub.ID, time.Now().Unix()); err != nil {
		resp := common.ServerErrorResp(err.Error())
		yl.ErrorMsg = err.Error()
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	// check ip
	ip := common.GetIPFromRequest(r)
	if ip == "" {
		resp := common.ServerErrorResp("empty ip")
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	ipPassed, err := routers.CheckIPLimit(appID, ub.ID, ip)
	if err != nil {
		resp := common.ServerErrorResp(err.Error())
		yl.ErrorMsg = err.Error()
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	if !ipPassed {
		resp := common.Resp{
			Timestamp: time.Now().Unix(),
			Status:    common.STATUS_IP_LIMIT,
		}
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	if err := db.SaveUserIP(appID, ip, ub.ID, time.Now().Unix()); err != nil {
		resp := common.ServerErrorResp(err.Error())
		yl.ErrorMsg = err.Error()
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	finalPasse = true
	d.Laush.SetLog(yl)
	d.Laush.SetUser(*ub)
	d.Laush.ServeHTTP(w, r)
}

func DeviceAndIPFilter(laush common.LogAndUserSetHandler) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var yl common.APIRecord
		finalPassed := false
		defer func() {
			if !finalPassed {
				log.RecordLog(yl)
			}
		}()
		rq := r.URL.Query()
		yl.UserToken = rq.Get("token")
		ub, resp, err := laush.GetUser(rq)
		if err != nil {
			http.Error(w, resp.String(), http.StatusOK)
			return
		}
		if resp != nil {
			http.Error(w, resp.String(), http.StatusOK)
			return
		}

		// check device
		appID := rq.Get("app_id")
		androidID := rq.Get("android_id")
		if androidID == "" {
			http.Error(w, common.ServerErrorResp("").String(), http.StatusOK)
			return
		}
		passed, err := routers.CheckDeviceLimit(appID, ub.ID, androidID)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if !passed {
			http.Error(w, common.Resp{
				Timestamp: time.Now().Unix(),
				Status:    common.STATUS_DEVICE_LIMIT,
			}.String(), http.StatusOK)
			return
		}

		if err := db.SaveUserDevice(appID, androidID, ub.ID, time.Now().Unix()); err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}

		ip := common.GetIPFromRequest(r)
		if ip == "" {
			http.Error(w, common.ServerErrorResp("").String(), http.StatusOK)
			return
		}
		ipPassed, err := routers.CheckIPLimit(appID, ub.ID, ip)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if !ipPassed {
			http.Error(w, common.Resp{
				Timestamp: time.Now().Unix(),
				Status:    common.STATUS_IP_LIMIT,
			}.String(), http.StatusOK)
			return
		}

		if err := db.SaveUserIP(appID, ip, ub.ID, time.Now().Unix()); err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		finalPassed = true
		laush.ServeHTTP(w, r)
	})
}

func DeviceFilterWithToken(next func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rq := r.URL.Query()
		token := rq.Get("token")
		userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if userID == "" {
			http.Error(w, common.TokenInvalidResp().String(), http.StatusOK)
			return
		}

		appID := rq.Get("app_id")
		androidID := rq.Get("android_id")
		if androidID == "" {
			next(w, r)
			return
		}
		passed, err := routers.CheckDeviceLimit(appID, userID, androidID)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if !passed {
			http.Error(w, common.Resp{
				Timestamp: time.Now().Unix(),
				Status:    common.STATUS_DEVICE_LIMIT,
			}.String(), http.StatusOK)
			return
		}

		if err := db.SaveUserDevice(appID, androidID, userID, time.Now().Unix()); err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}

		next(w, r)
	})
}

func IPFilterWithToken(next func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rq := r.URL.Query()
		token := rq.Get("token")
		userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if userID == "" {
			http.Error(w, common.TokenInvalidResp().String(), http.StatusOK)
			return
		}

		appID := rq.Get("app_id")
		ip := common.GetIPFromRequest(r)
		if ip == "" {
			next(w, r)
			return
		}
		passed, err := routers.CheckIPLimit(appID, userID, ip)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if !passed {
			http.Error(w, common.Resp{
				Timestamp: time.Now().Unix(),
				Status:    common.STATUS_IP_LIMIT,
			}.String(), http.StatusOK)
			return
		}

		if err := db.SaveUserIP(appID, ip, userID, time.Now().Unix()); err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}

		next(w, r)
	})
}
