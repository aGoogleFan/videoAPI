package middleware

import (
	"net/http"
	"time"

	// "github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type UserDataFilter struct {
	logRecord *common.APIRecord
	Laush     common.LogAndUserSetHandler
	Force     bool
}

func (u *UserDataFilter) New() common.LogSetHandler {
	f := u.Force
	udf := new(UserDataFilter)
	udf.Force = f
	udf.Laush = u.Laush.New()
	return udf
}

func (u *UserDataFilter) GetAPICode() int {
	return u.Laush.GetAPICode()
}

func (u *UserDataFilter) SetLog(l common.APIRecord) {
	u.logRecord = &l
}

func (u *UserDataFilter) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var yl common.APIRecord
	if u.logRecord != nil {
		yl = *u.logRecord
	} else {
		now := time.Now()
		yl.StartTime = now
		yl.Timestamp = now.Unix()
	}
	passed := false
	yl.APICode = u.Laush.GetAPICode()
	defer func() {
		if !u.Force || (u.Force && passed) {
			db.SaveUserChannel(yl.APPID, yl.UserID, yl.AppChannel, yl.StartTime.Unix())
		}

		if !passed && u.Force {
			yl.Duration = time.Now().Sub(time.Unix(yl.Timestamp, 0))
			yxwLog.RecordLog(yl)
		}
	}()

	rq := r.URL.Query()
	ub, resp, err := u.Laush.GetUser(rq)
	if err != nil {
		yl.ErrorMsg = err.Error()
		yl.Ret = resp.String()
		yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	if u.Force {
		if resp != nil {
			yl.Ret = resp.String()
			yl.RetCode = resp.Status
			http.Error(w, resp.String(), http.StatusOK)
			return
		}
	}
	if ub != nil {
		if ub.Status == common.STATUS_USER_DISABLED {
			errResp := common.Resp{
				Status:    common.STATUS_ACCOUNT_DISABLED,
				Timestamp: time.Now().Unix(),
			}
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		yl.UserID = ub.ID
		yl.UserCode = ub.Code
		yl.UserMobile = ub.Info.Mobile
		yl.WechatOpenID = ub.ThirdPart.Wechat.OpenID
		yl.AlipayOpenID = ub.ThirdPart.Alipay.ID
		u.Laush.SetUser(*ub)
	}

	u.Laush.SetLog(yl)
	passed = true
	u.Laush.ServeHTTP(w, r)
}
