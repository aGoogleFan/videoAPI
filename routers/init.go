package routers

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

var conf map[string]appConf
var confMux sync.RWMutex
var appIDs []string

type appConf struct {
	AppConf                 *common.AppConf                               // app配置文件
	FixedBoundConf          *common.AppFixedBoundHistory                  // 转盘参数
	WithdrawRateConf        *common.RateExchangeHistory                   // 提现汇率
	ApprenticeRewardConf    *common.UserApprenticeRewardConfHistoryDetail // 师徒每日奖励
	AdConf                  *common.AppAdConfHistory                      // 广告频率及概率设置
	VersionConf             *common.AppVersion                            // app版本相关信息
	SignInConf              *common.AppSignInConfHistory                  // 连续签到配置
	ShareContentConf        *common.AppShareContentHistory                // 分享内容
	AppURLConf              *common.AppUrlHistory                         // 各种url
	AppRewardConf           *common.AppRewardLimitHistory                 // 各种奖励上限
	ThirdpartConf           *common.AppThirdPart                          // 第三方参数配置
	AdPositionConf          *common.AppAdsHistory                         // 广告位配置
	AdPositionConfNew       *common.ADDATAHistory                         // 广告位配置(支持多上游的新版本)
	AppDeviceAndIPLimitConf *common.AppLimitHistory                       // 设备及ip限制
	WithdrawAutoCheck       *common.AppWithdrawAutoCheckHistory           // 提现自动审核标准
	AdClickConf             *common.AppADClickHistory                     // 广告点击配置
	VideoCoinConf           *common.VideoCoinHistory                      // 视频金币配置
	WithdrawLevelConf       *common.UserWithdrawLevelHistory              // 自动提现配置
	WithdrawStandConf       *common.UserWithdrawStandHistory              // 自动提现标准
	RewardNewsConf          *common.RewardNewsHistory                     // 红包新闻设置
	TimingRewardConf        *common.TimingRewardHistory                   // 红包雨设置
	RadomNews               []interface{}                                 // 随机新闻
	AdClickFrequece         *common.AdClickFrequece                       // 新闻点击频率
	DailyOnceTasks          []common.DailyOnceTask                        // 看看赚每日一次任务
	DailyManyTimesTasks     []common.DailyManyTimesTask                   // 看看赚每日多次任务
	StayPageTasks           []common.StayPageTask                         // 看看赚每日停留任务
	AppRecentWithdrawInfo   []common.AppRecentWithdrawInfo                // 最近提现的用户列表, 缓存用于缓解数据库压力

	WithdrawLevelData *withdrawLevelData
}

type withdrawLevelData struct {
	UL *common.UserLevel
}

func Init(m map[string]common.AppConf) {
	if len(m) == 0 {
		panic("empty config")
	}
	timingRewardMap = make(map[string]timingReward)
	db.InitRedis()
	if err := db.InitMongo(); err != nil {
		panic(err)
	}
	conf = make(map[string]appConf)
	for k, v := range m {
		appIDs = append(appIDs, k)
		ac := appConf{
			AppConf: &v,
		}
		conf[k] = ac
	}
	if err := refreshConf(); err != nil {
		panic(err)
	}
	go func() {
		for {
			time.Sleep(time.Second * 30)
			db.InitMongo()
			if err := refreshConf(); err != nil {
				log.Fields{
					"err": err.Error(),
				}.Error("fail to refresh config")
			}
		}
	}()
	for _, appID := range appIDs {
		confMux.Lock()
		ac := conf[appID].AppConf
		confMux.Unlock()
		if ac == nil {
			continue
		}
		if ac.AutoWithdrawScan.IsValid {
			interval := ac.AutoWithdrawScan.Interval
			if interval <= 0 {
				interval = time.Hour * 3
			}
			go func() {
				for {
					time.Sleep(interval)
					withdrawOrderAutoDealByAppID(appID)
				}
			}()
		}
	}
}

func refreshConf() error {
	var lastErr error
	for _, v := range appIDs {
		if err := refreshConfigByAppID(v); err != nil {
			lastErr = err
		}
	}
	return lastErr
}

func refreshConfigByAppID(appID string) error {
	var lastErr error
	confMux.Lock()
	ac := conf[appID]
	confMux.Unlock()
	fixedBoundConf, err := db.GetCurrentFixedBoundConf(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.FixedBoundConf = fixedBoundConf
	}

	withdrawRateConf, err := db.GetCurrentExchangeRate(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.WithdrawRateConf = withdrawRateConf
	}

	apprenticeRewardConf, err := db.GetUserMiddleSet(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.ApprenticeRewardConf = apprenticeRewardConf
	}

	adConf, err := db.GetCurrentAppADConf(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.AdConf = adConf
	}

	versionConf, err := db.GetAppHistory(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.VersionConf = versionConf
	}

	signInConf, err := db.GetAppSignInConf(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.SignInConf = signInConf
	}

	shareContentConf, err := db.GetShareContent(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.ShareContentConf = shareContentConf
	}

	appURLConf, err := db.GetCurrentAppURL(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.AppURLConf = appURLConf
	}

	appRewardConf, err := db.GetCurrentAppRewardLimit(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.AppRewardConf = appRewardConf
	}

	thirdpartConf, err := db.GetCurrentAppThirdPartConf(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.ThirdpartConf = thirdpartConf
	}

	adPositionConf, err := db.GetAppAds(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.AdPositionConf = adPositionConf
	}

	adPositionConfNew, err := db.GetCurrentADDataHistory(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.AdPositionConfNew = adPositionConfNew
	}

	appDeviceAndIPLimitConf, err := db.GetAppLimitCurrentConf(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.AppDeviceAndIPLimitConf = appDeviceAndIPLimitConf
	}

	withdrawAutoCheck, err := db.GetCurrentWitdrawAutocCheckHistory(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.WithdrawAutoCheck = withdrawAutoCheck
	}

	adClick, err := db.GetCurrentAdClickConf(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.AdClickConf = adClick
	}

	videoCoinConf, err := db.GetCurrentVedioCoinConf(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.VideoCoinConf = videoCoinConf
	}

	withdrawLevelConf, err := db.GetCurrentUserWithdrawLevelHistory(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.WithdrawLevelConf = withdrawLevelConf
	}

	withdrawStandConf, err := db.GetCurrentUserWithdrawStandHistory(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.WithdrawStandConf = withdrawStandConf
	}

	rewardNewsConf, err := db.GetCurrentRewardNews(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.RewardNewsConf = rewardNewsConf
	}

	dailyOnceTask, err := db.GetSpecialTaskDailyOnce(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.DailyOnceTasks = dailyOnceTask
	}

	dailyManyTimesTasks, err := db.GetSpecialTaskDailyManyTimes(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.DailyManyTimesTasks = dailyManyTimesTasks
	}

	stayPageTask, err := db.GetSpecialTaskDailyStay(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.StayPageTasks = stayPageTask
	}

	// 以下缓存不强制更新lastErr
	appRecentWithdrawInfo, err := db.GetRecentUserWithdrawList(appID)
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to cache recent withdraw info")
	} else {
		ac.AppRecentWithdrawInfo = appRecentWithdrawInfo
	}

	rd, err := initAppRadomAD()
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to init app radom ad")
	} else {
		ac.RadomNews = rd
	}

	timingRewardConf, err := db.GetCurrentTimingReward(appID)
	if err != nil {
		lastErr = err
	} else {
		ac.TimingRewardConf = timingRewardConf
		go refreshTimingReward(appID, timingRewardConf)
	}

	ac.AdClickFrequece = &common.AdClickFrequece{
		Hours: 1,
		Limit: 2,
	}

	confMux.Lock()
	conf[appID] = ac
	confMux.Unlock()
	return lastErr
}

func withdrawOrderAutoDealByAppID(appID string) {
	uos, err := db.GetCurrentUncompleteUserOrder(appID)
	if err != nil {
		return
	}
	if len(uos) == 0 {
		return
	}
	now := time.Now()
	for _, uo := range uos {
		for _, od := range uo.History {
			if od.Status != common.ORDER_STATUS_TO_BE_CHECKED {
				continue
			}
			usr, err := db.GetUserInfo(appID, bson.M{"id": uo.ID})
			if err != nil {
				continue
			}
			if usr == nil {
				continue
			}
			ud := getUserLevelData(appID, usr.ID)
			if ud == nil {
				continue
			}
			confMux.Lock()
			wsc := conf[appID].WithdrawStandConf
			confMux.Unlock()
			if wsc == nil {
				continue
			}
			uwsl := getUserStand(usr.Status, ud.Total, *wsc)
			if uwsl == nil {
				continue
			}
			if !uwsl.IsAutoWithdraw {
				if od.Timestamp <= common.GetDateStartUnix(now.Add(time.Hour*(-72)).Unix()) {
					reason := "审批超时, 请重新申请"
					if err := db.DoWithdrawRefuse(appID, *usr, od, reason, od.PreDeducate); err != nil {
						log.Fields{
							"appid":   appID,
							"userID":  usr.ID,
							"orderID": od.OrderID,
							"err":     err.Error(),
						}.Error("fail to fail withdraw")
					}
				}
				continue
			}
			extra := getWithdrawExtra((*usr).IncomeRemaining, od.Cost, uwsl.CoinKeepRate, od.PreDeducate)
			if extra > 0 {
				if od.Timestamp <= common.GetDateStartUnix(now.Add(time.Hour*(-72)).Unix()) {
					reason := "你的活跃度暂时偏低, 未达到该金额的审批标准. 请适当增加阅读、视频、师徒收入后再申请提现。"
					if err := db.DoWithdrawRefuse(appID, *usr, od, reason, od.PreDeducate); err != nil {
						log.Fields{
							"appid":   appID,
							"userID":  usr.ID,
							"orderID": od.OrderID,
							"err":     err.Error(),
						}.Error("fail to fail withdraw")
					}
				}
				continue
			}

			if !od.PreDeducate {
				// 用户余额确认
				if usr.IncomeRemaining < od.Cost {
					od.Status = common.ORDER_STATUS_PASSED
					if err := db.UpdateUserOrderResult(appID, usr.ID, od, false, "金币不足, 提现失败."); err != nil {
						log.Fields{
							"orderid": od.OrderID,
							"appid":   appID,
							"userid":  usr.ID,
							"err":     err.Error(),
						}.Error("fail to save user order history")
					}
					continue
				}
			}

			if err := withdrawPass(appID, "", *usr, od); err != nil {
				log.Fields{
					"appid":   appID,
					"userID":  usr.ID,
					"orderID": od.OrderID,
					"err":     err.Error(),
				}.Error("do withdraw fail")
			}
		}
	}
}

var timingRewardMap map[string]timingReward
var timingRewardMapMutex sync.Mutex

type timingReward struct {
	StartTimestamp int64
	EndTimestamp   int64
	Total          int
	Remaining      int
	MaxPerUser     int
}

func refreshTimingReward(appID string, timingRewardConf *common.TimingRewardHistory) {
	timingRewardMapMutex.Lock()
	defer timingRewardMapMutex.Unlock()
	now := time.Now()
	nowTimestamp := now.Unix()
	if _, ok := timingRewardMap[appID]; !ok {
		resetTimingRewardMap(appID, timingRewardConf, now)
		return
	}
	if nowTimestamp < timingRewardMap[appID].StartTimestamp || nowTimestamp >= timingRewardMap[appID].EndTimestamp {
		delete(timingRewardMap, appID)
		resetTimingRewardMap(appID, timingRewardConf, now)
		return
	}
}

func resetTimingRewardMap(appID string, timingRewardConf *common.TimingRewardHistory, now time.Time) {
	if timingRewardConf == nil {
		return
	}
	for _, d := range timingRewardConf.Details {
		if !d.IsValid {
			continue
		}
		if d.Reward <= 0 {
			continue
		}
		timeFormat := fmt.Sprintf("%s %s", now.Local().Format("2006-01-02"), d.StartTime)
		startTime, err := time.ParseInLocation("2006-01-02 15:04", timeFormat, time.Local)
		if err != nil {
			continue
		}
		timeFormat = fmt.Sprintf("%s %s", now.Local().Format("2006-01-02"), d.EndTime)
		endTime, err := time.ParseInLocation("2006-01-02 15:04", timeFormat, time.Local)
		if err != nil {
			continue
		}
		if !endTime.After(startTime) {
			continue
		}
		if now.After(endTime) {
			continue
		}
		if _, ok := timingRewardMap[appID]; !ok {
			timingRewardMap[appID] = timingReward{
				StartTimestamp: startTime.Unix(),
				EndTimestamp:   endTime.Unix(),
				Remaining:      d.Reward,
				Total:          d.Reward,
				MaxPerUser:     d.UserReward,
			}
			continue
		}
		if startTime.Unix() > timingRewardMap[appID].StartTimestamp {
			continue
		}
		timingRewardMap[appID] = timingReward{
			StartTimestamp: startTime.Unix(),
			EndTimestamp:   endTime.Unix(),
			Remaining:      d.Reward,
			Total:          d.Reward,
			MaxPerUser:     d.UserReward,
		}
	}
}

func initAppRadomAD() ([]interface{}, error) {
	var newses []interface{}
	for i := 0; i <= 10; i++ {
		ns, _ := get3PicAds(i)
		newses = append(newses, ns...)
		if len(newses) >= 2 {
			return []interface{}{newses[0], newses[1]}, nil
		}
	}
	if len(newses) == 1 {
		return []interface{}{newses[0]}, nil
	}
	return nil, errors.New("not found")
}

func get3PicAds(page int) ([]interface{}, error) {
	t := fmt.Sprintf("%s?media=24&submedia=46&page=%d", newsJsonURL, page)
	ct, _, err := common.VisitURL(t, time.Second)
	if err != nil {
		return nil, err
	}
	var i []map[string]interface{}
	if err := json.Unmarshal(ct, &i); err != nil {
		return nil, err
	}
	iLen := len(i)
	if iLen == 0 {
		return nil, errors.New("not found")
	}

	var newsToReturn []interface{}
	for _, news := range i {
		images, ok := news["images"].([]interface{})
		if !ok {
			continue
		}
		if len(images) != 3 {
			continue
		}
		newsToReturn = append(newsToReturn, news)
	}
	newsToRetuenLen := len(newsToReturn)
	switch newsToRetuenLen {
	case 0:
		return nil, nil
	case 1:
		return []interface{}{newsToReturn[0]}, nil
	default:
		return []interface{}{newsToReturn[0], newsToReturn[1]}, nil
	}
}
