package routers

import (
	"errors"
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetTimingRewardStruct struct {
	usr common.UserBasic
	yl  common.APIRecord
}

func (g *GetTimingRewardStruct) New() common.LogAndUserSetHandler {
	return new(GetTimingRewardStruct)
}

func (g *GetTimingRewardStruct) GetAPICode() int {
	return common.API_LOG_CODE_TIMING_REWARD
}

func (g *GetTimingRewardStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetTimingRewardStruct) SetUser(usr common.UserBasic) {
	g.usr = usr
}

func (g *GetTimingRewardStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"token", "app_id"}
	return getUserByToken(rq, mustParams)
}

func (g *GetTimingRewardStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	now := time.Now()
	trt, err := getTimingReward(appID, now)
	if err != nil {
		errResp := common.Resp{
			Status:    common.STATUS_NOT_IN_ACTIVE_TIME,
			Timestamp: time.Now().Unix(),
		}
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if trt.Rewared == 0 {
		resp := common.SuccessResp(struct {
			Reward int `json:"reward"`
		}{
			Reward: 0,
		})
		g.yl.Ret = resp.String()
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	alreadyGet, err := db.GetUserIncomeHistoryTotalByDetail(appID, g.usr.ID, trt.StartTimestamp, trt.EndTimestamp, common.INCOME_TYPE_INCOME, common.TASK_TYPE_TIMING_REWARD, "")
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	nowTimestamp := time.Now().Unix()
	if alreadyGet != 0 {
		resp := common.Resp{
			Status:    common.STATUS_ALREADY_REWARD,
			Timestamp: nowTimestamp,
		}
		g.yl.Ret = resp.String()
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	uits := db.UserIncomeToSave{
		AppID:      appID,
		Usr:        g.usr,
		OpType:     common.INCOME_TYPE_INCOME,
		TaskID:     "",
		TaskType:   common.TASK_TYPE_TIMING_REWARD,
		Count:      trt.Rewared,
		Timestamp:  nowTimestamp,
		Desc:       "抢宝箱奖励",
		SetMaster:  false,
		MasterRate: 0,
		From:       "",
		AppVersion: rq.Get("app_version"),
		IP:         common.GetIPFromRequest(r),
	}
	if err := uits.Save(); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	resp := common.SuccessResp(struct {
		Reward int `json:"reward"`
	}{
		Reward: trt.Rewared,
	})
	g.yl.Ret = resp.String()
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)

	go func() {
		t := task{
			appID:      appID,
			usr:        g.usr,
			taskType:   common.TASK_TYPE_NEWBIE,
			taskID:     common.TASK_NEWBIE_ID_FIRST_TIMING_REWARD,
			timestamp:  nowTimestamp,
			appVersion: rq.Get("app_version"),
			ip:         common.GetIPFromRequest(r),
		}
		t.complete()
	}()

	go func() {
		_, count, err := db.GetUserDailyIncomeByType(appID, g.usr.ID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_TIMING_REWARD, nowTimestamp)
		if err != nil {
			return
		}
		if count >= 5 {
			t := task{
				appID:      appID,
				usr:        g.usr,
				taskType:   common.TASK_TYPE_DAILY,
				taskID:     common.TASK_DAILY_ID_TIMING_REWARD_GTE_5,
				timestamp:  nowTimestamp,
				appVersion: rq.Get("app_version"),
				ip:         common.GetIPFromRequest(r),
			}
			t.complete()
		}
	}()
}

type tmpRewardTimestamp struct {
	Rewared        int
	StartTimestamp int64
	EndTimestamp   int64
}

func getTimingReward(appID string, now time.Time) (*tmpRewardTimestamp, error) {
	timingRewardMapMutex.Lock()
	defer timingRewardMapMutex.Unlock()
	tr, ok := timingRewardMap[appID]
	if !ok {
		return nil, errors.New("")
	}
	if now.Unix() >= timingRewardMap[appID].EndTimestamp {
		delete(timingRewardMap, appID)
		return nil, errors.New("")
	}
	if now.Unix() < timingRewardMap[appID].StartTimestamp {
		return nil, errors.New("")
	}
	if tr.Remaining == 0 || tr.MaxPerUser <= 0 {
		return &tmpRewardTimestamp{
			Rewared:        0,
			StartTimestamp: tr.StartTimestamp,
			EndTimestamp:   tr.EndTimestamp,
		}, nil
	}
	if tr.MaxPerUser < tr.Remaining {
		reward := common.GenRadomNumber(1, tr.MaxPerUser+1)
		tr.Remaining = tr.Remaining - reward
		timingRewardMap[appID] = tr
		return &tmpRewardTimestamp{
			Rewared:        reward,
			StartTimestamp: tr.StartTimestamp,
			EndTimestamp:   tr.EndTimestamp,
		}, nil
	}
	reward := common.GenRadomNumber(1, tr.Remaining+1)
	tr.Remaining = tr.Remaining - reward
	timingRewardMap[appID] = tr
	return &tmpRewardTimestamp{
		Rewared:        reward,
		StartTimestamp: tr.StartTimestamp,
		EndTimestamp:   tr.EndTimestamp,
	}, nil
}

func recoverTimestamp(appID string, trt tmpRewardTimestamp) {
	timingRewardMapMutex.Lock()
	defer timingRewardMapMutex.Unlock()
	tr, ok := timingRewardMap[appID]
	if !ok {
		return
	}
	if tr.StartTimestamp != trt.StartTimestamp || tr.EndTimestamp != trt.EndTimestamp {
		return
	}
	if tr.MaxPerUser < trt.Rewared {
		return
	}
	if tr.Remaining+trt.Rewared > tr.Total {
		return
	}
	tr.Remaining += trt.Rewared
	timingRewardMap[appID] = tr
}
