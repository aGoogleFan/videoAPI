package routers

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	// "github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetArticleShareContentStruct struct {
	yl common.APIRecord
}

func (g *GetArticleShareContentStruct) New() common.LogSetHandler {
	return new(GetArticleShareContentStruct)
}

func (g *GetArticleShareContentStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetArticleShareContentStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_ARTICLE_SHARE_CONTENT
}

func (g *GetArticleShareContentStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	mustParams := []string{"app_id", "title", "link"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	token := rq.Get("token")
	var uid string
	if token != "" {
		userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if userID == "" {
			errResp := common.TokenInvalidResp()
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		ui, err := db.GetUserInfo(appID, bson.M{"id": userID})
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if ui == nil {
			errResp := common.TokenInvalidResp()
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		uid = ui.ID
		go func() {
			t := task{
				appID:      appID,
				usr:        *ui,
				taskType:   common.TASK_TYPE_NEWBIE,
				taskID:     common.TASK_NEWBIE_ID_FIRST_SHARE_ARTICLE,
				timestamp:  time.Now().Unix(),
				appVersion: rq.Get("app_version"),
				ip:         common.GetIPFromRequest(r),
			}
			t.complete()
		}()
	}

	confMux.RLock()
	h := conf[appID].ShareContentConf
	confMux.RUnlock()
	if h == nil {
		errResp := common.OtherErrorResp("not found")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	title := rq.Get("title")
	link := rq.Get("link")
	u, err := url.QueryUnescape(link)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	f, err := url.Parse(u)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	uv := f.Query()
	if uv.Get("qid") == "qid10000" {
		uv.Set("qid", "qid02750")
	}
	if uid != "" {
		uv.Set("share_timestamp", strconv.FormatInt(time.Now().Unix(), 10))
	}
	f.RawQuery = uv.Encode()
	u = f.String()

	scts := shareContentSaveToRedis{
		AppID: appID,
		ID:    uid,
		URL:   u,
	}

	ct, _ := json.Marshal(scts)
	val := string(ct)
	key := common.StringToMD5(val)
	if err := db.SaveToRedis(key, val, time.Hour*24*30); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	confMux.RLock()
	auh := conf[appID].AppURLConf
	confMux.RUnlock()
	if auh == nil {
		errResp := common.OtherErrorResp("app url conf not found")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	tlink := strings.TrimSuffix(auh.ArticleShareContentURL, "/") + "/api/articleShare?id=" + url.QueryEscape(key)
	articleShareContent := h.Article
	articleShareContent.Moments = strings.Replace(articleShareContent.Moments, "{{.title}}", title, -1)
	articleShareContent.Moments = strings.Replace(articleShareContent.Moments, "{{.link}}", tlink, -1)
	articleShareContent.Person = strings.Replace(articleShareContent.Person, "{{.title}}", title, -1)
	articleShareContent.Person = strings.Replace(articleShareContent.Person, "{{.link}}", tlink, -1)
	articleShareContent.WechatURL = tlink

	resp := common.SuccessResp(articleShareContent)
	g.yl.Ret = resp.String()
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

type shareContentSaveToRedis struct {
	AppID string `json:"app_id"`
	ID    string `json:"id"`
	URL   string `json:"url"`
}

func getShareContentURL(link string) (string, error) {
	u, err := url.QueryUnescape(link)
	if err != nil {
		return "", err
	}
	f, err := url.Parse(u)
	if err != nil {
		return "", err
	}
	uv := f.Query()
	if uv.Get("qid") == "qid10000" {
		uv.Set("qid", "qid02750")
		f.RawQuery = uv.Encode()
		u = f.String()
	}
	return u, nil
}
