package routers

import (
	"fmt"
	"net/http"
	"net/url"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetPriodDurationStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetPriodDurationStruct) New() common.LogAndUserSetHandler {
	return new(GetPriodDurationStruct)
}

func (g *GetPriodDurationStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_PERIOD_INTERVAL
}

func (g *GetPriodDurationStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetPriodDurationStruct) SetUser(usr common.UserBasic) {
	g.usr = usr
}

func (g *GetPriodDurationStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"token", "app_id"}
	return getUserByToken(rq, mustParams)
}

func (g *GetPriodDurationStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	key := fmt.Sprintf("period:%s:%s", appID, g.usr.ID)
	d, err := db.GetUnexpiredDuration(key)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(
		struct {
			Duration int `json:"duration"`
		}{
			Duration: d,
		})
	g.yl.Ret = resp.String()
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}
