package routers

import (
	"errors"
	"fmt"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type task struct {
	appID      string
	usr        common.UserBasic
	taskType   int
	taskID     string
	timestamp  int64
	appVersion string
	ip         string
}

func (t *task) complete() error {
	done, err := checkDone(t.appID, t.usr.ID, t.taskType, t.taskID, t.timestamp)
	if err != nil {
		log.Fields{
			"appID":    t.appID,
			"userID":   t.usr.ID,
			"taskType": t.taskType,
			"taskID":   t.taskID,
			"err":      err.Error(),
		}.Error("fail to check done")
		return err
	}
	if done {
		return nil
	}

	task, err := db.GetTaskByID(t.appID, t.taskType, t.taskID)
	if err != nil {
		log.Fields{
			"appID":    t.appID,
			"userID":   t.usr.ID,
			"taskType": t.taskType,
			"taskID":   t.taskID,
			"err":      err.Error(),
		}.Error("fail to get task by id")
		return err
	}
	if task == nil {
		log.Fields{
			"appID":    t.appID,
			"userID":   t.usr.ID,
			"taskType": t.taskType,
			"taskID":   t.taskID,
		}.Info("no task")
		return nil
	}

	uits := db.UserIncomeToSave{
		AppID:      t.appID,
		Usr:        t.usr,
		OpType:     common.INCOME_TYPE_INCOME,
		TaskID:     t.taskID,
		TaskType:   t.taskType,
		Count:      task.Bounty,
		Timestamp:  t.timestamp,
		Desc:       "任务奖励: " + task.Name,
		SetMaster:  false,
		AppVersion: t.appVersion,
		IP:         t.ip,
	}

	// 某些新手任务大额奖励有时限, 超过时限只能领取到200
	if t.taskType == common.TASK_TYPE_NEWBIE {
		if t.taskID == common.TASK_NEWBIE_ID_SET_RECOMMEND_CODE || t.taskID == common.TASK_NEWBIE_ID_WECHAT_BIND || t.taskID == common.TASK_NEWBIE_ID_FIRST_READ {
			if time.Unix(t.timestamp, 0).Sub(time.Unix(t.usr.Create, 0)) > time.Hour*24*7 {
				uits.Count = 200
			}
		}
	}

	if err := uits.Save(); err != nil {
		log.Fields{
			"appID":    t.appID,
			"userID":   t.usr.ID,
			"taskType": t.taskType,
			"taskID":   t.taskID,
			"err":      err.Error(),
		}.Error("fail to save user income history")
		return err
	}

	if err := updateTaskDatabase(t.appID, t.usr.ID, t.taskType, t.taskID, t.timestamp); err != nil {
		log.Fields{
			"appID":    t.appID,
			"userID":   t.usr.ID,
			"taskType": t.taskType,
			"taskID":   t.taskID,
			"err":      err.Error(),
		}.Error("fail to update database")
		return err
	}
	return nil
}

func checkDone(appID, userID string, taskType int, taskID string, timestamp int64) (bool, error) {
	switch taskType {
	case common.TASK_TYPE_NEWBIE:
		ub, err := db.GetUserInfo(appID, bson.M{"id": userID})
		if err != nil {
			return false, err
		}
		if ub == nil {
			return false, nil
		}
		for _, v := range ub.NewbieTasks {
			if v.ID == taskID {
				return true, nil
			}
		}
		return false, nil
	case common.TASK_TYPE_DAILY:
		timeLocal := time.Unix(timestamp, 0).Local()
		timeLocalDateString := timeLocal.Format("20060102")
		key := fmt.Sprintf("task:d:%s:%s:%s", appID, timeLocalDateString, userID)
		res, err := db.GetHashFromRedis(key, taskID)
		if err != nil {
			return false, err
		}
		if res != "1" {
			return false, nil
		}
		return true, nil
	default:
		return false, errors.New("invalid task type")
	}
}

func updateTaskDatabase(appID, userID string, taskType int, taskID string, timestamp int64) error {
	switch taskType {
	case common.TASK_TYPE_NEWBIE:
		selector := bson.M{"id": userID}
		updator := bson.M{"$addToSet": bson.M{"newbie_task": common.UserBasicNewbieTask{
			ID:        taskID,
			Timestamp: timestamp,
		}}}
		return db.UpdateUser(appID, selector, updator)
	case common.TASK_TYPE_DAILY:
		timeLocal := time.Unix(timestamp, 0).Local()
		timeLocalDateString := timeLocal.Format("20060102")
		key := fmt.Sprintf("task:d:%s:%s:%s", appID, timeLocalDateString, userID)
		return db.SetHashToRedis(key, taskID, "1")
	default:
		return errors.New("invalid task type")
	}
}
