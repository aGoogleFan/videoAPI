package routers

import (
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type StayPageTask struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (s *StayPageTask) New() common.LogAndUserSetHandler {
	return new(StayPageTask)
}

func (s *StayPageTask) GetAPICode() int {
	return 0
}

func (s *StayPageTask) SetLog(yl common.APIRecord) {
	s.yl = yl
}

func (s *StayPageTask) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (s *StayPageTask) SetUser(u common.UserBasic) {
	s.usr = u
}

func (s *StayPageTask) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	confMux.Lock()
	stayPageTasks := conf[appID].StayPageTasks
	confMux.Unlock()
	t := time.Now().Unix()
	for i, stayPageTask := range stayPageTasks {
		spcs, err := db.GetUserSpecialTaskComplete(appID, s.usr.ID, stayPageTask.TaskID, common.SPECIAL_TASK_TYPE_STAY, t)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if len(spcs) >= 1 {
			stayPageTask.Status = 1
		}
		stayPageTasks[i] = stayPageTask
	}

	spt := stayPageTaskToReturn{
		Tasks:    stayPageTasks,
		TimeStay: 60,
	}
	http.Error(w, common.SuccessResp(spt).String(), http.StatusOK)
}

type stayPageTaskToReturn struct {
	Tasks    []common.StayPageTask `json:"tasks"`
	TimeStay int                   `json:"time_stay"`
}
