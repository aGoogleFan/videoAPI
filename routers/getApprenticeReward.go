package routers

import (
	"net/http"
	"net/url"
	"time"
	"vlion/yxw/db"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
)

type GetApprenticeReward struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetApprenticeReward) New() common.LogAndUserSetHandler {
	return new(GetApprenticeReward)
}

func (g *GetApprenticeReward) GetAPICode() int {
	return 0
}

func (g *GetApprenticeReward) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetApprenticeReward) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token", "apprentice_code"}
	return getUserByToken(rq, mustParams)
}

func (g *GetApprenticeReward) SetUser(u common.UserBasic) {
	g.usr = u
}

func (g *GetApprenticeReward) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	appVersion := rq.Get("app_version")
	ip := common.GetIPFromRequest(r)
	apprenticeCode := rq.Get("apprentice_code")
	apprentice, err := db.GetUserInfo(appID, bson.M{"code": apprenticeCode})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if apprentice == nil {
		errResp := common.OtherErrorResp("徒弟不存在")
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	uar, err := db.GetUserApprenticeRewardInfo(appID, g.usr.ID, apprentice.ID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	nowUnix := time.Now().Unix()
	if uar == nil {
		errResp := common.Resp{
			Timestamp: nowUnix,
			Status:    common.STATUS_APPRENTICE_REWARD_NOT_EXISTS,
		}
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	switch uar.Status {
	case common.STATUS_USER_APPRENTICE_REWARD_INVALID:
		errResp := common.Resp{
			Timestamp: nowUnix,
			Status:    common.STATUS_APPRENTICE_REWARD_INVALID,
		}
		http.Error(w, errResp.String(), http.StatusOK)
		return
	case common.STATUS_USER_APPRENTICE_REWARD_OPENED:
		errResp := common.Resp{
			Timestamp: nowUnix,
			Status:    common.STATUS_USER_APPRENTICE_REWARD_OPENED,
		}
		http.Error(w, errResp.String(), http.StatusOK)
		return
	case common.STATUS_USER_APPRENTICE_REWARD_VALID:
		reward := getRewards()
		if err := db.OpenUserApprenticeReward(appID, g.usr.ID, apprentice.ID, reward, nowUnix); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		uits := db.UserIncomeToSave{
			AppID:      appID,
			Usr:        g.usr,
			OpType:     common.INCOME_TYPE_INCOME,
			TaskID:     "",
			TaskType:   common.TASK_TYPE_USER_APPRENTICE_REWARD,
			Count:      reward,
			Timestamp:  nowUnix,
			Desc:       "师徒宝箱奖励",
			SetMaster:  false,
			MasterRate: 0,
			From:       apprentice.ID,
			AppVersion: appVersion,
			IP:         ip,
		}
		if err := uits.Save(); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		resp := common.SuccessResp(struct {
			Amount int `json:"amount"`
		}{
			Amount: reward,
		})
		http.Error(w, resp.String(), http.StatusOK)
		return
	default:
		errResp := common.OtherErrorResp("异常的师徒宝箱状态")
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
}

func getRewards() int {
	//	rewards := []int{18000, 28000, 38000, 48000, 58000}
	//	weights := []int{80, 15, 10, 3, 2}
	rewards := []int{5000, 18000}
	weights := []int{90, 10}
	return common.GetValueByWeight(weights, rewards)
}
