package routers

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type ShareContentStruct struct{}

func (s *ShareContentStruct) New() common.LogSetHandler {
	return new(ShareContentStruct)
}

func (s *ShareContentStruct) GetAPICode() int {
	return 0
}

func (s *ShareContentStruct) SetLog(yl common.APIRecord) {}

func (s *ShareContentStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ShareContent(w, r)
}

func ShareContent(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	mustParams := []string{"app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	token := rq.Get("token")
	reqType := rq.Get("type")
	http.Error(w, getShareContentByReqType(appID, token, reqType).String(), http.StatusOK)
}

func getShareContentByReqType(appID string, token string, reqType string) common.Resp {
	confMux.RLock()
	h := conf[appID].ShareContentConf
	confMux.RUnlock()
	if h == nil {
		return common.OtherErrorResp("not found")
	}
	if token == "" {
		return common.SuccessResp(
			struct {
				Article common.ShareContentDetail `json:"article"`
			}{
				Article: h.Article,
			})
	}
	if reqType == "0" {
		return common.SuccessResp(
			struct {
				Article common.ShareContentDetail `json:"article"`
			}{
				Article: h.Article,
			})
	}

	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		return common.ServerErrorResp(err.Error())
	}
	if userID == "" {
		return common.TokenInvalidResp()
	}

	ui, err := db.GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		return common.ServerErrorResp(err.Error())
	}
	h.Apprentice.Moments = strings.Replace(h.Apprentice.Moments, "{{.code}}", ui.Code, -1)
	h.Apprentice.Person = strings.Replace(h.Apprentice.Person, "{{.code}}", ui.Code, -1)

	if reqType == "1" {
		return common.SuccessResp(
			struct {
				Apprentice common.ShareContentDetail `json:"apprentice"`
			}{
				Apprentice: h.Apprentice,
			})
	}

	uo, err := db.GetUserBenefit(appID, userID)
	if err != nil {
		return common.ServerErrorResp(err.Error())

	}

	h.ShowBenifit.Moments = strings.Replace(h.ShowBenifit.Moments, "{{.code}}", ui.Code, -1)
	h.ShowBenifit.Moments = strings.Replace(h.ShowBenifit.Moments, "{{.coin}}", strconv.Itoa(ui.IncomeTotal), -1)
	h.ShowBenifit.Moments = strings.Replace(h.ShowBenifit.Moments, "{{.money}}", strconv.Itoa(uo.WithdrawTotal), -1)
	h.ShowBenifit.Person = strings.Replace(h.ShowBenifit.Person, "{{.code}}", ui.Code, -1)
	h.ShowBenifit.Person = strings.Replace(h.ShowBenifit.Person, "{{.coin}}", strconv.Itoa(ui.IncomeTotal), -1)
	h.ShowBenifit.Person = strings.Replace(h.ShowBenifit.Person, "{{.money}}", strconv.Itoa(uo.WithdrawTotal), -1)
	switch reqType {
	case "":
		return common.SuccessResp(h)
	case "2":
		return common.SuccessResp(
			struct {
				ShowBenifit common.ShareContentDetail `json:"show_benefit"`
			}{
				ShowBenifit: h.ShowBenifit,
			})
	default:
		return common.OtherErrorResp("invalid type")
	}
}
