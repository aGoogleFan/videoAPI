package routers

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	// "github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type GetAlipayLoginParamsStruct struct {
	yl common.APIRecord
}

func (g *GetAlipayLoginParamsStruct) New() common.LogSetHandler {
	return new(GetAlipayLoginParamsStruct)
}

func (g *GetAlipayLoginParamsStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_ALIPAY_LOGIN_PARAMS
}

func (g *GetAlipayLoginParamsStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetAlipayLoginParamsStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()

	resString, err := getAlipayLogin(rq.Get("app_id"))
	if err != nil {
		errResp := common.Resp{
			Timestamp: time.Now().Unix(),
			Status:    common.STATUS_SERVER_ERROR,
		}
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(struct {
		ParamsString string `json:"params_string"`
	}{
		ParamsString: resString,
	})
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

func getAlipayLogin(appID string) (string, error) {
	// alipayParams := getAlipayCommonParams("")
	alipayParams := getAlipayCommonParams(appID, "alipay.open.auth.sdk.code.get")
	alipayParams.Set("apiname", "com.alipay.account.auth")
	alipayParams.Set("biz_type", "openservice")
	confMux.RLock()
	alipayPID := conf[appID].ThirdpartConf.Alipay.PID
	confMux.RUnlock()
	alipayParams.Set("pid", alipayPID)
	alipayParams.Set("product_id", "APP_FAST_LOGIN")
	alipayParams.Set("scope", "kuaijie")
	alipayParams.Set("target_id", common.NewUUID())
	alipayParams.Set("auth_type", "LOGIN") // AUTHACCOUNT代表授权；LOGIN代表登录

	signString, err := signWithSha2(appID, alipayParams)
	if err != nil {
		return "", err
	}
	alipayParams.Set("sign", signString)
	var t []string
	for key := range alipayParams {
		t = append(t, fmt.Sprintf("%s=%s", key, url.QueryEscape(alipayParams.Get(key))))
	}
	return strings.Join(t, "&"), nil
}
