package routers

import (
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type SignInStruct struct {
	usr common.UserBasic
	yl  common.APIRecord
}

func (s *SignInStruct) New() common.LogAndUserSetHandler {
	return new(SignInStruct)
}

func (s *SignInStruct) GetAPICode() int {
	return common.API_LOG_CODE_SIGN_IN
}

func (s *SignInStruct) SetLog(yl common.APIRecord) {
	s.yl = yl
}

func (s *SignInStruct) SetUser(usr common.UserBasic) {
	s.usr = usr
}

func (s *SignInStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"token", "app_id"}
	return getUserByToken(rq, mustParams)
}

func (s *SignInStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(s.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")

	recentSignIn, err := db.GetUserSignIn(appID, s.usr.ID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		s.yl.ErrorMsg = err.Error()
		s.yl.RetCode = errResp.Status
		s.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	now := time.Now()
	dateString := now.Local().Format("2006-01-02")
	for _, v := range recentSignIn {
		if dateString != v.Date {
			continue
		}
		errResp := common.OtherErrorResp("the day already sign in")
		s.yl.ErrorMsg = errResp.ErrMsg
		s.yl.RetCode = errResp.Status
		s.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	confMux.RLock()
	asich := conf[appID].SignInConf
	confMux.RUnlock()
	if asich == nil {
		errResp := common.ServerErrorResp("")
		s.yl.ErrorMsg = errResp.ErrMsg
		s.yl.RetCode = errResp.Status
		s.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	asichds := asich.Details
	if asichds == nil {
		errResp := common.ServerErrorResp("")
		s.yl.ErrorMsg = errResp.ErrMsg
		s.yl.RetCode = errResp.Status
		s.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	continuous := getContinuous(now, recentSignIn)
	reward := getRewardByContinuous(continuous+1, asichds)
	if err := db.SaveSignIn(appID, s.usr.ID, now, reward, false); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		s.yl.ErrorMsg = err.Error()
		s.yl.RetCode = errResp.Status
		s.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	confMux.RLock()
	arc := conf[appID].ApprenticeRewardConf
	confMux.RUnlock()
	if arc == nil {
		errResp := common.OtherErrorResp("master rate not found")
		s.yl.ErrorMsg = errResp.ErrMsg
		s.yl.RetCode = errResp.Status
		s.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	masterRate := arc.Rate
	uits := db.UserIncomeToSave{
		AppID:      appID,
		Usr:        s.usr,
		OpType:     common.INCOME_TYPE_INCOME,
		TaskID:     "",
		TaskType:   common.TASK_TYPE_SIGN_IN,
		Count:      reward,
		Timestamp:  now.Unix(),
		Desc:       "签到奖励",
		SetMaster:  true,
		MasterRate: masterRate,
		AppVersion: rq.Get("app_version"),
		IP:         common.GetIPFromRequest(r),
	}
	if err := uits.Save(); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		s.yl.ErrorMsg = err.Error()
		s.yl.RetCode = errResp.Status
		s.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	go func() {
		t := task{
			appID:      appID,
			usr:        s.usr,
			taskType:   common.TASK_TYPE_NEWBIE,
			taskID:     common.TASK_NEWBIE_ID_FIRST_SIGN_IN,
			timestamp:  now.Unix(),
			appVersion: rq.Get("app_version"),
			ip:         common.GetIPFromRequest(r),
		}
		t.complete()
	}()

	s.yl.Reward = reward
	resp := common.SuccessResp(userSignInToReturn{
		Reward:     reward,
		LT:         1,
		Total:      len(recentSignIn) + 1,
		Continuous: continuous + 1,
	})
	s.yl.RetCode = resp.Status
	s.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

type userSignInToReturn struct {
	Reward     int `json:"reward"`
	LT         int `json:"lt"`
	Total      int `json:"total"`
	Continuous int `json:"continuous"`
}
