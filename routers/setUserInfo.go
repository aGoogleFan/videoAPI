package routers

import (
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type SetUserInfoStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (s *SetUserInfoStruct) New() common.LogAndUserSetHandler {
	return new(SetUserInfoStruct)
}

func (s *SetUserInfoStruct) GetAPICode() int {
	return common.API_LOG_CODE_SET_USER_INFO
}

func (s *SetUserInfoStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (s *SetUserInfoStruct) SetUser(ub common.UserBasic) {
	s.usr = ub
}

func (s *SetUserInfoStruct) SetLog(yl common.APIRecord) {
	s.yl = yl
}

func (s *SetUserInfoStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(s.yl)
	}()

	rq := r.URL.Query()
	name := rq.Get("name")
	icon := rq.Get("icon")
	sex := rq.Get("sex")
	birth := rq.Get("birth")
	if icon == "" && sex == "" && birth == "" {
		resp := common.SuccessResp(nil)
		s.yl.Ret = resp.String()
		s.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	cond := make(bson.M)
	if name != "" {
		cond["info.name"] = name
	}
	if icon != "" {
		cond["info.icon"] = icon
	}
	if sex != "" {
		if sex != "0" && sex != "1" && sex != "2" {
			errResp := common.OtherErrorResp("invalid sex")
			s.yl.ErrorMsg = errResp.ErrMsg
			s.yl.Ret = errResp.String()
			s.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		cond["info.sex"], _ = strconv.Atoi(sex)
	}
	if birth != "" {
		if _, err := time.Parse("2006-01-02", birth); err != nil {
			if _, err := time.Parse("2006-1-2", birth); err != nil {
				errResp := common.OtherErrorResp("invalid birth")
				s.yl.ErrorMsg = err.Error()
				s.yl.Ret = errResp.String()
				s.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}
		}
		cond["info.birth"] = birth
	}

	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		s.yl.ErrorMsg = err.Error()
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if userID == "" {
		errResp := common.TokenInvalidResp()
		s.yl.ErrorMsg = errResp.ErrMsg
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	if err := db.UpdateUser(appID, bson.M{"id": userID}, bson.M{"$set": cond}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		s.yl.ErrorMsg = err.Error()
		s.yl.Ret = errResp.String()
		s.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(nil)
	s.yl.Ret = resp.String()
	s.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}
