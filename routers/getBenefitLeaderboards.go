package routers

import (
	"net/http"
	"strconv"

	// "github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetBenifitLeaderboardsStruct struct {
	yl common.APIRecord
}

func (g *GetBenifitLeaderboardsStruct) New() common.LogSetHandler {
	return new(GetBenifitLeaderboardsStruct)
}

func (g *GetBenifitLeaderboardsStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_BENIFIT_LEADERBOARDS
}

func (g *GetBenifitLeaderboardsStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetBenifitLeaderboardsStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	page := 1
	if opage := rq.Get("page"); opage != "" {
		fpage, err := strconv.Atoi(opage)
		if err == nil {
			page = fpage
		}
	}

	tmpBenefits, err := db.GetUserBenefitLeaderBoards(appID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	totalCount := len(tmpBenefits)
	pageContiner := common.PageContiner{
		TotalCount: totalCount,
		MaxPerPage: 10,
		Page:       page,
	}

	if page <= 0 || page > 3 {
		resp := common.SuccessResp(pageContiner)
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageStart := (page - 1) * 10
	if (page-1)*10 > totalCount {
		resp := common.SuccessResp(pageContiner)
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageEnd := page * 10
	if pageEnd > totalCount {
		pageContiner.List = tmpBenefits[pageStart:totalCount]
		resp := common.SuccessResp(pageContiner)
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageContiner.List = tmpBenefits[pageStart:pageEnd]
	resp := common.SuccessResp(pageContiner)
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}
