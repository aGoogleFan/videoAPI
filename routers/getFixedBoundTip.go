package routers

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetFixedBoundTipStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetFixedBoundTipStruct) New() common.LogAndUserSetHandler {
	return new(GetFixedBoundTipStruct)
}

func (g *GetFixedBoundTipStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_FIXED_BOUND_TIP
}

func (g *GetFixedBoundTipStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetFixedBoundTipStruct) SetUser(u common.UserBasic) {
	g.usr = u
}

func (g *GetFixedBoundTipStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"token", "app_id", "type"}
	return getUserByToken(rq, mustParams)
}

func (g *GetFixedBoundTipStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	mType := rq.Get("type")
	nowUinx := time.Now().Unix()
	seconds, coin, isLimit, interval, maxPerTime, err := getDailyRewardDetail(appID, g.usr.ID, mType, nowUinx)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if interval%5 != 0 {
		interval = 5 * (interval/5 + 1)
	}
	var readTitle string
	if mType == "read" {
		readTitle = "阅读"
	}
	if mType == "video" {
		readTitle = "观看视频"
	}
	fbttr := fixedBoundTipToReturn{
		Coin:   coin,
		Time:   seconds,
		IsFull: isLimit,
	}
	hours := seconds / 3600
	minutes := (seconds / 60) % 60
	if seconds%60 != 0 {
		minutes += 1
	}
	if minutes == 0 {
		minutes = 1
	}
	fbttr.MsgTip = fmt.Sprintf("每%s约%d秒最高可获得%d金币.", readTitle, interval, maxPerTime)
	if hours == 0 {
		fbttr.MsgTip += fmt.Sprintf("\n您今日有效%s时间约%d分钟, 获得了%d%s金币", readTitle, minutes, coin, readTitle)
	} else {
		fbttr.MsgTip += fmt.Sprintf("\n您今日有效%s时间约%d小时%d分钟, 获得了%d%s金币", readTitle, hours, minutes, coin, readTitle)
	}
	if isLimit {
		fbttr.MsgTip = fbttr.MsgTip + fmt.Sprintf("\n您今日%s金币已达上限.", readTitle)
	}

	resp := common.SuccessResp(fbttr)
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

type fixedBoundTipToReturn struct {
	Coin   int    `json:"coin"`
	Time   int    `json:"time"`
	IsFull bool   `json:"is_full"`
	MsgTip string `json:"msg_tip"`
}

// 返回值: 当天阅读时间(秒),当天获取阅读金币数,是否阅读已满,阅读奖励时间间隔(秒),每次阅读最高奖励
func getDailyRewardDetail(appID, userID string, mtype string, timestamp int64) (int, int, bool, int, int, error) {
	var interval, maxPerDay, historyTaskType, maxPerTime int
	historyType := common.INCOME_TYPE_INCOME
	switch mtype {
	case "video":
		confMux.RLock()
		vcc := conf[appID].VideoCoinConf
		confMux.RUnlock()
		interval = vcc.TimeInterval
		maxPerDay = vcc.MaxPerDay
		historyTaskType = common.TASK_TYPE_VIDEO_WATCH
		for _, w := range vcc.Weights {
			if w.Weight <= 0 {
				continue
			}
			if w.Coin <= maxPerTime {
				continue
			}
			maxPerTime = w.Coin
		}
	case "read":
		confMux.RLock()
		afbh := conf[appID].FixedBoundConf
		confMux.RUnlock()
		if afbh == nil {
			return 0, 0, false, 0, 0, errors.New("read conf not found")
		}
		interval = getFixedBoundTimeout(*afbh)
		maxPerDay = afbh.MaxPerDay
		historyTaskType = common.TASK_TYPE_FIXED_BOUNDS
		for _, w := range afbh.Probability {
			if w.Rate <= 0 {
				continue
			}
			if w.Amount <= maxPerTime {
				continue
			}
			maxPerTime = w.Amount
		}
	default:
		return 0, 0, false, 0, 0, errors.New("unknown type")
	}

	coin, times, err := db.GetUserDailyIncomeByType(appID, userID, historyType, historyTaskType, timestamp)
	if err != nil {
		return 0, 0, false, interval, maxPerTime, err
	}
	if times == 0 {
		return 0, 0, false, interval, maxPerTime, nil
	}

	return interval * times, coin, coin >= maxPerDay, interval, maxPerTime, nil
}
