package routers

import (
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type DailyManyTimesTask struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (d *DailyManyTimesTask) New() common.LogAndUserSetHandler {
	return new(DailyManyTimesTask)
}

func (d *DailyManyTimesTask) GetAPICode() int {
	return 0
}

func (d *DailyManyTimesTask) SetLog(yl common.APIRecord) {
	d.yl = yl
}

func (d *DailyManyTimesTask) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (d *DailyManyTimesTask) SetUser(u common.UserBasic) {
	d.usr = u
}

func (d *DailyManyTimesTask) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	confMux.Lock()
	dailyManyTimesTasks := conf[appID].DailyManyTimesTasks
	confMux.Unlock()
	t := time.Now().Unix()
	for i, dailyManyTimesTask := range dailyManyTimesTasks {
		spcs, err := db.GetUserSpecialTaskComplete(appID, d.usr.ID, dailyManyTimesTask.TaskID, common.SPECIAL_TASK_TYPE_MULTY, t)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		dailyManyTimesTask.CompleteToday = len(spcs)
		var lastComplete int64
		for _, spc := range spcs {
			if spc.FTimestamp > lastComplete {
				lastComplete = spc.FTimestamp
			}
		}
		passSeconds := int(t - lastComplete)
		if passSeconds >= dailyManyTimesTask.Interval {
			dailyManyTimesTask.TimeInterval = 0
		} else {
			dailyManyTimesTask.TimeInterval = dailyManyTimesTask.Interval - passSeconds
		}
		dailyManyTimesTasks[i] = dailyManyTimesTask
	}

	dmt := dailyManyTimesTaskToReturn{
		Tasks: dailyManyTimesTasks,
	}
	http.Error(w, common.SuccessResp(dmt).String(), http.StatusOK)
}

type dailyManyTimesTaskToReturn struct {
	Tasks []common.DailyManyTimesTask `json:"tasks"`
}
