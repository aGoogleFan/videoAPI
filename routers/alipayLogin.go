package routers

import (
	"net/http"
	"net/url"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type AlipayLoginStruct struct {
	yl common.APIRecord
	ub common.UserBasic
}

func (als *AlipayLoginStruct) New() common.LogAndUserSetHandler {
	return new(AlipayLoginStruct)
}

func (als *AlipayLoginStruct) GetAPICode() int {
	return common.API_LOG_CODE_ALIPAY_LOGIN
}

func (als *AlipayLoginStruct) SetLog(yl common.APIRecord) {
	als.yl = yl
}

func (als *AlipayLoginStruct) SetUser(ub common.UserBasic) {
	als.ub = ub
}

func (als *AlipayLoginStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"alipay_id", "app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		log.Fields{
			"err": errResp.ErrMsg,
		}.Error("params check fail")
		return nil, errResp, nil
	}

	alipayID := rq.Get("alipay_id")

	appID := rq.Get("app_id")
	usr, err := db.GetUserInfo(appID, bson.M{"third_part.alipay.id": alipayID})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		return nil, &errResp, err
	}

	if usr == nil {
		errResp := common.Resp{
			Status:    common.STATUS_ALIPAY_UNBIND,
			Timestamp: time.Now().Unix(),
		}
		return nil, &errResp, nil
	}
	return usr, nil, nil
}

func (als *AlipayLoginStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(als.yl)
	}()
	als.yl.APICode = common.API_LOG_CODE_ALIPAY_LOGIN
	rq := r.URL.Query()
	appID := rq.Get("app_id")

	// 如果是老用户
	if err := db.UpdateUser(appID, bson.M{"id": als.ub.ID}, bson.M{"$set": bson.M{"last_login": time.Now().Unix()}}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		als.yl.Ret = errResp.String()
		als.yl.RetCode = errResp.Status
		als.yl.ErrorMsg = err.Error()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	token := common.NewUUID()
	if err := db.SaveToken(token, als.ub.ID, common.REDIS_KEY_VALID_DURATION_TOKEN); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		als.yl.Ret = errResp.String()
		als.yl.RetCode = errResp.Status
		als.yl.ErrorMsg = err.Error()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	rsp := common.SuccessResp(struct {
		Token string `json:"token"`
		Code  string `json:"code"`
		IsNew bool   `json:"is_new"`
	}{
		Token: token,
		Code:  als.ub.Code,
		IsNew: false,
	})
	als.yl.Ret = rsp.String()
	als.yl.RetCode = rsp.Status
	http.Error(w, rsp.String(), http.StatusOK)
}

func AlipayLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	mustParams := []string{"alipay_id", "app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		log.Fields{
			"err": errResp.ErrMsg,
		}.Error("params check fail")
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	alipayID := rq.Get("alipay_id")

	appID := rq.Get("app_id")
	usr, err := db.GetUserInfo(appID, bson.M{"third_part.alipay.id": alipayID})
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to get user info by alipayid")
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	token := common.NewUUID()

	// 如果是新用户
	if usr == nil {
		errResp := common.Resp{
			Status:    common.STATUS_ALIPAY_NOT_BIND,
			Timestamp: time.Now().Unix(),
		}
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	// 如果是老用户
	if err := db.UpdateUser(appID, bson.M{"id": usr.ID}, bson.M{"$set": bson.M{"last_login": time.Now().Unix()}}); err != nil {
		log.Fields{
			"token":  token,
			"userid": usr.ID,
			"err":    err.Error(),
		}.Error("mongo error")
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if err := db.SaveToken(token, usr.ID, common.REDIS_KEY_VALID_DURATION_TOKEN); err != nil {
		log.Fields{
			"token":  token,
			"userid": usr.ID,
			"err":    err.Error(),
		}.Error("redis error")
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if androidID := rq.Get("android_id"); androidID != "" {
		devicePassed, err := CheckDeviceLimit(appID, usr.ID, androidID)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if !devicePassed {
			http.Error(w, common.Resp{
				Status:    common.STATUS_DEVICE_LIMIT,
				Timestamp: time.Now().Unix(),
			}.String(), http.StatusOK)
			return
		}

		ip := common.GetIPFromRequest(r)
		ipPassed, err := CheckIPLimit(appID, usr.ID, ip)
		if err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
		if !ipPassed {
			http.Error(w, common.Resp{
				Status:    common.STATUS_IP_LIMIT,
				Timestamp: time.Now().Unix(),
			}.String(), http.StatusOK)
			return
		}

		if err := db.SaveUserIP(appID, ip, usr.ID, time.Now().Unix()); err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}

		if err := db.SaveUserDevice(appID, androidID, usr.ID, time.Now().Unix()); err != nil {
			http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
			return
		}
	}

	rsp := common.SuccessResp(struct {
		Token string `json:"token"`
		Code  string `json:"code"`
		IsNew bool   `json:"is_new"`
	}{
		Token: token,
		Code:  usr.Code,
		IsNew: false,
	})
	http.Error(w, rsp.String(), http.StatusOK)
}
