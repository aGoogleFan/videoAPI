package routers

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type ADClickStruct struct {
	user common.UserBasic
	yl   common.APIRecord
}

func (acs *ADClickStruct) New() common.LogAndUserSetHandler {
	return new(ADClickStruct)
}

func (acs *ADClickStruct) GetAPICode() int {
	return common.API_LOG_AD_CLICK
}

func (acs *ADClickStruct) SetLog(yl common.APIRecord) {
	acs.yl = yl
}

func (acs *ADClickStruct) SetUser(ub common.UserBasic) {
	acs.user = ub
}

func (acs *ADClickStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	return getUserByToken(rq, nil)
}

func (acs *ADClickStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(acs.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	adID := rq.Get("ad_id")
	appVersion := strings.ToLower(rq.Get("app_version"))
	appVersion = strings.TrimPrefix(appVersion, "v")
	appVersion = strings.TrimPrefix(appVersion, "b")
	if common.IsNewVersion("1.0.2", appVersion, false) {
		mustParams := []string{"app_id", "ad_id", "ad_req_id", "ad_res", "ad_res_id"}
		if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		ip := common.GetIPFromRequest(r)
		cai := common.ADInfo{
			ADID:       adID,
			ADReqID:    rq.Get("ad_req_id"),
			ADReqType:  common.AD_REQ_TYPE_CLICK,
			ADRes:      rq.Get("ad_res"),
			ADResID:    rq.Get("ad_res_id"),
			IP:         ip,
			Timestamp:  time.Now().Unix(),
			AndroidID:  rq.Get("android_id"),
			UserID:     acs.user.ID,
			AppVersion: rq.Get("app_version"),
		}
		go sendADInfo(appID, cai)
	} else {
		mustParams := []string{"token", "app_id", "ad_id"}
		if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
			acs.yl.RetCode = errResp.Status
			acs.yl.ErrorMsg = errResp.ErrMsg
			acs.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
	}
	if acs.user.ID == "" {
		resp := common.SuccessResp(nil)
		acs.yl.RetCode = resp.Status
		acs.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	// 获取转盘参数
	confMux.RLock()
	acc := conf[appID].AdClickConf
	confMux.RUnlock()
	if acc == nil {
		errResp := common.OtherErrorResp("not found")
		acs.yl.ErrorMsg = errResp.ErrMsg
		acs.yl.Ret = errResp.String()
		acs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	now := time.Now()
	nowTimestamp := now.Unix()
	coin := getAdClickCoin(acc.Weights, now.UnixNano())
	if coin == 0 {
		resp := common.SuccessResp(adClickToReturn{Coin: 0})
		acs.yl.Ret = resp.String()
		acs.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	maxPerDay := acc.MaxPerDay
	if maxPerDay == 0 {
		errResp := common.RewardLimitResp()
		acs.yl.ErrorMsg = errResp.ErrMsg
		acs.yl.Ret = errResp.String()
		acs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	key := fmt.Sprintf("adclick:%s:%s", appID, acs.user.ID)
	d, err := db.CheckIfExpired(key)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		acs.yl.ErrorMsg = errResp.ErrMsg
		acs.yl.Ret = errResp.String()
		acs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if !d {
		errResp := common.Resp{
			Status:    common.STATUS_NOT_IN_ACTIVE_TIME,
			Timestamp: time.Now().Unix(),
		}
		acs.yl.Ret = errResp.String()
		acs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	value := strconv.FormatInt(nowTimestamp, 10)
	timeOut := time.Second * time.Duration(acc.TimeInterval)
	if timeOut > 0 {
		if err := db.SaveToRedis(key, value, timeOut); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			acs.yl.ErrorMsg = err.Error()
			acs.yl.Ret = errResp.String()
			acs.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
	}

	timestamp := time.Now().Unix()
	adIncomeToday, _, err := db.GetUserDailyIncomeByType(appID, acs.user.ID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_AD_CLICK, timestamp)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		acs.yl.ErrorMsg = err.Error()
		acs.yl.Ret = errResp.String()
		acs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	adIDClickTotalToday, _, err := db.GetUserDailyAdClickByADID(appID, acs.user.ID, adID, timestamp)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		acs.yl.ErrorMsg = err.Error()
		acs.yl.Ret = errResp.String()
		acs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if (adIncomeToday >= acc.MaxPerDay && acc.MaxPerDay >= 0) || (adIDClickTotalToday >= acc.MaxPerAdID && acc.MaxPerAdID >= 0) {
		errResp := common.RewardLimitResp()
		acs.yl.Ret = errResp.String()
		acs.yl.RetCode = errResp.Status
		acs.yl.ErrorMsg = errResp.ErrMsg
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	//	ui, err := db.GetUserDailyIncomeHistoryByDetail(appID, acs.user.ID, timestamp, common.TYPE_USR_HISTORY_INCOME, common.TASK_TYPE_AD_CLICK, "")
	//	if err != nil {
	//		errResp := common.ServerErrorResp(err.Error())
	//		acs.yl.ErrorMsg = err.Error()
	//		acs.yl.Ret = errResp.String()
	//		acs.yl.RetCode = errResp.Status
	//		http.Error(w, errResp.String(), http.StatusOK)
	//		return
	//	}

	//	if ui != nil {
	//		dateTotal := 0
	//		dateADIDtotal := 0
	//		for _, v := range ui {
	//			dateTotal = dateTotal + v.Count
	//			if v.From == adID {
	//				dateADIDtotal = dateADIDtotal + v.Count
	//			}
	//		}
	//		if (dateTotal >= acc.MaxPerDay && acc.MaxPerDay >= 0) || (dateADIDtotal > acc.MaxPerAdID && acc.MaxPerAdID >= 0) {
	//			errResp := common.RewardLimitResp()
	//			acs.yl.Ret = errResp.String()
	//			acs.yl.RetCode = errResp.Status
	//			acs.yl.ErrorMsg = errResp.ErrMsg
	//			http.Error(w, errResp.String(), http.StatusOK)
	//			return
	//		}
	//	}

	uits := db.UserIncomeToSave{
		AppID:      appID,
		Usr:        acs.user,
		OpType:     common.INCOME_TYPE_INCOME,
		TaskID:     "",
		TaskType:   common.TASK_TYPE_AD_CLICK,
		Count:      coin,
		Timestamp:  timestamp,
		Desc:       "神秘点击",
		SetMaster:  false,
		From:       adID,
		AppVersion: rq.Get("app_version"),
		IP:         common.GetIPFromRequest(r),
	}
	if err := uits.Save(); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		acs.yl.ErrorMsg = err.Error()
		acs.yl.Ret = errResp.String()
		acs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	acs.yl.Reward = coin
	resp := common.SuccessResp(adClickToReturn{Coin: coin})
	acs.yl.Ret = resp.String()
	acs.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

type adClickToReturn struct {
	Coin int `json:"coin"`
}

func getAdClickCoin(weights []common.CoinWeight, seed int64) int {
	m := make([][2]int, 0) // []{金币数, 范围}
	current := 0
	for _, weight := range weights {
		if weight.Coin < 0 {
			continue
		}
		if weight.Weight <= 0 {
			continue
		}
		current += weight.Weight
		m = append(m, [2]int{weight.Coin, current})
	}
	target := int(seed) % current
	fewest, largest := 0, 0
	for _, v := range m {
		largest = v[1]
		if fewest <= target && target < largest {
			return v[0]
		}
		fewest = largest
	}
	return 0
}
