package routers

import (
	"net/http"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type GetFixedBoundConfStruct struct {
	yl common.APIRecord
}

func (g *GetFixedBoundConfStruct) New() common.LogSetHandler {
	return new(GetFixedBoundConfStruct)
}

func (g *GetFixedBoundConfStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_READ_FIXED_BOUND_CONF
}

func (g *GetFixedBoundConfStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetFixedBoundConfStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	confMux.RLock()
	cb := conf[appID].FixedBoundConf
	confMux.RUnlock()

	resp := common.SuccessResp(cb)
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}
