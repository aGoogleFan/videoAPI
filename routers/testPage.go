package routers

import (
	"fmt"
	"net/http"

	"vlion/yxw/common"
)

func TestPage(w http.ResponseWriter, r *http.Request) {
	ip := common.GetIPFromRequest(r)
	http.Error(w, fmt.Sprintf("尊敬的阅新闻用户, 这是一个测试页. 如果您能看到这个页面, 说明您到阅新闻之间的链路是畅通的.\n您的ip是: %s\n", ip), http.StatusOK)
}
