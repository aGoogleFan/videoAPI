package routers

import (
	"net/http"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

func QQBind(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	mustParams := []string{"openid", "token"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if userID == "" {
		http.Error(w, common.TokenInvalidResp().String(), http.StatusOK)
		return
	}

	usr, err := db.GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		errResp := common.OtherErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if usr.ThirdPart.QQ.OpenID != "" {
		errResp := common.OtherErrorResp("already bind wechat")
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	openID := rq.Get("openid")
	if err := db.UpdateUser(appID, bson.M{"id": userID}, bson.M{"$set": bson.M{"third_part.qq.openid": openID, "third_part.qq.bind_time": time.Now().Unix()}}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	http.Error(w, common.SuccessResp(nil).String(), http.StatusOK)
}
