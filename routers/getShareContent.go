package routers

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

const (
	TYPE_SHARE_ARTICLE    = "0"
	TYPE_SHARE_APPRENTICE = "1"
	TYPE_SHARE_BENEFIT    = "2"
)

type GetShareContentStruct struct {
	yl common.APIRecord
}

func (g *GetShareContentStruct) New() common.LogSetHandler {
	return new(GetShareContentStruct)
}

func (g *GetShareContentStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_SHARE_CONTENT
}

func (g *GetShareContentStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetShareContentStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()
	mustParams := []string{"app_id", "type"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	token := rq.Get("token")
	confMux.RLock()
	h := conf[appID].ShareContentConf
	confMux.RUnlock()
	if h == nil {
		errResp := common.OtherErrorResp("not found")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	reqType := rq.Get("type")
	switch reqType {
	case TYPE_SHARE_ARTICLE:
		resp := common.SuccessResp(h.Article)
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	case TYPE_SHARE_APPRENTICE:
		userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if userID == "" {
			errResp := common.TokenInvalidResp()
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		ui, err := db.GetUserInfo(appID, bson.M{"id": userID})
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if ui == nil {
			errResp := common.TokenInvalidResp()
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		h.Apprentice.Moments = strings.Replace(h.Apprentice.Moments, "{{.code}}", ui.Code, -1)
		h.Apprentice.Person = strings.Replace(h.Apprentice.Person, "{{.code}}", ui.Code, -1)
		http.Error(w, common.SuccessResp(h.Apprentice).String(), http.StatusOK)
		return
	case TYPE_SHARE_BENEFIT:
		userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if userID == "" {
			errResp := common.TokenInvalidResp()
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		ui, err := db.GetUserInfo(appID, bson.M{"id": userID})
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if ui == nil {
			errResp := common.TokenInvalidResp()
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		uo, err := db.GetUserBenefit(appID, userID)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		withdrawTotal := 0
		if uo != nil {
			withdrawTotal = uo.WithdrawTotal
		}
		go func() {
			user, err := db.GetUserInfo(appID, bson.M{"id": userID})
			if err != nil {
				return
			}
			if user == nil {
				return
			}

			nowTimestamp := time.Now().Unix()
			key := fmt.Sprintf("task_daily_apprentice_%s", userID)
			db.SaveToRedis(key, strconv.FormatInt(nowTimestamp, 10), time.Hour*24)
		}()
		withdrawTotalString := strconv.Itoa(withdrawTotal)
		incomeTotalString := strconv.Itoa(ui.IncomeTotal)
		h.ShowBenifit.Moments = strings.Replace(h.ShowBenifit.Moments, "{{.code}}", ui.Code, -1)
		h.ShowBenifit.Moments = strings.Replace(h.ShowBenifit.Moments, "{{.coin}}", incomeTotalString, -1)
		h.ShowBenifit.Moments = strings.Replace(h.ShowBenifit.Moments, "{{.money}}", withdrawTotalString, -1)
		h.ShowBenifit.Person = strings.Replace(h.ShowBenifit.Person, "{{.code}}", ui.Code, -1)
		h.ShowBenifit.Person = strings.Replace(h.ShowBenifit.Person, "{{.coin}}", incomeTotalString, -1)
		h.ShowBenifit.Person = strings.Replace(h.ShowBenifit.Person, "{{.money}}", withdrawTotalString, -1)
		resp := common.SuccessResp(h.ShowBenifit)
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	default:
		errResp := common.OtherErrorResp("invalid type")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
}
