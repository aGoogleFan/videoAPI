package routers

import (
	"net/http"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetPushNewsDetailsStruct struct {
	yl common.APIRecord
}

func (g *GetPushNewsDetailsStruct) New() common.LogSetHandler {
	return new(GetPushNewsDetailsStruct)
}

func (g *GetPushNewsDetailsStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_PUSH_NEWS_DETAILS
}

func (g *GetPushNewsDetailsStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetPushNewsDetailsStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() { yxwLog.RecordLog(g.yl) }()
	rq := r.URL.Query()
	mustParams := []string{"id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	newsID := rq.Get("id")
	youmengPushMessage, err := db.GetPushArticleByID(appID, newsID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if youmengPushMessage == nil {
		errResp := common.ServerErrorResp("not found")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if rq.Get("inApp") == "true" {
		resp := common.SuccessResp(pushDetailToRet{
			ID:    newsID,
			Title: youmengPushMessage.Article.ArticleTitle,
			Desc:  youmengPushMessage.Message,
			URL:   youmengPushMessage.Article.ArticleURL,
		})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	http.Redirect(w, r, youmengPushMessage.Article.ArticleURL, http.StatusFound)
	go func() {
		if youmengPushMessage.Reward == 0 {
			return
		}
		if rq.Get("token") == "" {
			return
		}
		usr, _, _ := getUserByToken(rq, nil)
		if usr == nil {
			return
		}
		uih, err := db.GetUserIncomeHistory(appID, usr.ID)
		if err != nil {
			return
		}
		switch uih == nil {
		case false:
			for _, h := range uih {
				if h.Type != common.INCOME_TYPE_INCOME {
					continue
				}
				if h.TaskType != common.TASK_TYPE_READ_PUSH {
					continue
				}
				if h.Timestamp < youmengPushMessage.Timestamp {
					continue
				}
				if h.From == "" || h.From != youmengPushMessage.Ret.Data.MessageID {
					continue
				}
				return
			}
			fallthrough
		case true:
			uits := db.UserIncomeToSave{
				AppID:      appID,
				Usr:        *usr,
				OpType:     common.INCOME_TYPE_INCOME,
				TaskID:     "",
				TaskType:   common.TASK_TYPE_READ_PUSH,
				Count:      youmengPushMessage.Reward,
				Timestamp:  time.Now().Unix(),
				Desc:       "阅读推送新闻奖励",
				SetMaster:  false,
				MasterRate: 0,
				From:       youmengPushMessage.Ret.Data.MessageID,
				AppVersion: rq.Get("app_version"),
				IP:         common.GetIPFromRequest(r),
			}
			uits.Save()
		}
	}()

}

type pushDetailToRet struct {
	ID    string `json:"id"`
	Title string `json:"title"`
	Desc  string `json:"desc"`
	URL   string `json:"url"`
}
