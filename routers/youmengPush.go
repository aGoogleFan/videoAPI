package routers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

const YoumengPushPostURL = "http://msg.umeng.com/api/send"

func YoumengPush(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	mustParams := []string{"app_id", "to", "message"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	to := rq.Get("to")
	message := rq.Get("message")
	sendTime := rq.Get("send_time")
	title := rq.Get("title")
	deepLink := rq.Get("deep_link")
	articleID := rq.Get("article_id")
	articleURL := rq.Get("article_url")
	if articleURL == "" {
		articleURL = rq.Get("artcle_url")
	}
	articleTitle := rq.Get("article_title")
	if articleID == "" && articleURL != "" {
		articleID = common.StringToMD5(articleURL)
	}
	switch deepLink {
	case "0":
	case "1":
		deepLink = "yxw://yuexinwen.cn/tab_reading"
	case "2":
		deepLink = "yxw://yuexinwen.cn/tab_video"
	case "3":
		deepLink = "yxw://yuexinwen.cn/tab_task"
	case "4":
		deepLink = "yxw://yuexinwen.cn/tab_my"
	case "5":
		deepLink = "yxw://yuexinwen.cn/login"
	case "6":
		deepLink = "yxw://yuexinwen.cn/setting"
	case "7":
		deepLink = "yxw://yuexinwen.cn/shoutu"
	case "8":
		deepLink = "yxw://yuexinwen.cn/orders"
	case "9":
		deepLink = "yxw://yuexinwen.cn/mall"
	case "10":
		deepLink = "yxw://yuexinwen.cn/income"
	case "11":
		deepLink = "yxw://yuexinwen.cn/newsDetails?id=" + articleID
	case "yxw://yuexinwen.cn/newsDetails":
		deepLink = "yxw://yuexinwen.cn/newsDetails?id=" + articleID
	default:

	}

	now := time.Now()
	if sendTime != "" {
		if _, err := time.Parse("2006-01-02 15:04:05", sendTime); err != nil {
			http.Error(w, common.OtherErrorResp("time format must be yyyy-MM-dd HH:mm:ss").String(), http.StatusOK)
			return
		}
	}
	nowTimestamp := now.Unix()
	outBizNo := common.NewUUID()
	confMux.RLock()
	youmengPushAppKey := conf[appID].ThirdpartConf.YoumengPush.AppKey
	youmengPushAppSecret := conf[appID].ThirdpartConf.YoumengPush.AppMasterSecret
	confMux.RUnlock()
	yp := common.YoumengPush{
		AppKey:    youmengPushAppKey,
		Timestamp: strconv.FormatInt(nowTimestamp, 10),
		PayLoad: common.YoumengPushPayLoad{
			DisplayType: "notification",
			Body: common.YoumengPushPayLoadBody{
				Ticker:    message,
				Title:     title,
				Text:      message,
				AfterOpen: "go_activity",
				Activity:  "com.aym.toutiao.MainActivity",
				Custom:    nil,
			},
			Extra: map[string]string{"deep_link": deepLink},
		},
		Policy: common.YoumengPushPolicy{
			OutBizNo: outBizNo,
		},
		MiPush: "true",
	}
	if sendTime != "" {
		yp.Policy.StartTime = sendTime
	}

	if to == "all" {
		yp.Type = "broadcast"
	} else {
		yp.Type = "customizedcast"
		yp.AliasType = "OPEN_ID"
		yp.Alias = to
	}
	if sendTime != "" {
		yp.Policy.StartTime = sendTime
	}

	reward, _ := strconv.Atoi(rq.Get("reward"))
	ypts := common.YoumengPushToSave{
		To:        strings.Split(to, "|"),
		Timestamp: nowTimestamp,
		SendTime:  sendTime,
		DeepLink:  deepLink,
		Title:     title,
		Message:   message,
		Reward:    reward,
		Article: common.YoumengPushArticle{
			ArticleID:    articleID,
			ArticleTitle: articleTitle,
			ArticleURL:   articleURL,
		},
	}
	if ypts.SendTime == "" {
		ypts.SendTime = now.Local().Format("2006-01-02 15:04:05")
	}

	b, err := json.Marshal(yp)
	if err != nil {
		ypts.Success = false
		ypts.Reason = err.Error()
		db.SaveYoumengPush(appID, ypts)
		http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
		return
	}

	stringToBeSign := fmt.Sprintf("%s%s%s%s", http.MethodPost, YoumengPushPostURL, b, youmengPushAppSecret)
	sign := strings.ToLower(common.StringToMD5(stringToBeSign))
	t := YoumengPushPostURL + "?sign=" + url.QueryEscape(sign)
	resp, err := http.Post(t, "application/json;charset=utf-8", bytes.NewBuffer(b))
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to send message to Youmeng push")
		ypts.Success = false
		ypts.Reason = err.Error()
		db.SaveYoumengPush(appID, ypts)
		http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
		return
	}

	ct, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to send message through Youmeng push")
		ypts.Success = false
		ypts.Reason = err.Error()
		db.SaveYoumengPush(appID, ypts)
		http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
		return
	}

	var ypr common.YoumengPushRet
	if err := json.Unmarshal(ct, &ypr); err != nil {
		ypts.Success = false
		ypts.Reason = err.Error()
		db.SaveYoumengPush(appID, ypts)
		http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
		ypts.Success = false
		ypts.Reason = err.Error()
		db.SaveYoumengPush(appID, ypts)
		return
	}

	ypts.Ret = ypr
	if ypr.Ret != "SUCCESS" {
		ypts.Success = false
		db.SaveYoumengPush(appID, ypts)
		log.Fields{
			"errcode": ypr.Data.ErrCode,
			"errmsg":  ypr.Data.ErrMsg,
		}.Error("fail to send message")
		http.Error(w, common.ServerErrorResp(fmt.Sprintf("code: %s, msg: %s", ypr.Data.ErrCode, ypr.Data.ErrMsg)).String(), http.StatusOK)
		return
	}

	ypts.Success = true
	db.SaveYoumengPush(appID, ypts)
	http.Error(w, common.SuccessResp(nil).String(), http.StatusOK)
	return
}
