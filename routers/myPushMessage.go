package routers

import (
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type MyPushMessageStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (m *MyPushMessageStruct) New() common.LogAndUserSetHandler {
	return new(MyPushMessageStruct)
}

func (m *MyPushMessageStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_MY_PUSH_MESSAGE
}

func (m *MyPushMessageStruct) SetLog(yl common.APIRecord) {
	m.yl = yl
}

func (m *MyPushMessageStruct) SetUser(usr common.UserBasic) {
	m.usr = usr
}

func (m *MyPushMessageStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token", "page"}
	return getUserByToken(rq, mustParams)
}

func (m *MyPushMessageStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	defer func() {
		yxwLog.RecordLog(m.yl)
	}()
	rq := r.URL.Query()

	appID := rq.Get("app_id")
	yptss, err := db.GetUserPushMessage(appID, m.usr.ID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		m.yl.ErrorMsg = err.Error()
		m.yl.RetCode = errResp.Status
		m.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	page := 1
	if opage := rq.Get("page"); opage != "" {
		fpage, err := strconv.Atoi(opage)
		if err == nil {
			page = fpage
		}
	}
	yptsLan := len(yptss)
	pageContiner := common.PageContiner{
		TotalCount: yptsLan,
		MaxPerPage: 10,
		Page:       page,
	}

	if yptsLan == 0 {
		resp := common.SuccessResp(pageContiner)
		m.yl.RetCode = resp.Status
		m.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	mptrs := make([]myPushToReturn, yptsLan)
	for i, ypts := range yptss {
		mptrs[i].Title = ypts.Title
		mptrs[i].Content = ypts.Message
		mptrs[i].Success = ypts.Success
		mptrs[i].SendTime = ypts.SendTime
		mptrs[i].Action = ypts.DeepLink
		mptrs[i].Timestamp = ypts.Timestamp
		if !mptrs[i].Success {
			if mptrs[i].Reason = ypts.Reason; mptrs[i].Reason != "" {
				mptrs[i].Reason = ypts.Ret.Data.ErrMsg
			}
		}
	}

	sort.Sort(sort.Reverse(mPTRS(mptrs)))
	if page <= 0 || page > (yptsLan/10)+1 {
		resp := common.SuccessResp(pageContiner)
		m.yl.RetCode = resp.Status
		m.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageStart := (page - 1) * 10
	if (page-1)*10 > yptsLan {
		resp := common.SuccessResp(pageContiner)
		m.yl.RetCode = resp.Status
		m.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageEnd := page * 10
	if pageEnd > yptsLan {
		pageContiner.List = mptrs[pageStart:yptsLan]
		resp := common.SuccessResp(pageContiner)
		m.yl.RetCode = resp.Status
		m.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageContiner.List = mptrs[pageStart:pageEnd]
	resp := common.SuccessResp(pageContiner)
	m.yl.RetCode = resp.Status
	m.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

type myPushToReturn struct {
	Title     string `json:"title"`
	Content   string `json:"content"`
	Success   bool   `json:"success"`
	SendTime  string `json:"send_time"`
	Reason    string `json:"reason,omitempty"`
	Action    string `json:"action"`
	Timestamp int64  `json:"timestamp"`
}

type mPTRS []myPushToReturn

func (m mPTRS) Len() int      { return len(m) }
func (m mPTRS) Swap(i, j int) { m[i], m[j] = m[j], m[i] }
func (m mPTRS) Less(i, j int) bool {
	return getMPTRTimestamp(m[i]) < getMPTRTimestamp(m[j])
}

func getMPTRTimestamp(m myPushToReturn) int64 {
	if m.SendTime == "" {
		return m.Timestamp
	}
	t, err := time.ParseInLocation("2006-01-02 15:04:05", m.SendTime, time.Local)
	if err != nil {
		return m.Timestamp
	}
	return t.Unix()
}
