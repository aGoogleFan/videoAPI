package routers

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"

	"vlion/yxw/common"
)

const (
	WECHAT_TOKEN_URL    = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code"
	WECHAT_USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s"
	WECHAT_TRANSFER_URL = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers"
)

func getWechatAccessTokenByWechatCode(appID, wechatCode string) (*common.WechatAccessToken, *common.Resp) {
	confMux.RLock()
	wechatAppID := conf[appID].ThirdpartConf.Wechat.AppID
	wechatAppSecret := conf[appID].ThirdpartConf.Wechat.AppSecret
	confMux.RUnlock()
	targetURL := fmt.Sprintf(WECHAT_TOKEN_URL, url.QueryEscape(wechatAppID), url.QueryEscape(wechatAppSecret), url.QueryEscape(wechatCode))
	ct, statusCode, err := common.VisitURL(targetURL, time.Second)
	if err != nil {
		errResp := common.OtherErrorResp(err.Error())
		return nil, &errResp
	}

	if statusCode != http.StatusOK {
		errResp := common.ServerErrorResp(fmt.Sprintf("wechat return unexpected code: %d", statusCode))
		return nil, &errResp
	}

	var wat common.WechatAccessToken
	if err := json.Unmarshal(ct, &wat); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		return nil, &errResp
	}
	return &wat, nil
}

func getWechatUserInfoByAccessToken(openID, accessToken string) (*common.WechatUserInfo, error) {
	targetURL := fmt.Sprintf(WECHAT_USERINFO_URL, accessToken, openID)
	ct, statusCode, err := common.VisitURL(targetURL, time.Second)
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, fmt.Errorf("wechat return unexpected code: %d", statusCode)
	}

	var wui common.WechatUserInfo
	if err := json.Unmarshal(ct, &wui); err != nil {
		return nil, err
	}
	return &wui, nil
}

func getWechatSign(appID string, u url.Values) string {
	var keys []string
	for key := range u {
		if key == "sign" {
			continue
		}
		if u.Get(key) == "" {
			continue
		}
		keys = append(keys, key)
	}
	sort.Strings(keys)

	var targetStringSlice []string
	for _, key := range keys {
		targetStringSlice = append(targetStringSlice, fmt.Sprintf("%s=%s", key, u.Get(key)))
	}

	confMux.RLock()
	wechatAPISecret := conf[appID].ThirdpartConf.Wechat.APISecret
	confMux.RUnlock()
	stringToBeSign := strings.Join(targetStringSlice, "&") + "&key=" + wechatAPISecret
	return strings.ToUpper(common.StringToMD5(stringToBeSign))
}

// 微信转账
type WechatTransfer struct {
	XMLName xml.Name `xml:"xml"`

	// 申请商户号的appid或商户号绑定的appid（企业号corpid即为此appId）
	MchAPPID string `xml:"mch_appid"`

	// 微信支付分配的商户号
	MchID string `xml:"mchid"`

	// 随机字符串，不长于32位
	NonceStr string `xml:"nonce_str"`

	// 签名
	Sign string `xml:"sign"`

	// 商户订单号，需保持唯一性 (只能是字母或者数字，不能包含有符号)
	PartnerTradeNo string `xml:"partner_trade_no"`

	// 商户appid下，某用户的openid
	OpenID string `xml:"openid"`

	// NO_CHECK：不校验真实姓名  FORCE_CHECK：强校验真实姓名
	CheckName string `xml:"check_name"`

	// 收款用户真实姓名。如果check_name设置为FORCE_CHECK，则必填用户真实姓名
	ReUserName string `xml:"re_user_name"`

	// 企业付款金额，单位为分
	Amount int `xml:"amount"`

	// 企业付款操作说明信息。必填。
	Desc string `xml:"desc"`

	// 调用接口的机器IP地址
	SpbillCreateIP string `xml:"spbill_create_ip"`
}
