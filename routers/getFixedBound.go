package routers

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	// "github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetFixedBoundStruct struct {
	usr common.UserBasic
	yl  common.APIRecord
}

func (g *GetFixedBoundStruct) New() common.LogAndUserSetHandler {
	return new(GetFixedBoundStruct)
}

func (g *GetFixedBoundStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_READ_REWARD
}

func (g *GetFixedBoundStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetFixedBoundStruct) SetUser(u common.UserBasic) {
	g.usr = u
}

func (g *GetFixedBoundStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	appVersion := strings.ToLower(rq.Get("app_version"))
	appVersion = strings.TrimPrefix(appVersion, "v")
	appVersion = strings.TrimPrefix(appVersion, "b")

	mustParams := []string{"token", "app_id"}

	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		return nil, errResp, nil
	}
	return getUserByToken(rq, mustParams)
}

func (g *GetFixedBoundStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()
	appID := rq.Get("app_id")

	// 获取转盘参数
	confMux.RLock()
	afbh := conf[appID].FixedBoundConf
	confMux.RUnlock()
	if afbh == nil {
		errResp := common.OtherErrorResp("not found")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	maxPerDay := afbh.MaxPerDay
	if maxPerDay == 0 {
		errResp := common.RewardLimitResp()
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	psLen := len(afbh.Probability)
	if psLen == 0 {
		errResp := common.OtherErrorResp("not found")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	weights, values := make([]int, psLen), make([]int, psLen)
	for i, p := range afbh.Probability {
		weights[i] = int(p.Rate)
		values[i] = p.Amount
	}

	coin := common.GetValueByWeight(weights, values)
	if coin == 0 {
		resp := common.SuccessResp(fixedBoundToReturn{Reward: 0})
		g.yl.Ret = resp.String()
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	key := fmt.Sprintf("fixedbound:%s:%s", appID, g.usr.ID)
	d, err := db.CheckIfExpired(key)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	nowTimestamp := time.Now().Unix()
	if !d {
		errResp := common.Resp{
			Status:    common.STATUS_NOT_IN_ACTIVE_TIME,
			Timestamp: time.Now().Unix(),
		}
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	value := strconv.FormatInt(nowTimestamp, 10)

	// 因为客户端提交间隔有可能比服务端短, 故而预留50%余量
	oTimeout := getFixedBoundTimeout(*afbh)
	timeOut := time.Second * time.Duration(oTimeout)
	if oTimeout >= 2 {
		timeOut = time.Second * time.Duration(float64(oTimeout)*0.5)
	}
	if timeOut != 0 {
		if err := db.SaveToRedis(key, value, timeOut); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
	}

	dateTotal, _, err := db.GetUserDailyIncomeByType(appID, g.usr.ID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_FIXED_BOUNDS, nowTimestamp)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if dateTotal >= afbh.MaxPerDay {
		errResp := common.RewardLimitResp()
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	confMux.RLock()
	arc := conf[appID].ApprenticeRewardConf
	confMux.RUnlock()
	if arc == nil {
		errResp := common.OtherErrorResp("master rate not found")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	masterRate := arc.Rate
	uits := db.UserIncomeToSave{
		AppID:      appID,
		Usr:        g.usr,
		OpType:     common.INCOME_TYPE_INCOME,
		TaskID:     "",
		TaskType:   common.TASK_TYPE_FIXED_BOUNDS,
		Count:      coin,
		Timestamp:  nowTimestamp,
		Desc:       "阅读奖励",
		SetMaster:  true,
		MasterRate: masterRate,
		AppVersion: rq.Get("app_version"),
		IP:         common.GetIPFromRequest(r),
	}
	if err := uits.Save(); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	g.yl.Reward = coin

	go func() {
		t := task{
			appID:      appID,
			usr:        g.usr,
			taskType:   common.TASK_TYPE_DAILY,
			taskID:     common.TASK_DAILY_ID_READ_ARTICLE,
			timestamp:  nowTimestamp,
			appVersion: rq.Get("app_version"),
			ip:         common.GetIPFromRequest(r),
		}
		t.complete()
	}()

	go func() {
		t := task{
			appID:      appID,
			usr:        g.usr,
			taskType:   common.TASK_TYPE_NEWBIE,
			taskID:     common.TASK_NEWBIE_ID_FIRST_READ,
			timestamp:  nowTimestamp,
			appVersion: rq.Get("app_version"),
			ip:         common.GetIPFromRequest(r),
		}
		t.complete()
	}()

	go func() {
		seconds, _, _, _, _, err := getDailyRewardDetail(appID, g.usr.ID, "read", nowTimestamp)
		if err != nil {
			return
		}
		if seconds >= 3600 {
			t := task{
				appID:      appID,
				usr:        g.usr,
				taskType:   common.TASK_TYPE_DAILY,
				taskID:     common.TASK_DAILY_ID_READ_AN_HOUR,
				timestamp:  nowTimestamp,
				appVersion: rq.Get("app_version"),
				ip:         common.GetIPFromRequest(r),
			}
			t.complete()
		}
	}()

	go func() {
		if g.usr.From == "" {
			return
		}
		validApprenticeReward(appID, g.usr.From, g.usr.ID, nowTimestamp)
	}()

	var successResp common.Resp

	successResp = common.SuccessResp(fixedBoundToReturn{Reward: coin})

	g.yl.Ret = successResp.String()
	g.yl.RetCode = successResp.Status
	http.Error(w, successResp.String(), http.StatusOK)

	go func() {
		if g.usr.ThirdPart.Wechat.OpenID == "" {
			return
		}
		confMux.RLock()
		uarchd := conf[appID].ApprenticeRewardConf
		confMux.RUnlock()
		if uarchd == nil {
			return
		}
		if len(uarchd.Rewards) == 0 {
			return
		}

		if uarchd.MustRead > dateTotal+coin {
			return
		}
		reward, days, err := setApprenticeMiddle(appID, g.usr.ID, nowTimestamp, uarchd.Rewards)
		if err != nil {
			return
		}
		if reward == 0 {
			return
		}

		user, err := db.GetUserInfo(appID, bson.M{"id": g.usr.From})
		if err != nil {
			return
		}
		if user == nil {
			return
		}
		auits := db.UserIncomeToSave{
			AppID:      appID,
			Usr:        *user,
			OpType:     common.INCOME_TYPE_INCOME,
			TaskID:     "",
			TaskType:   common.TASK_TYPE_APPRENTICE_ONCE,
			Count:      reward,
			Timestamp:  nowTimestamp,
			Desc:       fmt.Sprintf("徒弟第%d天奖励", days),
			SetMaster:  false,
			AppVersion: rq.Get("app_version"),
			From:       g.usr.ID,
		}
		auits.Save()
	}()
}

// 获取时间间隔
func getFixedBoundTimeout(afbh common.AppFixedBoundHistory) int {
	if afbh.RatePerPart > 99 || afbh.RatePerPart <= 0 {
		return 0
	}
	if afbh.TimePerRate <= 0 {
		return 0
	}
	val := afbh.TimePerRate * (99 / afbh.RatePerPart)
	if val <= 1 {
		return val
	}
	return val - 1
}

func setApprenticeMiddle(appID, userID string, timestamp int64, rewards []int) (int, int, error) {
	uma, err := db.GetUserMiddleInfo(appID, userID)
	if err != nil {
		return 0, 0, err
	}
	if uma == nil {
		return 0, 0, nil
	}
	if uma.Finished {
		return 0, 0, nil
	}

	var reward int
	umaDetailsLen := len(uma.Details)
	rewardLen := len(rewards)
	getReawrd := false
	if umaDetailsLen == 0 {
		reward = rewards[0]
		getReawrd = true
	} else {
		lastUpdate := uma.Details[umaDetailsLen-1]
		dateStart := common.GetDateStartUnix(timestamp)
		dateEnd := common.GetDateEndUnix(timestamp)
		if lastUpdate >= dateStart && lastUpdate < dateEnd {
			return 0, 0, nil
		}
		if umaDetailsLen < rewardLen {
			reward = rewards[umaDetailsLen]
			getReawrd = true
		}
	}
	var updator bson.M
	if !getReawrd {
		if umaDetailsLen < rewardLen {
			return 0, 0, nil
		}
		updator = bson.M{"$set": bson.M{"finished": true}}
	} else {
		if umaDetailsLen+1 == rewardLen {
			updator = bson.M{
				"$set": bson.M{
					"finished": true,
					"details":  append(uma.Details, timestamp),
				},
			}
		} else {
			updator = bson.M{
				"$set": bson.M{
					"details": append(uma.Details, timestamp),
				},
			}
		}
	}

	return reward, umaDetailsLen + 1, db.UpdateApprenticeMiddle(appID, userID, updator)
}

type fixedBoundToReturn struct {
	Reward int `json:"reward"`
}

//func apprentceDateValid(readMust int, ui []common.UserIncomeHistory) (bool, error) {
//	dateTotal := 0
//	for _, v := range ui {
//		if v.Type == common.INCOME_TYPE_INCOME && v.TaskType == common.TASK_TYPE_FIXED_BOUNDS {
//			dateTotal = dateTotal + v.Count
//			continue
//		}
//	}
//	return dateTotal >= readMust, nil
//}
