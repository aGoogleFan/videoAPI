package routers

import (
	"net/http"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type GetAdsStruct struct {
	yl common.APIRecord
}

func (g *GetAdsStruct) New() common.LogSetHandler {
	return new(GetAdsStruct)
}

func (g *GetAdsStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_ADS
}

func (g *GetAdsStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetAdsStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	confMux.RLock()
	ads := conf[appID].AdPositionConf
	confMux.RUnlock()
	if ads == nil {
		errResp := common.OtherErrorResp("not found")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	adID := rq.Get("ad_id")
	var validH common.AppAdsHistory
	for _, d := range ads.Details {
		if d.Status == common.ADS_STATUS_INVALID {
			continue
		}
		if adID != "" {
			if d.ADID != adID {
				continue
			}
			validH.Details = append(validH.Details, d)
			continue
		}

		validH.Details = append(validH.Details, d)
	}

	if validH.Details == nil {
		errResp := common.OtherErrorResp("not found")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(validH)
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}
