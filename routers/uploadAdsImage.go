package routers

import (
	"net/http"

	"vlion/yxw/common"
)

func UploadAdsImage(w http.ResponseWriter, r *http.Request) {
	path := "adsimages"

	_, fileURL, needContinue, _ := UploadInnerImage(w, r, path, false, "", nil)
	if !needContinue {
		return
	}
	http.Error(w, common.SuccessResp(fileURL).String(), http.StatusOK)
}
