package routers

import (
	"net/http"
	"strconv"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type GetRecentWithdrawListStruct struct {
	yl common.APIRecord
}

func (g *GetRecentWithdrawListStruct) New() common.LogSetHandler {
	return new(GetRecentWithdrawListStruct)
}

func (g *GetRecentWithdrawListStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_RECENT_WITHDRAW_LIST
}

func (g *GetRecentWithdrawListStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetRecentWithdrawListStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	page := 1
	if opage := rq.Get("page"); opage != "" {
		fpage, err := strconv.Atoi(opage)
		if err == nil {
			page = fpage
		}
	}

	confMux.Lock()
	recentWithdrawList := conf[appID].AppRecentWithdrawInfo
	confMux.Unlock()

	totalCount := len(recentWithdrawList)
	pageContiner := common.PageContiner{
		TotalCount: totalCount,
		MaxPerPage: 10,
		Page:       page,
	}

	if page <= 0 || page > 3 {
		resp := common.SuccessResp(pageContiner)
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageStart := (page - 1) * 10
	if (page-1)*10 > totalCount {
		resp := common.SuccessResp(pageContiner)
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageEnd := page * 10
	if pageEnd > totalCount {
		pageContiner.List = recentWithdrawList[pageStart:totalCount]
		resp := common.SuccessResp(pageContiner)
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	pageContiner.List = recentWithdrawList[pageStart:pageEnd]
	resp := common.SuccessResp(pageContiner)
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, common.SuccessResp(pageContiner).String(), http.StatusOK)
}
