package routers

import (
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetMyDataStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetMyDataStruct) New() common.LogAndUserSetHandler {
	return new(GetMyDataStruct)
}

func (g *GetMyDataStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_MY_DATA
}

func (g *GetMyDataStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetMyDataStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token", "timestamp"}
	return getUserByToken(rq, mustParams)
}

func (g *GetMyDataStruct) SetUser(u common.UserBasic) {
	g.usr = u
}

func (g *GetMyDataStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	apprenticeTotal, err := db.GetMyApprenticeLeaderboardsCount(appID, g.usr.ID, common.NO_LIMIT, common.NO_LIMIT)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	timestamp := time.Now().Unix()
	todayTotal, _, err := db.GetUserDailyIncomeByType(appID, g.usr.ID, common.INCOME_TYPE_INCOME, common.TOTAL_CONF_USER_INCOME, timestamp)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(
		struct {
			Name             string `json:"name"`
			Icon             string `json:"icon"`
			Sex              int    `json:"sex"`
			IncomeTotal      int    `json:"income_total"`
			IncomeRemaining  int    `json:"income_remaining"`
			IncomeToday      int    `json:"income_today"`
			ApprenticeNumber int    `json:"apprentice_number"`
		}{
			Name:             g.usr.Info.Name,
			Icon:             g.usr.Info.Icon,
			Sex:              g.usr.Info.Sex,
			IncomeTotal:      g.usr.IncomeTotal,
			IncomeRemaining:  g.usr.IncomeRemaining,
			IncomeToday:      todayTotal,
			ApprenticeNumber: apprenticeTotal,
		})
	g.yl.Ret = resp.String()
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}
