package routers

import (
	"net/http"
	"sort"
	"strconv"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetTaskListStruct struct {
	usr common.UserBasic
	yl  common.APIRecord
}

func (g GetTaskListStruct) New() common.LogSetHandler {
	return new(GetTaskListStruct)
}

func (g *GetTaskListStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_TASKS
}

func (g *GetTaskListStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetTaskListStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	mustParams := []string{"app_id", "task_type"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	otaskType := rq.Get("task_type")
	taskType, err := strconv.Atoi(otaskType)
	if err != nil {
		errResp := common.OtherErrorResp("invalid task type")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if taskType != common.TASK_TYPE_NEWBIE && taskType != common.TASK_TYPE_DAILY {
		errResp := common.OtherErrorResp("invalid task type")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	var ub *common.UserBasic
	if token := rq.Get("token"); token != "" {
		userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		if userID == "" {
			errResp := common.TokenInvalidResp()
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		ub, err = db.GetUserInfo(appID, bson.M{"id": userID})
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if ub == nil {
			errResp := common.TokenInvalidResp()
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.Ret = errResp.String()
			g.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		g.yl.UserID = ub.ID
		g.yl.UserCode = ub.Code
		g.yl.UserMobile = ub.Info.Mobile
		g.yl.WechatOpenID = ub.ThirdPart.Wechat.OpenID
		g.yl.AlipayOpenID = ub.ThirdPart.Alipay.ID
	}

	tasks, err := db.GetTaskList(appID, taskType)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.Ret = errResp.String()
		g.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if len(tasks) == 0 {
		resp := common.SuccessResp(taskList{List: nil})
		g.yl.Ret = resp.String()
		g.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	nowTimestamp := time.Now().Unix()
	var lists []taskDetailToReturn
	for _, t := range tasks {
		tdtr := taskDetailToReturn{
			TaskType:       taskType,
			TaskID:         t.ID,
			TaskName:       t.Name,
			TaskDesc:       t.Desc,
			TaskActionType: t.ActionType,
			TaskReward:     t.Bounty,
			TaskTag:        t.Tag,
			Order:          t.Order,
			ActionName:     t.ActionName,
			Action:         t.Action,
		}
		if ub != nil {
			done, err := checkDone(appID, ub.ID, taskType, t.ID, nowTimestamp)
			if err != nil {
				errResp := common.ServerErrorResp(err.Error())
				g.yl.ErrorMsg = err.Error()
				g.yl.Ret = errResp.String()
				g.yl.RetCode = errResp.Status
				http.Error(w, errResp.String(), http.StatusOK)
				return
			}

			// 已完成的新手任务不予显示
			if done && tdtr.TaskType == common.TASK_TYPE_NEWBIE {
				tdtr.TaskStatus = 1
				continue
			}

			// 某些新手任务大额奖励有时限, 超过时限只能领取到200
			if tdtr.TaskType == common.TASK_TYPE_NEWBIE {
				if tdtr.TaskID == common.TASK_NEWBIE_ID_SET_RECOMMEND_CODE || tdtr.TaskID == common.TASK_NEWBIE_ID_WECHAT_BIND || tdtr.TaskID == common.TASK_NEWBIE_ID_FIRST_READ {
					if time.Unix(nowTimestamp, 0).Sub(time.Unix(ub.Create, 0)) > time.Hour*24*7 {
						tdtr.TaskReward = 200
					}
				}
			}

			// !!!!!!!!
			if tdtr.TaskType == common.TASK_TYPE_DAILY && tdtr.TaskID == common.TASK_DAILY_ID_APPRENTICE {
				tdtr.TaskStatus = 0
			}
			lists = append(lists, tdtr)
		}
	}

	sort.Sort(tdtrToSort(lists))
	resp := common.SuccessResp(taskList{
		List: lists,
	})
	g.yl.Ret = resp.String()
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

func GetTaskList(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	mustParams := []string{"app_id", "task_type"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	otaskType := rq.Get("task_type")
	taskType, err := strconv.Atoi(otaskType)
	if err != nil {
		http.Error(w, common.OtherErrorResp("invalid task type").String(), http.StatusOK)
		return
	}
	if taskType != common.TASK_TYPE_NEWBIE && taskType != common.TASK_TYPE_DAILY {
		http.Error(w, common.OtherErrorResp("invalid task type").String(), http.StatusOK)
		return
	}

	var userID string
	if token := rq.Get("token"); token != "" {
		var err error
		userID, err = db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		if userID == "" {
			http.Error(w, common.TokenInvalidResp().String(), http.StatusOK)
			return
		}
	}

	tasks, err := db.GetTaskList(appID, taskType)
	if err != nil {
		http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
		return
	}
	if len(tasks) == 0 {
		http.Error(w, common.SuccessResp(taskList{List: nil}).String(), http.StatusOK)
		return
	}

	nowTimestamp := time.Now().Unix()
	lists := make([]taskDetailToReturn, len(tasks))
	for i, t := range tasks {
		lists[i] = taskDetailToReturn{
			TaskType:       taskType,
			TaskID:         t.ID,
			TaskName:       t.Name,
			TaskDesc:       t.Desc,
			TaskActionType: t.ActionType,
			TaskReward:     t.Bounty,
			TaskTag:        t.Tag,
			Order:          t.Order,
			ActionName:     t.ActionName,
			Action:         t.Action,
		}
		if userID != "" {
			done, err := checkDone(appID, userID, taskType, t.ID, nowTimestamp)
			if err != nil {
				http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
				return
			}
			if done {
				lists[i].TaskStatus = 1
			}

			// !!!!!!!!
			if lists[i].TaskType == common.TASK_TYPE_DAILY && lists[i].TaskID == "5" {
				lists[i].TaskStatus = 0
			}

		}
	}

	sort.Sort(tdtrToSort(lists))
	http.Error(w, common.SuccessResp(taskList{
		List: lists,
	}).String(), http.StatusOK)
}

type taskList struct {
	List []taskDetailToReturn `json:"list"`
}

type taskDetailToReturn struct {
	TaskType       int    `json:"task_type"`
	TaskID         string `json:"task_id"`
	TaskName       string `json:"task_name"`
	TaskDesc       string `json:"task_desc"`
	TaskActionType int    `json:"task_action_type"`
	TaskReward     int    `json:"task_reward"`
	TaskStatus     int    `json:"task_status"`
	TaskTag        string `json:"task_tag"`
	Order          int    `json:"-"`
	ActionName     string `json:"action_name"`
	Action         string `json:"action"`
}

type tdtrToSort []taskDetailToReturn

func (t tdtrToSort) Len() int      { return len(t) }
func (t tdtrToSort) Swap(i, j int) { t[i], t[j] = t[j], t[i] }
func (t tdtrToSort) Less(i, j int) bool {
	return t[i].Order < t[j].Order
}
