package routers

import (
	"net/http"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/tealeg/xlsx"

	"vlion/yxw/common"
)

const mongoAddr = "172.16.189.222?maxPoolSize=3000"

func GetRecentUserWithdraw(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	mustParams := []string{"app_id", "user", "password"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	user := rq.Get("user")
	password := rq.Get("password")
	if user != "admin" || password != "vlion.cn" {
		http.Error(w, "", http.StatusUnauthorized)
		return
	}
	ms, err := mgo.Dial(mongoAddr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusOK)
		return
	}
	defer ms.Close()

	xlsxFile := xlsx.NewFile()
	userWithdrawTotalSheet, err := xlsxFile.AddSheet("最近提现总览")
	if err != nil {
		http.Error(w, err.Error(), http.StatusOK)
		return
	}
	userWithdrawDetailSheet, err := xlsxFile.AddSheet("提现列表")
	if err != nil {
		http.Error(w, err.Error(), http.StatusOK)
		return
	}
	_ = userWithdrawTotalSheet
	_ = userWithdrawDetailSheet
	ms.SetSocketTimeout(time.Hour * 5)
	ms.SetSyncTimeout(time.Second * 10)
	ms.SetCursorTimeout(0)
	ms.SetMode(mgo.Monotonic, true)
	userOrderC := ms.DB("cr_app_" + appID).C("user_order")
	userBasicC := ms.DB("cr_app_" + appID).C("user_basic")
	_ = userBasicC
	now := time.Now()
	startTimestamp := common.GetDateStartUnix(now.Add(time.Hour * 24 * -7).Unix())
	iter := userOrderC.Find(bson.M{"history.ftimestamp": bson.M{"$gte": startTimestamp}}).Iter()
	var uo common.UserOrder
	for iter.Next(&uo) {
		for _, h := range uo.History {
			if h.Timestamp < startTimestamp {
				continue
			}
		}

	}
}
