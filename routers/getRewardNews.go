package routers

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetRewardNewsStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetRewardNewsStruct) New() common.LogAndUserSetHandler {
	return new(GetRewardNewsStruct)
}

func (g *GetRewardNewsStruct) GetAPICode() int {
	return common.API_LOG_CODE_READ_REWAR_NEWS
}

func (g *GetRewardNewsStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetRewardNewsStruct) SetUser(usr common.UserBasic) {
	g.usr = usr
}

func (g *GetRewardNewsStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"token", "app_id", "id"}
	return getUserByToken(rq, mustParams)
}

func (g *GetRewardNewsStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	rewardNewsID := rq.Get("id")
	rewardNewsURLBytes, err := base64.URLEncoding.DecodeString(rewardNewsID)
	if err != nil {
		log.Fields{
			"target": rewardNewsID,
			"err":    err.Error(),
		}.Error("fail to decode base64")
		g.yl.ErrorMsg = err.Error()
		return
	}
	targetURL := string(rewardNewsURLBytes)
	ct, code, err := common.VisitURL(targetURL, time.Second*3)
	if err != nil {
		log.Fields{
			"url": targetURL,
			"err": err.Error(),
		}.Error("fail to visit url")
		g.yl.ErrorMsg = err.Error()
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	fmt.Fprintln(w, string(ct))

	go func() {
		if g.usr.ID == "" {
			return
		}
		appID := rq.Get("app_id")
		category := rq.Get("category")
		confMux.RLock()
		rnc := conf[appID].RewardNewsConf
		confMux.RUnlock()
		if rnc == nil {
			return
		}
		rnhd := getRewardNewsConf(appID, category, *rnc)
		if rnhd == nil {
			return
		}
		if rnhd.RefreshInterval <= 0 {
			return
		}

		nowTimestamp := time.Now().Unix()
		uh, err := db.GetUserDailyIncomeHistoryByDetail(appID, g.usr.ID, nowTimestamp, common.INCOME_TYPE_INCOME, common.TASK_TYPE_REWARD_NEWS, "")
		if err != nil {
			return
		}

		var alreadyRead int
		var readTotal int
		crnd := getCurrentRewardNewsDuration(*rnhd, nowTimestamp)
		if uh != nil {
			for _, h := range uh {
				if h.From == targetURL {
					return
				}
				readTotal += h.Count
				if h.Timestamp < crnd.startTimestamp {
					continue
				}
				if h.Timestamp >= crnd.endTimestamp {
					continue
				}
				alreadyRead += 1
			}
		}

		if alreadyRead >= rnhd.Amount {
			return
		}
		if readTotal >= rnc.MaxPerDay {
			return
		}
		uits := db.UserIncomeToSave{
			AppID:      appID,
			Usr:        g.usr,
			OpType:     common.INCOME_TYPE_INCOME,
			TaskID:     "",
			TaskType:   common.TASK_TYPE_REWARD_NEWS,
			Count:      rnhd.Reward,
			Timestamp:  nowTimestamp,
			Desc:       "阅读红包新闻",
			SetMaster:  false,
			MasterRate: 0,
			From:       targetURL,
			AppVersion: rq.Get("app_version"),
			IP:         common.GetIPFromRequest(r),
		}
		uits.Save()

		val := alreadyRead + 1
		db.SetUserAlreadyRead(appID, g.usr.ID, crnd.startTimestamp, val)
	}()
}

type currentRewardNewsDuration struct {
	startTimestamp int64
	endTimestamp   int64
}

func getCurrentRewardNewsDuration(rnhd common.RewardNewsHistoryDetail, nowTimestamp int64) currentRewardNewsDuration {
	startTimestamp := common.GetDateStartUnix(nowTimestamp)
	var endTimestamp int64
	for {
		endTimestamp = startTimestamp + int64(rnhd.RefreshInterval)
		if dateEndTimestamp := common.GetDateEndUnix(nowTimestamp); endTimestamp >= dateEndTimestamp {
			endTimestamp = dateEndTimestamp
		}
		if nowTimestamp >= endTimestamp {
			startTimestamp = endTimestamp
			continue
		}
		break
	}
	return currentRewardNewsDuration{
		startTimestamp: startTimestamp,
		endTimestamp:   endTimestamp,
	}
}

func getRewardNewsConf(appID, category string, rnc common.RewardNewsHistory) *common.RewardNewsHistoryDetail {
	for _, h := range rnc.Details {
		if h.Category != category {
			continue
		}
		if !h.IsValid {
			continue
		}
		if h.Amount == 0 {
			h.Amount = 5
		}
		return &h
	}
	return nil
}
