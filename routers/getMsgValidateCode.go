package routers

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"

	"github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

const ALIYUN_SMS_URL = "http://dysmsapi.aliyuncs.com"

type GetMsgVlidateCodeStruct struct {
	yl common.APIRecord
}

func (g *GetMsgVlidateCodeStruct) New() common.LogSetHandler {
	return new(GetMsgVlidateCodeStruct)
}

func (g *GetMsgVlidateCodeStruct) GetAPICode() int {
	return common.API_LOG_CODE_SMS_CODE
}

func (g *GetMsgVlidateCodeStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetMsgVlidateCodeStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet && r.Method != http.MethodPost {
		log.Fields{
			"method": r.Method,
		}.Error("unexpected method")
		http.Error(w, "only get and post allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()
	mustParams := []string{"phone_number"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		log.Fields{
			"err": errResp.String(),
		}.Error("params check fail")
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	if appID == "" {
		appID = "101"
	}
	mobile := rq.Get("phone_number")
	smsCode := getRadomAliyunSMSCode()

	//	uv := getAliyunSMSUrlQuery(appID, mobile, smsCode)
	//	ct, _, err := common.VisitURL(ALIYUN_SMS_URL+"?"+uv.Encode(), time.Second)
	//	if err != nil {
	//		log.Fields{
	//			"mobile": mobile,
	//			"err":    err.Error(),
	//		}.Error("visit aliyun fail")
	//		errResp := common.ServerErrorResp(err.Error())
	//		g.yl.ErrorMsg = err.Error()
	//		g.yl.RetCode = errResp.Status
	//		g.yl.Ret = errResp.String()
	//		http.Error(w, errResp.String(), http.StatusOK)
	//		return
	//	}
	//	var asr common.AliyunSMSRet
	//	if err := json.Unmarshal(ct, &asr); err != nil {
	//		log.Fields{
	//			"mobile":        mobile,
	//			"returnMessage": string(ct),
	//			"err":           err.Error(),
	//		}.Error("fail to unmarshal return struct")
	//		errResp := common.ServerErrorResp(err.Error())
	//		g.yl.ErrorMsg = err.Error()
	//		g.yl.RetCode = errResp.Status
	//		g.yl.Ret = errResp.String()
	//		http.Error(w, errResp.String(), http.StatusOK)
	//		return
	//	}
	asr, err := sendMessage(appID, mobile, smsCode)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if asr.Code != "OK" {
		log.Fields{
			"code":       asr.Code,
			"mobile":     mobile,
			"errMessage": asr.Message,
		}.Error("fail to send message")
		if asr.Code == "isv.MOBILE_NUMBER_ILLEGAL" {
			errResp := common.Resp{
				Status:    common.STATUS_MOBILE_INVALID,
				Timestamp: time.Now().Unix(),
				ErrMsg:    asr.Message,
			}
			g.yl.ErrorMsg = errResp.ErrMsg
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		errResp := common.Resp{
			Status:    common.STATUS_MSG_SEND_ERROR,
			Timestamp: time.Now().Unix(),
			ErrMsg:    asr.Message,
		}
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	redisKey := "msg_" + mobile
	redisValue := smsCode
	if err := db.SaveToRedis(redisKey, redisValue, time.Minute*5); err != nil {
		log.Fields{
			"mobile": mobile,
			"err":    err.Error(),
		}.Error("fail to save to redis")
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(nil)
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

func sendMessage(appID, mobile string, smsCode int) (*common.AliyunSMSRet, error) {
	uv := getAliyunSMSUrlQuery(appID, mobile, smsCode)
	ct, _, err := common.VisitURL(ALIYUN_SMS_URL+"?"+uv.Encode(), time.Second)
	if err != nil {
		return nil, err
	}
	var asr common.AliyunSMSRet
	if err := json.Unmarshal(ct, &asr); err != nil {
		return nil, err
	}
	return &asr, nil
}

func getAliyunSMSUrlQuery(appID, mobile string, smsCode int) url.Values {
	confMux.RLock()
	tpca := conf[appID].ThirdpartConf.AliyunSMS
	confMux.RUnlock()
	uv := make(url.Values)
	// 填充阿里云系统参数
	// 系统参数为POP协议的基本参数
	uv.Set("AccessKeyId", tpca.AccessKeyID)
	uv.Set("Timestamp", time.Now().UTC().Format("2006-01-02T15:04:05Z")) // 格式为：yyyy-MM-dd’T’HH:mm:ss’Z’；时区为：GMT
	uv.Set("Format", "JSON")                                             // 没传默认为JSON，可选填值：XML
	uv.Set("SignatureMethod", "HMAC-SHA1")
	uv.Set("SignatureVersion", "1.0")
	uv.Set("SignatureNonce", common.NewUUID())
	// uv.Set("Signature", "")

	// 业务参数
	uv.Set("Action", "SendSms")                                   // API的命名，固定值，如发送短信API的值为：SendSms
	uv.Set("Version", "2017-05-25")                               // API的版本，固定值，如短信API的值为：2017-05-25
	uv.Set("RegionId", "cn-hangzhou")                             // API支持的RegionID，如短信API的值为：cn-hangzhou
	uv.Set("PhoneNumbers", mobile)                                // 短信接收号码,支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式；发送国际/港澳台消息时，接收号码格式为00+国际区号+号码，如“0085200000000”
	uv.Set("SignName", tpca.SignName)                             // 短信签名
	uv.Set("TemplateCode", tpca.TemplateCode)                     // 短信模板ID
	uv.Set("TemplateParam", fmt.Sprintf(`{"code": %d}`, smsCode)) // 短信模板变量替换JSON串,友情提示:如果JSON中需要带换行符,请参照标准的JSON协议。
	// uv.Set("OutId", "")										  // 外部流水扩展字段

	uv.Set("Signature", getAliyunSMSSign(uv, tpca.AccessSecret+"&"))
	return uv
}

func getAliyunSMSSign(uv url.Values, hmacSha1Key string) string {
	var keys []string
	for k := range uv {
		if k != "Signature" {
			keys = append(keys, k)
		}
	}
	sort.Strings(keys)
	var tmpString string
	for _, key := range keys {
		if tmpString == "" {
			tmpString = fmt.Sprintf("%s=%s", url.QueryEscape(key), url.QueryEscape(uv.Get(key)))
		} else {
			tmpString = fmt.Sprintf("%s&%s=%s", tmpString, url.QueryEscape(key), url.QueryEscape(uv.Get(key)))
		}
	}

	// 特殊字符串替换
	tmpString = strings.Replace(tmpString, "+", "%20", -1)
	tmpString = strings.Replace(tmpString, "*", "%2A", -1)
	tmpString = strings.Replace(tmpString, "%7E", "~", -1)

	stringToSign := fmt.Sprintf("%s&%s&%s", http.MethodGet, url.QueryEscape("/"), url.QueryEscape(tmpString))
	return common.HmacSha1AndBase64(hmacSha1Key, stringToSign)
}

func getRadomAliyunSMSCode() int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return 100000 + r.Intn(899999)
}
