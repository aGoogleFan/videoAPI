package routers

import (
	"net/http"
	"net/url"
	"strings"

	// "github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type appVersionToReturn struct {
	AppVersion    string `json:"app_version"`
	HasNew        bool   `json:"has_new"`
	IsForce       bool   `json:"is_force"`
	NewVersion    string `json:"new_version"`
	ReleasedNotes string `json:"released_notes"`
	DownloadURL   string `json:"download_url"`
}

type GetAppVersionStruct struct {
	yl common.APIRecord
}

func (g *GetAppVersionStruct) New() common.LogAndUserSetHandler {
	return new(GetAppVersionStruct)
}

func (g *GetAppVersionStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_APP_VERSION
}

func (g *GetAppVersionStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetAppVersionStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	return getUserByToken(rq, []string{"app_id"})
}

func (g *GetAppVersionStruct) SetUser(ub common.UserBasic) {
}

func (g *GetAppVersionStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()
	appVersion := strings.ToLower(rq.Get("app_version"))
	appID := rq.Get("app_id")
	confMux.RLock()
	appVersionHistory := conf[appID].VersionConf
	confMux.RUnlock()
	if appVersionHistory == nil {
		resp := common.SuccessResp(appVersionToReturn{
			AppVersion: appVersion,
		})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	avtr := getAppVersionReturnValue(appVersion, appVersionHistory)
	resp := common.SuccessResp(avtr)
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

func getAppVersionReturnValue(appVersion string, appVersionHistory *common.AppVersion) appVersionToReturn {
	appVersionHistoryLen := len(appVersionHistory.History)
	if appVersionHistoryLen == 0 {
		return appVersionToReturn{
			AppVersion: appVersion,
		}
	}
	bVersion := false
	if strings.HasPrefix(appVersion, "b") {
		bVersion = true
	}

	appVersion = strings.TrimPrefix(appVersion, "v")
	appVersion = strings.TrimPrefix(appVersion, "b")
	var hasNew, isForce bool
	for i := 0; i < appVersionHistoryLen; i++ {
		isNewVersion := common.IsNewVersion(appVersion, appVersionHistory.History[appVersionHistoryLen-(i+1)].Version, bVersion)
		if !isNewVersion {
			break
		}
		hasNew = true
		if appVersionHistory.History[appVersionHistoryLen-(i+1)].IsForce {
			isForce = true
		}
	}

	if !hasNew {
		return appVersionToReturn{
			AppVersion: appVersion,
		}
	}

	return appVersionToReturn{
		AppVersion:    appVersion,
		HasNew:        hasNew,
		IsForce:       isForce,
		NewVersion:    appVersionHistory.History[appVersionHistoryLen-1].Version,
		ReleasedNotes: appVersionHistory.History[appVersionHistoryLen-1].ReleasedNotes,
		DownloadURL:   appVersionHistory.History[appVersionHistoryLen-1].DownloadURL,
	}
}
