package routers

import (
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type WithdrawCheckStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (wcs *WithdrawCheckStruct) New() common.LogSetHandler {
	return new(WithdrawCheckStruct)
}

func (wcs *WithdrawCheckStruct) GetAPICode() int {
	return common.API_LOG_CODE_WITHDRAW_CHECK
}

func (wcs *WithdrawCheckStruct) SetLog(yl common.APIRecord) {
	wcs.yl = yl
}

// func WithdrawCheck(w http.ResponseWriter, r *http.Request) {
func (wcs *WithdrawCheckStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		log.Fields{
			"method": r.Method,
		}.Error("invalid method")
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(wcs.yl)
	}()
	rq := r.URL.Query()
	mustParams := []string{"app_id", "user_id", "order_id", "user", "password", "status"}
	ub, resp, err := getUserByUserID(rq, mustParams)
	if err != nil {
		wcs.yl.ErrorMsg = err.Error()
		wcs.yl.Ret = resp.String()
		wcs.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	if resp != nil {
		wcs.yl.Ret = resp.String()
		wcs.yl.RetCode = resp.Status
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	wcs.yl.UserID = ub.ID
	wcs.yl.UserCode = ub.Code
	wcs.yl.UserMobile = ub.Info.Mobile
	wcs.yl.WechatOpenID = ub.ThirdPart.Wechat.OpenID
	wcs.yl.AlipayOpenID = ub.ThirdPart.Alipay.ID
	wcs.usr = *ub

	if rq.Get("user") != adminUserName || rq.Get("password") != adminUserPassword {
		errResp := common.OtherErrorResp("invalid user")
		wcs.yl.ErrorMsg = errResp.ErrMsg
		wcs.yl.Ret = errResp.String()
		wcs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	status, err := strconv.Atoi(rq.Get("status"))
	if err != nil {
		errResp := common.OtherErrorResp("invalid status")
		wcs.yl.ErrorMsg = err.Error()
		wcs.yl.Ret = errResp.String()
		wcs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	wcs.yl.AutoResult = strconv.Itoa(status)
	appID := rq.Get("app_id")
	userID := rq.Get("user_id")
	orderID := rq.Get("order_id")
	od, err := db.GetOrderDetail(appID, userID, orderID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		wcs.yl.ErrorMsg = err.Error()
		wcs.yl.Ret = errResp.String()
		wcs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	wcs.yl.Consume = od.Cost
	wcs.yl.Withdraw = od.Amount
	if od.Status != common.ORDER_STATUS_TO_BE_CHECKED {
		errResp := common.OtherErrorResp("order already done or checked")
		wcs.yl.ErrorMsg = errResp.ErrMsg
		wcs.yl.Ret = errResp.String()
		wcs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if status != common.ORDER_STATUS_PASSED && status != common.ORDER_STATUS_REFUSED {
		errResp := common.OtherErrorResp("unknown status")
		wcs.yl.ErrorMsg = errResp.ErrMsg
		wcs.yl.Ret = errResp.String()
		wcs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	reason := rq.Get("reason")
	switch status {
	case common.ORDER_STATUS_REFUSED:
		if err := db.DoWithdrawRefuse(appID, wcs.usr, *od, reason, od.PreDeducate); err != nil {
			errResp := common.OtherErrorResp(err.Error())
			wcs.yl.ErrorMsg = err.Error()
			wcs.yl.Ret = errResp.String()
			wcs.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		http.Error(w, common.SuccessResp(nil).String(), http.StatusOK)
	case common.ORDER_STATUS_PASSED:
		// 用户余额确认
		if !od.PreDeducate {
			if wcs.usr.IncomeRemaining < od.Cost {
				od.Status = common.ORDER_STATUS_PASSED
				if err := db.UpdateUserOrderResult(appID, userID, *od, false, "金币不足, 提现失败."); err != nil {
					log.Fields{
						"orderid": od.OrderID,
						"appid":   appID,
						"userid":  userID,
						"err":     err.Error(),
					}.Error("fail to save user order history")
					errResp := common.ServerErrorResp(err.Error())
					wcs.yl.ErrorMsg = err.Error()
					wcs.yl.Ret = errResp.String()
					wcs.yl.RetCode = errResp.Status
					http.Error(w, errResp.String(), http.StatusOK)
					return
				}
				resp := common.SuccessResp(nil)
				wcs.yl.Ret = resp.String()
				wcs.yl.RetCode = resp.Status
				http.Error(w, resp.String(), http.StatusOK)
				return
			}
		}

		if err := withdrawPass(appID, rq.Get("app_version"), wcs.usr, *od); err != nil {
			errResp := common.OtherErrorResp(err.Error())
			wcs.yl.ErrorMsg = err.Error()
			wcs.yl.Ret = errResp.String()
			wcs.yl.RetCode = errResp.Status
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		http.Error(w, common.SuccessResp(nil).String(), http.StatusOK)
	default:
		errResp := common.OtherErrorResp("unknown status")
		wcs.yl.ErrorMsg = errResp.ErrMsg
		wcs.yl.Ret = errResp.String()
		wcs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
	}
}

func withdrawPass(appID, appVersion string, ui common.UserBasic, od common.OrderDetail) error {
	nowTimestamp := time.Now().Unix()
	hasMaster := false
	if ui.From != "" {
		master, err := db.GetUserInfo(appID, bson.M{"id": ui.From})
		if err == nil && master != nil {
			hasMaster = true
			go func() {
				t := task{
					appID:      appID,
					usr:        *master,
					taskType:   common.TASK_TYPE_NEWBIE,
					taskID:     "8",
					timestamp:  nowTimestamp,
					appVersion: appVersion,
				}
				t.complete()
			}()
		}
	}

	od.Status = common.ORDER_STATUS_PASSED
	if od.OrderType == common.ORDER_TYPE_ALIPAY_WITHDRAWA {
		success, err := alipayWithdraw(appID, ui, od)
		if err != nil {
			return err
		}
		if success && hasMaster {
			go func() {
				if ui.From == "" {
					return
				}
				master, err := db.GetUserInfo(appID, bson.M{"id": ui.From})
				if err != nil || master == nil {
					return
				}
				t := task{
					appID:     appID,
					usr:       *master,
					taskType:  common.TASK_TYPE_NEWBIE,
					taskID:    common.TASK_NEWBIE_ID_FIRST_APPRENTICE_WITHDRAW,
					timestamp: nowTimestamp,
				}
				t.complete()
			}()
			go validApprenticeReward(appID, ui.From, ui.ID, nowTimestamp)
		}
		return nil
	}
	if od.OrderType == common.ORDER_TYPE_WECHAT_WITHDRAWA {
		success, err := wechatWithdraw(appID, ui, od)
		if err != nil {
			return err
		}
		if success && hasMaster {
			go func() {
				if ui.From == "" {
					return
				}
				master, err := db.GetUserInfo(appID, bson.M{"id": ui.From})
				if err != nil || master == nil {
					return
				}
				t := task{
					appID:     appID,
					usr:       *master,
					taskType:  common.TASK_TYPE_NEWBIE,
					taskID:    common.TASK_NEWBIE_ID_FIRST_APPRENTICE_WITHDRAW,
					timestamp: nowTimestamp,
				}
				t.complete()
			}()
			go validApprenticeReward(appID, ui.From, ui.ID, nowTimestamp)
		}
		return nil
	}
	return errors.New("unknown withdraw type")
}

func validApprenticeReward(appID, masterID, apprenticeID string, timestamp int64) error {
	apprenticeReadTotal, err := db.GetUserReadTotal(appID, apprenticeID)
	if err != nil {
		return err
	}
	if apprenticeReadTotal < 10000 {
		return nil
	}
	return db.ValidUserApprenticeReward(appID, masterID, apprenticeID, timestamp)
}
