package routers

import (
	"net/http"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type AlipayBindStruct struct {
	yl common.APIRecord
}

func (abs *AlipayBindStruct) New() common.LogSetHandler {
	return new(AlipayBindStruct)
}

func (abs *AlipayBindStruct) GetAPICode() int {
	return common.API_LOG_CODE_ALIPAY_BIND
}

func (abs *AlipayBindStruct) SetLog(yl common.APIRecord) {
	abs.yl = yl
}

func (abs *AlipayBindStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(abs.yl)
	}()

	rq := r.URL.Query()
	mustParams := []string{"alipay_id", "token", "app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		abs.yl.Ret = errResp.String()
		abs.yl.RetCode = errResp.Status
		abs.yl.ErrorMsg = errResp.ErrMsg
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		abs.yl.Ret = errResp.String()
		abs.yl.RetCode = errResp.Status
		abs.yl.ErrorMsg = err.Error()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if userID == "" {
		errResp := common.TokenInvalidResp()
		abs.yl.Ret = errResp.String()
		abs.yl.RetCode = errResp.Status
		abs.yl.ErrorMsg = errResp.ErrMsg
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	usr, err := db.GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		abs.yl.Ret = errResp.String()
		abs.yl.RetCode = errResp.Status
		abs.yl.ErrorMsg = err.Error()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if usr.ThirdPart.Alipay.ID != "" {
		errResp := common.Resp{
			Status:    common.STATUS_USER_ALREADY_BIND_ALIPAY,
			Timestamp: time.Now().Unix(),
		}
		abs.yl.Ret = errResp.String()
		abs.yl.RetCode = errResp.Status
		abs.yl.ErrorMsg = errResp.ErrMsg
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	alipayID := rq.Get("alipay_id")

	if usr, err := db.GetUserInfo(appID, bson.M{"third_part.alipay.id": alipayID}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		abs.yl.Ret = errResp.String()
		abs.yl.RetCode = errResp.Status
		abs.yl.ErrorMsg = err.Error()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	} else if usr != nil {
		errResp := common.Resp{
			Status:    common.STATUS_ALIPAY_ALREADY_BIND,
			Timestamp: time.Now().Unix(),
		}
		abs.yl.Ret = errResp.String()
		abs.yl.RetCode = errResp.Status
		abs.yl.ErrorMsg = errResp.ErrMsg
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if err := db.UpdateUser(appID, bson.M{"id": userID}, bson.M{"$set": bson.M{"third_part.alipay.id": alipayID, "third_part.alipay.bind_time": time.Now().Unix()}}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		abs.yl.Ret = errResp.String()
		abs.yl.RetCode = errResp.Status
		abs.yl.ErrorMsg = err.Error()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	resp := common.SuccessResp(nil)
	abs.yl.Ret = resp.String()
	abs.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

func AlipayBind(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	mustParams := []string{"alipay_id", "token", "app_id"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if userID == "" {
		http.Error(w, common.TokenInvalidResp().String(), http.StatusOK)
		return
	}

	usr, err := db.GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if usr.ThirdPart.Alipay.ID != "" {
		errResp := common.OtherErrorResp("already bind alipay")
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	alipayID := rq.Get("alipay_id")

	if usr, err := db.GetUserInfo(appID, bson.M{"third_part.alipay.id": alipayID}); err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to get user info by alipayid")
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	} else if usr != nil {
		errResp := common.Resp{
			Status:    common.STATUS_ALIPAY_ALREADY_BIND,
			Timestamp: time.Now().Unix(),
		}
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if err := db.UpdateUser(appID, bson.M{"id": userID}, bson.M{"$set": bson.M{"third_part.alipay.id": alipayID, "third_part.alipay.bind_time": time.Now().Unix()}}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	http.Error(w, common.SuccessResp(nil).String(), http.StatusOK)
}
