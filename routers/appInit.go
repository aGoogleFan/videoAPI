package routers

import (
	"net/http"
	"strings"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type AppInitStruct struct {
	yl common.APIRecord
}

func (a *AppInitStruct) New() common.LogSetHandler {
	return new(AppInitStruct)
}

func (a *AppInitStruct) GetAPICode() int {
	return common.API_LOG_CODE_APP_INIT
}

func (a *AppInitStruct) SetLog(yl common.APIRecord) {
	a.yl = yl
}

func (a *AppInitStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(a.yl)
	}()
	rq := r.URL.Query()
	mustParams := []string{"app_id", "app_version"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		a.yl.ErrorMsg = errResp.ErrMsg
		a.yl.RetCode = errResp.Status
		a.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	appID := rq.Get("app_id")
	confMux.RLock()
	fb := conf[appID].FixedBoundConf
	uc := conf[appID].AppURLConf
	ac := conf[appID].AdConf
	apc := conf[appID].AdPositionConf
	apcn := conf[appID].AdPositionConfNew
	av := conf[appID].VersionConf
	acc := conf[appID].AdClickConf
	vcc := conf[appID].VideoCoinConf
	acfc := conf[appID].AdClickFrequece
	confMux.RUnlock()

	if fb == nil || uc == nil || ac == nil || apc == nil || av == nil || av == nil || acc == nil || vcc == nil || apcn == nil || acfc == nil {
		errResp := common.ServerErrorResp("init fail")
		a.yl.ErrorMsg = errResp.ErrMsg
		a.yl.RetCode = errResp.Status
		a.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	aitr := appIniToReturn{
		FixedBound:      *fb,
		URLConf:         *uc,
		AdClickConf:     *acc,
		VideoCoin:       *vcc,
		AdPosition:      *apcn,
		AdClickFrequece: *acfc,
	}
	appVersion := strings.ToLower(rq.Get("app_version"))
	aitr.AppVersion = getAppVersionReturnValue(appVersion, av)
	aitr.Ads.MessagePerAd = ac.MessagePerAd
	for _, v := range ac.Details {
		var aachdtr appAdConfHistoryDetailToReturn
		aachdtr.ADID = v.ADID
		aachdtr.ADWeight = v.ADWeight
		aachdtr.ThirdPartADID = v.ThirdPartADID
		aachdtr.ThirdPartADWeight = v.ThirdPartADWeight
		for _, apcd := range apc.Details {
			if apcd.ADID != aachdtr.ADID {
				continue
			}
			aachdtr.ADData = append(aachdtr.ADData, apcd)
		}
		aitr.Ads.Details = append(aitr.Ads.Details, aachdtr)
	}
	if len(uc.Extra) != 0 {
		var newExtra [][]common.AppUrlHistoryDetail
		for _, e := range uc.Extra {
			var newE []common.AppUrlHistoryDetail
			for _, ee := range e {
				if !ee.IsValid {
					continue
				}
				newE = append(newE, ee)
			}
			if len(newE) != 0 {
				newExtra = append(newExtra, newE)
			}
		}
		aitr.Extra = newExtra
	}

	resp := common.SuccessResp(aitr)
	a.yl.ErrorMsg = resp.ErrMsg
	a.yl.RetCode = resp.Status
	a.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

type appIniToReturn struct {
	FixedBound      common.AppFixedBoundHistory    `json:"fixed_bound"`
	URLConf         common.AppUrlHistory           `json:"url_conf"`
	AppVersion      appVersionToReturn             `json:"app_version"`
	Ads             adsToReturn                    `json:"ads"`
	AdClickConf     common.AppADClickHistory       `json:"ad_click"`
	VideoCoin       common.VideoCoinHistory        `json:"video_coin"`
	AdPosition      common.ADDATAHistory           `json:"ad_position"`
	Extra           [][]common.AppUrlHistoryDetail `json:"extra"`
	AdClickFrequece common.AdClickFrequece         `json:"ad_click_frequence"`
}

type adsToReturn struct {
	Details      []appAdConfHistoryDetailToReturn `json:"details"`
	MessagePerAd int                              `json:"message_per_ad"`
	Timestamp    int64                            `json:"timestamp"`
}

type appAdConfHistoryDetailToReturn struct {
	ADID              string                       `json:"ad_id"`
	ADWeight          int                          `json:"ad_weight"`
	ADData            []common.AppAdsHistoryDetail `json:"ad_data"`
	ThirdPartADID     string                       `json:"third_part_ad_id"`
	ThirdPartADWeight int                          `json:"third_part_ad_weight"`
}
