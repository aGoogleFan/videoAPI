package routers

import (
	"net/http"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type WechatBindStruct struct {
	yl common.APIRecord
}

func (wbs *WechatBindStruct) New() common.LogSetHandler {
	return new(WechatBindStruct)
}

func (wbs *WechatBindStruct) GetAPICode() int {
	return common.API_LOG_CODE_WECHAT_BIND
}

func (wbs *WechatBindStruct) SetLog(yl common.APIRecord) {
	wbs.yl = yl
}

func (wbs *WechatBindStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(wbs.yl)
	}()

	rq := r.URL.Query()
	mustParams := []string{"wechat_code", "token"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		wbs.yl.ErrorMsg = errResp.ErrMsg
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		wbs.yl.ErrorMsg = err.Error()
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if userID == "" {
		errResp := common.TokenInvalidResp()
		wbs.yl.ErrorMsg = errResp.ErrMsg
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	usr, err := db.GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		errResp := common.OtherErrorResp(err.Error())
		wbs.yl.ErrorMsg = err.Error()
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if usr.ThirdPart.Wechat.OpenID != "" {
		errResp := common.Resp{
			Status:    common.STATUS_USER_ALREADY_BIND_WECHAT,
			Timestamp: time.Now().Unix(),
		}
		wbs.yl.ErrorMsg = errResp.ErrMsg
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		wbs.yl.WechatOpenID = usr.ThirdPart.Wechat.OpenID
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	wechatCode := rq.Get("wechat_code")
	wat, errResp := getWechatAccessTokenByWechatCode(appID, wechatCode)
	if errResp != nil {
		wbs.yl.ErrorMsg = err.Error()
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if wat.ErrCode != 0 {
		errResp := common.OtherErrorResp(wat.ErrMsg)
		wbs.yl.ErrorMsg = err.Error()
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	wui, err := getWechatUserInfoByAccessToken(wat.OpenID, wat.AccessToken)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		wbs.yl.ErrorMsg = err.Error()
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if wui.ErrCode != 0 {
		errResp := common.OtherErrorResp(wui.ErrMsg)
		wbs.yl.ErrorMsg = errResp.ErrMsg
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if usr, err := db.GetUserInfo(appID, bson.M{"third_part.wechat.openid": wat.OpenID}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		wbs.yl.ErrorMsg = err.Error()
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	} else if usr != nil {
		errResp := common.Resp{
			Status:    common.STATUS_WECHAT_ALREADY_BIND,
			Timestamp: time.Now().Unix(),
		}
		wbs.yl.ErrorMsg = errResp.ErrMsg
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	var update bson.M
	if usr.Info.Icon == "" {
		update = bson.M{"$set": bson.M{"info.icon": wui.HeadImgUrl, "info.name": wui.NickName, "third_part.wechat.openid": wui.OpenID, "third_part.wechat.unionid": wui.UnionID, "third_part.wechat.bind_time": time.Now().Unix()}}
	} else {
		update = bson.M{"$set": bson.M{"third_part.wechat.openid": wui.OpenID, "info.name": wui.NickName, "third_part.wechat.unionid": wui.UnionID, "third_part.wechat.bind_time": time.Now().Unix()}}
	}

	if err := db.UpdateUser(appID, bson.M{"id": userID}, update); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		wbs.yl.ErrorMsg = err.Error()
		wbs.yl.Ret = errResp.String()
		wbs.yl.RetCode = errResp.Status
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	go func() {
		t := task{
			appID:      appID,
			usr:        *usr,
			taskType:   common.TASK_TYPE_NEWBIE,
			taskID:     common.TASK_NEWBIE_ID_WECHAT_BIND,
			timestamp:  time.Now().Unix(),
			appVersion: rq.Get("app_version"),
		}
		t.complete()
	}()
	resp := common.SuccessResp(nil)
	wbs.yl.WechatOpenID = wui.OpenID
	wbs.yl.Ret = resp.String()
	wbs.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}

func WechatBind(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	mustParams := []string{"wechat_code", "token"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	appID := rq.Get("app_id")
	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if userID == "" {
		http.Error(w, common.TokenInvalidResp().String(), http.StatusOK)
		return
	}

	usr, err := db.GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		errResp := common.OtherErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if usr.ThirdPart.Wechat.OpenID != "" {
		errResp := common.Resp{
			Status:    common.STATUS_USER_ALREADY_BIND_WECHAT,
			Timestamp: time.Now().Unix(),
		}
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	wechatCode := rq.Get("wechat_code")
	wat, errResp := getWechatAccessTokenByWechatCode(appID, wechatCode)
	if errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if wat.ErrCode != 0 {
		errResp := common.OtherErrorResp(wat.ErrMsg)
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	wui, err := getWechatUserInfoByAccessToken(wat.OpenID, wat.AccessToken)
	if err != nil {
		http.Error(w, common.ServerErrorResp(err.Error()).String(), http.StatusOK)
		return
	}
	if wui.ErrCode != 0 {
		errResp := common.OtherErrorResp(wui.ErrMsg)
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if usr, err := db.GetUserInfo(appID, bson.M{"third_part.wechat.openid": wat.OpenID}); err != nil {
		log.Fields{
			"err": err.Error(),
		}.Error("fail to get user info by openid")
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	} else if usr != nil {
		errResp := common.Resp{
			Status:    common.STATUS_WECHAT_ALREADY_BIND,
			Timestamp: time.Now().Unix(),
		}
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	var update bson.M
	if usr.Info.Icon == "" {
		update = bson.M{"$set": bson.M{"info.icon": wui.HeadImgUrl, "info.name": wui.NickName, "third_part.wechat.openid": wui.OpenID, "third_part.wechat.unionid": wui.UnionID, "third_part.wechat.bind_time": time.Now().Unix()}}
	} else {
		update = bson.M{"$set": bson.M{"third_part.wechat.openid": wui.OpenID, "info.name": wui.NickName, "third_part.wechat.unionid": wui.UnionID, "third_part.wechat.bind_time": time.Now().Unix()}}
	}

	if err := db.UpdateUser(appID, bson.M{"id": userID}, update); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	http.Error(w, common.SuccessResp(nil).String(), http.StatusOK)
}
