package routers

import (
	"net/http"
	"net/url"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type ApprenticeRewardLeaderboard struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (a *ApprenticeRewardLeaderboard) New() common.LogAndUserSetHandler {
	return new(ApprenticeRewardLeaderboard)
}

func (a *ApprenticeRewardLeaderboard) GetAPICode() int {
	return 0
}

func (a *ApprenticeRewardLeaderboard) SetLog(yl common.APIRecord) {
	a.yl = yl
}

func (a *ApprenticeRewardLeaderboard) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"sort_by"}
	return getUserByToken(rq, mustParams)
}

func (a *ApprenticeRewardLeaderboard) SetUser(u common.UserBasic) {
	a.usr = u
}

func (rar *ApprenticeRewardLeaderboard) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	var arlus []ApprenticeRewardLeaderboardUser
	sortBy := rq.Get("sort_by")
	if sortBy != "0" && sortBy != "1" {
		errResp := common.OtherErrorResp("unexpected sort by: " + sortBy)
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	uars, err := db.GetRecentApprenticeReward(appID, sortBy)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	if len(uars) == 0 {
		resp := common.SuccessResp(common.PageContiner{})
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	for _, uar := range uars {
		arlu := ApprenticeRewardLeaderboardUser{
			ID:              uar.ID,
			Reward:          uar.Reward,
			RewardTimestamp: uar.OpenTimestamp,
		}
		ub, err := db.GetUserInfo(appID, bson.M{"id": uar.ID})
		if err != nil || ub == nil {
			arlus = append(arlus, arlu)
			continue
		}
		arlu.Code = ub.Code
		arlu.Name = ub.Info.Name
		arlus = append(arlus, arlu)
	}
	resp := common.SuccessResp(common.PageContiner{
		List:       arlus,
		TotalCount: len(arlus),
		Page:       1,
		MaxPerPage: len(arlus),
	})
	http.Error(w, resp.String(), http.StatusOK)
}

type ApprenticeRewardLeaderboardUser struct {
	ID              string `json:"-"`
	Code            string `json:"code"`
	Name            string `json:"name"`
	Reward          int    `json:"reward"`
	RewardTimestamp int64  `json:"reward_timestamp"`
}
