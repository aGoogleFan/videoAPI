package routers

import (
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	// "github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

const (
	// newsCatURL = "http://www.yuexinwen.cn/bd/news/cat_list"
	newsCatURL = "http://www.viaweb.cn/bd/news/cat_list"
)

type GetNewsCatListStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetNewsCatListStruct) New() common.LogAndUserSetHandler {
	return new(GetNewsCatListStruct)
}

func (g *GetNewsCatListStruct) GetAPICode() int {
	return common.API_LOG_CODE_NEWS_CATLIST
}

func (g *GetNewsCatListStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id"}
	return getUserByToken(rq, mustParams)
}

func (g *GetNewsCatListStruct) SetUser(u common.UserBasic) {
	g.usr = u
}

func (g *GetNewsCatListStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetNewsCatListStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	// 对参数进行转化
	rq := r.URL.Query()
	uv := make(url.Values)
	uv.Set("media", "24")
	uv.Set("submedia", getUserSubmedia(g.usr, rq))
	uv.Set("make", rq.Get("make"))
	uv.Set("model", rq.Get("model"))
	uv.Set("sw", rq.Get("w"))
	uv.Set("sh", rq.Get("h"))
	uv.Set("devicetype", rq.Get("devicetype"))
	uv.Set("conn", rq.Get("conn"))
	uv.Set("carrier", rq.Get("carrier"))

	os := rq.Get("os")
	uv.Set("os", os)
	uv.Set("osv", rq.Get("osv"))
	if os == "1" {
		uv.Set("imei", rq.Get("imei"))
		uv.Set("anid", rq.Get("android_id"))
	}
	ip := common.GetIPFromRequest(r)
	uv.Set("ip", ip)
	ua := r.UserAgent()
	uv.Set("ua", ua)
	t := newsCatURL + "?" + uv.Encode()
	ct, _, err := common.VisitURL(t, time.Second)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	var i []struct {
		Title string `json:"title"`
	}
	if err := json.Unmarshal(ct, &i); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	lenI := len(i)
	resp := common.SuccessResp(
		common.PageContiner{
			List:       i,
			TotalCount: lenI,
			Page:       1,
			MaxPerPage: lenI,
		})
	g.yl.RetCode = resp.Status
	http.Error(w, resp.String(), http.StatusOK)
}
