package routers

import (
	"net/http"
	"time"

	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

func QQLogin(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}

	rq := r.URL.Query()
	mustParams := []string{"openid"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	openID := rq.Get("openid")
	appID := rq.Get("app_id")
	usr, err := db.GetUserInfo(appID, bson.M{"third_part.qq.openid": openID})
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	token := common.NewUUID()

	// 如果是新用户
	if usr != nil {
		nowUnix := time.Now().Unix()
		var image string
		radomImages, err := db.GetRadomImages(appID)
		if err == nil {
			image = radomImages.Images[nowUnix%int64(len(radomImages.Images))].URL
		}
		ub := common.UserBasic{
			ID: common.NewUUID(),
			Info: common.UserInfo{
				Icon: image,
			},
			Create:    nowUnix,
			LastLogin: nowUnix,
			ThirdPart: common.UserThirdPart{
				QQ: common.TencentAccount{
					OpenID:   openID,
					BindTime: nowUnix,
				},
			},
		}

		if err := db.SaveNewUser(appID, ub); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		if err := db.SaveToken(token, ub.ID, common.REDIS_KEY_VALID_DURATION_TOKEN); err != nil {
			errResp := common.ServerErrorResp(err.Error())
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}

		rsp := common.SuccessResp(struct {
			Token string `json:"token"`
			IsNew bool   `json:"is_new"`
		}{
			Token: token,
			IsNew: true,
		})
		http.Error(w, rsp.String(), http.StatusOK)
		return
	}

	// 如果是老用户
	if err := db.UpdateUser(appID, bson.M{"id": usr.ID}, bson.M{"$set": bson.M{"last_login": time.Now().Unix()}}); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if err := db.SaveToken(token, usr.ID, common.REDIS_KEY_VALID_DURATION_TOKEN); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	rsp := common.SuccessResp(struct {
		Token string `json:"token"`
		IsNew bool   `json:"is_new"`
	}{
		Token: token,
		IsNew: false,
	})
	http.Error(w, rsp.String(), http.StatusOK)
	return
}
