package routers

import (
	"net/http"
	"net/url"
	"strconv"
	"time"

	"vlion/yxw/common"
	"vlion/yxw/db"
)

type CompleteSpecialTaskStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (c *CompleteSpecialTaskStruct) New() common.LogAndUserSetHandler {
	return new(CompleteSpecialTaskStruct)
}

func (c *CompleteSpecialTaskStruct) GetAPICode() int {
	return 0
}

func (c *CompleteSpecialTaskStruct) SetLog(yl common.APIRecord) {
	c.yl = yl
}

func (c *CompleteSpecialTaskStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token", "task_id", "task_type"}
	return getUserByToken(rq, mustParams)
}

func (c *CompleteSpecialTaskStruct) SetUser(u common.UserBasic) {
	c.usr = u
}

func (c *CompleteSpecialTaskStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	taskID := rq.Get("task_id")
	t := time.Now().Unix()
	otaskType := rq.Get("task_type")
	taskType, err := strconv.Atoi(otaskType)
	if err != nil {
		resp := common.Resp{
			Status:    common.STATUS_TASK_TYPE_INVALID,
			Timestamp: t,
		}
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	var reward int
	var taskFound bool
	var completeDailyLimit int
	var userIncomeTaskType int
	switch taskType {
	case common.SPECIAL_TASK_TYPE_STAY:
		userIncomeTaskType = common.TASK_TYPE_SPECIAL_STAY
		confMux.Lock()
		stayPageTasks := conf[appID].StayPageTasks
		confMux.Unlock()
		for _, task := range stayPageTasks {
			if task.TaskID == taskID {
				reward = task.Reward
				taskFound = true
				break
			}
		}
		completeDailyLimit = 1
	case common.SPECIAL_TASK_TYPE_ONCE:
		userIncomeTaskType = common.TASK_TYPE_SPECIAL_ONCE
		confMux.Lock()
		dailyOnceTasks := conf[appID].DailyOnceTasks
		confMux.Unlock()
		for _, task := range dailyOnceTasks {
			if task.TaskID == taskID {
				reward = task.Reward
				taskFound = true
				break
			}
		}
		completeDailyLimit = 1
	case common.SPECIAL_TASK_TYPE_MULTY:
		userIncomeTaskType = common.TASK_TYPE_SPECIAL_MULTY
		confMux.Lock()
		dailyManyTimesTasks := conf[appID].DailyManyTimesTasks
		confMux.Unlock()
		for _, task := range dailyManyTimesTasks {
			if task.TaskID == taskID {
				reward = task.Reward
				taskFound = true
				completeDailyLimit = task.MaxPerDay
				break
			}
		}
	default:
		resp := common.Resp{
			Status:    common.STATUS_TASK_TYPE_INVALID,
			Timestamp: t,
		}
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	if !taskFound {
		resp := common.Resp{
			Status:    common.STATUS_TASK_INVALID,
			Timestamp: t,
		}
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	spcs, err := db.GetUserSpecialTaskComplete(appID, c.usr.ID, taskID, taskType, t)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if len(spcs) >= completeDailyLimit {
		resp := common.Resp{
			Status:    common.STATUS_TASK_ALREADY_COMPLETE,
			Timestamp: t,
		}
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	if err := db.CompleteSpecialTask(appID, c.usr.ID, taskID, taskType, t); err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	uits := db.UserIncomeToSave{
		AppID:      appID,
		Usr:        c.usr,
		OpType:     common.INCOME_TYPE_INCOME,
		TaskID:     taskID,
		TaskType:   userIncomeTaskType,
		Count:      reward,
		Timestamp:  t,
		Desc:       "看看赚奖励",
		SetMaster:  false,
		MasterRate: 0,
		From:       "",
		AppVersion: rq.Get("app_version"),
		IP:         common.GetIPFromRequest(r),
	}
	if err := uits.Save(); err != nil {
		resp := common.ServerErrorResp(err.Error())
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	http.Error(w, common.SuccessResp(nil).String(), http.StatusOK)
}
