package routers

import (
	"fmt"
	"net/url"
	"sort"
	"strings"
	"time"
)

// 支付宝对接文档: https://docs.open.alipay.com/309/106235

const (
	ALI_OPEN_URL    = "https://openapi.alipay.com/gateway.do"
	ALI_FUND_TRANS  = "alipay.fund.trans.toaccount.transfer"
	ALI_FOUND_QUERY = "alipay.fund.trans.order.query"
	ALI_FORMAT      = "JSON"
	ALI_CHARSET     = "utf-8"
	ALI_SIGN_TYPE   = "RSA2"
)

func getAlipayCommonParams(appID, method string) url.Values {
	u := make(url.Values)
	confMux.RLock()
	alipayAppID := conf[appID].ThirdpartConf.Alipay.AppID
	confMux.RUnlock()
	u.Set("app_id", alipayAppID)
	u.Set("method", method)
	u.Set("format", ALI_FORMAT)
	u.Set("charset", ALI_CHARSET)
	u.Set("sign_type", ALI_SIGN_TYPE)
	u.Set("timestamp", time.Now().Format("2006-01-02 15:04:05"))
	u.Set("version", "1.0")
	return u
}

func getURLWithSha2Sign(appID string, values url.Values) (string, error) {
	signString, err := signWithSha2(appID, values)
	if err != nil {
		return "", nil
	}
	values.Set("sign", signString)
	var targetURL []string
	for key := range values {
		targetURL = append(targetURL, fmt.Sprintf("%s=%s", key, url.QueryEscape(values.Get(key))))
	}
	return fmt.Sprintf("%s?%s", ALI_OPEN_URL, strings.Join(targetURL, "&")), nil
}

func signWithSha2(appID string, values url.Values) (string, error) {
	var keys []string
	for key := range values {
		if key == "sign" {
			continue
		}
		if values.Get(key) == "" {
			continue
		}
		keys = append(keys, key)
	}
	sort.Strings(keys)

	var targetStringSlice []string
	for _, key := range keys {
		targetStringSlice = append(targetStringSlice, fmt.Sprintf("%s=%s", key, values.Get(key)))
	}

	stringToBeSign := strings.Join(targetStringSlice, "&")

	confMux.RLock()
	alipayPrivateKey := conf[appID].ThirdpartConf.Alipay.PrivateKey
	confMux.RUnlock()
	sign, err := sha2Sign(stringToBeSign, alipayPrivateKey)
	if err != nil {
		return "", err
	}
	return sign, nil
}

// 支付宝转账细则
type alipayFoundTransBitContent struct {
	// 商户转账唯一订单号。发起转账来源方定义的转账单据ID，用于将转账回执通知给来源方。
	// 不同来源方给出的ID可以重复，同一个来源方必须保证其ID的唯一性。
	// 只支持半角英文、数字，及“-”、“_”。
	OutBizNo string `json:"out_biz_no"`

	// 收款方账户类型。可取值：
	// 1、ALIPAY_USERID：支付宝账号对应的支付宝唯一用户号。以2088开头的16位纯数字组成。
	// 2、ALIPAY_LOGONID：支付宝登录号，支持邮箱和手机号格式。
	PayeeType string `json:"payee_type"`

	// 收款方账户。与payee_type配合使用。付款方和收款方不能是同一个账户。
	PayeeAccount string `json:"payee_account"`

	// 转账金额，单位：元。
	// 只支持2位小数，小数点前最大支持13位，金额必须大于等于0.1元。
	// 最大转账金额以实际签约的限额为准。
	Amount string `json:"amount"`

	// 付款方姓名（最长支持100个英文/50个汉字）。显示在收款方的账单详情页。如果该字段不传，则默认显示付款方的支付宝认证姓名或单位名称。
	PlayerShowName string `json:"payer_show_name,omitempty"`

	// 收款方真实姓名（最长支持100个英文/50个汉字）。
	// 如果本参数不为空，则会校验该账户在支付宝登记的实名是否与收款方真实姓名一致。
	PayeeRealName string `json:"payee_real_name,omitempty"`

	// 转账备注（支持200个英文/100个汉字）。
	// 当付款方为企业账户，且转账金额达到（大于等于）50000元，remark不能为空。收款方可见，会展示在收款用户的收支详情中。
	Remark string `json:"remark,omitempty"`
}
