package routers

import (
	"net/http"
	// "sort"
	"strconv"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetProfitsDetailsStruct struct {
	yl common.APIRecord
}

func (g *GetProfitsDetailsStruct) New() common.LogSetHandler {
	return new(GetProfitsDetailsStruct)
}

func (g *GetProfitsDetailsStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_PROFILE_DETAILS
}

func (g *GetProfitsDetailsStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetProfitsDetailsStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	mustParams := []string{"app_id", "token", "page"}
	if errResp := common.ParamsFilter(rq, mustParams); errResp != nil {
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	token := rq.Get("token")
	userID, err := db.CheckToken(token, common.REDIS_KEY_VALID_DURATION_TOKEN)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	if userID == "" {
		errResp := common.TokenInvalidResp()
		g.yl.ErrorMsg = errResp.ErrMsg
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	page := 1
	if opage := rq.Get("page"); opage != "" {
		fpage, err := strconv.Atoi(opage)
		if err == nil {
			page = fpage
		}
	}

	appID := rq.Get("app_id")
	total, hs, err := db.GetUserIncomeHistoryByPage(appID, userID, page, 10)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	pageContiner := common.PageContiner{
		TotalCount: total,
		MaxPerPage: 10,
		Page:       page,
	}
	hLen := len(hs)
	if hLen == 0 {
		resp := common.SuccessResp(pageContiner)
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	htr := make([]common.UserIncomeHistory, hLen)
	for i, h := range hs {
		htr[i] = h
	}
	pageContiner.List = htr
	resp := common.SuccessResp(pageContiner)
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}
