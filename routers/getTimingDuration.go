package routers

import (
	"net/http"
	"net/url"
	"time"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type GetTimingDurationStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetTimingDurationStruct) New() common.LogAndUserSetHandler {
	return new(GetTimingDurationStruct)
}

func (g *GetTimingDurationStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_TIMING_REWARD_DURATION
}

func (g *GetTimingDurationStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetTimingDurationStruct) SetUser(usr common.UserBasic) {
	g.usr = usr
}

func (g *GetTimingDurationStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"token", "app_id"}
	return getUserByToken(rq, mustParams)
}

func (g *GetTimingDurationStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()

	rq := r.URL.Query()
	appID := rq.Get("app_id")
	now := time.Now()
	reward, interval := getNextTimingRewardAndInterval(appID, now)
	resp := common.SuccessResp(struct {
		Duration int `json:"duration"`
		Total    int `json:"total"`
	}{
		Duration: interval,
		Total:    reward,
	})
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

func getNextTimingRewardAndInterval(appID string, now time.Time) (int, int) {
	timingRewardMapMutex.Lock()
	defer timingRewardMapMutex.Unlock()
	if _, ok := timingRewardMap[appID]; !ok {
		return -1, -1
	}
	if now.Unix() >= timingRewardMap[appID].EndTimestamp {
		delete(timingRewardMap, appID)
		return -1, -1
	}
	if now.Unix() < timingRewardMap[appID].StartTimestamp {
		return timingRewardMap[appID].Total, int(timingRewardMap[appID].StartTimestamp - now.Unix())
	}
	return timingRewardMap[appID].Total, 0
}
