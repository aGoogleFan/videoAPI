package routers

import (
	"sync"
	"time"
	"vlion/yxw/db"

	"vlion/yxw/common"

	"github.com/anotherGoogleFan/log"
)

var atsMap map[string]*adToSend // map[appID]adToSend
var atsMapMux sync.Mutex
var FinishSignalChan chan struct{}

type adToSend struct {
	ads     []common.ADInfo
	im      chan struct{} // 用于传递立即存储的信息
	counter int
}

func init() {
	atsMap = make(map[string]*adToSend)
	FinishSignalChan = make(chan struct{})
}

func sendADInfo(appID string, appInfo common.ADInfo) {
	checkAtsMap(appID)
	atsMapMux.Lock()
	ats := atsMap[appID]
	ats.ads = append(atsMap[appID].ads, appInfo)
	ats.counter++
	atsMap[appID] = ats
	if atsMap[appID].counter >= 1000 {
		atsMap[appID].im <- struct{}{}
	}
	atsMapMux.Unlock()
}

func checkAtsMap(appID string) {
	atsMapMux.Lock()
	if _, ok := atsMap[appID]; !ok {
		ats := &adToSend{
			im: make(chan struct{}),
		}
		go ats.Monitor(appID)
		atsMap[appID] = ats
	}
	atsMapMux.Unlock()
}

func (a *adToSend) save(appID string) {
	atsMapMux.Lock()
	aToSend := a.ads
	a.ads = nil
	a.counter = 0
	atsMapMux.Unlock()
	go func() {
		if len(aToSend) == 0 {
			return
		}
		if err := db.SaveADInfo(appID, aToSend); err != nil {
			log.Fields{
				"appID": appID,
				"err":   err.Error(),
			}.Error("fail to save adinfo")
		}
	}()
}

func (a *adToSend) Monitor(appID string) {
	ticker := time.NewTicker(time.Second * 5)
	defer ticker.Stop()
	for {
		select {
		case <-a.im:
			a.save(appID)
		case <-ticker.C:
			a.save(appID)
		case <-FinishSignalChan:
			a.save(appID)
			return
		}
	}
}
