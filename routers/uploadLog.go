package routers

import (
	"fmt"
	"net/http"
	"time"

	"vlion/yxw/common"
	yxwLog "vlion/yxw/log"
)

type UploadLogStruct struct {
	yl common.APIRecord
}

func (m *UploadLogStruct) New() common.LogSetHandler {
	return new(UploadLogStruct)
}

func (m *UploadLogStruct) GetAPICode() int {
	return common.API_LOG_CODE_LOG_UOLOAD
}

func (m *UploadLogStruct) SetLog(yl common.APIRecord) {
	m.yl = yl
}

func (m *UploadLogStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		yxwLog.RecordLog(m.yl)
	}()
	path := "uploadLog"
	rq := r.URL.Query()
	fileName := fmt.Sprintf("%s_%d.log", rq.Get("android_id"), time.Now().Unix())
	_, _, needContinue, yl := UploadInnerImage(w, r, path, true, fileName, &m.yl)
	m.yl = yl
	if !needContinue {
		return
	}

	resp := common.SuccessResp(nil)
	m.yl.RetCode = resp.Status
	m.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}
