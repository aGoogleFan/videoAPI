package routers

import (
	"net/http"
	"net/url"
	"sort"
	"strconv"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetMyApprenticeLeaderboardsStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetMyApprenticeLeaderboardsStruct) New() common.LogAndUserSetHandler {
	return new(GetMyApprenticeLeaderboardsStruct)
}

func (g *GetMyApprenticeLeaderboardsStruct) GetAPICode() int {
	return common.API_LOG_CODE_MY_APPRENTICE_LEADERBOARDS
}

func (g *GetMyApprenticeLeaderboardsStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetMyApprenticeLeaderboardsStruct) SetUser(ub common.UserBasic) {
	g.usr = ub
}

func (g *GetMyApprenticeLeaderboardsStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"app_id", "token"}
	return getUserByToken(rq, mustParams)
}

func (g *GetMyApprenticeLeaderboardsStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	page := 1
	if opage := rq.Get("page"); opage != "" {
		fpage, err := strconv.Atoi(opage)
		if err == nil {
			page = fpage
		}
	}

	tmpApprenticeDetails, err := db.GetMyApprenticeLeaderboards(appID, g.usr.ID)
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}
	sort.Sort(sort.Reverse(tuadBySort(tmpApprenticeDetails)))

	totalCount := len(tmpApprenticeDetails)
	pageContiner := common.PageContiner{
		TotalCount: totalCount,
		MaxPerPage: 10,
		Page:       page,
	}

	if page <= 0 || page > (totalCount/10)+1 {
		http.Error(w, common.SuccessResp(pageContiner).String(), http.StatusOK)
		return
	}

	pageStart := (page - 1) * 10
	if (page-1)*10 > totalCount {
		http.Error(w, common.SuccessResp(pageContiner).String(), http.StatusOK)
		return
	}

	pageEnd := page * 10
	if pageEnd > totalCount {
		pageContiner.List = tmpApprenticeDetails[pageStart:totalCount]
		http.Error(w, common.SuccessResp(pageContiner).String(), http.StatusOK)
		return
	}

	pageContiner.List = tmpApprenticeDetails[pageStart:pageEnd]
	http.Error(w, common.SuccessResp(pageContiner).String(), http.StatusOK)
}

type tuadBySort []db.TmpUserApprenticeDetail

func (t tuadBySort) Len() int      { return len(t) }
func (t tuadBySort) Swap(i, j int) { t[i], t[j] = t[j], t[i] }
func (t tuadBySort) Less(i, j int) bool {
	if t[i].Amount < t[j].Amount {
		return true
	}
	if t[i].Amount > t[j].Amount {
		return false
	}
	return t[i].ApprenticeTimestamp <= t[j].ApprenticeTimestamp
}
