package routers

import (
	"net/http"
	"net/url"
	"time"

	// "github.com/anotherGoogleFan/log"

	"vlion/yxw/common"
	"vlion/yxw/db"
	yxwLog "vlion/yxw/log"
)

type GetNewsRewardTimesStruct struct {
	yl  common.APIRecord
	usr common.UserBasic
}

func (g *GetNewsRewardTimesStruct) New() common.LogAndUserSetHandler {
	return new(GetNewsRewardTimesStruct)
}

func (g *GetNewsRewardTimesStruct) GetAPICode() int {
	return common.API_LOG_CODE_GET_REWARD_TIMES
}

func (g *GetNewsRewardTimesStruct) SetLog(yl common.APIRecord) {
	g.yl = yl
}

func (g *GetNewsRewardTimesStruct) SetUser(usr common.UserBasic) {
	g.usr = usr
}

func (g *GetNewsRewardTimesStruct) GetUser(rq url.Values) (*common.UserBasic, *common.Resp, error) {
	mustParams := []string{"token", "app_id"}
	return getUserByToken(rq, mustParams)
}

func (g *GetNewsRewardTimesStruct) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only get allowed", http.StatusMethodNotAllowed)
		return
	}
	defer func() {
		yxwLog.RecordLog(g.yl)
	}()
	rq := r.URL.Query()
	appID := rq.Get("app_id")
	category := rq.Get("category")
	confMux.RLock()
	rnc := conf[appID].RewardNewsConf
	confMux.RUnlock()
	if rnc == nil {
		resp := common.SuccessResp(newsRewardTimesToReturn{Amount: 0})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	rnhd := getRewardNewsConf(appID, category, *rnc)
	if rnhd == nil {
		resp := common.SuccessResp(newsRewardTimesToReturn{Amount: 0})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}
	if rnhd.RefreshInterval <= 0 {
		resp := common.SuccessResp(newsRewardTimesToReturn{Amount: 0})
		g.yl.RetCode = resp.Status
		g.yl.Ret = resp.String()
		http.Error(w, resp.String(), http.StatusOK)
		return
	}

	nowTimestamp := time.Now().Unix()

	crnd := getCurrentRewardNewsDuration(*rnhd, nowTimestamp)
	alreadyRead, err := db.GetUserIncomeHistoryCountByDetail(appID, g.usr.ID, crnd.startTimestamp, crnd.endTimestamp, common.INCOME_TYPE_INCOME, common.TASK_TYPE_REWARD_NEWS, "")
	if err != nil {
		errResp := common.ServerErrorResp(err.Error())
		g.yl.ErrorMsg = err.Error()
		g.yl.RetCode = errResp.Status
		g.yl.Ret = errResp.String()
		http.Error(w, errResp.String(), http.StatusOK)
		return
	}

	var amount int
	if alreadyRead >= rnhd.Amount {
		amount = 0
	} else {
		amount = rnhd.Amount - alreadyRead
	}

	if amount != 0 {
		readTotal, _, err := db.GetUserDailyIncomeByType(appID, g.usr.ID, common.INCOME_TYPE_INCOME, common.TASK_TYPE_REWARD_NEWS, nowTimestamp)
		if err != nil {
			errResp := common.ServerErrorResp(err.Error())
			g.yl.ErrorMsg = err.Error()
			g.yl.RetCode = errResp.Status
			g.yl.Ret = errResp.String()
			http.Error(w, errResp.String(), http.StatusOK)
			return
		}
		if readTotal >= rnc.MaxPerDay {
			amount = 0
		} else {
			if rnhd.Reward != 0 {
				remain := rnc.MaxPerDay - readTotal
				tmpRemain := float64(remain) / float64(rnhd.Reward)
				var tmpRemainInt int
				if tmpRemain == float64(int(tmpRemain)) {
					tmpRemainInt = int(tmpRemain)
				} else {
					tmpRemainInt = int(tmpRemain) + 1
				}
				if tmpRemainInt < amount {
					amount = tmpRemainInt
				}
			}
		}
	}
	resp := common.SuccessResp(newsRewardTimesToReturn{Amount: amount})
	g.yl.RetCode = resp.Status
	g.yl.Ret = resp.String()
	http.Error(w, resp.String(), http.StatusOK)
}

type newsRewardTimesToReturn struct {
	Amount int
}
