package db

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

const (
	// redisAddr = "172.16.189.216:6379"
	redisAddr = "172.16.189.216:6380"
)

var redisCli *redis.Client

func InitRedis() {
	redisCli = redis.NewClient(&redis.Options{
		Addr: redisAddr,
	})
	_, err := redisCli.Ping().Result()
	if err != nil {
		panic(err)
	}
}

func SaveToRedis(key string, value interface{}, timeout time.Duration) error {
	return redisCli.Set(key, value, timeout).Err()
}

func GetFromRedis(key string) (string, error) {
	res, err := redisCli.Get(key).Result()
	if err != err {
		if err == redis.Nil {
			return "", nil
		}
		return "", err
	}
	return res, nil
}

func CheckToken(token string, timeout time.Duration) (string, error) {
	key := "token_" + token
	userID, err := redisCli.Get(key).Result()
	if err != nil {
		if err == redis.Nil {
			return "", nil
		}
		return "", err
	}
	if timeout > 0 {
		redisCli.Expire(key, timeout)
	}
	return userID, nil
}

func SaveToken(token, userID string, timeout time.Duration) error {
	return SaveToRedis("token_"+token, userID, timeout)
}

func GenRegisterUserName(appID string) (string, error) {
	radom := 0
	key := ""
	for {
		r := rand.New(rand.NewSource(time.Now().UnixNano()))
		radom = 10000000 + r.Intn(89999999)
		key = fmt.Sprintf("username_%d", radom)
		sAppID, err := redisCli.Get(key).Result()
		if err != nil {
			if err == redis.Nil {
				break
			}
			return "", err
		}
		if sAppID != appID {
			break
		}
	}
	return fmt.Sprintf("%8d", radom), redisCli.Set(key, appID, 0).Err()
}

func CheckAndSaveUserHourlyRewardInfo(appID, userID string, timestamp int64, timeOut time.Duration) (bool, error) {
	t := time.Unix(timestamp, 0)
	dateAndHourString := t.Format("20060102:15")
	key := fmt.Sprintf("hourly:%s:%s:%s:%s", appID, dateAndHourString, userID)
	_, err := redisCli.Get(key).Result()
	if err == nil {
		return true, nil
	}
	if err != redis.Nil {
		return false, err
	}

	return false, redisCli.Set(key, 1, timeOut).Err()
}

func GetHashFromRedis(key, field string) (string, error) {
	res, err := redisCli.HGet(key, field).Result()
	if err != nil {
		if err != redis.Nil {
			return "", err
		}
		return "", nil
	}
	return res, nil
}

func SetHashToRedis(key, field string, value interface{}) error {
	return redisCli.HSet(key, field, value).Err()
}

func SetHashToRedisWithExpire(key, field string, value interface{}, expired time.Duration) error {
	if err := redisCli.HSet(key, field, value).Err(); err != nil {
		return err
	}
	if expired != 0 {
		redisCli.Expire(key, expired)
	}
	return nil
}

func CheckIfExpired(key string) (bool, error) {
	timestampString, err := GetFromRedis(key)
	if err != nil {
		return false, err
	}
	if timestampString == "" {
		return true, nil
	}
	if _, err := strconv.ParseInt(timestampString, 10, 0); err != nil {
		return false, err
	}

	return false, nil
}

func GetUnexpiredDuration(key string) (int, error) {
	timestampString, err := GetFromRedis(key)
	if err != nil {
		return 0, err
	}
	if timestampString == "" {
		return 0, nil
	}
	up, err := strconv.ParseInt(timestampString, 10, 0)
	if err != nil {
		return 0, err
	}
	lastD := time.Unix(up, 0)
	nextD := lastD.Add(time.Hour)
	now := time.Now()
	if !nextD.After(now) {
		return 0, nil
	}
	return int(nextD.Sub(now).Seconds()), nil
}

func IsRepeatRequest(signOnce string, timeout time.Duration) (bool, error) {
	key := "sign_once_" + signOnce
	_, err := redisCli.Get(key).Result()
	if err == nil {
		return true, nil
	}
	if err != redis.Nil {
		return false, err
	}

	return false, redisCli.Set(key, 1, timeout).Err()
}

func IsAlarmSet() (bool, error) {
	key := "alarm"
	_, err := redisCli.Get(key).Result()
	if err == nil {
		return true, nil
	}
	if err != redis.Nil {
		return false, err
	}

	return false, redisCli.Set(key, 1, time.Hour*6).Err()
}

func GetUserAlreadyRead(appID, userID string, startTimestamp int64) (int, error) {
	key := fmt.Sprintf("user_already_read:%s:%s:%d", appID, userID, startTimestamp)
	val, err := redisCli.Get(key).Result()
	if err != nil {
		if err != redis.Nil {
			return 0, err
		}
		return 0, nil
	}
	return strconv.Atoi(val)
}

func SetUserAlreadyRead(appID, userID string, startTimestamp int64, value int) error {
	key := fmt.Sprintf("user_already_read:%s:%s:%d", appID, userID, startTimestamp)
	return redisCli.Set(key, strconv.Itoa(value), time.Hour*2).Err()
}
