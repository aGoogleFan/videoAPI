package db

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/anotherGoogleFan/log"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"vlion/yxw/common"
)

const (
	// mongoAddr = "172.16.189.215?maxPoolSize=3000"
	mongoAddr = "172.16.189.222?maxPoolSize=30000"

	allAppDBName                           = "cr_app_all"
	allAppDBAppHistoryCollection           = "cr_app_history"
	allAppRadomImageCollection             = "cr_app_radom_image"
	allAppExchangeRateHistoryCollection    = "cr_app_exchange_rate_history"
	allAppFixedBoundCollection             = "cr_app_fixed_bound"
	allAppShareContentCollection           = "cr_app_share_content"
	allAppAdsCollection                    = "cr_app_ad"
	allAppApprenticeRewardByDateCollection = "cr_app_apprentice_by_date"
	allAppRewardCollection                 = "cr_app_reward"
	allAppURLCollection                    = "cr_app_url"
	allAppThirdpartConfCollection          = "cr_app_thirdpart_conf"
	allAppCdConfCollection                 = "cr_app_ad_conf"
	allAppLimitConfCollection              = "cr_app_limit"
	allAppSignInConfCollection             = "cr_app_sign_in"
	allAppRewardLimitCollection            = "cr_app_reward_limit"
	allAppWithdrawAutoCheckCollection      = "cr_app_withdraw_role"
	allAppADClickConfCollection            = "cr_app_ad_click"
	allAppVedioCoinConfCollection          = "cr_app_video_coin"
	allAppRewardNewsCollection             = "cr_app_reward_news"
	allAppTimingRewardCollection           = "cr_app_timing_reward"
	allAppUserWithdrawLevelCollection      = "user_withdraw_level_role"
	allAppUserWithdrawStandCollection      = "user_withdraw_level_stand"

	appDetailDBNamePrefix                     = "cr_app_"
	appDetailUserBasicCollection              = "user_basic"
	appDetailUserOrderCollection              = "user_order"
	appDetailUserVideoCollection              = "user_video"
	appDetailUserApprenticeCollection         = "user_apprentice"
	appDetailUserApprenticesCollection        = "user_apprentices"
	appDetailUserApprenticeMiddleCollection   = "user_md_apprentice"
	appDetailUserIncomeHistoryCollection      = "user_income"
	appDetailUserArticleShareCollection       = "user_article_share"
	appDetailTaskNewbieCollection             = "task_newbie"
	appDetailTaskDailyCollection              = "task_daily"
	appDetailUserDeviceCollection             = "user_device"
	appDetailUserIPCollection                 = "user_ip"
	appDetailUserXianwanCollection            = "user_xianwan_reward"
	appDetailUserSignInCollection             = "user_sign_in"
	appDetailUserCoinStructCollection         = "cr_app_coin_struct"
	appDetailUserPushMessageCollection        = "user_push_message"
	appDetailAdPositionCollection             = "cr_app_ad_position"
	appDetailDeviceChannelCollection          = "device_channel"
	appDetailUserChannelCollection            = "user_channel"
	appDetailUserAPprenticeRewardCollection   = "user_apprentice_reward"
	appDetailUserDailyIncomeCollection        = "user_income_by_date"
	appDetailUserADDailyIncomeCollection      = "user_ad_income_by_date"
	appDetailUserArticleDailyIncomeCollection = "user_article_share_by_date"
	appDetailUserIncomeByTypeCollection       = "user_income_total"
	appDetailUserApprenticeByDateCollection   = "user_apprentice_by_date"
	appDetailUserApprenticeTotalCollection    = "user_apprentice_total"

	adDetailDBNameFormat           = "cr_app_%s_ad"
	adDetailADInfoCollectionFormat = "ad_info_%s"

	appSpecialTaskDBNameFormat             = "cr_app_%s_special"
	appSpecialTaskDailyOnceCollection      = "cr_daily_once_task"
	appSpecialTaskDailyManyTimesCollection = "cr_daily_many_times_task"
	appSpecialStayTaskCollection           = "cr_stay_task"
	appSpecialTaskCompleteCollection       = "cr_user_task_complete"
)

var mongoSessionPool *mspool

type mspool struct {
	dbSession *mgo.Session
	wg        *sync.WaitGroup
}

func (m *mspool) Clone() *mspool {
	m.wg.Add(1)
	return &mspool{
		dbSession: m.dbSession.Clone(),
		wg:        m.wg,
	}
}

func (m *mspool) DB(name string) *mgo.Database {
	return m.dbSession.DB(name)
}

func (m *mspool) Close() {
	m.dbSession.Close()
	m.wg.Done()
}

func InitMongo() error {
	ms, err := mgo.Dial(mongoAddr)
	if err != nil {
		return err
	}
	ms.SetSocketTimeout(time.Minute)
	ms.SetSyncTimeout(time.Minute * 2)

	ms.SetMode(mgo.Monotonic, true)

	if err := ms.Ping(); err != nil {
		ms.Close()
		return err
	}

	oldMongoSessionPool := mongoSessionPool
	mongoSessionPool = &mspool{
		dbSession: ms,
		wg:        new(sync.WaitGroup),
	}

	// 关闭老链接
	go func() {
		if oldMongoSessionPool == nil {
			return
		}
		oldMongoSessionPool.wg.Wait()
		oldMongoSessionPool.dbSession.Close()
	}()
	return nil
}

func UpdateUser(appID string, cond bson.M, update bson.M) error {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserBasicCollection)
	err := mongoCol.Update(cond, update)
	mongoSession.Close()
	return err
}

func GetUserInfo(appID string, cond bson.M) (*common.UserBasic, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserBasicCollection)
	var u common.UserBasic
	err := mongoCol.Find(cond).One(&u)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return &u, nil
}

func SaveUserOrderHistory(appID string, usr common.UserBasic, od common.OrderDetail, appVersion string) error {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserOrderCollection)
	err := mongoCol.Update(bson.M{
		"id": usr.ID,
	}, bson.M{
		"$addToSet": bson.M{
			"history": od,
		},
	})

	if err == nil {
		if !od.PreDeducate {
			return nil
		}
		uits := UserIncomeToSave{
			AppID:      appID,
			Usr:        usr,
			OpType:     common.INCOME_TYPE_WITHDRAW, // TYPE_USR_HISTORY_INCOME or TYPE_USR_INCOME_WITHDRAW
			TaskID:     "",
			TaskType:   common.TASK_TYPE_PRE_DEDUCATE,
			Count:      od.Cost,
			Timestamp:  od.Timestamp,
			Desc:       "提现预扣",
			SetMaster:  false,
			MasterRate: 0,
			From:       od.OrderID,
			AppVersion: appVersion,
		}
		return uits.Save()
	}

	if err != mgo.ErrNotFound {
		return err
	}

	if err := mongoCol.Insert(
		common.UserOrder{
			ID: usr.ID,
			History: []common.OrderDetail{
				od,
			},
		},
	); err != nil {
		return err
	}
	if !od.PreDeducate {
		return nil
	}
	uits := UserIncomeToSave{
		AppID:      appID,
		Usr:        usr,
		OpType:     common.INCOME_TYPE_WITHDRAW, // TYPE_USR_HISTORY_INCOME or TYPE_USR_INCOME_WITHDRAW
		TaskID:     "",
		TaskType:   common.TASK_TYPE_PRE_DEDUCATE,
		Count:      od.Cost,
		Timestamp:  od.Timestamp,
		Desc:       "提现预扣",
		SetMaster:  false,
		MasterRate: 0,
		From:       od.OrderID,
		AppVersion: appVersion,
	}
	return uits.Save()
}

// 获取多上游广告配置
func GetCurrentADDataHistory(appID string) (*common.ADDATAHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(appDetailAdPositionCollection)
	var ad common.ADDATA
	err := mongoCol.Find(bson.M{"id": appID}).One(&ad)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}

	for _, h := range ad.History {
		if !h.IsCurrent {
			continue
		}

		for i := range h.Details {
			if h.Details[i].ADID != "3" {
				continue
			}
			for j := range h.Details[i].Direct {
				h.Details[i].Direct[j].ADID = h.Details[i].Direct[j].BelongTo
			}
		}

		return &h, nil
	}
	return nil, nil
}

func SaveNewUser(appID string, u common.UserBasic) error {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserBasicCollection)
	err := mongoCol.Insert(u)
	mongoSession.Close()
	return err
}

func GetVideoList(appID, userID string) (*common.UserVideos, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserVideoCollection)
	var uv common.UserVideos
	err := mongoCol.Find(bson.M{"id": userID}).One(&uv)
	mongoSession.Close()
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil, nil
		}
		return nil, err
	}
	return &uv, nil
}

func SaveReleasedFile(appID, appVersion, appChannel, downloadURL string) error {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppDBAppHistoryCollection)
	err := mongoCol.Update(bson.M{
		"id": appID,
	}, bson.M{
		"$addToSet": bson.M{
			"history": common.ReleasedHistory{
				Timestamp:     time.Now().Unix(),
				Version:       appVersion,
				ReleasedNotes: "",
				DownloadURL:   downloadURL,
			},
		},
	})

	if err != mgo.ErrNotFound {
		return err
	}

	return mongoCol.Insert(
		common.ReleasedFile{
			ID:      appID,
			Channel: appChannel,
			History: []common.ReleasedHistory{
				{
					Timestamp:     time.Now().Unix(),
					Version:       appVersion,
					ReleasedNotes: "",
					DownloadURL:   downloadURL,
				},
			},
		})
}

func GetRadomImages(appID string) (*common.RadomAppImage, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppRadomImageCollection)
	var rai common.RadomAppImage
	err := mongoCol.Find(bson.M{"id": appID}).One(&rai)
	mongoSession.Close()
	if err != nil {
		return nil, err
	}
	return &rai, nil
}

func SaveRadomImage(appID, imageURL string) error {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppRadomImageCollection)
	err := mongoCol.Update(bson.M{
		"id": appID,
	}, bson.M{
		"$addToSet": bson.M{
			"images": common.RadomImage{
				URL: imageURL,
			},
		},
	})

	if err != mgo.ErrNotFound {
		return err
	}

	return mongoCol.Insert(
		common.RadomAppImage{
			ID: appID,
			Images: []common.RadomImage{
				{
					URL: imageURL,
				},
			},
		})
}

func GetCurrentExchangeRate(appID string) (*common.RateExchangeHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppExchangeRateHistoryCollection)
	var aerh common.AppExchangeRateHistory
	err := mongoCol.Find(bson.M{
		"id": appID,
	}).One(&aerh)
	mongoSession.Close()
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil, nil
		}
		return nil, err
	}
	for _, h := range aerh.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

type TmpUserApprenticeDetail struct {
	ID                  string `json:"-"`
	ApprenticeCode      string `json:"apprentice_code"`
	ApprenticeName      string `json:"apprentice_name"`
	Amount              int    `json:"amount"`
	ApprenticeTimestamp int64  `json:"apprentice_timestamp"`
}

func GetMyApprenticeLeaderboardsCount(appID, userID string, startTimestamp, endTimestamp int64) (int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticesCollection)
	cond := bson.M{"id": userID}
	timestampCond := make(bson.M)
	hasTimeCond := false
	if startTimestamp != common.NO_LIMIT {
		hasTimeCond = true
		timestampCond["$gte"] = startTimestamp
	}
	if endTimestamp != common.NO_LIMIT {
		hasTimeCond = true
		timestampCond["$lt"] = endTimestamp
	}
	if hasTimeCond {
		cond["timestamp"] = timestampCond
	}
	count, err := mongoCol.Find(cond).Count()
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return count, nil
}

func GetMyApprenticeLeaderboards(appID, userID string) ([]TmpUserApprenticeDetail, error) {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticesCollection)
	var userApprentices []common.UserApprentice
	if err := mongoCol.Find(bson.M{"id": userID}).All(&userApprentices); err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}

	tusds := make([]TmpUserApprenticeDetail, len(userApprentices))
	for i, v := range userApprentices {
		tusds[i].ID = v.ApprenticeID
		tusds[i].Amount = v.Amount
		tusds[i].ApprenticeTimestamp = v.Timestamp
		var u common.UserBasic
		cond := bson.M{"id": v.ApprenticeID}
		userBasicMongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserBasicCollection)
		err := userBasicMongoCol.Find(cond).One(&u)
		if err != nil {
			if err == mgo.ErrNotFound {
				continue
			}
			return nil, err
		}
		tusds[i].ApprenticeCode = u.Code
		tusds[i].ApprenticeName = u.Info.Name
	}
	return tusds, nil
	/*
		mongoSession := mongoSessionPool.Clone()
		defer mongoSession.Close()
		mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticeCollection)
		var userApprentices common.UserApprenticeHistory
		if err := mongoCol.Find(bson.M{"id": userID}).One(&userApprentices); err != nil {
			if err != mgo.ErrNotFound {
				return nil, err
			}
			return nil, nil
		}

		tusds := make([]TmpUserApprenticeDetail, len(userApprentices.List))
		for i, v := range userApprentices.List {
			tusds[i].ID = v.ApprenticeID
			tusds[i].Amount = v.Amount
			tusds[i].ApprenticeTimestamp = v.Timestamp
			var u common.UserBasic
			cond := bson.M{"id": v.ApprenticeID}
			userBasicMongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserBasicCollection)
			err := userBasicMongoCol.Find(cond).One(&u)
			if err != nil {
				if err == mgo.ErrNotFound {
					continue
				}
				return nil, err
			}
			tusds[i].ApprenticeCode = u.Code
			tusds[i].ApprenticeName = u.Info.Name
		}
		return tusds, nil
	*/
}

func GetCurrentFixedBoundConf(appID string) (*common.AppFixedBoundHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppFixedBoundCollection)
	var appFixedBound common.AppFixedBound
	err := mongoCol.Find(bson.M{"id": appID}).One(&appFixedBound)
	mongoSession.Close()
	if err != nil {
		return nil, err
	}
	for _, h := range appFixedBound.History {
		if h.IsCurrent {
			return &h, nil
		}
	}
	return nil, errors.New("no current fixed bounds")
}

func GetUserDailyWithdrawAmount(appID string, userID string, timestamp int64) (int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserOrderCollection)
	var uo common.UserOrder
	err := mongoCol.Find(bson.M{"id": userID}).One(&uo)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return -1, err
		}
		return 0, nil
	}
	var total int
	dateStart := time.Unix(timestamp, 0).Truncate(time.Hour * 24)
	start := dateStart.Unix()
	end := dateStart.Add(time.Hour * 24).Unix()
	for _, v := range uo.History {
		if v.Success && (v.Timestamp >= start && v.Timestamp < end) {
			total = total + v.Amount
		}
	}
	return total, nil
}

func GetUserOrder(appID, userID string) (*common.UserOrder, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserOrderCollection)
	var uo common.UserOrder
	err := mongoCol.Find(bson.M{"id": userID}).One(&uo)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return &uo, nil
}

// 获取用户本周提现次数
// 每周: 从周一到周日
func GetUserWithdrawCountThisWeek(appID, userID string) (int, error) {
	uo, err := GetUserOrder(appID, userID)
	if err != nil {
		return 0, err
	}
	if uo == nil {
		return 0, nil
	}
	now := time.Now().Local()
	w := now.Weekday()
	startTimestamp := common.GetDateStartUnix(now.Add(time.Hour * -24 * time.Duration(w)).Unix())
	var total int
	for _, h := range uo.History {
		if h.Timestamp >= startTimestamp {
			total++
		}
	}
	return total, nil
}

func GetOrderDetail(appID, userID, orderID string) (*common.OrderDetail, error) {
	uo, err := GetUserOrder(appID, userID)
	if err != nil {
		return nil, err
	}
	for _, v := range uo.History {
		if v.OrderID == orderID {
			return &v, nil
		}
	}
	return nil, errors.New("order not found")
}

func DoWithdrawRefuse(appID string, usr common.UserBasic, od common.OrderDetail, reason string, isPreDeducate bool) error {
	mongoSession := mongoSessionPool.Clone()
	nowUnix := time.Now().Unix()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserOrderCollection)
	query := bson.M{
		"id": usr.ID,
		"history": bson.M{
			"$elemMatch": bson.M{
				"order_id": od.OrderID,
				"status":   common.ORDER_STATUS_TO_BE_CHECKED,
			},
		},
	}

	desc := "审核不通过." + reason

	updator := bson.M{
		"$set": bson.M{
			"history.$.status":     common.ORDER_STATUS_REFUSED,
			"history.$.ftimestamp": nowUnix,
			"history.$.desc":       desc,
		},
	}

	err := mongoCol.Update(query, updator)
	mongoSession.Close()
	if err != nil {
		return err
	}
	if !isPreDeducate {
		return nil
	}
	uits := UserIncomeToSave{
		AppID:      appID,
		Usr:        usr,
		OpType:     common.INCOME_TYPE_INCOME_NO_TOTAL, // TYPE_USR_HISTORY_INCOME or TYPE_USR_INCOME_WITHDRAW
		TaskID:     "",
		TaskType:   common.TASK_TYPE_PRE_DEDUCATE_RET, // TASK_TYPE_XXX
		Count:      od.Cost,
		Timestamp:  nowUnix,
		Desc:       common.USER_ORDER_RET_DESC,
		SetMaster:  false,
		MasterRate: 0,
		From:       od.OrderID,
		AppVersion: od.AppVersion,
	}

	return uits.Save()
}

func UpdateUserOrderResult(appID, userID string, od common.OrderDetail, success bool, desc string) error {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserOrderCollection)
	timeNowUnix := time.Now().Unix()
	updator := bson.M{
		"$set": bson.M{
			"history.$.status":     od.Status,
			"history.$.desc":       desc,
			"history.$.success":    success,
			"history.$.ftimestamp": timeNowUnix,
		},
	}
	if od.OrderType == common.ORDER_TYPE_ALIPAY_WITHDRAWA || od.OrderType == common.ORDER_TYPE_WECHAT_WITHDRAWA {
		if success {
			updator = bson.M{
				"$set": bson.M{
					"history.$.status":     od.Status,
					"history.$.desc":       desc,
					"history.$.success":    success,
					"history.$.ftimestamp": timeNowUnix,
					"recent_withdraw":      timeNowUnix,
				},
				"$inc": bson.M{"withdraw_total": od.Amount * (-1)},
			}
		}
	}
	err := mongoCol.Update(bson.M{
		"id":               userID,
		"history.order_id": od.OrderID,
	}, updator)
	mongoSession.Close()
	return err
}

func GetShareContent(appID string) (*common.AppShareContentHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppShareContentCollection)
	var asc common.AppShareContent
	err := mongoCol.Find(bson.M{"id": appID}).One(&asc)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}

	for _, h := range asc.History {
		if h.IsCurrent != true {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func GetAppAds(appID string) (*common.AppAdsHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppAdsCollection)
	var aa common.AppAds
	err := mongoCol.Find(bson.M{"id": appID}).One(&aa)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range aa.History {
		if !h.IsCurrent {
			continue
		}
		details := h.Details
		h.Details = nil
		for _, d := range details {
			if d.Status != 0 {
				continue
			}
			h.Details = append(h.Details, d)
		}
		return &h, nil
	}
	return nil, nil
}

func GetWithdrawDailyTimes(appID, userID string, timestamp int64) (int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserOrderCollection)
	var uo common.UserOrder
	err := mongoCol.Find(bson.M{"id": userID}).One(&uo)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return -1, err
		}
		return 0, nil
	}

	dateStart := common.GetDateStartUnix(timestamp)
	dateEnd := common.GetDateEndUnix(timestamp)
	total := 0
	for _, h := range uo.History {
		if h.Timestamp > dateEnd || h.Timestamp < dateStart {
			continue
		}
		total += 1
	}
	return total, nil
}

type tmpBenefitOrder struct {
	UserName         string `json:"user_name"`
	Amount           int    `json:"amount"`
	ApprenticeNumber int    `json:"apprentice_number"`
	Order            int    `json:"order"`
}

func GetUserBenefitLeaderBoards(appID string) ([]tmpBenefitOrder, error) {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	dbName := appDetailDBNamePrefix + appID
	mongoCol := mongoSession.DB(dbName).C(appDetailUserOrderCollection)
	var uos []common.UserOrder
	if err := mongoCol.Find(nil).Sort("-withdraw_total").Limit(30).All(&uos); err != nil {
		return nil, err
	}

	leaderBoardsLen := len(uos)
	lds := make([]tmpBenefitOrder, leaderBoardsLen)
	for k, v := range uos {
		var u common.UserBasic
		cond := bson.M{"id": v.ID}
		userBasicMongoCol := mongoSession.DB(dbName).C(appDetailUserBasicCollection)
		err := userBasicMongoCol.Find(cond).One(&u)
		if err != nil {
			if err == mgo.ErrNotFound {
				return nil, nil
			}
			return nil, err
		}

		count, err := mongoSession.DB(dbName).C(appDetailUserApprenticesCollection).Find(cond).Count()
		if err != nil {
			if err != mgo.ErrNotFound {
				return nil, err
			}
		}

		lds[k] = tmpBenefitOrder{
			UserName:         u.Info.Name,
			Amount:           v.WithdrawTotal,
			ApprenticeNumber: count,
			Order:            k + 1,
		}

		/*
			var auh common.UserApprenticeHistory
			userApprenticeMongoCol := mongoSession.DB(dbName).C(appDetailUserApprenticeCollection)
			if err := userApprenticeMongoCol.Find(cond).One(&auh); err != nil {
				if err != mgo.ErrNotFound {
					return nil, err
				}
			}
			lds[k] = tmpBenefitOrder{
				UserName:         u.Info.Name,
				Amount:           v.WithdrawTotal,
				ApprenticeNumber: len(auh.List),
				Order:            k + 1,
			}
		*/
	}
	return lds, nil
}

func GetUserBenefit(appID, userID string) (*common.UserOrder, error) {
	mongoSession := mongoSessionPool.Clone()
	dbName := appDetailDBNamePrefix + appID
	mongoCol := mongoSession.DB(dbName).C(appDetailUserOrderCollection)
	var uo common.UserOrder
	err := mongoCol.Find(bson.M{"id": userID}).One(&uo)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}

	return &uo, nil
}

func GetRecentUserWithdrawList(appID string) ([]common.AppRecentWithdrawInfo, error) {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	dbName := appDetailDBNamePrefix + appID
	mongoCol := mongoSession.DB(dbName).C(appDetailUserOrderCollection)
	var uos []common.UserOrder
	if err := mongoCol.Find(nil).Sort("-recent_withdraw").Limit(30).All(&uos); err != nil {
		return nil, err
	}

	leaderBoardsLen := len(uos)
	wis := make([]common.AppRecentWithdrawInfo, leaderBoardsLen)
	for k, v := range uos {
		var u common.UserBasic
		cond := bson.M{"id": v.ID}
		userBasicMongoCol := mongoSession.DB(dbName).C(appDetailUserBasicCollection)
		err := userBasicMongoCol.Find(cond).One(&u)
		if err != nil {
			if err == mgo.ErrNotFound {
				return nil, nil
			}
			return nil, err
		}
		wi := common.AppRecentWithdrawInfo{
			UserName: u.Info.Name,
		}
		historyLen := len(v.History)
		for i := 0; i < historyLen; i++ {
			vhi := v.History[historyLen-i-1]
			if vhi.Success && (vhi.OrderType == common.ORDER_TYPE_ALIPAY_WITHDRAWA || vhi.OrderType == common.ORDER_TYPE_WECHAT_WITHDRAWA) {
				wi.Amount = vhi.Amount * (-1)
				wi.Timestamp = vhi.FTimestamp
				break
			}
		}

		wis[k] = wi
	}
	return wis, nil
}

func GetUserIncomeHistory(appID, userID string) ([]common.UserIncomeHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIncomeHistoryCollection)
	var uih []common.UserIncomeHistory
	err := mongoCol.Find(bson.M{"id": userID}).All(&uih)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return uih, nil
}

func GetUserIncomeHistoryByPage(appID, userID string, page, maxPerPage int) (int, []common.UserIncomeHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIncomeHistoryCollection)
	total, err := mongoCol.Find(bson.M{"id": userID}).Count()
	if err != nil {
		return 0, nil, err
	}
	if total == 0 {
		return 0, nil, nil
	}
	var uih []common.UserIncomeHistory
	if err := mongoCol.Find(bson.M{"id": userID}).Sort("-timestamp").Skip((page - 1) * maxPerPage).Limit(maxPerPage).All(&uih); err != nil {
		if err != mgo.ErrNotFound {
			return total, nil, err
		}
		return total, nil, nil
	}

	return total, uih, nil
}

func GetUserIncomeHistoryCountByDetail(appID, userID string, startTimestamp, endTimestamp int64, historyType, historyTaskType int, from string) (int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIncomeHistoryCollection)
	cond := bson.M{"id": userID}

	timestampCond := make(bson.M)
	if startTimestamp != common.NO_LIMIT {
		timestampCond["$gte"] = startTimestamp
	}
	if endTimestamp != common.NO_LIMIT {
		timestampCond["$lt"] = endTimestamp
	}
	if len(timestampCond) != 0 {
		cond["timestamp"] = timestampCond
	}

	if historyType != common.NO_LIMIT {
		cond["type"] = historyType
	}
	if historyTaskType != common.NO_LIMIT {
		cond["task_type"] = historyTaskType
	}
	if from != "" {
		cond["from"] = from
	}
	count, err := mongoCol.Find(cond).Count()
	mongoSession.Close()
	return count, err
}

func GetUserIncomeHistoryTotalByDetail(appID, userID string, startTimestamp, endTimestamp int64, historyType, historyTaskType int, from string) (int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIncomeHistoryCollection)
	cond := bson.M{"id": userID}
	timestampCond := make(bson.M)
	if startTimestamp != common.NO_LIMIT {
		timestampCond["$gte"] = startTimestamp
	}
	if endTimestamp != common.NO_LIMIT {
		timestampCond["$lt"] = endTimestamp
	}
	if len(timestampCond) != 0 {
		cond["timestamp"] = timestampCond
	}

	if historyType != common.NO_LIMIT {
		cond["type"] = historyType
	}
	if historyTaskType != common.NO_LIMIT {
		cond["task_type"] = historyTaskType
	}
	if from != "" {
		cond["from"] = from
	}
	pips := []bson.M{
		bson.M{"$match": cond},
		bson.M{"$group": bson.M{"_id": nil, "total": bson.M{"$sum": "$count"}}},
	}
	var t common.AggregationTotal
	err := mongoCol.Pipe(pips).One(&t)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return t.Total, nil
}

func GetUserIncomeHistoryByDetail(appID, userID string, startTimestamp, endTimestamp int64, historyType, historyTaskType int, from string) ([]common.UserIncomeHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIncomeHistoryCollection)
	cond := bson.M{"id": userID}
	timestampCond := make(bson.M)
	if startTimestamp != common.NO_LIMIT {
		timestampCond["$gte"] = startTimestamp
	}
	if endTimestamp != common.NO_LIMIT {
		timestampCond["$lt"] = endTimestamp
	}
	if len(timestampCond) != 0 {
		cond["timestamp"] = timestampCond
	}

	if historyType != common.NO_LIMIT {
		cond["type"] = historyType
	}
	if historyTaskType != common.NO_LIMIT {
		cond["task_type"] = historyTaskType
	}
	if from != "" {
		cond["from"] = from
	}
	var uis []common.UserIncomeHistory
	err := mongoCol.Find(cond).All(&uis)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return uis, nil
}

func GetUserDailyIncomeHistoryByDetail(appID, userID string, timestamp int64, historyType, historyTaskType int, from string) ([]common.UserIncomeHistory, error) {
	dateStartTimestamp := common.GetDateStartUnix(timestamp)
	dateEndTimestamp := common.GetDateEndUnix(timestamp)
	return GetUserIncomeHistoryByDetail(appID, userID, dateStartTimestamp, dateEndTimestamp, historyType, historyTaskType, from)
}

/*
func GetUserDailyIncomeCountByDetail(appID, userID string, timestamp int64, historyType, historyTaskType int, from string) (int, error) {
	dateStartTimestamp := common.GetDateStartUnix(timestamp)
	dateEndTimestamp := common.GetDateEndUnix(timestamp)
	return GetUserIncomeHistoryCountByDetail(appID, userID, dateStartTimestamp, dateEndTimestamp, historyType, historyTaskType, from)
}

func GetUserDailyIncomeTotalByDetail(appID, userID string, timestamp int64, historyType, historyTaskType int, from string) (int, error) {
	dateStartTimestamp := common.GetDateStartUnix(timestamp)
	dateEndTimestamp := common.GetDateEndUnix(timestamp)
	return GetUserIncomeHistoryTotalByDetail(appID, userID, dateStartTimestamp, dateEndTimestamp, historyType, historyTaskType, from)
}

func GetUserIncomeHistoryByTimeRange(appID, userID string, startTimestamp, endTimestamp int64) ([]common.UserIncomeHistory, error) {
	return GetUserIncomeHistoryByDetail(appID, userID, startTimestamp, endTimestamp, common.NO_LIMIT, common.NO_LIMIT, "")
}
*/

func GetUserDailyIncomeByType(appID, userID string, mtype, taskType int, dateTimestamp int64) (int, int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserDailyIncomeCollection)
	var uibd common.UserIncomeByDate
	timestamp := common.GetDateStartUnix(dateTimestamp)
	err := mongoCol.Find(bson.M{"id": userID, "type": mtype, "task_type": taskType, "timestamp": timestamp}).One(&uibd)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, 0, err
		}
		return 0, 0, nil
	}
	return uibd.Total, uibd.Count, nil
}

func GetUserIncomeTotalByDetail(appID, userID string, mtype, taskType int) (int, int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIncomeByTypeCollection)
	var uibd common.UserIncomeByType
	err := mongoCol.Find(bson.M{"id": userID, "type": mtype, "task_type": taskType}).One(&uibd)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, 0, err
		}
		return 0, 0, nil
	}
	return uibd.Total, uibd.Count, nil
}

func GetUserApprenticeByDate(appID, userID, apprenticeID string, dateTimestamp int64) (int, int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticeByDateCollection)
	var uad common.UserApprenticeDetailByDate
	timestamp := common.GetDateStartUnix(dateTimestamp)
	err := mongoCol.Find(bson.M{"id": userID, "apprentice_id": apprenticeID, "timestamp": timestamp}).One(&uad)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, 0, err
		}
		return 0, 0, nil
	}
	return uad.Total, uad.Count, nil
}

func GetUserApprenticeTotal(appID, userID, apprenticeID string) (int, int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticeTotalCollection)
	var uad common.UserApprenticeDetailTotal
	err := mongoCol.Find(bson.M{"id": userID, "apprentice_id": apprenticeID}).One(&uad)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, 0, err
		}
		return 0, 0, nil
	}
	return uad.Total, uad.Count, nil
}

func GetUserDailyAdClickByADID(appID, userID, adID string, dateTimestamp int64) (int, int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserADDailyIncomeCollection)
	var uad common.UserAdDetailByDate
	timestamp := common.GetDateStartUnix(dateTimestamp)
	err := mongoCol.Find(bson.M{"id": userID, "ad_id": adID, "timestamp": timestamp}).One(&uad)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, 0, err
		}
		return 0, 0, nil
	}
	return uad.Total, uad.Count, nil
}

func GetUserDailyAdShare(appID, userID, articleID string, dateTimestamp int64) (int, int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserArticleDailyIncomeCollection)
	var uad common.UserArticleDetailByDate
	timestamp := common.GetDateStartUnix(dateTimestamp)
	err := mongoCol.Find(bson.M{"id": userID, "article_id": articleID, "timestamp": timestamp}).One(&uad)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, 0, err
		}
		return 0, 0, nil
	}
	return uad.Total, uad.Count, nil

}

// 存储用户收益明细用
type UserIncomeToSave struct {
	AppID      string
	Usr        common.UserBasic
	OpType     int // TYPE_USR_HISTORY_INCOME or TYPE_USR_INCOME_WITHDRAW
	TaskID     string
	TaskType   int // TASK_TYPE_XXX
	Count      int
	Timestamp  int64
	Desc       string
	SetMaster  bool // 是否给师傅一定比例的奖励
	MasterRate float64
	From       string
	AppVersion string
	IP         string
}

func (u *UserIncomeToSave) Save() error {
	selector := bson.M{"id": u.Usr.ID}
	var updator bson.M
	switch u.OpType {
	case common.INCOME_TYPE_INCOME:
		if u.Usr.Status == common.STATUS_USER_DISABLED {
			return nil
		}
		if u.TaskType == common.TASK_TYPE_APPRENTICE || u.TaskType == common.TASK_TYPE_APPRENTICE_ONCE {
			updator = bson.M{
				"$inc": bson.M{
					"income_remaining":        u.Count,
					"income_total":            u.Count,
					"income_apprentice_total": u.Count,
				},
			}
		} else {
			updator = bson.M{
				"$inc": bson.M{
					"income_remaining": u.Count,
					"income_total":     u.Count,
				},
			}
		}
	case common.INCOME_TYPE_INCOME_NO_TOTAL:
		if u.TaskType == common.TASK_TYPE_APPRENTICE || u.TaskType == common.TASK_TYPE_APPRENTICE_ONCE {
			updator = bson.M{
				"$inc": bson.M{
					"income_remaining":        u.Count,
					"income_apprentice_total": u.Count,
				},
			}
		} else {
			updator = bson.M{
				"$inc": bson.M{
					"income_remaining": u.Count,
				},
			}
		}
	case common.INCOME_TYPE_WITHDRAW:
		updator = bson.M{
			"$inc": bson.M{
				"income_remaining": u.Count * (-1),
			},
		}
	default:
		return errors.New("invalid optype")
	}
	if err := UpdateUser(u.AppID, selector, updator); err != nil {
		return err
	}
	go u.SaveRedudance()
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + u.AppID).C(appDetailUserIncomeHistoryCollection)
	uh := common.UserIncomeHistory{
		ID:         u.Usr.ID,
		Timestamp:  u.Timestamp,
		Type:       u.OpType,
		TaskID:     u.TaskID,
		TaskType:   u.TaskType,
		Count:      u.Count,
		Desc:       u.Desc,
		From:       u.From,
		AppVersion: u.AppVersion,
		IP:         u.IP,
	}
	err := mongoCol.Insert(uh)
	mongoSession.Close()
	if err != nil {
		return err
	}

	if u.SetMaster {
		go func() {
			if err := SetMasterIncome(u.AppID, u.Usr.ID, u.Count, u.MasterRate, u.AppVersion); err != nil {
				log.Fields{
					"appID":  u.AppID,
					"userID": u.Usr.ID,
					"count":  u.Count,
					"err":    err.Error(),
				}.Error("fail to set master income")
			}
		}()
	}
	return nil
}

func (u *UserIncomeToSave) SaveRedudance() error {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	var lastErr error
	userIncomeByDateCol := mongoSession.DB(appDetailDBNamePrefix + u.AppID).C(appDetailUserDailyIncomeCollection)
	if err := doSaveUserIncomeByDateRedudance(userIncomeByDateCol, u.Usr.ID, u.OpType, u.TaskType, u.Count, u.Timestamp); err != nil {
		lastErr = err
	}
	if err := doSaveUserIncomeByDateRedudance(userIncomeByDateCol, u.Usr.ID, u.OpType, common.TOTAL_CONF_USER_INCOME, u.Count, u.Timestamp); err != nil {
		lastErr = err
	}
	if err := doSaveUserIncomeByDateRedudance(userIncomeByDateCol, common.TOTAL_CONF_VLION_ALL_ID, u.OpType, u.TaskType, u.Count, u.Timestamp); err != nil {
		lastErr = err
	}
	userIncomeTotalCol := mongoSession.DB(appDetailDBNamePrefix + u.AppID).C(appDetailUserIncomeByTypeCollection)
	if err := doSaveUserIncomeTotalRedudance(userIncomeTotalCol, u.Usr.ID, u.OpType, u.TaskType, u.Count); err != nil {
		lastErr = err
	}
	if err := doSaveUserIncomeTotalRedudance(userIncomeTotalCol, u.Usr.ID, u.OpType, common.TOTAL_CONF_USER_INCOME, u.Count); err != nil {
		lastErr = err
	}
	switch u.TaskType {
	case common.TASK_TYPE_AD_CLICK:
		adIncomeByDateCol := mongoSession.DB(appDetailDBNamePrefix + u.AppID).C(appDetailUserADDailyIncomeCollection)
		if err := doSaveUserADIncomeByDate(adIncomeByDateCol, u.Usr.ID, u.From, u.Count, u.Timestamp); err != nil {
			lastErr = err
		}
		if err := doSaveUserADIncomeByDate(adIncomeByDateCol, u.Usr.ID, common.TOTAL_CONF_USER_AD, u.Count, u.Timestamp); err != nil {
			lastErr = err
		}
		return lastErr
	case common.TASK_TYPE_SHARE_ARTICLE_READ:
		articleShareByDateCol := mongoSession.DB(appDetailDBNamePrefix + u.AppID).C(appDetailUserArticleDailyIncomeCollection)
		if err := doSaveUserArticleShareByDate(articleShareByDateCol, u.Usr.ID, u.From, u.Count, u.Timestamp); err != nil {
			lastErr = err
		}
		if err := doSaveUserArticleShareByDate(articleShareByDateCol, u.Usr.ID, common.TOTAL_CONF_USER_ARTICLE, u.Count, u.Timestamp); err != nil {
			lastErr = err
		}
		return lastErr
	case common.TASK_TYPE_APPRENTICE:
		if u.Usr.From == "" {
			return lastErr
		}
		userApprenticeByDateCol := mongoSession.DB(appDetailDBNamePrefix + u.AppID).C(appDetailUserApprenticeByDateCollection)
		if err := doSaveUserApprenticeByDate(userApprenticeByDateCol, u.Usr.From, u.Usr.ID, u.Count, u.Timestamp); err != nil {
			lastErr = err
		}
		if err := doSaveUserApprenticeByDate(userApprenticeByDateCol, u.Usr.From, common.TOTAL_CONF_USER_APPRENTICE, u.Count, u.Timestamp); err != nil {
			lastErr = err
		}
		userApprenticeTotalCol := mongoSession.DB(appDetailDBNamePrefix + u.AppID).C(appDetailUserApprenticeTotalCollection)
		if err := doSaveUserApprenticeTotalRedudance(userApprenticeTotalCol, u.Usr.From, u.Usr.ID, u.Count); err != nil {
			lastErr = err
		}
		if err := doSaveUserApprenticeTotalRedudance(userApprenticeTotalCol, u.Usr.From, common.TOTAL_CONF_USER_APPRENTICE, u.Count); err != nil {
			lastErr = err
		}
		return lastErr
	default:
		return lastErr
	}
	return lastErr
}

func doSaveUserIncomeByDateRedudance(col *mgo.Collection, userID string, mtype, taskType, amount int, timestamp int64) error {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	err := col.Update(bson.M{"id": userID, "type": mtype, "task_type": taskType, "timestamp": dateTimestamp}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserIncomeByDate{
			ID:        userID,
			Type:      mtype,
			TaskType:  taskType,
			Timestamp: dateTimestamp,
			Total:     amount,
			Count:     1,
		})
	}
	return nil
}

func doSaveUserIncomeTotalRedudance(col *mgo.Collection, userID string, mtype, taskType, amount int) error {
	err := col.Update(bson.M{"id": userID, "type": mtype, "task_type": taskType}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserIncomeByType{
			ID:       userID,
			Type:     mtype,
			TaskType: taskType,
			Total:    amount,
			Count:    1,
		})
	}
	return nil
}

func doSaveUserADIncomeByDate(col *mgo.Collection, userID, adID string, amount int, timestamp int64) error {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	err := col.Update(bson.M{"id": userID, "ad_id": adID, "timestamp": dateTimestamp}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserAdDetailByDate{
			ID:        userID,
			ADID:      adID,
			Timestamp: dateTimestamp,
			Total:     amount,
			Count:     1,
		})
	}
	return nil
}

func doSaveUserArticleShareByDate(col *mgo.Collection, userID, articleID string, amount int, timestamp int64) error {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	err := col.Update(bson.M{"id": userID, "article_id": articleID, "timestamp": dateTimestamp}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserArticleDetailByDate{
			ID:        userID,
			ArticleID: articleID,
			Timestamp: dateTimestamp,
			Total:     amount,
			Count:     1,
		})
	}
	return nil
}

func doSaveUserApprenticeByDate(col *mgo.Collection, userID, apprenticeID string, amount int, timestamp int64) error {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	err := col.Update(bson.M{"id": userID, "apprentice_id": apprenticeID, "timestamp": dateTimestamp}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserApprenticeDetailByDate{
			ID:           userID,
			ApprenticeID: apprenticeID,
			Timestamp:    dateTimestamp,
			Total:        amount,
			Count:        1,
		})
	}
	return nil
}

func doSaveUserApprenticeTotalRedudance(col *mgo.Collection, userID, apprenticeID string, amount int) error {
	err := col.Update(bson.M{"id": userID, "apprentice_id": apprenticeID}, bson.M{"$inc": bson.M{"total": amount, "count": 1}})
	if err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return col.Insert(common.UserApprenticeDetailTotal{
			ID:           userID,
			ApprenticeID: apprenticeID,
			Total:        amount,
			Count:        1,
		})
	}
	return nil
}

func SetMasterIncome(appID, userID string, apprenticeIncome int, masterRate float64, appVersion string) error {
	masterIncome := int(float64(apprenticeIncome) * masterRate)
	if masterIncome <= 0 {
		return nil
	}
	ui, err := GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		log.Fields{
			"apprenticeID": userID,
			"err":          err.Error(),
		}.Error("fail to find user by id")
		return err
	}
	if ui == nil {
		log.Fields{
			"apprenticeID": userID,
		}.Error("user not found")
		return nil
	}
	if ui.From == "" {
		return nil
	}

	if err := SetUserApprenticeRewardDetail(appID, ui.From, userID, masterIncome); err != nil {
		log.Fields{
			"appID":        appID,
			"masterID":     ui.From,
			"apprenticeID": userID,
			"err":          err.Error(),
		}.Error("fail to set user apprentice reward detail")
		return err
	}

	usr, err := GetUserInfo(appID, bson.M{"id": ui.From})
	if err != nil {
		return err
	}
	if usr == nil {
		return errors.New("master not found")
	}

	uits := UserIncomeToSave{
		AppID:      appID,
		Usr:        *usr,
		OpType:     common.INCOME_TYPE_INCOME,
		TaskID:     "",
		TaskType:   common.TASK_TYPE_APPRENTICE,
		Count:      masterIncome,
		Timestamp:  time.Now().Unix(),
		Desc:       "师徒收益",
		SetMaster:  false,
		MasterRate: masterRate,
		From:       userID,
		AppVersion: appVersion,
	}
	return uits.Save()
}

func SetUserApprenticeRewardDetail(appID, masterID, apprenticeID string, amount int) error {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticesCollection)
	err := mongoCol.Update(bson.M{"id": masterID, "apprentice_id": apprenticeID}, bson.M{
		"$inc": bson.M{
			"amount": amount,
		},
	})
	mongoSession.Close()
	return err
	/*
		mongoSession := mongoSessionPool.Clone()
		mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticeCollection)
		err := mongoCol.Update(bson.M{"id": masterID, "list.apprentice_id": apprenticeID}, bson.M{
			"$inc": bson.M{
				"list.$.amount": amount,
				"amount_total":  amount,
			},
		})
		mongoSession.Close()
		return err
	*/
}

func GetUserMiddleInfo(appID, userID string) (*common.UserMiddleApprentice, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticeMiddleCollection)
	var uma common.UserMiddleApprentice
	err := mongoCol.Find(bson.M{"id": userID}).One(&uma)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return &uma, nil
}

func GetUserMiddleSet(appID string) (*common.UserApprenticeRewardConfHistoryDetail, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppApprenticeRewardByDateCollection)
	var uarc common.UserApprenticeRewardConf
	err := mongoCol.Find(bson.M{"id": appID}).One(&uarc)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, v := range uarc.History {
		if v.IsCurrent {
			return &v, nil
		}
	}
	return nil, nil
}

func UpdateApprenticeMiddle(appID, userID string, updator bson.M) error {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticeMiddleCollection)
	err := mongoCol.Update(bson.M{"id": userID}, updator)
	mongoSession.Close()
	return err
}

func InitApprenticeMiddle(appID, userID, masterID string, firstReward bool, timestamp int64) error {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticeMiddleCollection)
	uma := common.UserMiddleApprentice{
		ID:       userID,
		MasterID: masterID,
		Finished: false,
	}
	if firstReward {
		uma.Details = []int64{timestamp}
	}
	err := mongoCol.Insert(uma)
	mongoSession.Close()
	return err
}

func SetUserApprentice(appID, masterID, apprenticeID string, source int, timestamp int64) error {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticesCollection)
	ua := common.UserApprentice{
		ID:           masterID,
		Timestamp:    timestamp,
		ApprenticeID: apprenticeID,
		Source:       source,
		Amount:       0,
	}
	err := mongoCol.Insert(ua)
	mongoSession.Close()
	return err

	/*
		mongoSession := mongoSessionPool.Clone()
		defer mongoSession.Close()
		mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserApprenticeCollection)
		uad := common.UserApprenticeDetail{
			Timestamp:    timestamp,
			ApprenticeID: apprenticeID,
			Source:       source,
			Amount:       0,
		}
		err := mongoCol.Update(bson.M{"id": masterID}, bson.M{
			"$addToSet": bson.M{
				"list": uad,
			},
			"$inc": bson.M{
				"apprentice_count": 1,
			},
		})
		if err != nil {
			if err != mgo.ErrNotFound {
				return err
			}
			return mongoCol.Insert(
				common.UserApprenticeHistory{
					ID:              masterID,
					AmountTotal:     0,
					ApprenticeCount: 1,
					List: []common.UserApprenticeDetail{
						uad,
					},
				})
		}
		return nil
	*/
}

func GetUserArticleShareClicks(appID, key string) (*common.UserArticelShare, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserArticleShareCollection)
	var uas common.UserArticelShare
	err := mongoCol.Find(bson.M{"id": key}).One(&uas)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return &uas, nil
}

func GetCurrentAppUserReward(appID string) (*common.AppRewardHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppRewardCollection)
	var ar common.AppReward
	err := mongoCol.Find(bson.M{"id": appID}).One(&ar)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, v := range ar.History {
		if v.IsCurrent {
			return &v, nil
		}
	}
	return nil, nil
}

func GetCurrentAppURL(appID string) (*common.AppUrlHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongCol := mongoSession.DB(allAppDBName).C(allAppURLCollection)
	var au common.AppURL
	err := mongCol.Find(bson.M{"id": appID}).One(&au)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range au.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func GetTaskList(appID string, taskType int) ([]common.Task, error) {
	var collectionName string
	switch taskType {
	case common.TASK_TYPE_NEWBIE:
		collectionName = appDetailTaskNewbieCollection
	case common.TASK_TYPE_DAILY:
		collectionName = appDetailTaskDailyCollection
	default:
		return nil, errors.New("invalid task type")
	}
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(collectionName)
	var tasks []common.Task
	err := mongoCol.Find(bson.M{"valid": true}).All(&tasks)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, nil
		}
		return nil, nil
	}
	return tasks, nil
}

func GetTaskByID(appID string, taskType int, taskID string) (*common.Task, error) {
	var collectionName string
	switch taskType {
	case common.TASK_TYPE_NEWBIE:
		collectionName = appDetailTaskNewbieCollection
	case common.TASK_TYPE_DAILY:
		collectionName = appDetailTaskDailyCollection
	default:
		return nil, errors.New("invalid task type")
	}
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(collectionName)
	var task common.Task
	err := mongoCol.Find(bson.M{"id": taskID}).One(&task)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, nil
		}
		return nil, nil
	}
	return &task, nil
}

func GetCurrentAppThirdPartConf(appID string) (*common.AppThirdPart, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppThirdpartConfCollection)
	var atc common.AppThirdpartConf
	err := mongoCol.Find(bson.M{"id": appID}).One(&atc)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range atc.ThirdPart {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func GetCurrentAppADConf(appID string) (*common.AppAdConfHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppCdConfCollection)
	var adc common.AppAdConf
	err := mongoCol.Find(bson.M{"id": appID}).One(&adc)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, v := range adc.History {
		if v.IsCurrent {
			return &v, nil
		}
	}
	return nil, errors.New("no current ad conf")
}

// 获取设备与ip限制设置
func GetAppLimitCurrentConf(appID string) (*common.AppLimitHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppLimitConfCollection)
	var al common.AppLimit
	err := mongoCol.Find(bson.M{"id": appID}).One(&al)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range al.History {
		if h.IsCurrent {
			return &h, nil
		}
	}
	return nil, nil
}

// 获取设备登录情况
func GetUserDevice(appID, androidID string, insertIfNotExist bool, userID string) (*common.AppDevice, error) {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserDeviceCollection)
	var ad common.AppDevice
	err := mongoCol.Find(bson.M{"android_id": androidID}).One(&ad)
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		if insertIfNotExist {
			return nil, mongoCol.Insert(
				common.AppDevice{
					AndroidID: androidID,
					DeviceHistory: []common.AppDeviceHistory{
						{
							UserID:    userID,
							Timestamp: time.Now().Unix(),
						},
					},
				})
		}
		return nil, nil
	}
	return &ad, nil
}

// 存储用户登录情况
func SaveUserDevice(appID, androidID, userID string, timestamp int64) error {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserDeviceCollection)
	err := mongoCol.Update(bson.M{"android_id": androidID}, bson.M{"$addToSet": bson.M{"history": common.AppDeviceHistory{
		UserID:    userID,
		Timestamp: timestamp,
	}}})
	mongoSession.Close()
	return err
}

// 获取ip登录情况
func GetUserIP(appID, ip string, insertIfNotExist bool, userID string) (*common.AppIP, error) {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIPCollection)
	var ai common.AppIP
	err := mongoCol.Find(bson.M{"ip": ip}).One(&ai)
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		if insertIfNotExist {
			return nil, mongoCol.Insert(
				common.AppIP{
					IP: ip,
					DeviceHistory: []common.AppDeviceHistory{
						{
							UserID:    userID,
							Timestamp: time.Now().Unix(),
						},
					},
				})
		}
		return nil, nil
	}
	return &ai, nil
}

// 存储用户ip情况
func SaveUserIP(appID, ip, userID string, timestamp int64) error {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIPCollection)
	err := mongoCol.Update(bson.M{"ip": ip}, bson.M{"$addToSet": bson.M{"history": common.AppDeviceHistory{
		UserID:    userID,
		Timestamp: timestamp,
	}}})
	mongoSession.Close()
	return err
}

// 存储用户闲玩订单
func SaveXianwanOrder(appID, userID, code string, uxrh common.UserXianwanRewardHistory) error {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserXianwanCollection)
	if err := mongoCol.Update(bson.M{"id": userID}, bson.M{"$addToSet": bson.M{"history": uxrh}}); err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return mongoCol.Insert(common.UserXianwanReward{
			ID:      userID,
			Code:    code,
			History: []common.UserXianwanRewardHistory{uxrh},
		})
	}
	return nil
}

func GetAppHistory(appID string) (*common.AppVersion, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppDBAppHistoryCollection)
	var av common.AppVersion
	err := mongoCol.Find(bson.M{"id": appID}).One(&av)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return &av, nil
}

// 获取用户连续签到情况
func GetUserSignIn(appID, userID string) ([]common.UserSignInHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserSignInCollection)
	var usi common.UserSignIn
	err := mongoCol.Find(bson.M{"id": userID}).One(&usi)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return usi.History, nil
}

// 获取用户连续签到配置
func GetAppSignInConf(appID string) (*common.AppSignInConfHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(allAppDBName).C(allAppSignInConfCollection)
	var aic common.AppSignInConf
	err := mongoCol.Find(bson.M{"id": appID}).One(&aic)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}

	for _, h := range aic.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func SaveSignIn(appID, userID string, t time.Time, reward int, isB bool) error {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserSignInCollection)
	var usi common.UserSignIn
	usih := common.UserSignInHistory{
		Date:      t.Local().Format("2006-01-02"),
		IsB:       isB,
		Reward:    reward,
		Timestamp: t.Unix(),
	}

	if err := mongoCol.Find(bson.M{"id": userID}).One(&usi); err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return mongoCol.Insert(common.UserSignIn{
			ID: userID,
			History: []common.UserSignInHistory{
				usih,
			},
		})
	}
	return mongoCol.Update(bson.M{"id": userID}, bson.M{"$addToSet": bson.M{"history": usih}})
}

func SaveArticleShareClick(appID, userID, id, url, ip, ua, referrer string, timestamp int64, insert bool) error {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserArticleShareCollection)
	uasc := common.UserArticleShareClick{
		Timestamp: timestamp,
		IP:        ip,
		UA:        ua,
		Referrer:  referrer,
	}
	if insert {
		return mongoCol.Insert(common.UserArticelShare{
			ID:     id,
			UserID: userID,
			URL:    url,
			Clicks: []common.UserArticleShareClick{
				uasc,
			},
		})
	}
	return mongoCol.Update(bson.M{"id": id}, bson.M{
		"$addToSet": bson.M{"clicks": uasc},
	})
}

func GetCurrentAppRewardLimit(appID string) (*common.AppRewardLimitHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(allAppDBName).C(allAppRewardLimitCollection)
	var arl common.AppRewardLimit
	err := c.Find(bson.M{"id": appID}).One(&arl)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range arl.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func GetCurrentWitdrawAutocCheckHistory(appID string) (*common.AppWithdrawAutoCheckHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(allAppDBName).C(allAppWithdrawAutoCheckCollection)
	var awac common.AppWithdrawAutoCheck
	err := c.Find(bson.M{"id": appID}).One(&awac)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range awac.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func GetCurrentAdClickConf(appID string) (*common.AppADClickHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(allAppDBName).C(allAppADClickConfCollection)
	var ac *common.AppADClick
	err := c.Find(bson.M{"id": appID}).One(&ac)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range ac.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}

	return nil, nil
}

func GetCurrentVedioCoinConf(appID string) (*common.VideoCoinHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(allAppDBName).C(allAppVedioCoinConfCollection)
	var avc *common.AppVideoCoin
	err := c.Find(bson.M{"id": appID}).One(&avc)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range avc.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}

	return nil, nil
}

func SaveYoumengPush(appID string, ypts common.YoumengPushToSave) error {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserPushMessageCollection)
	err := c.Insert(ypts)
	mongoSession.Close()
	return err
}

func GetUserPushMessage(appID string, userID string) ([]common.YoumengPushToSave, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserPushMessageCollection)
	var yptss []common.YoumengPushToSave
	err := c.Find(nil).All(&yptss)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	ui, err := GetUserInfo(appID, bson.M{"id": userID})
	if err != nil {
		return nil, err
	}
	if ui == nil {
		return nil, nil
	}
	code := ui.Code
	var t []common.YoumengPushToSave
	for _, ypts := range yptss {
		for _, sendTo := range ypts.To {
			if sendTo == "all" || sendTo == code {
				t = append(t, ypts)
			}
		}
	}
	return t, nil
}

func GetPushArticleByID(appID, newsID string) (*common.YoumengPushToSave, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserPushMessageCollection)
	var ypts common.YoumengPushToSave
	err := c.Find(bson.M{"article.article_id": newsID}).One(&ypts)
	mongoSession.Close()
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil, nil
		}
		return nil, err
	}
	return &ypts, nil
}

func GetCurrentUserWithdrawLevelHistory(appID string) (*common.UserWithdrawLevelHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(allAppDBName).C(allAppUserWithdrawLevelCollection)
	var uwl common.UserWithdrawLevel
	err := c.Find(bson.M{"id": appID}).One(&uwl)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range uwl.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func GetCurrentUserWithdrawStandHistory(appID string) (*common.UserWithdrawStandHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(allAppDBName).C(allAppUserWithdrawStandCollection)
	var uwl common.UserWithdrawStand
	err := c.Find(bson.M{"id": appID}).One(&uwl)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range uwl.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func GetCurrentRewardNews(appID string) (*common.RewardNewsHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(allAppDBName).C(allAppRewardNewsCollection)
	var rn common.RewardNews
	err := c.Find(bson.M{"id": appID}).One(&rn)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range rn.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func GetCurrentTimingReward(appID string) (*common.TimingRewardHistory, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(allAppDBName).C(allAppTimingRewardCollection)
	var tr common.TimingReward
	err := c.Find(bson.M{"id": appID}).One(&tr)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	for _, h := range tr.History {
		if !h.IsCurrent {
			continue
		}
		return &h, nil
	}
	return nil, nil
}

func GetCurrentUncompleteUserOrder(appID string) ([]common.UserOrder, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserOrderCollection)
	var uos []common.UserOrder
	err := c.Find(bson.M{"history.status": common.ORDER_STATUS_TO_BE_CHECKED}).All(&uos)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return uos, nil
}

func SaveDeviceChannel(appID, deviceID, channel string, timestamp int64) error {
	if appID == "" || deviceID == "" || channel == "" {
		return nil
	}
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	dateStartUnix := common.GetDateStartUnix(timestamp)
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailDeviceChannelCollection)
	var ac common.AppChannel
	if err := c.Find(bson.M{"id": deviceID}).Sort("-timestamp").One(&ac); err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return c.Insert(common.AppChannel{
			ID:          deviceID,
			Channel:     channel,
			Timestamp:   dateStartUnix,
			FirstActive: true,
		})
	}
	if ac.Timestamp == dateStartUnix {
		return nil
	}
	if err := c.Insert(common.AppChannel{
		ID:          deviceID,
		Channel:     ac.Channel,
		Timestamp:   dateStartUnix,
		FirstActive: false,
	}); err != nil {
		if !mgo.IsDup(err) {
			return err
		}
		return nil
	}
	return nil
}

func SaveUserChannel(appID, userID, channel string, timestamp int64) error {
	if appID == "" || userID == "" || channel == "" {
		return nil
	}
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	dateStartUnix := common.GetDateStartUnix(timestamp)
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserChannelCollection)
	var ac common.AppChannel
	if err := c.Find(bson.M{"id": userID}).Sort("-timestamp").One(&ac); err != nil {
		if err != mgo.ErrNotFound {
			return err
		}
		return c.Insert(common.AppChannel{
			ID:          userID,
			Channel:     channel,
			Timestamp:   dateStartUnix,
			FirstActive: true,
		})
	}
	if ac.Timestamp == dateStartUnix {
		return nil
	}
	if err := c.Insert(common.AppChannel{
		ID:          userID,
		Channel:     ac.Channel,
		Timestamp:   dateStartUnix,
		FirstActive: false,
	}); err != nil {
		if !mgo.IsDup(err) {
			return err
		}
		return nil
	}
	return nil
}

func GetApprenticeIncomeByID(appID, userID string) ([]common.AggregationTotalWithID, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIncomeHistoryCollection)
	pips := []bson.M{
		bson.M{"$match": bson.M{"id": userID, "type": common.INCOME_TYPE_INCOME, "task_type": common.TASK_TYPE_APPRENTICE}},
		bson.M{"$group": bson.M{"_id": "$from", "total": bson.M{"$sum": "$count"}}},
	}
	var aias []common.AggregationTotalWithID
	err := c.Pipe(pips).All(&aias)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return aias, nil
}

// 获取用户师徒宝箱
func GetApprenticeRewardByID(appID, userID string) ([]common.UserApprenticeReward, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	var uars []common.UserApprenticeReward
	err := c.Find(bson.M{"id": userID}).All(&uars)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return uars, nil
}

// 获取最后师徒宝箱数据
func GetRecentApprenticeReward(appID, sortBy string) ([]common.UserApprenticeReward, error) {
	mongoSession := mongoSessionPool.Clone()
	defer mongoSession.Close()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	var mgoQuery *mgo.Query
	switch sortBy {
	case "0":
		mgoQuery = c.Find(bson.M{"status": common.STATUS_USER_APPRENTICE_REWARD_OPENED}).Sort("-open_timestamp").Limit(10)
	case "1":
		mgoQuery = c.Find(bson.M{"status": common.STATUS_USER_APPRENTICE_REWARD_OPENED}).Sort("-reward", "-open_timestamp").Limit(10)
	default:
		return nil, errors.New("unexpected sory by: " + sortBy)
	}
	var uars []common.UserApprenticeReward
	err := mgoQuery.All(&uars)
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return uars, nil
}

// 获取师徒宝箱排行榜
func GetUserApprenticeRewardByTotal(appID string) ([]common.AggregationTotalAndCountWithID, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	Pips := []bson.M{
		bson.M{"$match": bson.M{"status": common.STATUS_USER_APPRENTICE_REWARD_OPENED}},
		bson.M{"$group": bson.M{"_id": "$id", "total": bson.M{"$sum": "$reward"}, "count": bson.M{"$sum": 1}}},
		bson.M{"$sort": bson.M{"total": -1}},
		bson.M{"$limit": 10},
	}

	var atws []common.AggregationTotalAndCountWithID
	err := c.Pipe(Pips).All(&atws)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return atws, nil
}

// 发放师徒宝箱
func SetUserApprenticeReward(appID, userID, apprenticeID string, timestamp int64) error {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	uar := common.UserApprenticeReward{
		ID:             userID,
		ApprenticeID:   apprenticeID,
		Timestamp:      timestamp,
		Status:         common.STATUS_USER_APPRENTICE_REWARD_INVALID,
		ValidTimestamp: 0,
		OpenTimestamp:  0,
		Reward:         0,
	}
	err := c.Insert(uar)
	mongoSession.Close()
	return err
}

// 使师徒宝箱生效
func ValidUserApprenticeReward(appID, userID, apprenticeID string, timestamp int64) error {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	err := c.Update(
		bson.M{
			"id":              userID,
			"apprentice_id":   apprenticeID,
			"status":          common.STATUS_USER_APPRENTICE_REWARD_INVALID,
			"valid_timestamp": 0,
			"open_timestamp":  0,
			"reward":          0,
		},
		bson.M{"$set": bson.M{"status": common.STATUS_USER_APPRENTICE_REWARD_VALID, "valid_timestamp": timestamp}},
	)
	mongoSession.Close()
	return err
}

// 开启师徒宝箱
func OpenUserApprenticeReward(appID, userID, apprenticeID string, reward int, timestamp int64) error {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	err := c.Update(
		bson.M{
			"id":             userID,
			"apprentice_id":  apprenticeID,
			"status":         common.STATUS_USER_APPRENTICE_REWARD_VALID,
			"open_timestamp": 0,
			"reward":         0,
		},
		bson.M{"$set": bson.M{"status": common.STATUS_USER_APPRENTICE_REWARD_OPENED, "open_timestamp": timestamp, "reward": reward}},
	)
	mongoSession.Close()
	return err
}

// 查询师徒宝箱状态
func GetUserApprenticeRewardInfo(appID, userID, apprenticeID string) (*common.UserApprenticeReward, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	var uar common.UserApprenticeReward
	err := c.Find(bson.M{"id": userID, "apprentice_id": apprenticeID}).One(&uar)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return &uar, nil
}

// 查询相应状态的宝箱数量
func GetUserApprenticeRewardCountByStatus(appID, userID string, status int) (int, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	count, err := c.Find(bson.M{"id": userID, "status": status}).Count()
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return count, nil
}

// 查询用户所有宝箱总数
func GetUserApprenticeRewardCount(appID, userID string) (int, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	count, err := c.Find(bson.M{"id": userID}).Count()
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return count, nil
}

// 获取用户宝箱详情
func GetUserApprenticeRewardDetail(appID, userID string, status []int, sortBy []string, skip, limit int) ([]common.UserApprenticeReward, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserAPprenticeRewardCollection)
	cond := make(bson.M)
	if userID != "" {
		cond["id"] = userID
	}
	if status != nil {
		cond["status"] = bson.M{"$in": status}
	}
	query := c.Find(cond)
	if sortBy != nil {
		query = query.Sort(sortBy...)
	}
	if skip != 0 {
		query = query.Skip(skip)
	}
	if limit != 0 {
		query = query.Limit(limit)
	}

	var uars []common.UserApprenticeReward
	err := query.All(&uars)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return uars, nil
}

// 获取用户提现数
func GetUserSuccessWithdrawTotal(appID, userID string, startTimestamp, endTimestamp int64, onlySuccess bool) (int, error) {
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserOrderCollection)
	pips := []bson.M{
		bson.M{"$match": bson.M{"id": userID}},
		bson.M{"$unwind": "$history"},
	}
	cond := make(bson.M)
	timestampCond := make(bson.M)
	if startTimestamp != common.NO_LIMIT {
		timestampCond["$gte"] = startTimestamp
	}
	if endTimestamp != common.NO_LIMIT {
		timestampCond["$lt"] = endTimestamp
	}
	if len(timestampCond) != 0 {
		cond["history.timestamp"] = timestampCond
	}

	cond["history.order_type"] = bson.M{"$in": []int{common.ORDER_TYPE_ALIPAY_WITHDRAWA, common.ORDER_TYPE_WECHAT_WITHDRAWA}}
	if onlySuccess {
		cond["history.success"] = true
	}

	pips = append(pips,
		bson.M{"$match": cond},
		bson.M{"$group": bson.M{"_id": nil, "total": bson.M{"$sum": "$history.amount"}}},
	)
	var t common.AggregationTotal
	err := mongoCol.Pipe(pips).One(&t)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return t.Total * -1, nil
}

func SaveADInfo(appID string, ais []common.ADInfo) error {
	aiLen := len(ais)
	if aiLen == 0 {
		return nil
	}
	dateString := time.Unix(ais[0].Timestamp, 0).Format("2006-01-02")
	tmps := make([]interface{}, aiLen)
	for i := range ais {
		tmps[i] = ais[i]
	}
	dbName := fmt.Sprintf(adDetailDBNameFormat, appID)
	colName := fmt.Sprintf(adDetailADInfoCollectionFormat, dateString)
	mongoSession := mongoSessionPool.Clone()
	mongoCol := mongoSession.DB(dbName).C(colName)
	err := mongoCol.Insert(tmps...)
	mongoSession.Close()
	return err

}

//  获取用户阅读总收入
func GetUserReadTotal(appID, userID string) (int, error) {
	mongoSession := mongoSessionPool.Clone()
	c := mongoSession.DB(appDetailDBNamePrefix + appID).C(appDetailUserIncomeByTypeCollection)
	var uibt common.UserIncomeByType
	err := c.Find(bson.M{"id": userID, "task_type": common.TASK_TYPE_FIXED_BOUNDS}).One(&uibt)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return 0, err
		}
		return 0, nil
	}
	return uibt.Total, nil
}

// 获取赚赚看每日一次任务
func GetSpecialTaskDailyOnce(appID string) ([]common.DailyOnceTask, error) {
	mongoSession := mongoSessionPool.Clone()
	dbName := fmt.Sprintf(appSpecialTaskDBNameFormat, appID)
	var dots []common.DailyOnceTask
	err := mongoSession.DB(dbName).C(appSpecialTaskDailyOnceCollection).Find(bson.M{"valid": true}).All(&dots)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return dots, nil
}

// 获取赚赚看每日多次任务
func GetSpecialTaskDailyManyTimes(appID string) ([]common.DailyManyTimesTask, error) {
	mongoSession := mongoSessionPool.Clone()
	dbName := fmt.Sprintf(appSpecialTaskDBNameFormat, appID)
	var dots []common.DailyManyTimesTask
	err := mongoSession.DB(dbName).C(appSpecialTaskDailyManyTimesCollection).Find(bson.M{"valid": true}).All(&dots)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return dots, nil
}

// 获取赚赚看页面停留任务
func GetSpecialTaskDailyStay(appID string) ([]common.StayPageTask, error) {
	mongoSession := mongoSessionPool.Clone()
	dbName := fmt.Sprintf(appSpecialTaskDBNameFormat, appID)
	var dots []common.StayPageTask
	err := mongoSession.DB(dbName).C(appSpecialStayTaskCollection).Find(bson.M{"valid": true}).All(&dots)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return dots, nil
}

func CompleteSpecialTask(appID, userID, taskID string, taskType int, timestamp int64) error {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	sptc := common.SpecialTaskComplete{
		UserID:        userID,
		TaskID:        taskID,
		TaskType:      taskType,
		DateTimestamp: dateTimestamp,
		FTimestamp:    timestamp,
		Tag:           "",
	}
	if taskType == common.SPECIAL_TASK_TYPE_MULTY {
		sptc.Tag = common.NewUUID()
	}

	mongoSession := mongoSessionPool.Clone()
	dbName := fmt.Sprintf(appSpecialTaskDBNameFormat, appID)
	c := mongoSession.DB(dbName).C(appSpecialTaskCompleteCollection)
	err := c.Insert(sptc)
	mongoSession.Close()
	return err
}

// 获取用户看看赚任务 - 停留任务
func GetUserSpecialTaskComplete(appID, userID, taskID string, taskType int, timestamp int64) ([]common.SpecialTaskComplete, error) {
	dateTimestamp := common.GetDateStartUnix(timestamp)
	mongoSession := mongoSessionPool.Clone()
	dbName := fmt.Sprintf(appSpecialTaskDBNameFormat, appID)
	var stcs []common.SpecialTaskComplete
	err := mongoSession.DB(dbName).C(appSpecialTaskCompleteCollection).Find(bson.M{"user_id": userID, "task_id": taskID, "task_type": taskType, "date_timestamp": dateTimestamp}).All(&stcs)
	mongoSession.Close()
	if err != nil {
		if err != mgo.ErrNotFound {
			return nil, err
		}
		return nil, nil
	}
	return stcs, nil
}
