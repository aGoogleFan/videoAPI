package common

import (
	"net/http"
	"net/url"
)

type LogAndUserSetHandler interface {
	New() LogAndUserSetHandler
	GetAPICode() int
	SetLog(APIRecord)
	GetUser(url.Values) (*UserBasic, *Resp, error)
	SetUser(UserBasic)
	ServeHTTP(http.ResponseWriter, *http.Request)
}

type LogSetHandler interface {
	New() LogSetHandler
	GetAPICode() int
	SetLog(APIRecord)
	ServeHTTP(http.ResponseWriter, *http.Request)
}
