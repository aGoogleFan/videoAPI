package common

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func StringToMD5(s string) string {
	h := md5.New()
	h.Write([]byte(s)) // 需要加密的字符串
	cipherStr := h.Sum(nil)
	return hex.EncodeToString(cipherStr)
}

func HmacSha1AndBase64(key string, toBeCrypto string) string {
	hasher := hmac.New(sha1.New, []byte(key))
	hasher.Write([]byte(toBeCrypto))
	seg := hasher.Sum(nil)
	return base64.StdEncoding.EncodeToString(seg)
}

func HmacSha256(s, key string) string {
	h := hmac.New(sha256.New, []byte(key))
	io.WriteString(h, s)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func StringToSha1(s string) string {
	h := sha1.New()
	h.Write([]byte(s))
	return fmt.Sprintf("%x", h.Sum(nil))
}

func VisitURL(targetURL string, timeout time.Duration) ([]byte, int, error) {
	client := http.Client{}
	client.Timeout = timeout
	resp, err := client.Get(targetURL)
	if err != nil {
		return nil, 0, err
	}

	ct, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, resp.StatusCode, err
	}

	return ct, resp.StatusCode, nil
}

// 获取用户ip
func GetIPFromRequest(r *http.Request) string {
	if headerXForawrdedIP := r.Header.Get("X-Forwarded-For"); headerXForawrdedIP != "" {
		ip, err := getValidIP(strings.Split(headerXForawrdedIP, ",")[0])
		if err == nil {
			return ip
		}
	}
	if headerRealIP := r.Header.Get("X-Real-IP"); headerRealIP != "" {
		ip, err := getValidIP(headerRealIP)
		if err == nil {
			return ip
		}
	}
	oip := r.RemoteAddr
	ip, err := getValidIP(oip)
	if err == nil {
		return ip
	}
	return oip
}

func getValidIP(oip string) (string, error) {
	ip := net.ParseIP(oip)
	if ip != nil {
		return ip.String(), nil
	}
	ipString, _, err := net.SplitHostPort(oip)
	return ipString, err
}

// 获取当天0点时间戳
func GetDateStartUnix(timestamp int64) int64 {
	t := time.Unix(timestamp, 0).Local()
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location()).Unix()
}

// 获取第二天0点时间戳
func GetDateEndUnix(timestamp int64) int64 {
	t := time.Unix(timestamp, 0).Local()
	return time.Date(t.Year(), t.Month(), t.Day()+1, 0, 0, 0, 0, t.Location()).Unix()
}

func OrderInNumber(i int, is []int) int {
	var count int
	for _, v := range is {
		if i < v {
			count++
		}
	}
	return count + 1
}

// 获取随机数 返回 start <= x <end
func GenRadomNumber(start, end int) int {
	if end < start || start < 0 {
		return 0
	}
	if end == start {
		return start
	}
	interval := end - start
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return start + r.Intn(interval)
}

func GetValueByWeight(weights []int, value []int) int {
	var totalWeight int
	for _, weight := range weights {
		if weight <= 0 {
			continue
		}
		totalWeight += weight
	}
	if totalWeight == 0 {
		return 0
	}
	r := GenRadomNumber(0, totalWeight)
	var startWeight int
	for i, weight := range weights {
		if weight <= 0 {
			continue
		}
		if r >= startWeight && r < startWeight+weight {
			return value[i]
		}
		startWeight += weight
	}
	return 0
}

// if the camparedVersion is newer than currentVersion
// if newer return true, else(older or equel) return false
func IsNewVersion(currentVersion, camparedVersion string, isBVersion bool) bool {
	currentVersions := strings.Split(currentVersion, ".")
	camparedVersions := strings.Split(camparedVersion, ".")
	currentLen := len(currentVersions)
	lengthToCampare := currentLen
	camparedLen := len(camparedVersions)
	if camparedLen < lengthToCampare {
		lengthToCampare = camparedLen
	}
	for i := 0; i < camparedLen; i++ {
		currentV, err := strconv.Atoi(currentVersions[i])
		if err != nil {
			return false
		}
		camparedV, err := strconv.Atoi(camparedVersions[i])
		if err != nil {
			return false
		}
		if currentV > camparedV {
			return false
		}
		if currentV < camparedV {
			return true
		}
	}
	if currentLen < camparedLen {
		return true
	}
	if isBVersion {
		if currentLen == camparedLen {
			return true
		}
	}
	return false
}
