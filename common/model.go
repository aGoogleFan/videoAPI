package common

import (
	"encoding/json"
	"encoding/xml"
	"time"
)

type UserVideos struct {
	ID     string      `bson:"id" json:"-"`          // 用户唯一id
	Videos []UserVideo `bson:"videos" json:"videos"` // 用户上传视频列表
}

type UserVideo struct {
	Timestamp int64  `bson:"timestamp" json:"timestamp"` // 存储时间戳
	Name      string `bson:"name"      json:"-"`         // 视频名
	Space     string `bson:"space"     json:"-"`         // ks3中 对应bucket
	Key       string `bson:"key"       json:"-"`         // ks3中 对应object_key
	Cloud     string `bson:"cloud"     json:"-"`         // 存储供应商, 比如 ks3
	URL       string `bson:"-"         json:"url"`       // 视频播放地址
}

type RadomAppImage struct {
	ID     string       `bson:"id"     json:"id"`
	Images []RadomImage `bson:"images" json:"images"`
}

type RadomImage struct {
	URL string `bson:"url" json:"url"`
}

type WechatAccessToken struct {
	AccessToken  string `json:"access_token"`
	ExpireIn     int    `json:"expire_in"`
	RefreshToken string `json:"refresh_token"`
	OpenID       string `json:"openid"`
	Scope        string `json:"scope"`
	ErrCode      int    `json:"errcode"`
	ErrMsg       string `json:"errmsg"`
}

type WechatUserInfo struct {
	OpenID     string `json:"openid"`
	NickName   string `json:"nickname"`
	Sex        int    `json:"sex"` // 普通用户性别，1为男性，2为女性
	HeadImgUrl string `bson:"headimgurl"`
	UnionID    string `bson:"unionid"`
	ErrCode    int    `json:"errcode"`
	ErrMsg     string `json:"errmsg"`
}

// 微信提现返回值
type WechatTransResp struct {
	XMLName    xml.Name `xml:"xml"`
	ReturnCode string   `xml:"return_code"` // SUCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
	ReturnMsg  string   `xml"return_msg"`   // 返回信息，如非空，为错误原因 签名失败 参数格式校验错误

	// 以下字段在return_code为SUCCESS的时候有返回
	MchAppID   string `xml:"mch_appid"`    // 申请商户号的appid或商户号绑定的appid（企业号corpid即为此appId）
	MchID      string `xml:"mchid"`        // 微信支付分配的商户号
	DeviceInfo string `xml:"device_info"`  // 微信支付分配的终端设备号
	NonceStr   string `xml:"nonce_str"`    // 随机字符串，不长于32位
	ResultCode string `xml:"result_code"`  // SUCCESS/FAIL
	ErrCode    string `xml:"err_code"`     // 错误码信息
	ErrCodeDes string `xml:"err_code_des"` // 结果信息描述

	// 以下字段在return_code 和result_code都为SUCCESS的时候有返回
	PartnerTradeNo string `xml:"partner_trade_no"` // 商户订单号，需保持唯一性 (只能是字母或者数字，不能包含有符号)
	PaymentNo      string `xml:"payment_no"`       // 企业付款成功，返回的微信订单号
	PaymentTime    string `xml:"payment_time"`     // 企业付款成功时间
}

// 以下对应支付宝提现的返回值
type AlipayTransResponse struct {
	AFTTTR AlipayFundTransToaccountTransferResponse `json:"alipay_fund_trans_toaccount_transfer_response"`
	Sign   string                                   `json:"sign"`
}

type AlipayFundTransToaccountTransferResponse struct {
	Code     string `json:"code"`
	Msg      string `json:"msg"`
	SubCode  string `json:"sub_code"`
	SubMsg   string `json:"sub_msg"`
	OutBizNo string `json:"out_biz_no"`
}

// 某些接口通用
type PageContiner struct {
	List       interface{} `json:"list"`
	TotalCount int         `json:"total_count"`
	Page       int         `json:"page"`
	MaxPerPage int         `json:"max_per_page"`
}

// 闲玩游戏
type Xianwan struct {
	ADID        int       // 广告id
	ADName      string    // 广告名称
	AppID       string    // 开发者id
	OrderNumber string    // 订单号
	DLevel      int       // 级别奖励
	PageName    string    // 用户体验游戏的包名
	DeviceID    string    // 手机设备号 imei 或 idfa
	SimID       string    // 手机sim卡id
	AppSign     string    // 开发者用户编号
	MerID       string    // 用户体验游戏注册的账号id
	Event       string    // 奖励说明—在开发者自己的APP中需显示给用户看，以便用户了解自己获得的奖励
	Price       float64   // 于开发者结算单价、保留2位小数 【人民币单位】
	Money       float64   // 开发者需奖励给用户金额 【人民币单位】
	ITime       time.Time // 用户获得奖励时间
	KeyCode     string    // 订单校验参数 加密规则MD5(adid+appid+ordernum+deviceid+key); + 为连接符不做加密 [MD5加密结果需转大写]
}

type XianwanToReturn struct {
	Success int    `json:"success"`
	Message string `json:"message"`
}

func (x XianwanToReturn) String() string {
	ct, _ := json.Marshal(x)
	return string(ct)
}

// 友盟推送 http://dev.umeng.com/push/android/api-doc#2
type YoumengPush struct {
	// 必填，应用唯一标识
	AppKey string `json:"appkey"`

	// 必填，时间戳，10位或者13位均可，时间戳有效期为10分钟
	Timestamp string `json:"timestamp"`

	// 必填，消息发送类型,其值可以为:
	//   unicast-单播
	//   listcast-列播，要求不超过500个device_token
	//   filecast-文件播，多个device_token可通过文件形式批量发送
	//   broadcast-广播
	//   groupcast-组播，按照filter筛选用户群, 请参照filter参数
	//   customizedcast，通过alias进行推送，包括以下两种case:
	//     - alias: 对单个或者多个alias进行推送
	//     - file_id: 将alias存放到文件后，根据file_id来推送
	Type string `json:"type"`

	// 当type=unicast时, 必填, 表示指定的单个设备
	// 当type=listcast时, 必填, 要求不超过500个, 以英文逗号分隔
	DeviceTokens string `json:"device_tokens,omitempty"`

	// 当type=customizedcast时, 必填
	// alias的类型, alias_type可由开发者自定义, 开发者在SDK中
	// 调用setAlias(alias, alias_type)时所设置的alias_type
	AliasType string `json:"alias_type,omitempty"`

	// 当type=customizedcast时, 选填(此参数和file_id二选一)
	// 开发者填写自己的alias, 要求不超过500个alias, 多个alias以英文逗号间隔
	// 在SDK中调用setAlias(alias, alias_type)时所设置的alias
	Alias string `json:"alias,omitempty"`

	// 当type=filecast时，必填，file内容为多条device_token，以回车符分割
	// 当type=customizedcast时，选填(此参数和alias二选一)
	// file内容为多条alias，以回车符分隔。注意同一个文件内的alias所对应
	// 的alias_type必须和接口参数alias_type一致。
	// 使用文件播需要先调用文件上传接口获取file_id，参照"2.4文件上传接口"
	FileID string `json:"file_id,omitempty"`

	// 当type=groupcast时，必填，用户筛选条件，如用户标签、渠道等，参考附录G。
	Filter interface{} `json:"filter,omitempty"`

	// 必填，JSON格式，具体消息内容(Android最大为1840B)
	PayLoad YoumengPushPayLoad `json:"payload"`

	// 可选，发送策略
	Policy YoumengPushPolicy `json:"policy,omitempty"`

	// 可选，正式/测试模式。默认为true
	// 测试模式只会将消息发给测试设备。测试设备需要到web上添加。
	// Android: 测试设备属于正式设备的一个子集。
	ProductionMode string `json:"production_mode"`

	// 可选，发送消息描述，建议填写。
	Description string `json:"description,omitempty"`

	// 可选，默认为false。当为true时，表示MIUI、EMUI、Flyme系统设备离线转为系统下发
	MiPush string `json:"mipush,omitempty"`

	// 可选，mipush值为true时生效，表示走系统通道时打开指定页面acitivity的完整包路径。
	MiActivity string `json:"mi_activity,omitempty"`
}

type YoumengPushPayLoad struct {
	// 必填，消息类型: notification(通知)、message(消息)
	DisplayType string `json:"display_type"`

	// 必填，消息体。
	// 当display_type=message时，body的内容只需填写custom字段。
	// 当display_type=notification时，body包含如下参数:
	Body YoumengPushPayLoadBody `json:"body"`

	// 可选，JSON格式，用户自定义key-value。只对"通知"
	// (display_type=notification)生效。
	// 可以配合通知到达后，打开App/URL/Activity使用。
	Extra map[string]string `json:"extra"`
}

type YoumengPushPayLoadBody struct {
	// 必填，通知栏提示文字
	Ticker string `json:"ticker"`

	// 必填，通知标题
	Title string `json:"title"`

	// 必填，通知文字描述
	Text string `json:"text"`

	// 点击"通知"的后续行为，默认为打开app。
	// 必填，值可以为:
	//   "go_app": 打开应用
	//   "go_url": 跳转到URL
	//   "go_activity": 打开特定的activity
	//   "go_custom": 用户自定义内容。
	AfterOpen string `json:"after_open"`

	// 当after_open=go_url时，必填。
	// 通知栏点击后跳转的URL，要求以http或者https开头
	URL string `json:"url,omitempty"`

	// 当after_open=go_activity时，必填。
	// 通知栏点击后打开的Activity
	Activity string `json:"activity,omietempty"`

	// 当display_type=message时, 必填
	// 当display_type=notification且
	// after_open=go_custom时，必填
	// 用户自定义内容，可以为字符串或者JSON格式。
	Custom interface{} `json:"custom"`
}

type YoumengPushPolicy struct {
	// 可选，定时发送时，若不填写表示立即发送。
	// 定时发送时间不能小于当前时间
	// 格式: "yyyy-MM-dd HH:mm:ss"。
	// 注意，start_time只对任务生效。
	StartTime string `json:"start_time,omitempty"`

	// 可选，消息过期时间，其值不可小于发送时间或者
	// start_time(如果填写了的话)，
	// 如果不填写此参数，默认为3天后过期。格式同start_time
	ExpireTime string `json:"expire_time,omitempty"`

	// 可选，发送限速，每秒发送的最大条数。最小值1000
	// 开发者发送的消息如果有请求自己服务器的资源，可以考虑此参数。
	MaxSendNum int `json:"max_send_num,omitempty"`

	// 可选，开发者对消息的唯一标识，服务器会根据这个标识避免重复发送。
	// 有些情况下（例如网络异常）开发者可能会重复调用API导致
	// 消息多次下发到客户端。如果需要处理这种情况，可以考虑此参数。
	// 注意, out_biz_no只对任务生效。
	OutBizNo string `json:"out_biz_no,omitempty"`
}

// 友盟推送的返回结果
type YoumengPushRet struct {
	// 返回结果，"SUCCESS"或者"FAIL"
	Ret string `json:"ret"`

	Data YoumengPushRetData `json:"data"`
}

type YoumengPushRetData struct {
	// 当"ret"为"SUCCESS"时，包含如下参数:
	// 单播类消息(type为unicast、listcast、customizedcast且不带file_id)返回:
	MessageID string `json:"msg_id"`
	// 任务类消息(type为broadcast、groupcast、filecast、customizedcast且file_id不为空)返回
	TaskID string `json:"task_id"`

	// 当"ret"为"FAIL"时,包含如下参数:
	// 错误码，详见附录I
	ErrCode string `json:"error_code"`
	// 错误信息
	ErrMsg string `json:"error_msg"`
}

// 阿里云sms平台返回值
type AliyunSMSRet struct {
	// 请求ID
	RequestID string `json:"RequestId"`

	// 状态码-返回OK代表请求成功,其他错误码详见错误码列表
	Code string `json:"Code"`

	// 状态码的描述
	Message string `json:"Message"`

	// 发送回执ID,可根据该ID查询具体的发送状态
	BizID string `json:"BizId"`
}

// 详见 http://wiki.vlion.cn/pages/viewpage.action?pageId=4656488
type APIRecord struct {
	StartTime       time.Time // 用于统计api耗时时专用, 不计入日志. 计入日志的是Timestamp, 单位为秒
	APICode         int
	SignVersion     string
	SignOnce        string
	IsSafe          string
	AppChannel      string
	UserIP          string
	Access          string
	Timestamp       int64
	Ret             string
	Duration        time.Duration
	UserToken       string
	UserID          string
	UserCode        string
	UserMobile      string
	SMSCode         int
	DeviceModel     string
	System          string
	SystemVersion   string
	AndroidID       string
	IMEI            string
	APPID           string
	APPVersion      string
	WechatOpenID    string
	AlipayOpenID    string
	OrderID         string
	WithdrawAccount string
	Reward          int
	Consume         int
	Withdraw        int
	AutoResult      string
	RetCode         int
	ErrorMsg        string
}

// 用户评分
type UserLevel struct {
	Timestamp       int64                    // 时间戳, 记录截止时间(一般为当天0点)
	ReadScores      map[string]UserLevelData // 阅读奖励排行
	MissionScores   map[string]UserLevelData // 任务排行
	Apprentices     map[string]UserLevelData // 师徒上供排行
	ValidApprentice map[string]UserLevelData // 有效徒弟排行
}

type UserLevelData struct {
	Name      string  `json:"name"`
	Value     int     `json:"value"`
	Rank      int     `json:"rank,omitempty"`
	RankTotal int     `json:"rank_total,omitempty"`
	Weight    int     `json:"weight,omitempty"`
	Score     float64 `json:"score"`
}

// 用户评分统计
type UserWithdrawLevelTotal struct {
	// 模块1, 阅读奖励
	Read []UserWithdrawLevelData `json:"read"`

	// 模块2, 徒弟上供
	Apprentice []UserWithdrawLevelData `json:"apprentice"`

	// 模块3, 任务完成度
	TasksComplete []UserWithdrawLevelData `json:"task_complete"`

	// 模块4, 有效徒弟数
	ValidApprenticeNum []UserWithdrawLevelData `json:"valid_apprentice_num"`

	// 隐藏模块, 不展示给用户. 广告点击数
	AdClick []UserWithdrawLevelData `json:"ad_click"`
}

type UserWithdrawLevelData struct {
	UserID string   `json:"user_id"`
	Num    int      `json:"num"`
	Tasks  []string `json:"tasks"`
	Days   int      `json:"days"`

	// 以下字段供临时计算使用
	Avg    float64 `json:"-"`
	Weight int     `json:"-"`
	Score  float64 `json:"-"`
	Total  int     `json:"-"`
	Rank   int     `json:"-"`
}

type Config struct {
	Apps map[string]AppConf `yaml:"apps"`
}

type AppConf struct {
	AutoWithdrawScan AutoWithdrawScan `yaml:"auto_withdraw_scan"`
	Domain           string           `yaml:"domain"`
}

type AutoWithdrawScan struct {
	IsValid  bool          `yaml:"is_valid"`
	Interval time.Duration `yaml:"interval"`
}

// 最近提现的用户
type AppRecentWithdrawInfo struct {
	UserName  string `json:"user_name"`
	Amount    int    `json:"amount"`
	Timestamp int64  `json:"timestamp"`
}
