package common

import "github.com/globalsign/mgo/bson"

type ReleasedFile struct {
	ID      string            `bson:"id"`
	Channel string            `bson:"channel"`
	History []ReleasedHistory `bson:"history"`
}

type ReleasedHistory struct {
	Timestamp     int64  `bson:"timestamp"`
	Version       string `bson:"version"`
	ReleasedNotes string `bson:"released_notes"`
	DownloadURL   string `bson:"download_url"`
}

// 以下对应user_basic
type UserBasic struct {
	ID              string                `bson:"id"`
	Code            string                `bson:"code"`
	From            string                `bson:"from"`
	FromTimestamp   int64                 `bson:"from_timestamp"`
	IncomeRemaining int                   `bson:"income_remaining"`
	IncomeTotal     int                   `bson:"income_total"`
	Create          int64                 `bson:"create"`
	LastLogin       int64                 `bson:"last_login"`
	Info            UserInfo              `bson:"info"`
	ThirdPart       UserThirdPart         `bson:"third_part"`
	Status          int                   `bson:"status"`
	NewbieTasks     []UserBasicNewbieTask `bson:"newbie_task"`
}

type UserBasicNewbieTask struct {
	ID        string `bson:"id"`
	Timestamp int64  `bson:"timestamp"`
}

type UserInfo struct {
	Mobile string `bson:"mobile"`
	Name   string `bson:"name"`
	Icon   string `bson:"icon"`
	Birth  string `bson:"birth"`
	Sex    int    `bson:"sex"`
}

type UserThirdPart struct {
	QQ     TencentAccount `bson:"qq"`
	Wechat TencentAccount `bson:"wechat"`
	Alipay AlipayAccount  `bson:"alipay"`
}

type TencentAccount struct {
	OpenID   string `bson:"openid,omitempty"`
	UnionID  string `bson:"unionid,omitempty"`
	BindTime int64  `bson:"bind_time,omitempty"`
}

type AlipayAccount struct {
	ID       string `bson:"id,omitempty"`
	BindTime int64  `bson:"bind_time,omitempty"`
}

// 以下对应用户订单
type UserOrder struct {
	ID             string        `bson:"id"`
	History        []OrderDetail `bson:"history"`
	WithdrawTotal  int           `bson:"withdraw_total"`
	RecentWithdraw int64         `bson:"recent_withdraw"`
}

type OrderDetail struct {
	Timestamp   int64  `bson:"timestamp"            json:"timestamp"`
	OrderID     string `bson:"order_id"             json:"order_id"`
	OrderType   int    `bson:"order_type"           json:"order_type"`
	Content     string `bson:"content"              json:"content"`
	Cost        int    `bson:"cost"                 json:"cost"`
	Amount      int    `bson:"amount"               json:"amount"`
	Account     string `bson:"account"              json:"-"`
	AccountName string `bson:"account_name"         json:"-"`
	Success     bool   `bson:"success"              json:"success"`
	Desc        string `bson:"desc"                 json:"desc"`
	Status      int    `bson:"status"               json:"status"`
	FTimestamp  int64  `bson:"ftimestamp,omitempty" json:"-"`
	IP          string `bson:"ip,omitempty"         json:"-"`
	PreDeducate bool   `bson:"pre_deducate"         json:"-"`
	AppVersion  string `bson:"app_version"          json:"-"`
}

// 以下对应app汇率发布
type AppExchangeRateHistory struct {
	ID      string                `bson:"id"`
	Channel string                `bson:"channel"`
	History []RateExchangeHistory `bson:"history"`
}

type RateExchangeHistory struct {
	Rates     []Rate `bson:"rates"`
	Timestamp int64  `bson:"timestamp"`
	IsCurrent bool   `bson:"is_current"`
}

type Rate struct {
	Details []RateDetail `bson:"details"`
	Type    string       `bson:"type"`
}

type RateDetail struct {
	Coin   int `bson:"coin"    json:"coin"`
	Amount int `bson:"amount"  json:"amount"`
}

/* 废弃的内容
// 以下对应用户收徒历史
type UserApprenticeHistory struct {
	ID              string                 `bson:"id"`
	AmountTotal     int                    `bson:"amount_total"`
	ApprenticeCount int                    `bson:"apprentice_count"`
	List            []UserApprenticeDetail `bson:"list"`
}

type UserApprenticeDetail struct {
	Timestamp    int64  `bson:"timestamp"`
	ApprenticeID string `bson:"apprentice_id"`
	Source       int    `bson:"source"`
	Amount       int    `bson:"amount"`
}
*/

type UserApprentice struct {
	ID           string `bson:"id"`
	Timestamp    int64  `bson:"timestamp"`
	ApprenticeID string `bson:"apprentice_id"`
	Source       int    `bson:"source"`
	Amount       int    `bson:"amount"`
}

// 以下对应广告设置
type AppAds struct {
	ID      string          `bson:"id"`
	Channel string          `bson:"channel"`
	History []AppAdsHistory `bson:"history"`
}

type AppAdsHistory struct {
	Details   []AppAdsHistoryDetail `bson:"details"     json:"details"`
	Timestamp int64                 `bson:"timestamp"   json:"-"`
	IsCurrent bool                  `bson:"is_current"  json:"-"`
}

type AppAdsHistoryDetail struct {
	ADID     string `bson:"ad_id"     json:"ad_id"`
	ADTitle  string `bson:"ad_title"  json:"ad_title"`
	BelongTo string `bson:"belong_to" json:"belong_to"`
	OnTop    bool   `bson:"on_top"    json:"on_top"`
	Pic      string `bson:"pic"       json:"pic"`
	Url      string `bson:"url"       json:"url"`
	Desc     string `bson:"desc"      json:"desc"`
	Status   int    `bson:"status"    json:"-"`
}

// 以下对应用户收支
//type UserIncome struct {
//	ID      string              `bson:"id"`
//	History []UserIncomeHistory `bson:"history"`
//}

type UserIncomeHistory struct {
	MongoID    bson.ObjectId `bson:"_id,omitempty"         json:"-"`
	ID         string        `bson:"id"                    json:"-"`
	Timestamp  int64         `bson:"timestamp"             json:"timestamp"`
	Type       int           `bson:"type"                  json:"type"`
	TaskID     string        `bson:"task_id"               json:"task_id"`
	TaskType   int           `bson:"task_type"             json:"task_type"`
	Count      int           `bson:"count"                 json:"count"`
	Desc       string        `bson:"desc"                  json:"desc"`
	From       string        `bson:"from"                  json:"-"`
	AppVersion string        `bson:"app_version"           json:"-"`
	IP         string        `bson:"ip"                    json:"-"`
}

// 以下对应师徒奖励设置表
type UserApprenticeRewardConf struct {
	ID      string                                  `bson:"id"`
	Channel string                                  `bson:"channel"`
	History []UserApprenticeRewardConfHistoryDetail `bson:"history"`
}

type UserApprenticeRewardConfHistoryDetail struct {
	Rate      float64 `bson:"rate"`
	MustRead  int     `bson:"must_read"`
	Rewards   []int   `bson:"rewards"`
	Timestamp int64   `bson:"timestamp"`
	IsCurrent bool    `bson:"is_current"`
}

// 以下对应师徒中间表
type UserMiddleApprentice struct {
	MongoID  bson.ObjectId `bson:"_id,omitempty" json:"-"`
	ID       string        `bson:"id"`
	MasterID string        `bson:"master_id"`
	Finished bool          `bson:"finished"`
	Details  []int64       `bson:"details"`
}

// 以下对应转盘设置
type AppFixedBounds struct {
	ID      string                  `bson:"id"`
	Channel string                  `bson:"channel"`
	History []AppFixedBoundsHistory `bson:"history"`
}

type AppFixedBoundsHistory struct {
	Speed       int   `bson:"speed"         json:"speed"`
	CoinPerTime int   `bson:"coin_per_time" json:"coin_per_time"`
	Timestamp   int64 `bson:"timestamp"     json:"timestamp"`
	MaxPerDay   int   `bson:"max_per_day"   json:"max_per_day"`
	IsCurrent   bool  `bson:"is_current"    json:"-"`
}

// 以下对应内容分享
type AppShareContent struct {
	ID      string                   `bson:"id"`
	Channel string                   `bson:"channel"`
	History []AppShareContentHistory `bson:"history"`
}

type AppShareContentHistory struct {
	Article     ShareContentDetail `bson:"article"      json:"article"`
	Apprentice  ShareContentDetail `bson:"apprentice"   json:"apprentice"`
	ShowBenifit ShareContentDetail `bson:"show_benefit" json:"show_benefit"`
	Timestamp   int64              `bson:"timestamp"    json:"-"`
	IsCurrent   bool               `bson:"is_current"   json:"-"`
}

type ShareContentDetail struct {
	Person    string `bson:"person"  json:"person"`
	Moments   string `bson:"moments" json:"moments"`
	WechatURL string `bson:"-"       json:"wechat_url,omitempty"`
}

// 以下对应用户文章点击记录
type UserArticelShare struct {
	ID     string                  `bson:"id"`
	UserID string                  `bson:"user_id"`
	URL    string                  `bson:"url"`
	Clicks []UserArticleShareClick `bson:"clicks"`
}

type UserArticleShareClick struct {
	Timestamp int64  `bson:"timestamp"`
	IP        string `bson:"ip"`
	UA        string `bson:"ua"`
	Referrer  string `bson:"referrer"`
}

// 以下对应app奖励设置
type AppReward struct {
	ID      string             `bson:"id"`
	Channel string             `bson:"channel"`
	History []AppRewardHistory `bson:"history"`
}

type AppRewardHistory struct {
	Register     int   `bson:"register"`
	ShareRead    int   `bson:"share_read"`
	TimeDuration int   `bson:"time_duration"`
	Timestamp    int64 `bson:"timestamp"`
	IsCurrent    bool  `bson:"is_current"`
}

// 以下对应app_url表
type AppURL struct {
	ID      string          `bson:"id"`
	Channel string          `bson:"channel"`
	History []AppUrlHistory `bson:"history"`
}

type AppUrlHistory struct {
	RegisterURL            string                  `bson:"register_url"              json:"register_url"`
	SignInRoleURL          string                  `bson:"sign_in_role_url"          json:"sign_in_role_url"`
	HelpURL                string                  `bson:"help_url"                  json:"help_url"`
	ApprenticeRoleURL      string                  `bson:"apprentice_role_url"       json:"apprentice_role_url"`
	PrenticeUrl            string                  `bson:"prentice_url"              json:"prentice_url"`
	AgreementUrl           string                  `bson:"agreement_url"             json:"agreement_url"`
	UserAgreementURL       string                  `bson:"user_agreement_url"        json:"user_agreement_url"`
	ArticleShareContentURL string                  `bson:"article_share_content_url" json:"-"`
	Extra                  [][]AppUrlHistoryDetail `bson:"extra"                     json:"-"`
	Timestamp              int64                   `bson:"timestamp"                 json:"timestamp"`
	IsCurrent              bool                    `bson:"is_current"                json:"-"`
}

type AppUrlHistoryDetail struct {
	Name    string `bson:"name"     json:"name"`
	URL     string `bson:"url"      json:"url"`
	Pic     string `bson:"pic"      json:"pic"`
	Weight  int    `bson:"weight"   json:"weight"`
	IsValid bool   `bson:"is_valid" json:"-"`
}

// 以下对应任务
type Task struct {
	ID         string `bson:"id"`
	Name       string `bson:"name"`
	Indicator  string `bson:"indicator"`
	Bounty     int    `bson:"bounty"`
	ActionType int    `bson:"action_type"`
	Tag        string `bson:"tag"`
	Desc       string `bson:"desc"`
	Order      int    `bson:"order"`
	Valid      bool   `bson:"valid"`
	ActionName string `bson:"action_name"`
	Action     string `bson:"action"`
	Weight     int    `bson:"weight"`
}

// 以下对应第三方设置
type AppThirdpartConf struct {
	ID        string         `bson:"id"`
	Channel   string         `bson:"channel"`
	ThirdPart []AppThirdPart `bson:"third_part"`
}

type AppThirdPart struct {
	Alipay      AppThirdPartDetail `bson:"alipay"`
	Wechat      AppThirdPartDetail `bson:"wechat"`
	YoumengPush AppThirdPartDetail `bson:"youmeng_push"`
	AliyunSMS   AppThirdPartDetail `bson:"aliyun_sms"`
	Timestamp   int64              `bson:"timestamp"`
	IsCurrent   bool               `bson:"is_current"`
}

type AppThirdPartDetail struct {
	AppID           string `bson:"app_id,omitempty"`  // alipay and wechat
	AppSecret       string `bson:"app_secret"`        // wechat
	PID             string `bson:"pid"`               // alipay
	PrivateKey      string `bson:"private_key"`       // alipay
	MuchID          string `bson:"much_id"`           // wechat
	APISecret       string `bson:"api_secret"`        // wechat
	AppKey          string `bson:"app_key"`           // youmengPush
	AppMasterSecret string `bson:"app_master_secret"` // youmengPush
	AccessKeyID     string `bson:"access_key_id"`     // aliyunSMS
	AccessSecret    string `bson:"access_secret"`     // aliyunSMS
	TemplateCode    string `bson:"template_code"`     // aliyunSMS
	SignName        string `bson:"sign_name"`         // aliyunSMS
}

// 以下对应新转盘结构
type AppFixedBound struct {
	ID      string                 `bson:"id"`
	Channel string                 `bson:"channel"`
	History []AppFixedBoundHistory `bson:"history"`
}

type AppFixedBoundHistory struct {
	TimePerRate int                        `bson:"time_per_rate" json:"time_per_rate"`
	RatePerPart int                        `bson:"rate_per_part" json:"rate_per_part"`
	MaxPerDay   int                        `bson:"max_per_day"   json:"max_per_day"`
	Timestamp   int64                      `bson:"timestamp"     json:"timestamp"`
	IsCurrent   bool                       `bson:"is_current"    json:"-"`
	Probability []AppFixedBoundPorbability `bson:"probability"   json:"probability"`
}

type AppFixedBoundPorbability struct {
	Amount int     `bson:"amount" json:"amount"`
	Rate   float64 `bson:"rate"   json:"rate"`
}

// 以下对应广告频率/权重设置
type AppAdConf struct {
	ID      string             `bson:"id"`
	Channel string             `bson:"channel"`
	History []AppAdConfHistory `bson:"history"`
}

type AppAdConfHistory struct {
	Details      []AppAdConfHistoryDetail `bson:"details"        json:"details"`
	MessagePerAd int                      `bson:"message_per_ad" json:"message_per_ad"`
	Timestamp    int64                    `bson:"timestamp"      json:"timestamp"`
	IsCurrent    bool                     `bson:"is_current"     json:"-"`
}

type AppAdConfHistoryDetail struct {
	ADID              string `bson:"ad_id"             json:"ad_id"`
	ADWeight          int    `bson:"ad_weight"         json:"ad_weight"`
	ThirdPartADID     string `bson:"third_part_ad_id"  json:"third_part_ad_id"`
	ThirdPartADWeight int    `bson:"third_part_weight" json:"third_part_weight"`
}

// 以下对应app登录限制
type AppLimit struct {
	ID      string            `bson:"id"`
	Channel string            `bson:"channel"`
	History []AppLimitHistory `bson:"history"`
}

type AppLimitHistory struct {
	DeviceLimit AppLimitHistoryDetail `bson:"device_limit"`
	IPLimit     AppLimitHistoryDetail `bson:"ip_limit"`
	Timestamp   int64                 `bson:"timestamp"`
	IsCurrent   bool                  `bson:"is_current"`
}

type AppLimitHistoryDetail struct {
	AccountLimit int `bson:"account_limit"`
	Duration     int `bson:"duration"`
}

// 以下记录device帐号登录情况
type AppDevice struct {
	AndroidID     string             `bson:"android_id"`
	DeviceHistory []AppDeviceHistory `bson:"history"`
}

type AppDeviceHistory struct {
	UserID    string `bson:"user_id"`
	Timestamp int64  `bson:"timestamp"`
}

// 以下记录ip帐号登录情况
type AppIP struct {
	IP            string             `bson:"ip"`
	DeviceHistory []AppDeviceHistory `bson:"history"`
}

// 以下对应闲玩游戏
type UserXianwanReward struct {
	ID      string                     `bson:"id"`
	Code    string                     `bson:"code"`
	History []UserXianwanRewardHistory `bson:"history"`
}

type UserXianwanRewardHistory struct {
	Timestamp   int64   `bson:"timestamp"`
	Access      string  `bson:"access"`
	FromIP      string  `bson:"from_ip"`
	OrderNumber string  `bson:"order_number"`
	Success     bool    `bson:"success"`
	Price       float64 `bson:"price"`
	Money       float64 `bson:"money"`
	Desc        string  `bson:"desc"`
}

// 以下对应app_history
type AppVersion struct {
	ID      string              `bson:"id"`
	Channel string              `bson:"channel"`
	History []AppVersionHistory `bson:"history"`
}

type AppVersionHistory struct {
	Timestamp     int64  `bson:"timestamp"`
	Version       string `bson:"version"`
	ReleasedNotes string `bson:"released_notes"`
	DownloadURL   string `bson:"download_url"`
	IsForce       bool   `bson:"is_force"`
}

// 以下对应用户获取奖金上限
type AppRewardLimit struct {
	ID      string                  `bson:"id"`
	History []AppRewardLimitHistory `bson:"history"`
}

type AppRewardLimitHistory struct {
	ReadSharePerArticle  int   `bson:"read_share_per_article"`
	ReadSharePerUser     int   `bson:"read_share_per_user"`
	PeriodReward         int   `bson:"period_reward"`
	XianwanGameReward    int   `bson:"xianwan_game_reward"`
	ApprenticeReward     int   `bson:"apprentice_reward"`
	ApprenticeOnceReward int   `bson:"apprentice_once_reward"`
	Timestamp            int64 `bson:"timestamp"`
	IsCurrent            bool  `bson:"is_current"`
}

// 以下对应用户签到配置
type AppSignInConf struct {
	ID      string                 `bson:"id"`
	Channel string                 `bson:"channel"`
	History []AppSignInConfHistory `bson:"history"`
}

type AppSignInConfHistory struct {
	Details   []AppSignInConfHistoryDetail `bson:"details"`
	Timestamp int64                        `bson:"timestamp"`
	IsCurrent bool                         `bson:"is_current"`
}

type AppSignInConfHistoryDetail struct {
	Max  int `bson:"max"`
	Min  int `bson:"min"`
	Coin int `bson:"coin"`
}

// 以下对应用户签到
type UserSignIn struct {
	ID      string              `bson:"id"`
	History []UserSignInHistory `bson:"history"`
}

type UserSignInHistory struct {
	Date      string `bson:"date"`
	IsB       bool   `bson:"is_b"`
	Reward    int    `bson:"reward"`
	Timestamp int64  `bson:"timestamp"`
}

// 以下对应自动审核条件
type AppWithdrawAutoCheck struct {
	ID      string                        `bson:"id"`
	Channel string                        `bson:"channel"`
	History []AppWithdrawAutoCheckHistory `bson:"history"`
}

type AppWithdrawAutoCheckHistory struct {
	Roles     []AppWithdrawAutoCheckHistoryRole `bson:"roles"`
	Timestamp int64                             `bson:"timestamp"`
	IsCurrent bool                              `bson:"is_current"`
}

type AppWithdrawAutoCheckHistoryRole struct {
	Details []AppWithdrawAutoCheckHistoryRoleDetail `bson:"details"`
	Done    int                                     `bson:"done"`
	Valid   bool                                    `bson:"valid"`
}

type AppWithdrawAutoCheckHistoryRoleDetail struct {
	By       int     `bson:"by"`
	JudgedBy int     `bson:"judged_by"`
	Amount   float64 `bson:"amount"`
}

// 以下对应广告点击
type AppADClick struct {
	AppID   string              `bson:"app_id"`
	Channel string              `bson:"channel"`
	History []AppADClickHistory `bson:"history"`
}

type AppADClickHistory struct {
	Weights      []CoinWeight `bson:"weights"       json:"weights"`
	MaxPerAdID   int          `bson:"max_per_ad_id" json:"max_per_ad_id"`
	MaxPerDay    int          `bson:"max_per_day"   json:"-"`
	TimeInterval int          `bson:"time_interval" json:"time_interval"`
	Timestamp    int64        `bson:"timestamp"     json:"timestamp"`
	IsCurrent    bool         `bson:"is_current"    json:"-"`
}

type CoinWeight struct {
	Coin   int `bson:"coin"   json:"coin"`
	Weight int `bson:"weight" json:"weight"`
}

// 以下对应视频奖励
type AppVideoCoin struct {
	ID      string             `bson:"id"`
	Channel string             `bson:"channel"`
	History []VideoCoinHistory `bson:"history"`
}

type VideoCoinHistory struct {
	Weights      []CoinWeight `bson:"weights"       json:"weights"`
	MaxPerDay    int          `bson:"max_per_day"   json:"-"`
	TimeInterval int          `bson:"time_interval" json:"time_interval"`
	Timestamp    int64        `bson:"timestamp"     json:"timestamp"`
	IsCurrent    bool         `bson:"is_current"    json:"-"`
}

// 存储友盟推送的结果
type YoumengPushToSave struct {
	To        []string           `bson:"to"`
	Timestamp int64              `bson:"timestamp"`
	SendTime  string             `bson:"send_time"`
	DeepLink  string             `bson:"deep_link"`
	Title     string             `bson:"title"`
	Message   string             `bson:"message"`
	Article   YoumengPushArticle `bson:"article,omitempty"`
	Success   bool               `bson:"success"`
	Reason    string             `bson:"reason,omitempty"`
	Ret       YoumengPushRet     `bson:"ret,omitempty"`
	Reward    int                `bson:"reward"`
}

type YoumengPushArticle struct {
	ArticleID    string `bson:"article_id,omitempty"`
	ArticleURL   string `bson:"article_url,omitempty"`
	ArticleTitle string `bson:"article_title,omitempty"`
}

// 以下对应用户评级
type UserWithdrawLevel struct {
	ID      string                     `bson:"id"`
	Channel string                     `bson:"channel"`
	History []UserWithdrawLevelHistory `bson:"history"`
}

type UserWithdrawLevelHistory struct {
	Weights   []UserWithdrawLevelWeight `bson:"weights"`
	Timestamp int64                     `bson:"timestamp"`
	IsCurrent bool                      `bson:"is_current"`
}

type UserWithdrawLevelWeight struct {
	ID     int `bson:"id"`
	Weight int `bson:"weight"`
}

// 以下对应用户提现标准
type UserWithdrawStand struct {
	ID      string                     `bson:"id"`
	Channel string                     `bson:"channel"`
	History []UserWithdrawStandHistory `bson:"history"`
}

type UserWithdrawStandHistory struct {
	Levels    []UserWithdrawStandLevel `bson:"levels"`
	Timestamp int64                    `bson:"timestamp"`
	IsCurrent bool                     `bson:"is_current"`
}

type UserWithdrawStandLevel struct {
	LevelName      string  `bson:"level_name"`
	Score          [2]int  `bson:"score"`
	CoinKeepRate   float64 `bson:"coin_keep_rate"`
	WithdrawRate   float64 `bson:"withdraw_rate"`
	WithdrawAmount []int   `bson:"withdraw_amount"`
	IsAutoWithdraw bool    `bson:"is_auto_withdraw"`
}

// 以下对应新版广告位配置
type ADDATA struct {
	ID      string          `bson:"id"`
	Channel string          `bson:"channel"`
	History []ADDATAHistory `bson:"history"`
}

type ADDATAHistory struct {
	Details      []ADDATAHistoryDetail `bson:"details"        json:"details"`
	MessagePerAd int                   `bson:"message_per_ad" json:"message_per_ad"`
	Timestamp    int64                 `bson:"timestamp"      json:"timestamp"`
	IsCurrent    bool                  `bson:"is_current"     json:"-"`
}

type ADDATAHistoryDetail struct {
	ADID      string                         `bson:"ad_id"      json:"ad_id"`
	ADName    string                         `bson:"ad_name"    json:"ad_name"`
	ADDesc    string                         `bson:"ad_desc"    json:"ad_desc"`
	ADType    int                            `bson:"ad_type"    json:"ad_type"`
	IsValid   bool                           `bson:"is_valid"   json:"is_valid"`
	Direct    []ADDATAHistoryDetailDirect    `bson:"direct"     json:"direct"`
	ThirdPart []ADDATAHistoryDetailThirdPart `bson:"third_part" json:"third_part"`
}

type ADDATAHistoryDetailDirect struct {
	ADID     string `bson:"ad_id"     json:"ad_id"`
	ADTitle  string `bson:"ad_title"  json:"ad_title"`
	Weight   int    `bson:"weight"    json:"weight"`
	BelongTo string `bson:"belong_to" json:"belong_to"`
	ADType   int    `bson:"ad_type"   json:"ad_type"`
	OnTop    bool   `bson:"on_top"    json:"on_top"`
	Pic      string `bson:"pic"       json:"pic"`
	URL      string `bson:"url"       json:"url"`
	Desc     string `bson:"desc"      json:"desc"`
	IsValid  bool   `bson:"is_valid"  json:"is_valid"`
}

type ADDATAHistoryDetailThirdPart struct {
	ID      string `bson:"id"       json:"id"`
	ADID    string `bson:"ad_id"    json:"ad_id"`
	ADTitle string `bson:"ad_title" json:"ad_title"`
	Weight  int    `bson:"weight"   json:"weight"`
	OnTop   bool   `bson:"on_top"   json:"on_top"`
	ADType  int    `bson:"ad_type"  json:"ad_type"`
	Desc    string `bson:"desc"     json:"desc"`
	IsValid bool   `bson:"is_valid" json:"is_valid"`
}

// 以下对应红包新闻
type RewardNews struct {
	ID      string              `bson:"id"`
	Channel string              `bson:"channel"`
	History []RewardNewsHistory `bson:"history"`
}

type RewardNewsHistory struct {
	Details   []RewardNewsHistoryDetail `bson:"details"`
	MaxPerDay int                       `bson:"max_per_day"`
	Timestamp int64                     `bson:"timestamp"`
	IsCurrent bool                      `bson:"is_current"`
}

type RewardNewsHistoryDetail struct {
	Category        string   `bson:"category"`
	RewardFrequency [][3]int `bson:"reward_frequecy"`
	Amount          int      `bson:"amount"`
	Reward          int      `bson:"reward"`
	RefreshInterval int      `bson:"refresh_interval"`
	IsValid         bool     `bson:"is_valid"`
}

// 以下对应红包雨
type TimingReward struct {
	ID      string                `bson:"id"`
	Channel string                `bson:"channel"`
	History []TimingRewardHistory `bson:"history"`
}

type TimingRewardHistory struct {
	Details   []TimingRewardHistoryDetail `bson:"details"`
	MaxPerDay int                         `bson:"max_per_day"`
	Timestamp int64                       `bson:"timestamp"`
	IsCurrent bool                        `bson:"is_current"`
}

type TimingRewardHistoryDetail struct {
	StartTime  string `bson:"start_time"`
	EndTime    string `bson:"end_time"`
	Reward     int    `bson:"reward"`
	UserReward int    `bson:"user_reward"`
	IsValid    bool   `bson:"is_valid"`
}

// 多渠道支持
type AppChannel struct {
	ID          string `bson:"id"`
	Channel     string `bson:"channel"`
	Timestamp   int64  `bson:"timestamp"`
	FirstActive bool   `bson:"first_active"`
}

// 以下对应用户师徒宝箱
type UserApprenticeReward struct {
	ID             string `bson:"id"`              // 用户id
	ApprenticeID   string `bson:"apprentice_id"`   // 徒弟id
	Timestamp      int64  `bson:"timestamp"`       // 宝箱创建时间
	Status         int    `bson:"status"`          // 宝箱状态
	ValidTimestamp int64  `bson:"valid_timestamp"` // 宝箱生效时间
	OpenTimestamp  int64  `bson:"open_timestamp"`  // 宝箱开启时间
	Reward         int    `bson:"reward"`          // 宝箱奖励
}

// use for aggregation
type UserIncomeAggregate struct {
	ID      string            `bson:"id"`
	History UserIncomeHistory `bson:"history"`
}

type AggregationTotal struct {
	Total int `bson:"total"`
}

type AggregationTotalAndCountWithID struct {
	ID    string `bson:"_id"`
	Total int    `bson:"total"`
	Count int    `bson:"count"`
}

type AggregationTotalWithID struct {
	ID    string `bson:"_id"`   // 对应聚合内的徒弟id
	Total int    `bson:"total"` // 对应聚合内的该徒弟上供总额
}

// 广告数据
type ADInfo struct {
	ADID       string `bson:"ad_id"`       // 广告位id
	ADReqID    string `bson:"ad_req_id"`   // 广告请求id
	ADReqType  int    `bson:"ad_req_type"` // 广告上报类型, 请求/返回/展示/点击
	ADRes      string `bson:"ad_res"`      // 广告来源
	ADResID    string `bson:"ad_res_id"`   // 广告源id
	IP         string `bson:"ip"`          // 上报信息的客户端ip
	Timestamp  int64  `bson:"timestamp"`   // 上报的时间戳
	AndroidID  string `bson:"android_id"`  // 上报的设备id
	UserID     string `bson:"user_id"`     // 上报的用户id
	AppVersion string `bson:"app_version"` // 上报的客户端版本
}

// 用户收益 -- 每日
type UserIncomeByDate struct {
	ID        string `bson:"id"`        // 用户id
	Type      int    `bson:"type"`      // 总类型, 收入还是支出
	TaskType  int    `bson:"task_type"` // 收入类型.  注意有个总类型
	Timestamp int64  `bson:"timestamp"` // 当地时间凌晨0点时间戳
	Total     int    `bson:"total"`     // 当日总收入
	Count     int    `bson:"count"`     // 当日收入次数
}

// 用户收益 -- 总
type UserIncomeByType struct {
	ID       string `bson:"id"`        // 用户id
	Type     int    `bson:"type"`      // 总类型, 收入还是支出
	TaskType int    `bson:"task_type"` // 收入类型.  注意有个总类型
	Total    int    `bson:"total"`     // 当日总收入
	Count    int    `bson:"count"`     // 当日收入次数
}

// 用户广告记录 -- 每日
type UserAdDetailByDate struct {
	ID        string `bson:"id"`        // 用户id
	ADID      string `bson:"ad_id"`     // 广告位id. 注意有个总id
	Timestamp int64  `bson:"timestamp"` // 当地时间凌晨0点时间戳
	Total     int    `bson:"total"`     // 总收入
	Count     int    `bson:"count"`     // 收入次数
}

// 用户文章分享记录 -- 每日
type UserArticleDetailByDate struct {
	ID        string `bson:"id"`         // 用户id
	ArticleID string `bson:"article_id"` // 分享文章id. 注意有个总id
	Timestamp int64  `bson:"timestamp"`  // 当地时间凌晨0点时间戳
	Total     int    `bson:"total"`      // 总收入
	Count     int    `bson:"count"`      // 收入次数
}

// 用户徒弟 -- 每日
type UserApprenticeDetailByDate struct {
	ID           string `bson:"id"`            // 用户id
	ApprenticeID string `bson:"apprentice_id"` // 徒弟id
	Timestamp    int64  `bson:"timestamp"`     // 当地时间凌晨0点时间戳
	Total        int    `bson:"total"`         // 当日上供总额
	Count        int    `bson:"count"`         // 当日上供总次数
}

// 用户徒弟 -- 总
type UserApprenticeDetailTotal struct {
	ID           string `bson:"id"`            // 用户id
	ApprenticeID string `bson:"apprentice_id"` // 徒弟id
	Total        int    `bson:"total"`         // 当日上供总额
	Count        int    `bson:"count"`         // 当日上供总次数
}

// 广告点击频率限制
type AdClickFrequece struct {
	Hours int `bson:"hours" json:"hours"`
	Limit int `bson:"limit" json:"limit"`
}

// 以下对应看看赚任务
// 看看赚 - 停留时长任务
type StayPageTask struct {
	TaskID   string `bson:"task_id"   json:"task_id"`
	Desc     string `bson:"desc"      json:"desc"`
	DeepLink string `bson:"deep_link" json:"deep_link"`
	Reward   int    `bson:"reward"    json:"reward"`
	Pic      string `bson:"pic"       json:"pic"`
	TimeStay int    `bson:"time_stay" json:"time_stay"`
	Status   int    `bson:"-"         json:"status"`
	Valid    bool   `bson:"valid"     json:"-"`
}

// 看看赚 - 每日一次任务
type DailyOnceTask struct {
	TaskID   string `bson:"task_id"   json:"task_id"`
	Desc     string `bson:"desc"      json:"desc"`
	Count    int    `bson:"count"     json:"count"`
	Pic      string `bson:"pic"       json:"pic"`
	DeepLink string `bson:"deep_link" json:"deep_link"`
	Reward   int    `bson:"reward"    json:"reward"`
	Status   int    `bson:"-"         json:"status"`
	Valid    bool   `bson:"valid"     json:"-"`
}

// 看看赚 - 每日多次任务
type DailyManyTimesTask struct {
	TaskID        string `bson:"task_id"        json:"task_id"`
	Desc          string `bson:"desc"           json:"desc"`
	DeepLink      string `bson:"deep_link"      json:"deep_link"`
	Reward        int    `bson:"reward"         json:"reward"`
	Pic           string `bson:"pic"            json:"pic"`
	TimeStay      int    `bson:"time_stay"      json:"time_stay"`
	MaxPerDay     int    `bson:"max_per_day"    json:"max_per_day"`
	CompleteToday int    `bson:"complete_today" json:"complete_today"`
	TimeInterval  int    `bson:"-"              json:"time_interval"`
	Interval      int    `bson:"interval"       json:"interval"`
	Valid         bool   `bson:"valid"          json:"-"`
}

// 看看赚 - 完成任务
type SpecialTaskComplete struct {
	UserID        string `bson:"user_id"`
	TaskID        string `bson:"task_id"`
	TaskType      int    `bson:"task_type"`
	DateTimestamp int64  `bson:"date_timestamp"`
	FTimestamp    int64  `bson:"ftimestamp"`
	Tag           string `bson:"tag"` // 无实际意义的标记, 用于控制数据库唯一性索引
}
