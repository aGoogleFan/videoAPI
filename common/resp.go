package common

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"
)

const (
	STATUS_OK                            = 0
	STATUS_OTHER_ERROR                   = 20009
	STATUS_MOBILE_EXISTS                 = 21001
	STATUS_MSM_INVALID                   = 21002
	STATUS_MSG_SEND_ERROR                = 21003
	STATUS_RECOMMEND_CODE_INVALID        = 21005
	STATUS_ALREADY_BIND_RECOMMEND_CODE   = 21006
	STATUS_BIND_SELF                     = 21007
	STATUS_MOBILE_INVALID                = 21008
	STATUS_PASSWORD_INVALID              = 21009
	STATUS_TASK_TYPE_INVALID             = 21010
	STATUS_TASK_INVALID                  = 21011
	STATUS_DEVICE_LIMIT                  = 21012
	STATUS_IP_LIMIT                      = 21013
	STATUS_APPRENTICE_CIRCLED            = 21014
	STATUS_TASK_ALREADY_COMPLETE         = 21015
	STATUS_APPRENTICE_REWARD_NOT_EXISTS  = 22000
	STATUS_APPRENTICE_REWARD_INVALID     = 22001
	STATUS_APPRENTICE_REWARD_ALREADY_GET = 22002
	STATUS_SERVER_ERROR                  = 30000
	STATUS_SIGN_INVALID                  = 30001
	STATUS_APPID_INVALID                 = 30002
	STATUS_TOKEN_INVALID                 = 30003
	STATUS_PARAMS_LACK                   = 30004
	STATUS_ACCOUNT_DISABLED              = 30005
	STATUS_DATA_TO_BE_STATISTICAL        = 30006
	STATUS_TIME_ERROR                    = 30007
	STATUS_APP_VERSION_TOO_OLD           = 30008
	STATUS_COIN_NOT_ENOUGH               = 40001
	STATUS_RATE_CHANGE                   = 40002
	STATUS_REWARD_OUT_OF_LIMIT           = 40003
	STATUS_WITHDRAW_OUT_OF_LIMIT         = 40004
	STATUS_WITHDRAW_REQ_OUT_OF_LIMIT     = 40005
	STATUS_ALIPAY_NOT_BIND               = 50000
	STATUS_WECHAT_NOT_BIND               = 50001
	STATUS_ALIPAY_ALREADY_BIND           = 50002
	STATUS_WECHAT_ALREADY_BIND           = 50003
	STATUS_ALIPAY_UNBIND                 = 50004
	STATUS_WECHAT_UNBIND                 = 50005
	STATUS_USER_ALREADY_BIND_ALIPAY      = 50006
	STATUS_USER_ALREADY_BIND_WECHAT      = 50007
	STATUS_BEFORE_TIME                   = 60001
	STATUS_AFTER_TIME                    = 60002
	STATUS_ALREADY_REWARD                = 60003
	STATUS_NOT_IN_ACTIVE_TIME            = 60004
)

type Resp struct {
	Status    int         `json:"status"`
	Timestamp int64       `json:"timestamp"`
	ErrMsg    string      `json:"err_msg"`
	Result    interface{} `json:"result"`
}

func ParamsFilter(rq url.Values, params []string) *Resp {
	for _, param := range params {
		if rq.Get(param) == "" {
			return &Resp{
				Status:    STATUS_PARAMS_LACK,
				ErrMsg:    fmt.Sprintf("param %s not exists", param),
				Timestamp: time.Now().Unix(),
			}
		}
	}
	return nil
}

func SignInvalidErrorResp() Resp {
	return Resp{
		Status:    STATUS_SIGN_INVALID,
		ErrMsg:    "",
		Timestamp: time.Now().Unix(),
	}
}

func (r Resp) String() string {
	ct, _ := json.Marshal(r)
	return string(ct)
}

func OtherErrorResp(errMsg string) Resp {
	return Resp{
		Status:    STATUS_OTHER_ERROR,
		ErrMsg:    errMsg,
		Timestamp: time.Now().Unix(),
	}
}

func ServerErrorResp(errMsg string) Resp {
	return Resp{
		Status:    STATUS_SERVER_ERROR,
		ErrMsg:    errMsg,
		Timestamp: time.Now().Unix(),
	}
}

func RateErrorResp(errMsg string) Resp {
	return Resp{
		Status:    STATUS_RATE_CHANGE,
		ErrMsg:    errMsg,
		Timestamp: time.Now().Unix(),
	}
}

func SuccessResp(res interface{}) Resp {
	return Resp{
		Status:    STATUS_OK,
		Timestamp: time.Now().Unix(),
		Result:    res,
	}
}

func TokenInvalidResp() Resp {
	return Resp{
		Timestamp: time.Now().Unix(),
		Status:    STATUS_TOKEN_INVALID,
	}
}

func RewardLimitResp() Resp {
	return Resp{
		Timestamp: time.Now().Unix(),
		Status:    STATUS_REWARD_OUT_OF_LIMIT,
	}
}
