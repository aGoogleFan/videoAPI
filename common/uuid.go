package common

import (
	"sync"

	"github.com/pborman/uuid"
)

var uuidLock sync.Mutex
var lastUUID uuid.UUID

func NewUUID() string {
	uuidLock.Lock()
	defer uuidLock.Unlock()
	result := uuid.NewUUID()
	for uuid.Equal(result, lastUUID) {
		result = uuid.NewUUID()
	}
	lastUUID = result
	return result.String()
}
