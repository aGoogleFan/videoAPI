package log

import (
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/anotherGoogleFan/log"
)

const (
	LOGFILE_PATH = "/data/log/yxw"
)

var logFile *os.File
var logMux *sync.RWMutex

func init() {
	hostName, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	fileName := fmt.Sprintf("%s/monitorLog", strings.TrimRight(LOGFILE_PATH, "/"))

	f, err := os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		log.Fields{
			"filename": fileName,
			"err":      err.Error(),
		}.Error("fail to create log file")
		panic(err)
	}
	logFile = f
	logMux = new(sync.RWMutex)
	go autoRefreshLogFile(hostName)
}

// 每隔1小时自动将原日志文件更名, 并生成新日志文件, 在新日志文件内继续写入日志
func autoRefreshLogFile(hostName string) {
	filePath := strings.TrimRight(LOGFILE_PATH, "/") + "/"
	for {
		t := time.Now().Local()
		oldFileName := fmt.Sprintf("%smonitorLog_%s_%s.log", filePath, hostName, t.Format("2006-01-02_15"))
		t1 := t.Truncate(time.Hour).Add(time.Hour)
		for t2 := t1.Sub(time.Now()); t2 > 0; {
			time.Sleep(t2)
			t2 = t1.Sub(time.Now())
		}

		os.Rename(logFile.Name(), oldFileName)
		f, err := os.OpenFile(filePath+"monitorLog", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
		if err != nil {
			log.Fields{
				"fileName": filePath + "monitorLog",
				"err":      err.Error(),
			}.Error("fail to open file")
			continue
		}
		logMux.Lock()
		logFile.Close() // close old file to avoid memory leak
		logFile = f     // switch to new file
		logMux.Unlock()
	}
}
