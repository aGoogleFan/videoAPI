package log

import (
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"

	"vlion/yxw/common"
)

var HttpRequestCount = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_request_count",
		Help: "http request count",
	},
	[]string{"yxw_endpoint"},
)

var HttpErrorCount = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "http_status_count",
		Help: "http status count",
	},
	[]string{"yxw_endpoint", "yxw_ret_code"},
)

var HttpRequestDuration = prometheus.NewSummaryVec(
	prometheus.SummaryOpts{
		Name: "http_request_duration",
		Help: "http request duration",
	},
	[]string{"yxw_endpoint"},
)

func RecordLog(ar common.APIRecord) {
	ar.Duration = time.Since(ar.StartTime)
	tlog := make([]string, 32)
	tlog[0] = strconv.Itoa(ar.APICode)
	tlog[1] = logTrim(ar.SignOnce)
	tlog[2] = logTrim(ar.SignVersion)
	tlog[3] = logTrim(ar.IsSafe)
	tlog[4] = logTrim(ar.AppChannel)
	tlog[5] = logTrim(ar.UserIP)
	tlog[6] = logTrim(ar.Access)
	tlog[7] = strconv.FormatInt(ar.Timestamp, 10)
	tlog[8] = logTrim(ar.Ret)
	tlog[9] = strconv.FormatInt(int64(ar.Duration/time.Millisecond), 10)
	tlog[10] = logTrim(ar.UserToken)
	tlog[11] = logTrim(ar.UserID)
	tlog[12] = logTrim(ar.UserCode)
	tlog[13] = logTrim(ar.UserMobile)
	tlog[14] = strconv.Itoa(ar.SMSCode)
	tlog[15] = logTrim(ar.DeviceModel)
	tlog[16] = logTrim(ar.System)
	tlog[17] = logTrim(ar.SystemVersion)
	tlog[18] = logTrim(ar.AndroidID)
	tlog[19] = logTrim(ar.IMEI)
	tlog[20] = logTrim(ar.APPID)
	tlog[21] = logTrim(ar.APPVersion)
	tlog[22] = logTrim(ar.WechatOpenID)
	tlog[23] = logTrim(ar.AlipayOpenID)
	tlog[24] = logTrim(ar.OrderID)
	tlog[25] = logTrim(ar.WithdrawAccount)
	tlog[26] = strconv.Itoa(ar.Reward)
	tlog[27] = strconv.Itoa(ar.Consume)
	tlog[28] = strconv.Itoa(ar.Withdraw)
	tlog[29] = logTrim(ar.AutoResult)
	tlog[30] = strconv.Itoa(ar.RetCode)
	tlog[31] = logTrim(ar.ErrorMsg)

	logMux.RLock()
	logFile.WriteString(strings.Join(tlog, "\t") + "\n")
	logMux.RUnlock()

	go func() {
		HttpRequestCount.WithLabelValues(strconv.Itoa(ar.APICode)).Inc()
		HttpErrorCount.WithLabelValues(strconv.Itoa(ar.APICode), strconv.Itoa(ar.RetCode)).Inc()
		HttpRequestDuration.WithLabelValues(strconv.Itoa(ar.APICode)).Observe(float64(ar.Duration.Nanoseconds()))
	}()
}

func logTrim(s string) string {
	return strings.Replace(strings.Replace(strings.Replace(s, "\n", " ", -1), "\t", " ", -1), "\r", " ", -1)
}
